﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    
    public class Feature 
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FeatureID { get; set; }
        public string FeatureName { get; set; }
        public string FeatureDescription { get; set; }
        public string FeatureGroup { get; set; }
        public string FeatureDataType { get; set; }
        public string TableName { get; set; }
        //public Feature(Feature f)
        //{
        //    this.FeatureID = f.FeatureID;
        //    this.FeatureName = f.FeatureName;
        //    this.FeatureDescription = FeatureDescription;
        //    this.FeatureGroup = FeatureGroup;
        //    this.FeatureDataType = FeatureDataType;
        //    this.TableName = TableName;
        //}
  

}


        //public virtual ICollection<Model> Models { get; set; }
        //public virtual ICollection<Ratio> Ratios { get; set; }

   
 }

