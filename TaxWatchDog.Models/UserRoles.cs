﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class UserRole
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }
       
        public virtual List<Role> Roles { get; set; }
        public virtual List<User> Users { get; set; }
    }
}
