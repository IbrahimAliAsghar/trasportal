﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class Profile
    {
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }
        public int ModelID { get; set; }
        public string ModelName { get; set; }
        public string ODSQuery { get; set; }
        public string FileType { get; set; }
        public int UserID { get; set; }
        public int AssessmentYear { get; set; }
        public int JobId { get; set; }
        public string BranchCode { get; set; }
        public bool Saved { get; set; }
        public string ProfileParametersJson { get; set; }
        public string StateCode { get; set; }
        public string BusinessCode { get; set; }
        public bool IsActive { get; set; }
        public string NoteDetail { get; set; }
        public string BranchName { get; set; }
        public string ModelDesc { get; set; }
        public string NoteProfile { get; set; }

        [NotMapped]
        public string JobStatus { get; set; }

        public virtual User User { get; set; }
        public virtual Model Model { get; set; }
    }
}
