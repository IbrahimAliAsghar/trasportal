﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    [Table("modelresult_abt")]
    public class ModelResultsAbt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ModelResultAbtID { get; set; }
        public string feature{ get; set; }
        public int profile_id { get; set; }
        public int model_id { get; set; }
        public double value{ get; set; }
        public double score{ get; set; }
        public double ranks { get; set; }
        public int year_d { get; set; }
        public string taxpayer { get; set; }

        public double it_ref_no { get; set; }
        public double mean { get; set; }
        public double median { get; set; }
        public double mode { get; set; }
        public double standard_deviation { get; set; }
        public double max_value { get; set; }
        public double min_value { get; set; }
    }
}
