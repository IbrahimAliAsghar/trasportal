﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class User
    {
        public int UserID { get; set; }
        //public int RoleID { get; set; }
        public string UserType { get; set; }
        public string Username { get; set; }
        public int ADID { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string SecurityToken { get; set; }
        public int Status { get; set; }
        public string Role { get; set; }
        public virtual List<Role> Roles { get; set; }

        //public User()
        //{
        //    Roles = new HashSet<Role>();
        //}
    }
}
