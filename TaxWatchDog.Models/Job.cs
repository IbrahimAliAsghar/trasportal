﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class Job
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string JobStatus { get; set; }
        public string JobType { get; set; }
        public string JobParams { get; set; }
        public DateTime LastRunDate { get; set; }
        public TimeSpan ExecutionTime { get; set; }
        public  virtual Profile Profile { get; set; }
    }
}
