﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class RatioFeature
    {
        public int ID { get; set; }
        public int RatioID { get; set; }
        public int FeatureID { get; set; }
      
        public virtual List<Feature> Features { get; set; }
        public virtual List<Ratio> Ratios { get; set; }

    }
}
