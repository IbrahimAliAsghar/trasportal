﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class ModelFeature
    {

        public int ID { get; set; }
        public int FeatureID { get; set; }
        public int ModelID { get; set; }

        //public virtual List<Model> Models { get; set; }
        //public virtual List<Feature> Features { get; set; }
        
    }
}
