﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class TaxProfilingHelper
    {
        public int AssesmentYear { get; set; }
        public List<string> It_Ref_nos { get; set; }
        public string file_type { get; set; }
    }
}
