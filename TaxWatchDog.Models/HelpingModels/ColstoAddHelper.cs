﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
   public class ColstoAddHelper
    {
        public int assesmentYear { get; set; }
        public string query { get; set; }
        public string tableName { get; set; }
        public List<string> itRefNo { get; set; }
    }
}
