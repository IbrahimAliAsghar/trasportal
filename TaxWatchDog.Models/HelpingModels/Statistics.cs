﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
   public class FeatureStatistics
    {

        public string feature { get; set; }
        public string mean { get; set; }
        public float median { get; set; }
        public List<string> mode { get; set; }
        public string standard_deviation { get; set; }
        public float max_value { get; set; }
        public float min_value { get; set; }




    }
}
