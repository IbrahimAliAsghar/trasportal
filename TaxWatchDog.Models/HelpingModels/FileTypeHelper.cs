﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class FileTypeHelper
    {
        public FileTypeHelper()
        {
            Years = new List<string>();
        }
        public string FileTypeName { get; set; }
        public List<string> Years { get; set; }

    }
}
