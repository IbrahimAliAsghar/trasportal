﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
   public class ReportHelper
    {
        public string BranchName { get; set; }
        public string Result { get; set; }
        public string BranchCode { get; set; }
        public string AuditType { get; set; }
        public string Count { get; set; }

    }
}
