﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class Profile_Input
    {
        public int ModelID { get; set; }
        public int ProfileID { get; set; }
        public string ModelName { get; set; }
        public int UserID { get; set; }
        public int Year { get; set; }
        public string FileType { get; set; }
        public List<Feature_Input> Features { get; set; }
        public string ProfileName { get; set; }
        public string BranchCode { get; set; }
        public string StateCode { get; set; }
        public string BusinessCode { get; set; }
        public bool IsActive { get; set; }
        public string NoteDetail { get; set; }
        public string BranchName { get; set; }
        public string ModelDesc { get; set; }
        public string NoteProfile { get; set; }
        public int AssessmentYear { get; set; }


    }
}
