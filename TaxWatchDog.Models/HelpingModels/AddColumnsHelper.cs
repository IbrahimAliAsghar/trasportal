﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class AddColumnsHelper
    {
        public string it_ref_no_taxassm_dim { get; set; }
        public string name { get; set; }
        public string main_business_code { get; set; }
        public int assessment_year { get; set; }
    }
}
