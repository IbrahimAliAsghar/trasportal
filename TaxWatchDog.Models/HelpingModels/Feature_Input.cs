﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class Feature_Input
    {
        public string FeatureName { get; set; }
        public List<Filter_Input> Filters { get; set; }
        public int FeatureID { get; set; }
        public string FeatureDescription { get; set; }
        public string FeatureGroup { get; set; }
        public string FeatureDataType { get; set; }
        public string TableName { get; set; }

    }
}
