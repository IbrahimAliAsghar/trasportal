﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
   public class CaseStatusReportReply
    {
        
        public string branchname { get; set; }
        public string result { get; set; }
        public string branchcode { get; set; }
        public string audit_type { get; set; }
        public string Count { get; set; }
    }

}
