﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class Filter_Input
    {
        public string Type { get; set; }
        public List<string> Values { get; set; }
    }
}
