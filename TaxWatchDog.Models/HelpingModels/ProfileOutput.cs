﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class ProfileOutput
    {
        public string ODSQuery { get; set; }
        public int ProfileID { get; set; }
        public int ModelID { get; set; }
        public int JobID { get; set; }
        public List<Ratio> Ratios { get; set; }
        public List<Feature> Features { get; set; }
        public int AssesmentYear { get; set; }

    }
}
