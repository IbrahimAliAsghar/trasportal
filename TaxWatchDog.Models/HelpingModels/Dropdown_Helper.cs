﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class Dropdown_Helper
    {
        List<string> filetype { get; set; }
        List<string> branchcode { get; set; }
        List<string> assesmet_year { get; set; }
        List<string> bussinessCode { get; set; }
        List<string> stateCode { get; set; }
    }
}
