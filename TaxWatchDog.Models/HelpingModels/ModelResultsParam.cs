﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class ModelResultsParam
    {
        public int ProfileId { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int AssesmentYear { get; set; }
    }
}
