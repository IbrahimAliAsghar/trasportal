﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class ModelResultsMetadata
    {
        public string TableName { get; set; }
        public string FeatureColumnName { get; set; }
        public string YearColumnName { get; set; }
        public string ScoreColumnName { get; set; }
        public string RankColumnName { get; set; }
        public string ValueColumnName { get; set; }
        //public string TaxpyerColumnName { get; set; }
        public string ItRefColumnName { get; set; }
        public string MeanColumnName { get; set; }
        public string MedianColumnName { get; set; }
        public string ModeColumnName { get; set; }
        public string SDColumnName { get; set; }
        public string MinColumnName { get; set; }
        public string MaxColumnName { get; set; }
        public string Year1ColumnName { get; set; }
        public string Year2ColumnName { get; set; }

    }
}
