﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
   public class ODS_Helper
    {
        public string ODSQuery { get; set; }
        public int AssesmentYear { get; set; }
    }
}
