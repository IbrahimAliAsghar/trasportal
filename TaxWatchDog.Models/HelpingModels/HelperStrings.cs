﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models.HelpingModels
{
    public class HelperStrings

    {
        public const string FilterType_GreaterThan = "Greater";
        public const string FilterType_LessThan = "Less";
        public const string FilterType_EqualTo = "Equal";
        public const string FilterType_Between = "Between";
        public const string FilterType_In = "Greater";
        public const string FilterType_IsNull = "IS NULL";
        public const string FilterType_IsNotNull = "IS NOT NULL";
        public const string FilterType_Contains = "Contains";
        public const string FilterType_GreaterThanOrEqualTo = "Greater than Equal to";
        public const string FilterType_LessThanOrEqualTo = "Less than Equal to";
        public const string FilterType_NotEqualTo = "Not Equal";
        public const string FilterType_Like = "Like";
        public const string JobStatus_InQueue = "In Queue";
        public const string JobStatus_Running = "Running";
        public const string JobStatus_SendingToSpark = "Sending To Spark";
        public const string JobStatus_Done = "Done";
        public const string JobStatus_Failed = "Failed";
        public const string JobStatus_Assigned = "Assigned";
        public const string JobSType_Spark = "Spark";
        public const string JobSType_CMS = "CMS";
        public const string JobExecutioner_Spark = "Spark Executioner";
        public const string JobExecutioner_CMS = "CMS Executioner";
        public const string DataSource_MySql = "MySQL";
        public const string DataSource_Hive = "Hive";
        public const string FilterType_All = "All";
    }
}
