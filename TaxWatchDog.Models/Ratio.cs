﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class Ratio
    {
        public int RatioID { get; set; }
        public string RatioName { get; set; }
        public int Propotional { get; set; }
        [NotMapped]
        public List<Feature> Features { get; set; }
    }
}
