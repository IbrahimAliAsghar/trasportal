﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class ActivityLog
    {
        public int ActivityLogID { get; set; }
        public int UserID { get; set; }
        public DateTime DateTime{ get; set; }
        public string ActivityName { get; set; }
        public string ActivityDescription { get; set; }


        public virtual User User { get; set; }
    }
}
