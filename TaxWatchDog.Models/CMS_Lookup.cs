﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class CMS_Lookup
    {
        [Key]
        public int key { get; set; }
        public int ProfileID { get; set; }
        public string it_ref_no { get; set; }
        public int AssessmentYear { get; set; }

    }
}
