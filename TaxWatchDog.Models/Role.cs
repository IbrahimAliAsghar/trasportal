﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.Models
{
    public class Role
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        //public virtual ICollection<User> Users { get; set; }

        //public Role()
        //{
        //    Users = new HashSet<User>();
        //}
    }
}
