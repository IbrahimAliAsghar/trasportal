﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TaxWatchDog.Models.DBContext
{
    public class MySQLDbContext : DbContext
    {
        public MySQLDbContext() : base("WebDbConnectionString")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<ActivityLog> ActivityLogSet { get; set; }
        public DbSet<Feature> FeatureSet { get; set; }
        public DbSet<Model> ModelSet { get; set; }
        public DbSet<Profile> ProfileSet { get; set; }
        public DbSet<Ratio> RatioSet { get; set; }
        public DbSet<Role> RoleSet { get; set; }
        public DbSet<User> UserSet { get; set; }
        public DbSet<UserRole> UserRolesSet { get; set; }
        public DbSet<ModelFeature> ModelFeaturesSet { get; set; }
        public DbSet<RatioFeature> RatioFeaturesSet { get; set; }
        public DbSet<RatioModel> RatioModelSet { get; set; }
        //public DbSet<ModelResultsAbt> ModelResultsAbtSet { get; set; }
        public DbSet<Job> JobSet { get; set; }
        public DbSet<CMS_Lookup> CMS_LookupSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

      //      //for modelfeature bridge
      //      modelBuilder.Entity<Model>().
      //HasMany(c => c.Features).
      //WithMany(p => p.Models).
      //Map(
      // m =>
      // {
      //     m.MapLeftKey("ModelID");
      //     m.MapRightKey("FeatureID");
      //     m.ToTable("ModelsFeatures");
      // });


      //      //for featureRatio bridge
      //      modelBuilder.Entity<Feature>().
      //HasMany(c => c.Ratios).
      //WithMany(p => p.Features).
      //Map(
      // m =>
      // {
      //     m.MapLeftKey("FeatureID");
      //     m.MapRightKey("RatioID");
      //     m.ToTable("FeaturesRatios");
      // });

      //      //for userRoles bridge
      //      modelBuilder.Entity<User>().
      //HasMany(c => c.Roles).
      //WithMany(p => p.Users).
      //Map(
      // m =>
      // {
      //     m.MapLeftKey("UserID");
      //     m.MapRightKey("RoleID");
      //     m.ToTable("UsersRoles");
      // });
            
        }

    }
}
