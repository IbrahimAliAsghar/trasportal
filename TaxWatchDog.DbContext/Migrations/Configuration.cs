namespace TaxWatchDog.Models.DBContext.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TaxWatchDog.Models.DBContext.MySQLDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            // register mysql code generator
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(TaxWatchDog.Models.DBContext.MySQLDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var roles = new List<Role>
            {
            new Role{RoleName="Admin"},
            new Role{RoleName="BusinessUser"},
            };

            var users = new List<User>
            {
                new User{ Username="admin" , Roles = new List<Role>{ roles[0]} },
                new User{ Username="buser", Roles = new List<Role>{ roles[1]} },
            };

            var userRoles = new List<UserRole>
            {
                new UserRole{UserID= 1, RoleID=1 },
                new UserRole{UserID= 2, RoleID=2 },
            };

            userRoles.ForEach(s => context.UserRolesSet.Add(s));
            roles.ForEach(s => context.RoleSet.Add(s));
            users.ForEach(u => context.UserSet.Add(u));

            context.SaveChanges();

            #region OldSeededData

            //var features1 = new List<Feature>
            //{
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 1 Des", FeatureGroup = "Feature Group 1", FeatureName = "Feature 1", FeatureID = 1 ,TableName = "Table 1"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 2 Des", FeatureGroup = "Feature Group 1", FeatureName = "Feature 2", FeatureID = 2 ,TableName = "Table 1"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 3 Des", FeatureGroup = "Feature Group 1", FeatureName = "Feature 3", FeatureID = 3 ,TableName = "Table 2"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 4 Des", FeatureGroup = "Feature Group 1", FeatureName = "Feature 4", FeatureID = 4 ,TableName = "Table 1"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 5 Des", FeatureGroup = "Feature Group 1", FeatureName = "Feature 5", FeatureID = 5 ,TableName = "Table 3"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 6 Des", FeatureGroup = "Feature Group 2", FeatureName = "Feature 6", FeatureID = 6 ,TableName = "Table 3" },
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 7 Des", FeatureGroup = "Feature Group 2", FeatureName = "Feature 7", FeatureID = 7 ,TableName = "Table 2"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 8 Des", FeatureGroup = "Feature Group 2", FeatureName = "Feature 8", FeatureID = 8 ,TableName = "Table 1"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 9 Des", FeatureGroup = "Feature Group 2", FeatureName = "Feature 9", FeatureID = 9 ,TableName = "Table 4"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 10 Des", FeatureGroup = "Feature Group 2", FeatureName = "Feature 10", FeatureID = 10 ,TableName = "Table 2"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 11 Des", FeatureGroup = "Feature Group 3", FeatureName = "Feature 11", FeatureID = 11 ,TableName = "Table 3" },
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 12 Des", FeatureGroup = "Feature Group 3", FeatureName = "Feature 12", FeatureID = 12 ,TableName = "Table 3"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 13 Des", FeatureGroup = "Feature Group 3", FeatureName = "Feature 13", FeatureID = 13,TableName = "Table 1" },
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 14 Des", FeatureGroup = "Feature Group 3", FeatureName = "Feature 14", FeatureID = 14 ,TableName = "Table 2"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Feature 15 Des", FeatureGroup = "Feature Group 3", FeatureName = "Feature 15", FeatureID = 15,TableName = "Table 2"}
            //};

            //var model1Features = new List<Feature>//model1
            //{
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Plant and Machinery", FeatureGroup = "ASSETS", FeatureName = "PLANT_AND_MACHINERY", FeatureID = 16 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Assets", FeatureGroup = "ASSETS", FeatureName = "TOTAL_ASSETS", FeatureID = 17 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Trade Debtors", FeatureGroup = "ASSETS", FeatureName = "TRADE_DEBTORS", FeatureID = 18 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Loans from related companies outside Mal", FeatureGroup = "INTERCOMPANY", FeatureName = "LOANS_FROM_OUTSIDE_MALAYSIA", FeatureID = 19 ,TableName = "related_comp_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "", FeatureGroup = "INTERCOMPANY", FeatureName = "TOTAL_PURCHASE_OUTSIDE_MALAYSI", FeatureID = 20 ,TableName = "related_comp_fact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "", FeatureGroup = "INTERCOMPANY", FeatureName = "OTHER_PAYMENT_OUTSIDE_MALAYSIA", FeatureID = 21 ,TableName = "related_comp_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Receipt from related companies outside Msia", FeatureGroup = "INTERCOMPANY", FeatureName = "RECEIPT_FROM_OUTSIDE_MALAYSIA", FeatureID = 22 ,TableName = "related_comp_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total sales from related companies outside Malaysia", FeatureGroup = "INTERCOMPANY", FeatureName = "TOTAL_SALES_OUTSIDE_MALAYSIA", FeatureID = 23 ,TableName = "related_comp_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Advertising Expense", FeatureGroup = "EXPENSE", FeatureName = "PROMO_ADVERT_EXPENSE", FeatureID = 24 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Cost of Sales or Cost of Goods Sold", FeatureGroup = "EXPENSE", FeatureName = "COST_OF_GOODS_SOLD", FeatureID = 25 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Opening Stock", FeatureGroup = "INTERCOMPANY", FeatureName = "OPENING_STOCK", FeatureID = 26 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Production Cost", FeatureGroup = "INTERCOMPANY", FeatureName = "PRODUCTION_COST", FeatureID = 27 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Gross Profit or Loss", FeatureGroup = "INTERCOMPANY", FeatureName = "GROSS_PROFIT", FeatureID = 28 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Gross Sales", FeatureGroup = "EXPENSE", FeatureName = "GROSS_SALES", FeatureID = 29 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Net Profit or Loss", FeatureGroup = "EXPENSE", FeatureName = "NET_PROFIT_OR_LOSS", FeatureID = 30 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Contractor and Subcontractor Expense", FeatureGroup = "INTERCOMPANY", FeatureName = "CONTRACT_SUBCON_EXPENSE", FeatureID = 31 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Loan Interest Expense", FeatureGroup = "INTERCOMPANY", FeatureName = "LOAN_INTEREST_EXPENSE", FeatureID = 32 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Non-allowable expenses", FeatureGroup = "INTERCOMPANY", FeatureName = "NON_ALLOW_EXPENSE", FeatureID = 33 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Purchases", FeatureGroup = "EXPENSE", FeatureName = "PURCHASES", FeatureID = 34 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Business Expense", FeatureGroup = "EXPENSE", FeatureName = "TOTAL_EXPENSES", FeatureID = 35 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Business Income", FeatureGroup = "INTERCOMPANY", FeatureName = "OTHER_BUSINESS_INCOME", FeatureID = 36 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Income", FeatureGroup = "INTERCOMPANY", FeatureName = "OTHER_NON_SALES_INCOME", FeatureID = 37 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Closing Stock", FeatureGroup = "INTERCOMPANY", FeatureName = "CLOSING_STOCK", FeatureID = 38 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Loan from Directors", FeatureGroup = "EXPENSE", FeatureName = "LOAN_FROM_DIRECTOR", FeatureID = 39 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Creditors", FeatureGroup = "EXPENSE", FeatureName = "OTHER_CREDITORS", FeatureID = 40 ,TableName = "financialfact_ods"},

            //    //new Feature { FeatureDataType = "numeric", FeatureDescription = "Tax", FeatureGroup = "INTERCOMPANY", FeatureName = "TOTAL_TAX", FeatureID = 41 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "DUMMY", FeatureDescription = "DUMMY", FeatureGroup = "DUMMY", FeatureName = "DUMMY", FeatureID = 41 ,TableName = "DUMMY"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Tax Liability Balance (Owe / Refund)", FeatureGroup = "INTERCOMPANY", FeatureName = "TOTAL_TAX_LIABILITY", FeatureID = 42 ,TableName = "tax_assm_fact_ods"},
            //    //new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Tax Liability", FeatureGroup = "INTERCOMPANY", FeatureName = "TAX_PAYABLE_REPAYABLE", FeatureID = 43 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "DUMMY", FeatureDescription = "DUMMY", FeatureGroup = "DUMMY", FeatureName = "DUMMY", FeatureID = 43 ,TableName = "DUMMY"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "State Code", FeatureGroup = "EXPENSE", FeatureName = "STATE", FeatureID = 44 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Non-taxable profits", FeatureGroup = "EXPENSE", FeatureName = "TAX_EXEMPTED_INCOME", FeatureID = 45 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Non-Business Income", FeatureGroup = "EXPENSE", FeatureName = "OTHER_NON_SALES_INCOME", FeatureID = 46 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Trade Creditors", FeatureGroup = "EXPENSE", FeatureName = "TRADE_CREDITORS", FeatureID = 47 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Branch Code", FeatureGroup = "EXPENSE", FeatureName = "IT_ASSM_BRANCH", FeatureID = 48 ,TableName = "financialfact_ods"} // present in dim address

            //};

            //var model2Features = new List<Feature> // model2
            //{
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Equity", FeatureGroup = "INFORMATIONAL", FeatureName = "TOTAL_EQUITY", FeatureID = 49 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Interest Income", FeatureGroup = "INCOME", FeatureName = "INTEREST_INCOME", FeatureID = 50 ,TableName = "income_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Dividends", FeatureGroup = "INCOME", FeatureName = "DIVIDEND_INCOME ", FeatureID = 51 ,TableName = "income_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "", FeatureGroup = "LIABILITY", FeatureName = "LOANS", FeatureID = 52 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Current Liabilities", FeatureGroup = "LIABILITY", FeatureName = "TOTAL_CURRENT_LIABILITY", FeatureID = 53 ,TableName = "financialfact_ods"},


            //};
            //var model3Features = new List<Feature> // model3
            //{
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Employment Income (salary and wages)", FeatureGroup = "INFORMATIONAL", FeatureName = "SALARY_AND_WAGES", FeatureID = 120 ,TableName = "income_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Stocks", FeatureGroup = "ASSETS", FeatureName = "STOCKS", FeatureID = 121 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Advances", FeatureGroup = "LIABILITY", FeatureName = "CAP_FUND_ADV", FeatureID = 122 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Drawings", FeatureGroup = "LIABILITY", FeatureName = "CAP_FUND_WITHDRAWAL", FeatureID = 123 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Tax", FeatureGroup = "LIABILITY", FeatureName = "TOTAL_TAX", FeatureID = 124 ,TableName = "tax_assm_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Tax Payable", FeatureGroup = "LIABILITY", FeatureName = "TAX_PAYABLE_REPAYABLE", FeatureID = 125 ,TableName = "tax_assm_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Bad Debt", FeatureGroup = "EXPENSE", FeatureName = "BAD_DEBT", FeatureID = 126 ,TableName = "financialfact_ods"},

            //    //new Feature { FeatureDataType = "numeric", FeatureDescription = "Cash balance in hand and in bank", FeatureGroup = "ASSET_RT", FeatureName = "CASH_IN_HAND0 + CASH_IN_BANK0", FeatureID = 127 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Investments", FeatureGroup = "ASSET_RT", FeatureName = "TOTAL_INVESTMENT", FeatureID = 128 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Debtors", FeatureGroup = "ASSET_RT", FeatureName = "OTHER_DEBTORS", FeatureID = 129 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Fixed Assets", FeatureGroup = "ASSET_RT", FeatureName = "TOTAL_FIXED_ASSETS", FeatureID = 130 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Commissions Expense", FeatureGroup = "EXPENSE", FeatureName = "COMMISSION_EXPENSE", FeatureID = 131 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Other Business Expense", FeatureGroup = "EXPENSE", FeatureName = "OTHER_EXPENSES", FeatureID = 132 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Interest Income", FeatureGroup = "EXT_INCOME", FeatureName = "INTEREST_BUSINESS_INCOME", FeatureID = 133 ,TableName = "financialfact_ods"},
            //    //new Feature { FeatureDataType = "numeric", FeatureDescription = "Tax Paid", FeatureGroup = "TAX", FeatureName = "STD_PAID0 + STD_FROM_SPOUSE0", FeatureID = 134 ,TableName = "tax_assm_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Tax Paid", FeatureGroup = "TAX", FeatureName = "TAX_PAYABLE_REPAYABLE", FeatureID = 135 ,TableName = "tax_assm_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Chargeable Income", FeatureGroup = "TAX", FeatureName = "CHARGEABLE_INCOME", FeatureID = 136 ,TableName = "tax_assm_fact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Chargeable Income", FeatureGroup = "TAX", FeatureName = "CHARGEABLE_INCOME", FeatureID = 136 ,TableName = "tax_assm_fact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Chargeable Income", FeatureGroup = "TAX", FeatureName = "CHARGEABLE_INCOME", FeatureID = 136 ,TableName = "tax_assm_fact_ods"},

            //};

            //var model4Features = new List<Feature>//model4
            //{

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Current Assets", FeatureGroup = "ASSETS", FeatureName = "TOTAL_CURRENT_ASSETS", FeatureID = 163 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Lease Rental Expense", FeatureGroup = "EXPENSE", FeatureName = "RENT_OR_LEASE_EXPENSE", FeatureID = 164 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Royalties", FeatureGroup = "EXPENSE", FeatureName = "ROYALTY_EXPENSE", FeatureID = 165 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Repairs and Maintenance Expense", FeatureGroup = "EXPENSE", FeatureName = "MAINT_EXPENSE", FeatureID = 166 ,TableName = "financialfact_ods"},

            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Travel Expense", FeatureGroup = "EXPENSE", FeatureName = "TRAVELLING_EXPENSE", FeatureID = 167 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Wage Expense", FeatureGroup = "EXPENSE", FeatureName = "SALARY_EXPENSE", FeatureID = 168 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Total Liabilities", FeatureGroup = "LIABILITY �", FeatureName = "TOTAL_LIABILITIES", FeatureID = 169 ,TableName = "financialfact_ods"},
            //    new Feature { FeatureDataType = "numeric", FeatureDescription = "Branch Codes", FeatureGroup = "Dimension", FeatureName = "BRANCH_CODE", FeatureID = 170 ,TableName = "financialfact_ods"}

            //};



            //var ratios = new List<Ratio>
            //{
            //    //model 1
            //    new Ratio { RatioID = 1, RatioName = "RT_GP_GS", Propotional = 0},
            //    new Ratio { RatioID = 2, RatioName = "RT_EXP_GS", Propotional = 1},
            //    new Ratio { RatioID = 3, RatioName = "RT_NP_GS", Propotional = 0},
            //    new Ratio { RatioID = 4, RatioName = "RT_NP_TOTAST", Propotional = 0},
            //    new Ratio { RatioID = 5, RatioName = "RT_PRD_GS", Propotional = 1},
            //    new Ratio { RatioID = 6, RatioName = "RT_TRCRT_PUR", Propotional = 1},
            //    new Ratio { RatioID = 7, RatioName = "RT_TRDR_GS", Propotional = 1},
            //    new Ratio { RatioID = 8, RatioName = "RT_PUR_GS", Propotional = 1},
            //    new Ratio { RatioID = 9, RatioName = "RT_GS_TOTAST", Propotional = 0},
            //    new Ratio { RatioID = 10, RatioName = "RT_GP_EXP", Propotional = 1},
            //    new Ratio { RatioID = 11, RatioName = "INC_OPPT_B1", Propotional = 1},
            //    new Ratio { RatioID = 12, RatioName = "RT_STKTURN", Propotional = 1},
            //    new Ratio { RatioID = 13, RatioName = "RT_TRCRT_COS", Propotional = 1},

            //    //model 2
            //    new Ratio { RatioID = 14, RatioName = "RT_GP_GS", Propotional = 1},
            //    new Ratio { RatioID = 15, RatioName = "RT_EXP_GS", Propotional = 0},
            //    new Ratio { RatioID = 16, RatioName = "RT_NP_GS", Propotional = 1},
            //    new Ratio { RatioID = 17, RatioName = "RT_NP_TOTAST", Propotional = 1},
            //    new Ratio { RatioID = 18, RatioName = "RT_STKTURN", Propotional = 0},
            //    new Ratio { RatioID = 19, RatioName = "RT_TRDR_GS", Propotional = 0},
            //    new Ratio { RatioID = 20, RatioName = "RT_PUR_GS", Propotional = 0},
            //    new Ratio { RatioID = 21, RatioName = "RT_AV_STK", Propotional = 0},
            //    new Ratio { RatioID = 22, RatioName = "RT_COGS_GS", Propotional = 0},
            //    //new Ratio { RatioID = 23, RatioName = "RT_LNFRDR_GS", Propotional = 1},

            //    // Model 3
            //    new Ratio { RatioID = 24, RatioName = "RT_GP_EXP", Propotional = 1},
            //    new Ratio { RatioID = 25, RatioName = "RT_EXP_GS", Propotional = 0  },
            //    new Ratio { RatioID = 26, RatioName = "RT_GP_GS", Propotional = 1},
            //    new Ratio { RatioID = 27, RatioName = "RT_GS_TOTAST", Propotional = 1},
            //    new Ratio { RatioID = 28, RatioName = "RT_STKTURN", Propotional = 0  },
            //    new Ratio { RatioID = 29, RatioName = "RT_TRDR_GS", Propotional = 0},
            //    new Ratio { RatioID = 30, RatioName = "RT_NP_TOTAST", Propotional = 1},
            //    new Ratio { RatioID = 31, RatioName = "RT_TRCRT_PUR", Propotional = 0  },
            //    new Ratio { RatioID = 32, RatioName = "RT_GP_TOTAST", Propotional = 0},
            //    new Ratio { RatioID = 33, RatioName = "RT_NP_GS", Propotional = 1},

            //    //model4
            //    new Ratio { RatioID = 34, RatioName = "RT_AV_STK", Propotional = 0},
            //    new Ratio { RatioID = 35, RatioName = "RT_COGS_GS", Propotional = 0  },
            //    new Ratio { RatioID = 36, RatioName = "RT_GP_GS", Propotional = 1},
            //    new Ratio { RatioID = 37, RatioName = "RT_NP_GS", Propotional = 1},
            //    new Ratio { RatioID = 38, RatioName = "RT_PRD_GS", Propotional = 0  },
            //    new Ratio { RatioID = 39, RatioName = "RT_STKTURN", Propotional = 0},
            //    new Ratio { RatioID = 40, RatioName = "RT_NP_TOTAST", Propotional = 1},


            //    //add ratios
            //};

            //var models = new List<Model>
            //{
            //   new Model { ModelID = 1, ModelName = "Model (GPNP3 & GPRATIO)"},
            //   new Model { ModelID = 2, ModelName = "Model (OV_DIR)"},
            //   new Model { ModelID = 3, ModelName = "Model (GPNP & EXPTAX)"},
            //   new Model { ModelID = 4, ModelName = "Model (INDNORM)"},
            //};

            //var modelFeatures = new List<ModelFeature>
            //{

            //    //added
            //    new ModelFeature{ ModelID = 1, FeatureID = 16 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 17 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 18 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 19 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 20 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 21 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 22 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 23 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 24 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 25 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 26 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 27 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 28 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 29 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 30 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 31 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 32 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 33 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 34 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 35 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 36 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 37 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 38 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 39 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 40 },
            //    //new ModelFeature{ ModelID = 1, FeatureID = 41 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 124 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 42 },
            //    //new ModelFeature{ ModelID = 1, FeatureID = 43 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 125 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 44 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 45 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 46 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 47 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 48 },
            //    new ModelFeature{ ModelID = 1, FeatureID = 170 },

            //     //added

            //    new ModelFeature{ ModelID = 2, FeatureID = 18 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 48 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 25 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 34 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 26 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 28 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 29 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 30 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 36 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 38 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 39 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 40 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 47 },
            //    //new ModelFeature{ ModelID = 2, FeatureID = 41 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 124 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 37 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 45 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 17 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 34 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 156 },







            //    new ModelFeature{ ModelID = 2, FeatureID = 49 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 50 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 51 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 52 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 53 },
            //    new ModelFeature{ ModelID = 2, FeatureID = 170 },

            //    // model 3

            //    new ModelFeature{ ModelID = 3, FeatureID = 17 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 18 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 48 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 25 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 31 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 32 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 33 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 34 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 35 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 26 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 28 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 29 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 30 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 36 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 37 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 38 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 40 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 47 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 45 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 170 },




            //    new ModelFeature{ ModelID = 3, FeatureID = 120 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 121 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 122 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 123 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 124 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 125 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 126 },             
            //    //new ModelFeature{ ModelID = 3, FeatureID = 127 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 128 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 129 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 130 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 131 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 132 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 133 },
            //    //new ModelFeature{ ModelID = 3, FeatureID = 134 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 135 },
            //    new ModelFeature{ ModelID = 3, FeatureID = 136 },


            //    // model 4

            //     new ModelFeature{ ModelID = 4, FeatureID = 16 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 17 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 18 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 48 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 24 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 25 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 31 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 32 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 33 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 34 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 35 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 26 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 28 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 29 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 30 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 36 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 38 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 40 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 47 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 37 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 44 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 121 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 126 },
            //     //new ModelFeature{ ModelID = 4, FeatureID = 127 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 128 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 130 },
            //     new ModelFeature{ ModelID = 4, FeatureID = 132 },


            //    new ModelFeature{ ModelID = 4, FeatureID = 163 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 164 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 165 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 166 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 167 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 168 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 169 },
            //    new ModelFeature{ ModelID = 4, FeatureID = 170 },



            //};

            //var ratioFeatures = new List<RatioFeature>
            //{
            //    new RatioFeature{ RatioID = 1, FeatureID = 28},
            //    new RatioFeature{ RatioID = 1, FeatureID = 29},
            //    new RatioFeature{ RatioID = 2, FeatureID = 29},
            //    new RatioFeature{ RatioID = 2, FeatureID = 35},
            //    new RatioFeature{ RatioID = 3, FeatureID = 29},
            //    new RatioFeature{ RatioID = 3, FeatureID = 30},
            //    new RatioFeature{ RatioID = 4, FeatureID = 30},
            //    new RatioFeature{ RatioID = 4, FeatureID = 17},
            //    new RatioFeature{ RatioID = 5, FeatureID = 29},
            //    new RatioFeature{ RatioID = 5, FeatureID = 27},
            //    new RatioFeature{ RatioID = 6, FeatureID = 34},
            //    new RatioFeature{ RatioID = 6, FeatureID = 47},
            //    new RatioFeature{ RatioID = 7, FeatureID = 29},
            //    new RatioFeature{ RatioID = 7, FeatureID = 18},
            //    new RatioFeature{ RatioID = 8, FeatureID = 29},
            //    new RatioFeature{ RatioID = 8, FeatureID = 34},
            //    new RatioFeature{ RatioID = 9, FeatureID = 29},
            //    new RatioFeature{ RatioID = 9, FeatureID = 17},
            //    new RatioFeature{ RatioID = 10, FeatureID = 28},
            //    new RatioFeature{ RatioID = 10, FeatureID = 35},
            //    new RatioFeature{ RatioID = 11, FeatureID = 30},
            //    new RatioFeature{ RatioID = 11, FeatureID = 37},
            //    new RatioFeature{ RatioID = 12, FeatureID = 25},
            //    new RatioFeature{ RatioID = 12, FeatureID = 26},
            //    new RatioFeature{ RatioID = 12, FeatureID = 38},
            //    new RatioFeature{ RatioID = 13, FeatureID = 25},
            //    new RatioFeature{ RatioID = 13, FeatureID = 47},

            //    //model2
            //    new RatioFeature{ RatioID = 14, FeatureID = 28},
            //    new RatioFeature{ RatioID = 14, FeatureID = 29},
            //    new RatioFeature{ RatioID = 15, FeatureID = 35},
            //    new RatioFeature{ RatioID = 15, FeatureID = 29},
            //    new RatioFeature{ RatioID = 16, FeatureID = 30},
            //    new RatioFeature{ RatioID = 16, FeatureID = 29},
            //    new RatioFeature{ RatioID = 17, FeatureID = 29},
            //    new RatioFeature{ RatioID = 17, FeatureID = 17},
            //    new RatioFeature{ RatioID = 18, FeatureID = 25},
            //    new RatioFeature{ RatioID = 18, FeatureID = 26},
            //    new RatioFeature{ RatioID = 18, FeatureID = 38},
            //    new RatioFeature{ RatioID = 19, FeatureID = 18},
            //    new RatioFeature{ RatioID = 19, FeatureID = 29},
            //    new RatioFeature{ RatioID = 20, FeatureID = 34},
            //    new RatioFeature{ RatioID = 20, FeatureID = 29},
            //    new RatioFeature{ RatioID = 21, FeatureID = 26},
            //    new RatioFeature{ RatioID = 21, FeatureID = 38},
            //    new RatioFeature{ RatioID = 22, FeatureID = 25},
            //    new RatioFeature{ RatioID = 22, FeatureID = 29},
            //    //new RatioFeature{ RatioID = 23, FeatureID = 39},
            //    //new RatioFeature{ RatioID = 23, FeatureID = 29},

            //    //model3
            //    //making the below ratio ids -1. 34 becomes 23 because i commented out the above ratio
            //    new RatioFeature{ RatioID = 23, FeatureID = 28},
            //    new RatioFeature{ RatioID = 23, FeatureID = 35},
            //    new RatioFeature{ RatioID = 24, FeatureID = 35},
            //    new RatioFeature{ RatioID = 24, FeatureID = 29},
            //    new RatioFeature{ RatioID = 25, FeatureID = 28 },
            //    new RatioFeature{ RatioID = 25, FeatureID = 29},
            //    new RatioFeature{ RatioID = 26, FeatureID = 29},
            //    new RatioFeature{ RatioID = 26, FeatureID = 17},

            //    new RatioFeature{ RatioID = 27, FeatureID = 25},
            //    new RatioFeature{ RatioID = 27, FeatureID = 26},
            //    new RatioFeature{ RatioID = 27, FeatureID = 38},

            //    new RatioFeature{ RatioID = 28, FeatureID = 18},
            //    new RatioFeature{ RatioID = 28, FeatureID = 29},
            //    new RatioFeature{ RatioID = 29, FeatureID = 17},
            //    new RatioFeature{ RatioID = 29, FeatureID = 30},
            //    new RatioFeature{ RatioID = 30, FeatureID = 47},
            //    new RatioFeature{ RatioID = 30, FeatureID = 34},
            //    new RatioFeature{ RatioID = 31, FeatureID = 28},
            //    new RatioFeature{ RatioID = 31, FeatureID = 27},
            //    new RatioFeature{ RatioID = 32, FeatureID = 30},
            //    new RatioFeature{ RatioID = 32, FeatureID = 29  },

            //    //model4
            //    new RatioFeature{ RatioID = 33, FeatureID = 26},
            //    new RatioFeature{ RatioID = 33, FeatureID = 38},

            //    new RatioFeature{ RatioID = 34, FeatureID = 25},
            //    new RatioFeature{ RatioID = 34, FeatureID = 29},

            //    new RatioFeature{ RatioID = 35, FeatureID = 28},
            //    new RatioFeature{ RatioID = 35, FeatureID = 29},

            //    new RatioFeature{ RatioID = 36, FeatureID = 30},
            //    new RatioFeature{ RatioID = 36, FeatureID = 29},

            //    new RatioFeature{ RatioID = 37, FeatureID = 27},
            //    new RatioFeature{ RatioID = 37, FeatureID = 29},

            //    new RatioFeature{ RatioID = 38, FeatureID = 25},
            //    new RatioFeature{ RatioID = 38, FeatureID = 26},
            //    new RatioFeature{ RatioID = 38, FeatureID = 38},
            //    new RatioFeature{ RatioID = 39, FeatureID = 30},
            //    new RatioFeature{ RatioID = 39, FeatureID = 17}

            //};

            //profiles.ForEach(u => context.ProfileSet.Add(u));

            //model1Features.ForEach(a => { a.FeatureName = a.FeatureName.ToLower(); });
            //model2Features.ForEach(a => { a.FeatureName = a.FeatureName.ToLower(); });
            //model3Features.ForEach(a => { a.FeatureName = a.FeatureName.ToLower(); });
            //model4Features.ForEach(a => { a.FeatureName = a.FeatureName.ToLower(); });

            //model1Features.ForEach(u => context.FeatureSet.Add(u));
            //model2Features.ForEach(u => context.FeatureSet.Add(u));
            //model3Features.ForEach(u => context.FeatureSet.Add(u));
            //model4Features.ForEach(u => context.FeatureSet.Add(u));

            //context.SaveChanges();
            //models.ForEach(u => context.ModelSet.Add(u));
            //ratios.ForEach(u => context.RatioSet.Add(u));
            //context.SaveChanges();
            //modelFeatures.ForEach(u => context.ModelFeaturesSet.Add(u));
            //ratioFeatures.ForEach(u => context.RatioFeaturesSet.Add(u));
            //context.SaveChanges();

            #endregion
        }
    }
}
