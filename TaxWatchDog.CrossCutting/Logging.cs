﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace TaxWatchDog.CrossCutting.Logging
{
    public class Logger : ILog
    {
        #region Private Properties

        private ILog ActualLogger
        {
            get
            {
                var callingMethod =
                (
                from frame in new StackTrace().GetFrames()
                let type = frame.GetMethod()
                where type.DeclaringType != typeof(Logger)
                select type
                )
                .First();

                return LogManager.GetLogger(callingMethod.DeclaringType.FullName + "." + callingMethod.Name);
            }
        }

        #endregion

        #region ILog Implementation

        public bool IsDebugEnabled
        {
            get { return this.ActualLogger.IsDebugEnabled; }
        }

        public bool IsErrorEnabled
        {
            get { return this.ActualLogger.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return this.ActualLogger.IsFatalEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return this.ActualLogger.IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return this.ActualLogger.IsWarnEnabled; }
        }

        public void Debug(object message, Exception exception)
        {
            this.ActualLogger.Debug(message, exception);
        }

        public void Debug(object message)
        {
            this.ActualLogger.Debug(message);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            this.ActualLogger.DebugFormat(provider, format, args);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            this.ActualLogger.DebugFormat(format, arg0, arg1, arg2);
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            this.ActualLogger.DebugFormat(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0)
        {
            this.ActualLogger.DebugFormat(format, arg0);
        }

        public void DebugFormat(string format, params object[] args)
        {
            this.ActualLogger.DebugFormat(format, args);
        }

        public void Error(object message, Exception exception)
        {
            this.ActualLogger.Error(message, exception);
        }

        public void Error(object message)
        {
            this.ActualLogger.Error(message);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            this.ActualLogger.ErrorFormat(provider, format, args);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            this.ActualLogger.ErrorFormat(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            this.ActualLogger.ErrorFormat(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0)
        {
            this.ActualLogger.ErrorFormat(format, arg0);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            this.ActualLogger.ErrorFormat(format, args);
        }

        public void Fatal(object message, Exception exception)
        {
            this.ActualLogger.Fatal(message, exception);
        }

        public void Fatal(object message)
        {
            this.ActualLogger.Fatal(message);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            this.ActualLogger.ErrorFormat(provider, format, args);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            this.ActualLogger.FatalFormat(format, arg0, arg1, arg2);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            this.ActualLogger.FatalFormat(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0)
        {
            this.ActualLogger.FatalFormat(format, arg0);
        }

        public void FatalFormat(string format, params object[] args)
        {
            this.ActualLogger.FatalFormat(format, args);
        }

        public void Info(object message, Exception exception)
        {
            this.ActualLogger.Info(message, exception);
        }

        public void Info(object message)
        {
            this.ActualLogger.Info(message);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            this.ActualLogger.InfoFormat(provider, format, args);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            this.ActualLogger.InfoFormat(format, arg0, arg1, arg2);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            this.ActualLogger.InfoFormat(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0)
        {
            this.ActualLogger.InfoFormat(format, arg0);
        }

        public void InfoFormat(string format, params object[] args)
        {
            this.ActualLogger.InfoFormat(format, args);
        }

        public void Warn(object message, Exception exception)
        {
            this.ActualLogger.Warn(message, exception);
        }

        public void Warn(object message)
        {
            this.ActualLogger.Warn(message);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            this.ActualLogger.WarnFormat(provider, format, args);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            this.ActualLogger.WarnFormat(format, arg0, arg1, arg2);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            this.ActualLogger.WarnFormat(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0)
        {
            this.ActualLogger.WarnFormat(format, arg0);
        }

        public void WarnFormat(string format, params object[] args)
        {
            this.ActualLogger.WarnFormat(format, args);
        }

        log4net.Core.ILogger log4net.Core.ILoggerWrapper.Logger
        {
            get { return this.ActualLogger.Logger; }
        }

        #endregion
    }
}
