﻿using log4net;
using Microsoft.Practices.Unity;
using MySql.Data.MySqlClient;
using SedaAtlas.CrossCutting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxWatchDog.CrossCutting
{
    public class OdbcConnector
    {
        public static DataSet GetDataFromHive(string query)
        {
            var _logService = UnityConfig.GetConfiguredContainer().Resolve<ILog>();
            _logService.Info("Inside GetDataFromHive");
            var conn = new OdbcConnection { ConnectionString = @"DSN=" + System.Configuration.ConfigurationManager.AppSettings["HiveDSN"] };
            try
            {
                _logService.Info("Will now run conn.open");
                conn.Open();
                _logService.Info("Will now run new adp");
                var adp = new OdbcDataAdapter(query, conn);
                adp.SelectCommand.CommandTimeout = Int32.MaxValue;
                var ds = new DataSet();
                _logService.Info("Will now fill ds");
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataSet GetDataFromMySql(string query)
        {
            var conn = new MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["WebDbConnectionString"].ConnectionString);
            try
            {
                conn.Open();
                var adp = new MySqlDataAdapter(query, conn);
                var ds = new DataSet();
                adp.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                var _logService = UnityConfig.GetConfiguredContainer().Resolve<ILog>();
                _logService.Error(ex);
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        //[Dependency]
        //public static ILog _logService { get; set; }
    }
}
