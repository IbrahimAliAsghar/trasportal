﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;


namespace TaxWatchDog.JobExecutioners
{
    public class CMSExecutioner : IJobExecutioner
    {
        public int Id { get { return 1; } set { this.Id = value; } }
        public string Name { get { return HelperStrings.JobExecutioner_CMS; } set { this.Name = value; } }
        public string JobType { get { return HelperStrings.JobSType_CMS; } set { this.JobType = value; } }
        public string Execute(Job job)
        {
            try
            {
                //this._logService.Info("Starting job & Updating job " + job.JobID + " status to " + HelperStrings.JobStatus_SendingToSpark);
                //_jobService.UpdateJobStatus(job.JobID, HelperStrings.JobStatus_SendingToSpark);
                var result = this._jobService.ExecuteCMSJob(job);
                //this._logService.Info("Finishing job & Updating job " + job.JobID + " status to " + HelperStrings.JobStatus_Done);

                _jobService.UpdateJobStatus(job.JobID, HelperStrings.JobStatus_InQueue);
                _jobService.UpdateLastRunDate(job.JobID);
                return result;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ConnectFailure)
                {
                    _jobService.UpdateJobStatus(job.JobID, HelperStrings.JobStatus_InQueue);
                }
                throw;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                //_jobService.UpdateJobStatus(job.JobID, HelperStrings.JobStatus_Failed);
                throw;
            }
        }

        [Dependency]
        public IJobService _jobService { get; set; }
        [Dependency]
        public ILog _logService { get; set; }
    }
}
