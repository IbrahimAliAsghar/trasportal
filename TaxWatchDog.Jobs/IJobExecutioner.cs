﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models;

namespace TaxWatchDog.JobExecutioners
{
    public interface IJobExecutioner
    {
        int Id { get; set; }
        string Name { get; set; }
        string JobType { get; set; }
        string Execute(Job job);

    }
}
