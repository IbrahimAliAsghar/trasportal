from flask import Flask, jsonify
from flask import request
import Driver as driver_model
import Config

app = Flask(__name__)

# Just for testing purpose
@app.route('/test', methods=['POST', 'GET'])
def TestJobUpdateApi():
    import requests
    url = Config.ApiUrls["UpdateJobStatus"]
    data = {'jobId': 7, 'status': 'done'}
    response = requests.post(url, data=data)
    return "done"

@app.route('/Echo', methods=['POST', 'GET'])
def Echo():
    return "ECHO"

# Execute Run Model
@app.route('/RunModel', methods=['POST'])
def RunModel():
    # Recieves Json from backend
    params = request.get_json()
    print(params)
    # Params json is passed as argument
    result = driver_model.main(params)
    return  jsonify(result)

# Getting results from Hive
@app.route('/GetResultsFromHive', methods=['POST'])
def GetResultsFromHive():
    import json
    # Recieves Json from backend
    params = request.get_json()
    print(params)
    # Calling Spark_Connection object
    from Spark_Connection import Spark_Connection
    con_spark = Spark_Connection()
    sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
    # Calling Read_write_hive object
    from Read_write_hive import Read_write_hive
    read_from_hive = Read_write_hive()
    # Params contain ProfileId , offset and limit
    df= read_from_hive.GetResultsFromHive(params,spark)
    df = df.toPandas()
    # Converting df to json
    jsonString = df.to_json(orient='records')
    return jsonString

#method to get WHT details
@app.route('/GetWHTResults', methods=['POST'])
def GetWHTResults():
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive   
   read_from_hive = Read_write_hive()
   # Params contains AssesmentYear and It_ref_nos
   df = read_from_hive.wht_integration(params,spark)
   import pandas as pd
   df = df.toPandas()
   # Changing type to str
   df['wht_final_scoringrisk_factor']= df['wht_final_scoringrisk_factor'].astype(str)
   #df.head(1)
   # Converting df to json
   jsonString = df.to_json(orient='records')   
   #print(jsonString)
   return jsonString 

@app.route('/GetMeansTestResults', methods=['POST'])
def GetMeansTestResults():
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive   
   read_from_hive = Read_write_hive()
   # Params contains AssesmentYear and It_ref_nos
   df = read_from_hive.GetMeansTestResults(params,spark)
   import pandas as pd
   df = df.toPandas()
   # Converting df to json
   jsonString = df.to_json(orient='records')   
   #print(jsonString)
   return jsonString 

@app.route('/GetDigitalEconomyResults', methods=['POST'])
def GetDigitalEconomyResults():
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive   
   read_from_hive = Read_write_hive()
   # Params contains AssesmentYear and It_ref_nos
   df = read_from_hive.GetDigitalEconomyResults(params,spark)
   import pandas as pd
   df = df.toPandas()
   # Converting df to json
   jsonString = df.to_json(orient='records')   
   #print(jsonString)
   return jsonString 

# Getting Drop down data
@app.route('/GetDropDownData', methods=['POST'])
def GetDropDownData():
   import json
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   from Read_write_hive import Read_write_hive
   # Calling Read_write_hive object
   read_from_hive = Read_write_hive()
   # Calling function to fetch data
   df_assessmentYear,df_branchCode,df_businessCode,df_stateCode = read_from_hive.dropdownData(spark)
   import pandas as pd
   # Converting to pandas
   df_assessmentYear = df_assessmentYear.toPandas()
   df_branchCode = df_branchCode.toPandas()
   df_businessCode = df_businessCode.toPandas()
   df_stateCode = df_stateCode.toPandas()
   # Converting df to json
   json_assessmentYear = df_assessmentYear.to_json(orient='records')
   json_branchCode = df_branchCode.to_json(orient='records')
   json_businessCode = df_businessCode.to_json(orient='records')
   json_stateCode = df_stateCode.to_json(orient='records')
   # Setting individual columns in dict
   dropdown_dict = {"assessmentYear": json_assessmentYear,"branchCode":json_branchCode,"businessCode":json_businessCode,"stateCode":json_stateCode}
   dropdown_json = json.dumps(dropdown_dict)
   return dropdown_json 

# Getting Count of data according to ODSQuery
@app.route('/GetODSQueryCount', methods=['POST'])
def GetODSQueryCount():
   import json
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive
   read_from_hive = Read_write_hive()
   # Calling read_from_hive to get its count
   df= read_from_hive.read_from_hive(params,spark)
   df = df.filter(df['assessment_year'].isin([int(params["AssesmentYear"])]))
   recordscount= df.count()
   # Converting df to json
   recordscount_dict = {"recordscount": recordscount}
   recordscount_json = json.dumps(recordscount_dict)
   return recordscount_json 
    

# Getting Repord Card data
@app.route('/GetReportCardData', methods=['POST'])
def GetReportCardData():
   import json
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive
   read_from_hive = Read_write_hive()
   # Calling GetReportCardData
   # Params contain Profile_Id and it_ref_no
   df= read_from_hive.GetReportCardData(params,spark)
   df = df.toPandas()
   # Converting df to json
   jsonString = df.to_json(orient='records')
   return jsonString 

# Getting Feature count
@app.route('/GetFeatureCount', methods=['POST'])
def GetFeatureCount():
   import json
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive
   read_from_hive = Read_write_hive()
   # Calling GetFeatureCount
   # Params contain Profile_Id 
   df= read_from_hive.GetFeatureCount(params,spark)
   # Converting df to json
   jsonString = df.to_json(orient='records')
   return jsonString 

# Getting TP results integrated 
@app.route('/GetTPResults', methods=['POST'])
def GetTPResults():
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Spark_Connection object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive
   read_from_hive = Read_write_hive()
   # Calling tp_integration
   # Params contain Assessment year and  it_ref_nos
   df = read_from_hive.tp_integration(params,spark)
   import pandas as pd
   #df.head(1)
   # Converting df to json
   jsonString = df.to_json(orient='records')
   print(jsonString)
   return jsonString 

# Getting Columns to add
@app.route('/GetColsToAdd', methods=['POST'])
def GetColsToAdd():
   # Recieves Json from backend
   params = request.get_json()
   print(params)
   # Calling Read_write_hive object
   from Spark_Connection import Spark_Connection
   con_spark = Spark_Connection()
   sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
   # Calling Read_write_hive object
   from Read_write_hive import Read_write_hive   
   read_from_hive = Read_write_hive()
   # Calling ColsToAdd_CMS
   # Params contain assesmentYear, query, tableName and  itRefNo
   df = read_from_hive.ColsToAdd_CMS(params,spark)
   import pandas as pd
   #df.head(1)
   # Converting df to json
   jsonString = df.to_json(orient='records')  
   print(jsonString)
   return jsonString 

if __name__ == '__main__':
    app.run(debug=Config.API['DebugMode'],threaded = True) 