# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 11:39:19 2019


"""
class Ratios(object):
    	# Ratio formulas
        def Add_RT_ADVRT_GS(self,df):
            df1=df.withColumn('RT_ADVRT_GS', (df['promo_advert_expense'] / df['gross_sales']))
            return df1
        def Add_RT_BADDBT_GS(self,df):
            df1=df.withColumn('RT_BADDBT_GS', (df['bad_debt'] / df['gross_sales']))
            return df1
        def Add_RT_CAST_TA(self,df):
            df1=df.withColumn('RT_CAST_TA', (df['total_current_assets'] / df['total_assets']))
            return df1
        def Add_RT_CLST_CAST(self,df):
            df1=df.withColumn('RT_CLST_CAST', (df['closing_stock'] / df['total_current_assets']))
            return df1
        def Add_RT_COM_GS(self,df):
            df1=df.withColumn('RT_COM_GS', (df['commission'] / df['gross_sales']))
            return df1
        def Add_RT_EXP_GS(self,df):
            df1=df.withColumn('RT_EXP_GS', (df['total_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_FA_TOTAST(self,df):
            df1=df.withColumn('RT_FA_TOTAST', (df['total_fixed_assets'] / df['total_assets']))
            return df1
        def Add_RT_GP_GS(self,df):
            df1=df.withColumn('RT_GP_GS', (df['gross_profit'] / df['gross_sales']))
            return df1
        def Add_RT_GP_TOTAST(self,df):
            df1=df.withColumn('RT_GP_TOTAST', (df['gross_profit'] / df['total_assets']))
            return df1
        def Add_RT_GP_EXP(self,df):
            df1=df.withColumn('RT_GP_EXP',(df['gross_profit'] / df['total_expenses']))
            return df1
        def Add_RT_GS_TOTAST(self,df):
            df1=df.withColumn('RT_GS_TOTAST', (df['gross_sales'] / df['total_assets']))
            return df1
        def Add_RT_LIQUIDITY(self,df):
            df1=df.withColumn('RT_LIQUIDITY', (df['total_current_assets'] / df['total_current_liability']))
            return df1
        def Add_RT_LOAN_EQY(self,df):
            df1=df.withColumn('RT_LOAN_EQY', (df['loans'] / df['total_equity']))
            return df1
        def Add_RT_NAEXP_TEX(self,df):
            df1=df.withColumn('RT_NAEXP_TEX', (df['non_allow_expense'] / df['total_expenses']))
            return df1
        def Add_RT_NALW_GS(self,df):
            df1=df.withColumn('RT_NALW_GS', (df['non_allow_expense'] / df['gross_sales']))
            return df1
        def Add_RT_NP_FA(self,df):
            df1=df.withColumn('RT_NP_FA', (df['net_profit_or_loss'] / df['total_fixed_assets']))
            return df1
        def Add_RT_NP_GS(self,df):
            df1=df.withColumn('RT_NP_GS', (df['net_profit_or_loss'] / df['gross_sales']))
            return df1
        def Add_RT_NP_INT(self,df):
            df1=df.withColumn('RT_NP_INT', (df['net_profit_or_loss'] / df['loan_interest_expense']))
            return df1
        def Add_RT_NP_TOTAST(self,df):
            df1=df.withColumn('RT_NP_TOTAST', (df['net_profit_or_loss'] / df['total_assets']))
            return df1
        def Add_RT_OTHCR_GS(self,df):
            df1=df.withColumn('RT_OTHCR_GS', (df['other_creditors'] / df['gross_sales']))
            return df1
        def Add_RT_OCR_TOTEX(self,df):
            df1=df.withColumn('RT_OCR_TOTEX', (df['other_creditors'] / df['total_expenses']))
            return df1
        def Add_RT_OTHEXP_GS(self,df):
            df1=df.withColumn('RT_OTHEXP_GS', (df['other_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_OTHINC_TI(self,df):
            df1=df.withColumn('RT_OTHINC_TI', (df['other_non_sales_income'] / df['total_income']))
            return df1
        def Add_RT_PRD_GS(self,df):
            df1=df.withColumn('RT_PRD_GS', (df['production_cost'] / df['gross_sales']))
            return df1
        def Add_RT_PUR_GS(self,df):
            df1=df.withColumn('RT_PUR_GS', (df['purchases'] / df['gross_sales']))
            return df1
        def Add_RT_RND_GS(self,df):
            df1=df.withColumn('RT_RND_GS', (df['r_d_expense'] / df['gross_sales']))
            return df1
        def Add_RT_RND_TEX(self,df):
            df1=df.withColumn('RT_RND_TEX', (df['r_d_expense'] / df['total_expenses']))
            return df1
        def Add_RT_ROYLTY_GS(self,df):
            df1=df.withColumn('RT_ROYLTY_GS', (df['royalty_expense'] / df['gross_sales']))
            return df1
        def Add_RT_CSUBC_GS(self,df):
            df1=df.withColumn('RT_CSUBC_GS', (df['contract_subcon_expense'] / df['gross_sales']))
            return df1
        def Add_RT_TOTEX_OCR(self,df):
            df1=df.withColumn('RT_TOTEX_OCR', (df['total_expenses'] / df['other_creditors']))
            return df1
        def Add_RT_TOTEXP_GS(self,df):
            df1=df.withColumn('RT_TOTEXP_GS', (df['total_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_TRAVEL_GS(self,df):
            df1=df.withColumn('RT_TRAVEL_GS', (df['travelling_expense'] / df['gross_sales']))
            return df1
        def Add_RT_AV_STK(self,df):#
            df1=df.withColumn('RT_AV_STK', (df['opening_stock'] + df['closing_stock'])/2)
            return df1
        def Add_RT_CAP(self,df):
            df1=df.withColumn('RT_CAP', (df['total_current_assets'] - df['total_current_liability']))
            return df1
        def Add_RT_DEBT_EQY(self,df):
            df1=df.withColumn('RT_DEBT_EQY', ((df['other_creditors'] + df['loan_from_director'] + df['other_current_liability'])/df['total_equity_liability']))
            return df1
        def Add_RT_FA_TOTPRD(self,df):
            df1=df.withColumn('RT_FA_TOTPRD', (df['total_fixed_assets'] / (df['purchases'] + df['production_cost'])))
            return df1
        def Add_RT_PURPRD_GS(self,df):
            df1=df.withColumn('RT_PURPRD_GS', ((df['purchases'] + df['production_cost']) / df['gross_sales']))
            return df1
        def Add_RT_TRDR_GS(self,df):
            df1=df.withColumn('RT_TRDR_GS', (df['trade_debtors'] / df['gross_sales']) * 365)
            return df1
        def Add_RT_WAGES_GS(self,df):
            df1 = df.withColumn('RT_WAGES_GS', (df["salary_expense"] / df["gross_sales"]))
            return df1
        def Add_RT_CA_GS(self,df):
            df1 = df.withColumn('RT_CA_GS', (df["capital_allow_claim"] / df["gross_sales"]))
            return df1
        def Add_RT_CA_TOTFA(self,df):
            df1 = df.withColumn('RT_CA_TOTFA', (df["capital_allow_claim"] / df["total_fixed_assets"]))
            return df1
        def Add_RT_CASH_TCL(self,df):
            df1 = df.withColumn('RT_CASH_TCL', (df["cash_in_hand"] / df["total_current_liability"]))
            return df1
        def Add_RT_COGS_GS(self,df):
            df1 = df.withColumn('RT_COGS_GS', (df["cost_of_goods_sold"] / df["gross_sales"]))
            return df1
        def Add_RT_COM_SINC(self,df):
            df1 = df.withColumn('RT_COM_SINC', (df["commission"] / df["aggr_stat_bus_inc"]))
            return df1
        def Add_RT_FA_TOTINC(self,df):
            df1 = df.withColumn('RT_FA_TOTINC', (df["total_fixed_assets"] / (df["gross_sales"]+df["other_business_income"]+df["other_non_sales_income"])))
            return df1
        def Add_RT_GS_TOTFA(self,df):
            df1 = df.withColumn('RT_GS_TOTFA', (df["gross_sales"] / df["total_fixed_assets"]))
            return df1
        def Add_RT_INT_GS(self,df):
            df1 = df.withColumn('RT_INT_GS', (df["loan_interest_expense"] / df["gross_sales"]))
            return df1
        def Add_RT_LTLIB_EQY(self,df):
            df1 = df.withColumn('RT_LTLIB_EQY', (df["long_term_liability"] / df["total_equity_liability"]))
            return df1
        def Add_RT_NAINT_TEX(self,df):
            df1 = df.withColumn('RT_NAINT_TEX', (df["total_investment"] + df["loan_to_director"]) / (df["total_liabilities"] - df["trade_creditors"]) * df["loan_interest_expense"])
            return df1
        def Add_RT_STKTURN(self,df):
            #(cost_of_goods_sold/((opening_stock+closing_stock)/2))*365
            df1 = df.withColumn('RT_STKTURN', ((df["cost_of_goods_sold"] / ((df["opening_stock"] + df["closing_stock"])/2)))*365)
            return df1
        def Add_RT_TINE_TINC(self,df):
            df1 = df.withColumn('RT_TINE_TINC', (df["total_claim"] / df["total_income"]))
            return df1
        def Add_RT_TINE_NPNL(self,df):
            df1 = df.withColumn('RT_TINE_NPNL', (df["total_claim"] / df["net_profit_or_loss"]))
            return df1
        def Add_RT_TOTCL_EQY(self,df):
            df1 = df.withColumn('RT_TOTCL_EQY', (df["total_current_liability"] / df["total_equity_liability"]))
            return df1
        def Add_RT_TRCRT_PUR(self,df):
            df1 = df.withColumn('RT_TRCRT_PUR', (df["trade_creditors"] / df["purchases"]))
            return df1
        def Add_RT_GS_CAP(self,df):
            df1 = df.withColumn('RT_GS_CAP', (df["gross_sales"] / (df["total_current_assets"] - df["total_current_liability"])))
            return df1
        def Add_INC_OPPT_B1(self,df):
            df1 = df.withColumn('INC_OPPT_B1', (df["net_profit_or_loss"] - df["other_non_sales_income"]))
            return df1
        def Add_RT_TRCRT_COS(self,df):
            df1 = df.withColumn('RT_TRCRT_COS', (df["trade_creditors"] / df["cost_of_goods_sold"])*365)
            return df1
        def Add_RT_LNFRDR_GS(self,df):
            df1 = df.withColumn('RT_LNFRDR_GS',(df["loan_from_director"] / df["gross_sales"]))
            return df1
        def Add_RT_GS_WAGES(self,df):
            df1 = df.withColumn('RT_GS_WAGES', (df["gross_sales"] / df["salary_wages"]))
            return df1
        def Add_RT_LN_INT_EXP(self,df):
            df1 = df.withColumn('RT_LN_INT_EXP', (df["loan_from_director"] / df["gross_sales"])*100)
            return df1
        def Add_RT_PUR_IN_MSIA(self,df):
            df1 = df.withColumn('RT_PUR_IN_MSIA', (df["total_purchase_in_malaysia"] / df["purchases"])*100)
            return df1
        def Add_RT_PUR_OUT_M(self,df):
            df1 = df.withColumn('RT_PUR_OUT_M', (df["total_purchase_outside_malaysi"] / df["purchases"])*100)
            return df1
        def Add_RT_REL_SALE_OUT_MSIA(self,df):
            df1 = df.withColumn('RT_REL_SALE_OUT_MSIA', (df["receipt_from_outside_malaysia"] / df["gross_sales"])*100)
            return df1
        def Add_RT_REL_SALE_IN_MSIA(self,df):
            df1 = df.withColumn('RT_REL_SALE_IN_MSIA', (df["total_sales_in_malaysia"] / df["gross_sales"])*100)
            return df1
        def Add_ETR(self,df):
            df1 = df.withColumn('ETR', (df["tax_payable_repayable"] / df["net_profit_or_loss"]))
            return df1
        def Add_DF_LOANTO_DIR(self,df):
            df1 = df.withColumn('DF_LOANTO_DIR', (df["loan_to_director"] / df["gross_sales"])*100)
            return df1
        def Add_DF_LOANFR_DIR(self,df):
            df1 = df.withColumn('DF_LOANFR_DIR', (df["loan_from_director"] / df["gross_sales"])*100)
            return df1
        def Add_RT_OEX_TOTEX(self,df):
            df1 = df.withColumn('RT_OEX_TOTEX', (df["other_expenses"] / df["total_expenses"]))
            return df1
        def Add_RT_INT_TOTLN(self,df):
            df1 = df.withColumn('RT_INT_TOTLN', (df["loan_interest_expense"] / df["loans"]))
            return df1
        def Add_RT_OCRIN_TEX(self,df):
            df1 = df.withColumn('RT_OCRIN_TEX', (df["other_creditors"] / df["total_expenses"]))
            return df1
        def Add_RT_PROFEE_GS(self,df):
            df1 = df.withColumn('RT_PROFEE_GS', (df["nr_tech_fee_pymt"] / df["gross_sales"]))
            return df1
        def Add_RT_RPMNT_GS(self,df):
            df1 = df.withColumn('RT_RPMNT_GS', (df["maint_expense"] / df["gross_sales"]))
            return df1
        def Add_RT_TRAVL_TEX(self,df):
            df1 = df.withColumn('RT_TRAVL_TEX', (df["travelling_expense"] / df["total_expenses"]))
            return df1
        def Add_RT_RPMNT_PM(self,df):
            df1 = df.withColumn('RT_RPMNT_PM', (df["maint_expense"] / df["plant_and_machinery"]))
            return df1
        def Add_RT_PRD_PNM(self,df):
            df1 = df.withColumn('RT_PRD_PNM', (df["production_cost"] / df["plant_and_machinery"]))
            return df1
        def Add_RT_TRCRT_PRD(self,df):
            df1 = df.withColumn('RT_TRCRT_PRD', (df["trade_creditors"] / df["production_cost"]))
            return df1
        def Add_RT_NP_CAP(self,df):
            df1 = df.withColumn('RT_NP_CAP', (df["net_profit_or_loss"] / (df["total_current_assets"] - df["total_current_liability"])))
            return df1
        def Add_DF_GPNP(self,df):
            df1 = df.withColumn('DF_GPNP', (df["gross_profit"] - df["net_profit_or_loss"]))
            return df1
        def Add_RT_TAX_TOAST(self,df):
            df1 = df.withColumn('RT_TAX_TOAST', (df["total_tax"] / df["total_assets"]))
            return df1

        

