# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 15:58:51 2019

@author: hafsa.lutfi
"""

from Read_write_hive import Read_write_hive
# Db_connection class
class Db_connection():
    # returns connection object
    def connection(self):
        read_write = Read_write_hive()
        csv_read = read_write.read_csv
        read_hive = read_write.read_from_hive()
        #write_hive = read_write.write_to_hive()
        return read_hive, csv_read