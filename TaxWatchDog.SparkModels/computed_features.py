# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 15:07:08 2019


"""
#Packages Imported
from datetime import date
import pandas as pd
import numpy as np
from pyspark.sql.functions import udf
from pyspark.sql.functions import lit
from pyspark.sql.types import IntegerType


class Computed_features():
    # Binning features
    def Binning(self, df,exludeColList):
        # Binned cols for each feature created 
        # Data is divided into 4 bins
        for feature in df.columns:
            if feature not in exludeColList:
                df[str(feature)+ "_Bin"]=pd.qcut(df[feature].rank(method='first'), 4, labels=False,duplicates='drop')+1
        features2=df.apply(pd.to_numeric)
        return features2 

    
    def Create_TR_NBIN_YRM(self, x,y,i):
        if (y == date.today().year -i ):
            x=x
        else:
            x=None
        return x

    
    #model_1 trending columns
    def Create_YRM_Columns(self, df, computed):
        func_udf = udf(computed.Create_TR_NBIN_YRM, IntegerType())
        Cal_col_list ={"TR_NBIN_YRM":"other_non_sales_income","TR_NPNL_YRM":"net_profit_or_loss","TR_GP_YRM":"gross_profit","TR_GS_YRM":"gross_sales","TR_GPRATIOT_YRM":"RT_GP_GS"}
        year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
        for key in Cal_col_list:
            for i in range(0,2):
                df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
        for i in range(len(year_list)):
                df=df.withColumn("TR_OPPT_YRM"+str(i+1),df['TR_NPNL_YRM'+str(i+1)]-df['TR_NBIN_YRM'+str(i+1)])
                return df

# =============================================================================
#     #model_2 trending columns
#     def CreateYRMColumns1(self, df, computed):
#         func_udf = udf(computed.Create_TR_NBIN_YRM, IntegerType())
#         Cal_col_list ={"TR_LNFDR_YM":"loan_from_director","TR_GS_YRM":"gross_sales"}
#         year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
#         for key in Cal_col_list:
#             for i in range(len(year_list)):
#                 df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
#                 return df
#     #model_3 trending columns
#     def CreateYRMColumns2(self, df, computed):
#         func_udf = udf(computed.Create_TR_NBIN_YRM, IntegerType())
#         Cal_col_list ={"TR_GP_YRM":"gross_profit","TR_TOTEX_YM":"total_expenses","TR_GS_YRM":"gross_sales"}
#         year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
#         for key in Cal_col_list:
#             for i in range(len(year_list)):
#                 df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
#             for i in range(len(year_list)):
#                 df=df.withColumn("TR_GPEXP_YM"+str(i+1),df['TR_GP_YRM'+str(i+1)] / df['TR_TOTEX_YM'+str(i+1)])
#                 return df
#     
#     #model_4 trending columns
#     def CreateYRMColumns3(self, df, computed):
#         func_udf = udf(computed.Create_TR_NBIN_YRM, IntegerType())
#         Cal_col_list ={"TR_PNM_YM":"plant_and_machinery","TR_OPSTK_YM":"opening_stock","TR_NPNL_YM":"net_profit_or_loss","TR_GP_YM":"gross_profit","TR_GS_YM":"gross_sales","TR_COGS_YM":"cost_of_goods_sold","TR_CLSTK_YM":"closing_stock"}
#         year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
#         for key in Cal_col_list:
#             for i in range(len(year_list)):
#                 df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
#                 return df
#     
# =============================================================================
    # Normalize and Bin ratios
    def Normalize_And_Bin_Ratios(self,df_pandas,fields):
        # Bin columns for each ratio created
        # Each Ratio data is divided into 4 bins
        for ratio in fields:
             df_pandas[str(ratio)+ "_bin"]=pd.qcut(df_pandas[ratio].rank(method='first'), 4, labels=False,duplicates='drop')+1
        df_pandas=df_pandas.astype(np.float64)
        return df_pandas
    
    
    