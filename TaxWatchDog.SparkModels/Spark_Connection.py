# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 11:12:05 2019


"""
import findspark
import Config
class Spark_Connection:
    #Creating a spark session
    def Make_Spark_Connection(self):
        findspark.init(Config.Spark['Location'])
        from pyspark.sql import SparkSession
        from pyspark.sql import SQLContext
        spark = SparkSession.builder.getOrCreate()
        sc = spark.sparkContext
        sql_sc = SQLContext(sc)
        print("connection created successfully")
        return sc ,sql_sc,spark
