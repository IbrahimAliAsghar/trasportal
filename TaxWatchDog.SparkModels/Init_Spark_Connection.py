# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 14:42:01 2019


"""
# to initialize spark
class Init_Spark_Connection:
    #initialize a spark session
    def __init__(self, spark, sparkContext, sqlContext):
        self.spark = spark
        self.sparkContext = sparkContext
        self.sqlContext = sqlContext
    