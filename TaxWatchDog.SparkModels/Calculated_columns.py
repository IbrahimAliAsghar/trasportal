# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 12:31:10 2019

@author: hafsa.lutfi
"""

class Calculated_columns(object):
    #Model 2 calulated_column
    def Add_SM_TOT_INC(self,df):
        df1 = df.withColumn('SM_TOT_INC', df["gross_sales"] + df["other_business_income"])
        return df1
        
        df1 = df
        data_df = df1.toPandas()
        data_df = data_df.astype(float) 
        
    #EXP_COGS_B1v/(1-median(INC_GPORL_B1v/INC_GS_B1v))/INC_GS_B1v
    #COST_OF_GOODS_SOLD/(1-median(GROSS_PROFIT/GROSS_SALES))/GROSS_SALES
    def Add_RT_ESTGS_COS(self,data_df):
        data_df['gross_divide'] = data_df['gross_profit'] / data_df['gross_sales'] # divided two columns [gp/gs]
        data_df['gross_median'] = data_df['gross_divide'].median() # calcul the divide column median 
        data_df['gross_subtract'] = 1 - data_df['gross_median'] # subtr the calc median with 1
        data_df['RT_ESTGS_COS'] = data_df['cost_of_goods_sold'] / (data_df['gross_subtract'] / data_df['gross_sales'])#divide the columns
        return data_df
        
    #(EXP_COGS_B1v/(1-(median(INC_GPORL_B1v/INC_GS_B1v))))-INC_GS_B1v
    #(COST_OF_GOODS_SOLD/(1-(median(GROSS_PROFIT/GROSS_SALES)))) - GROSS_SALES
    def Add_DF_ESTGS_COS(self,data_df):
        data_df['divide_gross'] = data_df['gross_profit'] / data_df['gross_sales']
        data_df['median_gross'] = data_df['divide_gross'].median()
        data_df['sub_gross'] = data_df['median_gross']
        data_df['DF_ESTGS_COS'] = data_df['cost_of_goods_sold'] / (data_df['sub_gross'] - data_df['gross_sales'])
        return data_df
    
    #minimum(LIB_OTHCR_B1s,(1000-SM_TOT_INCs))
    #minimum(OTHER_CREDITORS,(1000-feature 79))
    #minimum(AST_TOTA_B1s,RT_GS_TOTASTs)
    def Add_HL_OTHCR_INC(self,data_df):
        data_df['sub_SM_TOT_INC'] = 1000 - data_df['SM_TOT_INC']
        data_df['HL_OTHCR_INC'] = data_df[['other_creditors','sub_SM_TOT_INC']].min(axis=1)
        return data_df
    
    def Add_HL_TOTA_GSTA(self,data_df):
        data_df['HL_TOTA_GSTA'] = data_df[["total_assets","RT_GS_TOTAST"]].min(axis=1)
        return data_df
    
    def Add_HL_TEX_NPRT(self,data_df):
        data_df['HL_TEX_NPRT'] = data_df[["total_expenses","RT_NP_GS"]].min(axis=1)
        return data_df
    
    #data_df1=data_df.drop(['gross_divide', 'gross_median','gross_subtract','sub_SM_TOT_INC','divide_gross','median_gross','sub_gross'], axis=1)
    #df1 = spark.createDataFrame(data_df1)
    
   #Model 3 calculated _columns
    def Add_AST_CASH_B1(self,df):
        df1 = df.withColumn('AST_CASH_B1', df["cash_in_hand"] + df["cash_in_bank"])
        return df1
    def Add_RT_TOTEXP_GS(self,df):
        df1 = df.withColumn('RT_TOTEXP_GS', df["total_expenses"] / df["gross_sales"])
        return df1
    def Add_PYT_TAX_B1(self,df):
        df1 = df.withColumn('PYT_TAX_B1', df["std_paid"] + df["std_from_spouse"])
        return df1
    #Model 4 calculated_columns
    #Above two cal_col of Model 3 are used in model 4 as well [Add_AST_CASH_B1 ,Add_RT_TOTEXP_GS]
#    def Add_RT_FA_TOTAST(self,df):
#        df1 = df.withColumn('RT_FA_TOTAST', df["total_fixed_assets"] / df["total_assets"])
#        return df1
#    def Add_RT_GS_TOTFA(self,df):
#        df1 = df.withColumn('RT_GS_TOTFA', df["gross_sales"] / df["total_fixed_assets"])
#        return df1
#    def Add_RT_ADVRT_GS(self,df):
#        df1 = df.withColumn('RT_ADVRT_GS', df["promo_advert_expense"] / df["gross_sales"])
#        return df1
#    def Add_RT_CSUBC_GS(self,df):
#        df1 = df.withColumn('RT_CSUBC_GS', df["contract_subcon_expense"] / df["gross_sales"])
#        return df1
#    def Add_RT_NAEXP_TEX(self,df):
#        df1 = df.withColumn('RT_NAEXP_TEX', df["non_allow_expense"] / df["total_expenses"])
#        return df1
#    def Add_RT_NP_INT(self,df):
#        df1 = df.withColumn('RT_NP_INT', df["net_profit_or_loss"] / df["loan_interest_expense"])
#        return df1
    def Add_RT_OEX_TOTEX(self,df):
        df1 = df.withColumn('RT_OEX_TOTEX', df["other_expenses"] / df["total_expenses"])
        return df1
#    def Add_RT_OTHEXP_GS(self,df):
#        df1 = df.withColumn('RT_OTHEXP_GS', df["other_expenses"] / df["gross_sales"])
#        return df1
#    def Add_RT_WAGES_GS(self,df):
#        df1 = df.withColumn('RT_WAGES_GS', df["salary_expense"] / df["gross_sales"])
#        return df1


cal_col = Calculated_columns()      