# -*- coding: utf-8 -*-
"""
Created on Sun Jan 13 20:47:18 2019


"""
#Packages Imported
import Config
import pandas as pd
import numpy as np
from pyspark.sql.types import IntegerType,FloatType
from pyspark.sql.functions import udf ,lit
import traceback
class Scorer():

    #ratio_feature_dict = {}
    
    # ratios proportionality 
    def ratios_proportionality(self,modelParams):
        #data = modelParams
        ratio_feature_dict ={}
        for ratios in modelParams["Ratios"]:
            ratio_feature_dict[ratios["RatioName"]] = [str(ratios["Propotional"])]
            for features in ratios["Features"]:
                ratio_feature_dict[ratios["RatioName"]].append(features["FeatureName"])
#        ratio_feature_dict ={'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_EXP_GS':['1','total_expenses','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets'],'RT_PRD_GS':['1','production_cost','gross_sales'],'RT_TRCRT_PUR':['1','trade_creditors','purchases'],'RT_TRDR_GS':['1','trade_debtors','gross_sales'],'RT_PUR_GS':['1','purchases','gross_sales'],'RT_GS_TOTAST':['0','gross_sales','total_assets'],'RT_GP_EXP':['0','gross_profit','total_expenses'],'INC_OPPT_B1':['1','net_profit_or_loss','other_non_sales_income'],'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRCRT_COS':['1','trade_creditors','cost_of_goods_sold']}
        return ratio_feature_dict
        #for ratios in modelParams["Ratios"]:
         #   self.ratio_feature_dict[ratios["RatioName"]] = [str(ratios["Propotional"])]
        #for features in ratios["Features"]:
         #   self.ratio_feature_dict[ratios["RatioName"]].append(features["FeatureName"])
            #0 inverse 
            #1 direct
    #ratio_feature_dict ={'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_EXP_GS':['1','total_expenses','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets'],'RT_PRD_GS':['1','production_cost','gross_sales'],'RT_TRCRT_PUR':['1','trade_creditors','purchases'],'RT_TRDR_GS':['1','trade_debtors','gross_sales'],'RT_PUR_GS':['1','purchases','gross_sales'],'RT_GS_TOTAST':['0','gross_sales','total_assets'],'RT_GP_EXP':['1','gross_profit','total_expenses'],'INC_OPPT_B1':['1','net_profit_or_loss','other_non_sales_income'],'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRCRT_COS':['1','trade_creditors','cost_of_goods_sold']}
    #ratio_feature_dict = {'RT_GP_GS':['1','gross_profit','gross_sales'],'RT_EXP_GS':['0','total_expenses','gross_sales'],'RT_NP_GS':['1','net_profit_or_loss','gross_sales'],'RT_GS_TOTAST':['1','gross_sales','total_assets'],'RT_STKTURN':['0','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRDR_GS':['0','trade_debtors','gross_sales'],'RT_PUR_GS':['0',"purchases","gross_sales"],'RT_AV_STK':['0','opening_stock','closing_stock'],'RT_COGS_GS':['0','cost_of_goods_sold','gross_sales'],'RT_LNFRDR_GS':['1','loan_from_director','gross_sales']}
    #ratio_feature_dict ={'RT_GP_EXP':['1','gross_profit','total_expenses'],'RT_EXP_GS':['0','total_expenses','gross_sales'],'RT_GP_GS':['1','gross_profit','gross_sales'],'RT_GS_TOTAST':['1','gross_sales','total_assets'],'RT_NP_GS':['1','net_profit_or_loss','gross_sales'],'RT_NP_TOTAST':['1','net_profit_or_loss','total_assets'],'RT_STKTURN':['0','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRCRT_PUR':['0','trade_creditors','purchases'],'RT_TRDR_GS':['0','trade_debtors','gross_sales'],'RT_GP_TOTAST':['0','gross_profit','total_assets']}
    #ratio_feature_dict ={'RT_AV_STK':['0','opening_stock','closing_stock'],'RT_COGS_GS':['0','cost_of_goods_sold','gross_sales'] ,'RT_GP_GS':['1','gross_profit','gross_sales'],'RT_NP_GS':['1','net_profit_or_loss','gross_sales'],'RT_PRD_GS':['0','production_cost','gross_sales'] ,'RT_STKTURN':['0','cost_of_goods_sold','opening_stock','closing_stock'],'RT_NP_TOTAST':['1','net_profit_or_loss','total_assets']}
    
    
    #ratio_feature_dict ={'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_EXP_GS':['1','total_expenses','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets'],'RT_PRD_GS':['1','production_cost','gross_sales'],'RT_TRCRT_PUR':['1','trade_creditors','purchases'],'RT_TRDR_GS':['1','trade_debtors','gross_sales'],'RT_PUR_GS':['1','purchases','gross_sales'],'RT_GS_TOTAST':['0','gross_sales','total_assets'],'RT_GP_EXP':['0','gross_profit','total_expenses'],'INC_OPPT_B1':['1','net_profit_or_loss','other_non_sales_income'],'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRCRT_COS':['1','trade_creditors','cost_of_goods_sold']}
    #ratio_feature_dict = {'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_EXP_GS':['1','total_expenses','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_GS_TOTAST':['0','gross_sales','total_assets'],'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRDR_GS':['1','trade_debtors','gross_sales'],'RT_PUR_GS':['1',"purchases","gross_sales"],'RT_AV_STK':['1','opening_stock','closing_stock'],'RT_COGS_GS':['1','cost_of_goods_sold','gross_sales'],'RT_LNFRDR_GS':['0','loan_from_director','gross_sales']}
    #ratio_feature_dict ={'RT_GP_EXP':['0','gross_profit','total_expenses'],'RT_EXP_GS':['1','total_expenses','gross_sales'],'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_GS_TOTAST':['0','gross_sales','total_assets'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets'],'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_TRCRT_PUR':['1','trade_creditors','purchases'],'RT_TRDR_GS':['1','trade_debtors','gross_sales'],'RT_GP_TOTAST':['1','gross_profit','total_assets']}
    #ratio_feature_dict ={'RT_AV_STK':['1','opening_stock','closing_stock'],'RT_COGS_GS':['1','cost_of_goods_sold','gross_sales'] ,'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_PRD_GS':['1','production_cost','gross_sales'] ,'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets']}
    
    # direct and indirect features saved in list
    def direct_and_inverse_features(self,ratio_feature_dict):
#        inverse_features_list = []
#        inverse_features = []
#        mergelist = []
#        all_features = []
#        direct_features_list = []
#        direct_features = []
#        for key in ratio_feature_dict:
#            for i in range(1,len(ratio_feature_dict[key])):
#                if len(ratio_feature_dict[key]) >0:
#                    all_features.append(ratio_feature_dict[key][i])
#                    if ratio_feature_dict[key][0] == '0':
#                        direct_features.append(ratio_feature_dict[key][i])
#                    else:
#                        inverse_features.append(ratio_feature_dict[key][i])
#    
#        #moving direct/indirect features into list
#        direct_features_list = direct_features
#        inverse_features_list = inverse_features
#        
#        #getting list of those features that are direct and inverse
#        direct_features = list(set(direct_features))
#        inverse_features = list(set(inverse_features))
#        
#        #getting combined list of direct and indirect features
#        direct_features1 = direct_features_list
#        direct_features1.extend(inverse_features)
#        mergelist = list(set(direct_features1))
        all_features=[]
        direct_features=[]
        inverse_features=[]
        for key in ratio_feature_dict:
            for i in range(1,len(ratio_feature_dict[key])):
                if len(ratio_feature_dict[key]) >0:
                    all_features.append(ratio_feature_dict[key][i])
                    if ratio_feature_dict[key][0] == '0':
                        direct_features.append(ratio_feature_dict[key][i])
                    else:
                        inverse_features.append(ratio_feature_dict[key][i])
                        
        direct_features_list = direct_features.copy()
        inverse_features_list = inverse_features.copy()
        
        direct_features =list(set(direct_features))  
        inverse_features =list(set(inverse_features)) 
        print(direct_features,inverse_features)
        
        
        # In[23]:
        
        
        
        direct_features1 = direct_features.copy()
        direct_features1.extend(inverse_features)
        mergelist = list(set(direct_features1))
        return direct_features,inverse_features,mergelist,all_features,direct_features_list,inverse_features_list
    
    #features that aren't present in direct and inverse feature
    def get_explicit_features(self, feature_list,mergelist): 
        same_list = []
        for element in feature_list:
            if element not in mergelist:
                same_list.append(element)
        return same_list
    #getting direct feature count
    def get_direct_feature_frequency(self,direct_features,direct_features_list):
         DirectFeatureFrequency = {}
         for feature in direct_features:
             DirectFeatureFrequency[feature] = direct_features_list.count(feature)
         return DirectFeatureFrequency
    #getting inverse feature count
    def get_inverse_feature_frequency(self,inverse_features,inverse_features_list):
        InverseFeatureFrequency = {}
        for feature in inverse_features:
            InverseFeatureFrequency[feature] = inverse_features_list.count(feature)
        return InverseFeatureFrequency
    #getting combined feature count of direct and inverse feature
    def get_all_feature_frequency(self,all_features):
        AllFeaturesFrequency = {}
        for feature in all_features:
            AllFeaturesFrequency[feature] = all_features.count(feature)
        return AllFeaturesFrequency
            
    #call all the methods
    def feature_logic_functions(self,modelParams,feature_list):
        self.ratios_proportionality(modelParams)
        self.direct_and_inverse_features()
        self.get_explicit_features(feature_list)
        self.get_direct_feature_frequency()
        self.get_inverse_feature_frequency()
        self.get_all_feature_frequency()

    # assign weightage to each taxpayer's feature        
    def Assign_Weigtages_Feature(x,y,z):
        if x== np.nan:
            k =None
            return k
        else:  
            if z==1.0:
                if x ==1.0:
                    k= y* 10.0
                    return k
                elif x==2.0:
                    k= y* 25.0
                    return k
                elif x==3:
                    k= y* 50.0
                    return k
                elif x==4:
                    k= y* 100.0
                    return k
                elif x ==5.0:
                    k= y* 150.0
                    return k
            else:
                if x ==1.0:
                    k= y* 100.0
                    return k
                elif x==2:
                    k= y* 50.0
                    return k
                elif x==3:
                    k= y* 25.0
                    return k
                elif x==4:
                    k= y* 10.0
                    return k
                elif x ==5.0:
                    k= y* 150.0
                    return k
     
    def __init__(self):
        self.func_udf2 = udf(self.Assign_Weigtages_Feature, FloatType())
        #return self.func_udf2
           
    # direct feature scoring            
    def direct_feature_scoring(self, direct_features, fields,ratio_feature_dict, DirectFeatureFrequency,df):
        for feature in direct_features:
            count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "0" in ratio_feature_dict[ratio]:
                    if DirectFeatureFrequency[feature]==1:
                        df=df.withColumn(feature + "_score"+str(count),self.func_udf2(df[ratio+ "_Bin"],df[feature],lit(0)))
                    else:
                        df=df.withColumn(feature + "_score"+str(count),self.func_udf2(df[ratio+ "_Bin"],df[feature],lit(0)))
                        count=count+1
        return df
    
    # inverse feature scoring       
    def inverse_feature_Scoring(self,inverse_features,direct_features,DirectFeatureFrequency,InverseFeatureFrequency,fields,ratio_feature_dict,df):
        for feature in inverse_features:
            if feature in direct_features:
                count = DirectFeatureFrequency[feature]
            else:
                count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "1" in ratio_feature_dict[ratio]:
                        if InverseFeatureFrequency[feature]==1:
                            df=df.withColumn(feature + "_score"+str(count),self.func_udf2(df[ratio+ "_Bin"],df[feature],lit(1)))
                        else:
                            df=df.withColumn(feature + "_score"+str(count),self.func_udf2(df[ratio+ "_Bin"],df[feature],lit(1)))
                            count=count+1
        return df
    
    # Get final score of features having frequency greater than 1
    def get_final_sum_scored_features(self,mergelist,AllFeaturesFrequency,df):
        for i in mergelist:
            df = df.withColumn("sum_"+i,sum(df[i+"_score"+str(j)].cast("float") for j in range(AllFeaturesFrequency[i]))) 
        for i in mergelist:
            df = df.withColumn("Final"+i+"_score",df["sum_"+i]/AllFeaturesFrequency[i])
        return df
    
    
    def get_final_score_features(self,Group_dict,same_list,modelParams,df):
        #Group_dict = {"ASSETS":"1","EXPENSE":"1","INCOME":"0","ASSET_RT":"1","TAX":"0","LIABILITY":"1","EXT_INCOME":"1","STOCK":"1","INTERCOMPANY":"1","PNL":"1","INFORMATIONAL":"1","KEY_DERIVATV":"0","DEFAULT":"1"}
        for feature in same_list:
            a=0
            for item in modelParams["Features"]:
                if item["FeatureName"] == feature.upper():
                    feature_group=item["FeatureGroup"]
                    a=int(Group_dict[feature_group])
                    break
            df=df.withColumn("Final"+feature + "_score",self.func_udf2(df[feature+ "_Bin"],df[feature],lit(a)))
        return df
    
    #saving final_selected float columns in final_score and replacing null values with 0
    def manipulate_scores(self, df,fields,featurelist):
        Final_score_columns=[]
        for names in df.columns:
            if "Final" in names or names in fields or names in featurelist or "TR" in names:    
                Final_score_columns.append(names) 
     
        #saving final_selected float columns in final_score and replacing null values with 0
        temp_score = df.select(Final_score_columns)
        final = temp_score.toPandas()
        return final
    #final_score conversion to pandas df
    def convert_spark_to_pandas(self,temp_score):    
        final = temp_score.toPandas()
        return final
    
    # normalize score and ranked them
    def normalize_scores_rank(self,final):
        finalcolnames = []
        for name in final.columns:
            if "Final" in name:
                finalcolnames.append(name)
        for col in finalcolnames:
            final[str(col) + '_Ranked'] = final[col].rank(ascending= False ,method = 'dense')
    
        for col in finalcolnames:
            final[col+"norm_score"]=((final[col]-final[col].min())/(final[col].max()-final[col].min()))*100
        return final
    
    # Calculate summary_statistics including mean,median,mode,std_dev,min,max
    def calculate_summary_statistics(self,final,feature_list,exludeColList,fields):
        final_df_to_write = final
        mean_dict ={}
        median_dict = {}
        std_dict = {}
        mode_dict = {}
        max_dict ={}
        min_dict={}
        for i in feature_list:
            if i not in exludeColList and "assessment_year" not in i and "branch_code" not in i :
                 mean_dict[i]=final_df_to_write[i].mean()
                 median_dict[i]=final_df_to_write[i].median()
                 std_dict[i]=final_df_to_write[i].std()
                 mode_dict[i]=final_df_to_write[i].mode()
                 max_dict[i]=final_df_to_write[i].max()
                 min_dict[i]=final_df_to_write[i].min()
        for i in fields:
            if i not in exludeColList and "assessment_year" not in i and "branch_code" not in i :
                 mean_dict[i]=final_df_to_write[i].mean()
                 median_dict[i]=final_df_to_write[i].median()
                 std_dict[i]=final_df_to_write[i].std()
                 mode_dict[i]=final_df_to_write[i].mode()
                 max_dict[i]=final_df_to_write[i].max()
                 min_dict[i]=final_df_to_write[i].min()
        
        return mean_dict,median_dict,std_dict,mode_dict,max_dict,min_dict
    
    # transforming data according to hive table schema inorder to insert into Hive
    def transform_df_to_write(self,final,feature_list,exludeColList,fields,modelParams,it_ref_df,originalDfPandas_1,originalDfPandas_2,mean_dict,median_dict,std_dict,mode_dict,max_dict,min_dict,external_counter):
        print("In transform Function")   
        final_df_to_write = final
        #final_df_to_write1 = spark.createDataFrame(final)
        counter = 0 
        #tt = pd.DataFrame(columns=['feature','profile_id','model_id','value','score','rank','year_d','taxpayer'])
        #sqlInsertQuery = "INSERT INTO model_result_abt VALUES"
        final_df_to_write = final_df_to_write.astype(object).replace(np.nan, 'Null')
        modelParams = modelParams
        dictList=[]
        print(len(final_df_to_write))
        print("In final_df_to_write.iterrows()")
        for finalRow in final_df_to_write.iterrows():
            try:
                if counter < len(final_df_to_write):
                    finalRow = finalRow[1]
                    
                    # insertion of feature values
                    for feature in feature_list:
                         if (feature != "it_ref_no_taxassm_dim"):
                             #print(type(originalDfPandas_1['it_ref_no_taxassm_dim']))
                             #print(type(final_df_to_write['it_ref_no_taxassm_dim']))
                             
                             # Setting year -1 values
                             if (len(originalDfPandas_1[originalDfPandas_1['it_ref_no_taxassm_dim'] == finalRow['it_ref_no_taxassm_dim']])>0):
                                 val_year_1= str(originalDfPandas_1.loc[originalDfPandas_1['it_ref_no_taxassm_dim'] == finalRow['it_ref_no_taxassm_dim'], feature+"_right"].values[0])
                             else:
                                 val_year_1="Null"
                                 
                             # Setting year -2 values
                             if (len(originalDfPandas_2[originalDfPandas_2['it_ref_no_taxassm_dim'] == finalRow['it_ref_no_taxassm_dim']])>0):
                                 val_year_2= str(originalDfPandas_2.loc[originalDfPandas_2['it_ref_no_taxassm_dim'] == finalRow['it_ref_no_taxassm_dim'], feature+"_right"].values[0])
                             else:
                                 val_year_2="Null"
                                 
                             # Setting values for each feature for every taxpayer    
                             if(str(finalRow['Final' +feature + '_score_Ranked'])== 'Null'):
                                 if feature not in exludeColList and "assessment_year" not in feature and "branch_code" not in feature and "it_ref_no_taxassm_dim" not in feature :
                                     dict1={}
                                     #dict1 ={'feature':feature,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[feature]),'score':str(finalRow['Final' + feature + '_scorenorm_score']),'rank':str(finalRow['Final' +feature + '_score_Ranked']),'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(it_ref_df['it_ref_no_taxassm_dim'][counter]),'mean':str(mean_dict[feature]),'median':str(median_dict[feature]),'mode':str(mode_dict[feature].tolist()),'standard_deviation':str(std_dict[feature]),'max_value':str(max_dict[feature]),'min_value':str(min_dict[feature]),'val_year_1':val_year_1,'val_year_2':val_year_2}
                                     dict1 ={'feature':feature,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[feature]),'score':str(finalRow['Final' + feature + '_scorenorm_score']),'rank':str(finalRow['Final' +feature + '_score_Ranked']),'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(it_ref_df['it_ref_no_taxassm_dim'][counter+external_counter]),'mean':str(mean_dict[feature]),'median':str(median_dict[feature]),'mode':"Null",'standard_deviation':str(std_dict[feature]),'max_value':str(max_dict[feature]),'min_value':str(min_dict[feature]),'val_year_1':val_year_1,'val_year_2':val_year_2}
                                     dictList.append(dict1)
                                     #sqlInsertQuery += "('" + feature + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[feature]) + "," + str(finalRow['Final' + feature + '_scorenorm_score']) + ", " +  str(finalRow['Final' +feature + '_score_Ranked']) + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) +"," + str(finalRow['it_ref_no_taxpay_dim']) + "),"
                             else:                         
                                 if feature not in exludeColList and "assessment_year" not in feature and "branch_code" not in feature and "it_ref_no_taxassm_dim" not in feature :
                                     dict1={}
                                     #dict1 ={'feature':feature,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[feature]),'score':str(finalRow['Final' + feature + '_scorenorm_score']),'rank':str(int(finalRow['Final' +feature + '_score_Ranked'])),'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(finalRow['it_ref_no_taxassm_dim']),'mean':str(mean_dict[feature]),'median':str(median_dict[feature]),'mode':str(mode_dict[feature].tolist()),'standard_deviation':str(std_dict[feature]),'max_value':str(max_dict[feature]),'min_value':str(min_dict[feature]),'val_year_1':val_year_1,'val_year_2':val_year_2}
                                     dict1 ={'feature':feature,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[feature]),'score':str(finalRow['Final' + feature + '_scorenorm_score']),'rank':str(int(finalRow['Final' +feature + '_score_Ranked'])),'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(it_ref_df['it_ref_no_taxassm_dim'][counter+external_counter]),'mean':str(mean_dict[feature]),'median':str(median_dict[feature]),'mode':"Null",'standard_deviation':str(std_dict[feature]),'max_value':str(max_dict[feature]),'min_value':str(min_dict[feature]),'val_year_1':val_year_1,'val_year_2':val_year_2}
                                     dictList.append(dict1)
                                     #sqlInsertQuery += "('" + feature + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[feature]) + "," + str(finalRow['Final' + feature + '_scorenorm_score']) + ", " +  str(finalRow['Final' +feature + '_score_Ranked']) + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) +"," + str(finalRow['it_ref_no_taxpay_dim']) + "),"
                    
                    # Insertion of ratio values         
                    for ratio in fields:
                         # Setting values for each ratio for every taxpayer
                         if ratio not in exludeColList and "assessment_year" not in ratio and "branch_code" not in ratio and "it_ref_no_taxassm_dim" not in ratio :
                             dict2={}
                             #dict2={'feature':ratio,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[ratio]),'score':"Null",'rank':"Null",'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(it_ref_df['it_ref_no_taxassm_dim'][counter]),'mean':str(mean_dict[ratio]),'median':str(median_dict[ratio]),'mode':str(mode_dict[ratio].tolist()),'standard_deviation':str(std_dict[ratio]),'max_value':str(max_dict[ratio]),'min_value':str(min_dict[ratio]),'val_year_1':"Null",'val_year_2':"Null"}
                             dict2={'feature':ratio,'profile_id':str(modelParams['ProfileID']),'model_id':str(modelParams['ModelID']),'value':str(finalRow[ratio]),'score':"Null",'rank':"Null",'year_d':str(int(finalRow['assessment_year'])),'it_ref_no':str(it_ref_df['it_ref_no_taxassm_dim'][counter+external_counter]),'mean':str(mean_dict[ratio]),'median':str(median_dict[ratio]),'mode':"Null",'standard_deviation':str(std_dict[ratio]),'max_value':str(max_dict[ratio]),'min_value':str(min_dict[ratio]),'val_year_1':"Null",'val_year_2':"Null"}
                             dictList.append(dict2)
                             #sqlInsertQuery += "('" + ratio + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[ratio]) + "," + "Null" + ", " +  "Null" + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) + "," + str(finalRow['it_ref_no_taxpay_dim']) +"),"
                counter = counter + 1
                print(counter)
            except Exception as Exp:
               #logger.error(str(Exp), exc_info=True)
               #print("updating status")
               print(str(Exp))
               print(Exp)
               traceback.print_exc()
        return dictList
    
    # Conversion into spark df
    def df_conversion(self,dictList,spark_session):    
            finalResult = pd.DataFrame(dictList)
            sparkResult = spark_session.createDataFrame(finalResult)
            return sparkResult
      
    
    