# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 12:22:32 2019


"""
import Config
from pyspark import SparkContext,SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
import json
#modelParams = Config.ModelParam
class Read_write_hive():
    # Read Data from Hive table
    def read_from_hive(self,modelParams,spark):
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        df = spark.sql(modelParams["ODSQuery"])
        return df

# =============================================================================
    # Write Data into Hive table
    def write_to_hive(self,spark1,sparkResult):        
         SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
         spark1.sql('use ' + Config.Hive["Database"])
         sparkResult.write.mode("append").saveAsTable("mts_modelresult",format="hive")
         return sparkResult
             
# =============================================================================
    # Read Data from csv (Just for testing purpose)
    def read_csv(self,sql_sc):     
        df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load(Config.SampleDataFile['Location'])
        return df

# =============================================================================
    # Write Data into csv (Just for testing purpose)    
    def write_csv(self,sparkResult):     
        #sparkResult.write.csv(Config.SampleDataFile["ResultLocation"])
        sparkResult.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save(Config.SampleDataFile["ResultLocation"])
        #sparkResult.toPandas().to_csv(Config.SampleDataFile['Location'], header=True)
        return sparkResult
    
    # Integrating Wht results with MTS model results
    def wht_integration(self,whtJson,spark):
        print(whtJson)
        year = whtJson['AssesmentYear']
        #It_ref_nos = whtJson['It_ref_Nos'] 
        it_ref_no_str=""
        counter =0
        # Combining all it_ref_nos in string comma seprated
        for it_ref_no in whtJson["It_Ref_nos"]:
            if(counter != len(whtJson["It_Ref_nos"])-1):
                it_ref_no_str += "'"+it_ref_no+"',"
            else:
                it_ref_no_str += "'"+it_ref_no+"'"
            counter=counter+1           
        # Fetching data from Wht result table
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        # query = "select wht_final_scoringrisk_factor,wht_final_scoringassessment_year,wht_final_scoringit_ref_no from wht_final_scoring where wht_final_scoringassessment_year ="+str(year)+"  and wht_final_scoringit_ref_no in ("+it_ref_no_str+")"
        query = "select * from wht_final_scoring where wht_final_scoringassessment_year ="+str(year)+"  and wht_final_scoringit_ref_no in ("+it_ref_no_str+")"
        df = spark.sql(query)
        return df
    
    #get results of means test
    #TODO: implement
    def GetMeansTestResults(self,params,spark):
        params = json.loads(params)
        itref = params['It_RefNo']
                
        # Fetching data from Wht result table
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use edw')## Database ?
        # query = "select it_ref_no, taxpayer_per_year_0, tax_unrep_income_y0 from edw.MT_ASSETS_INCOME_FACT_CALCULATION where it_ref_no = '"+str(itref)+"'"
        #query = "select * from edw.MT_ASSETS_INCOME_FACT_CALCULATION where it_ref_no = '"+str(itref)+"'"
        query = "select * from edw.mt_consolidate where it_ref_no = '"+str(itref)+"'"
        #for testing on a single it ref
        #query = "select it_ref_no, taxpayer_per_year_0, tax_unrep_income_y0 from edw.MT_ASSETS_INCOME_FACT_CALCULATION where it_ref_no = 555299001"
        print("sql query for means test:   " + query)
        df = spark.sql(query)
        print(df.head())
        return df
    
    #get results of means test
    #TODO: implement
    def GetDigitalEconomyResults(self, params, spark):
        params = json.loads(params)
        itref = params['It_RefNo']
                
        # Fetching data from Wht result table
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use product_zone')## Database ?
        # query = "select similarity_score  from product_zone.external_kapow_similarityscore_all_reg_taxpayer where TRIM(it_ref_no) = " + str(itref)
        query = "select * from product_zone.external_kapow_similarityscore_all_reg_taxpayer where TRIM(it_ref_no) = " + str(itref)
        #for testing on a single it ref
        #query = "select it_ref_no, similarity_score from product_zone.external_kapow_similarityscore_all_reg_taxpayer where TRIM(it_ref_no) = 4471843080" 
        print("sql query for means test:   " + query)
        df = spark.sql(query)
        print("digital economy df is ")
        print(df.head())
        return df
    
    # Getting dropdown data to show in MTS portal dropdown
    def dropdownData(self,spark):
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        queryBusinessCode = "select distinct business_code FROM financialfact_ods where file_type in('C','OG','SG','D')"
        queryBranchCode ="select distinct branch_code,branch_name FROM financialfact_ods where file_type in('C','OG','SG','D')"
        queryStateCode="select distinct state_cd,state FROM financialfact_ods where file_type in('C','OG','SG','D')"
        queryAssessmentYear="select distinct assessment_year FROM financialfact_ods where file_type in('C','OG','SG','D') and assessment_year <= year(CURRENT_DATE)"
        df_assessmentYear = spark.sql(queryAssessmentYear)
        df_branchCode = spark.sql(queryBranchCode)
        df_businessCode = spark.sql(queryBusinessCode)
        df_stateCode=spark.sql(queryStateCode)
        return df_assessmentYear,df_branchCode,df_businessCode,df_stateCode
    
    # Getting report card data for individual itrefno
    def GetReportCardData(self,rcquery,spark):
        profile_id = rcquery['Profile_Id']
        It_ref_no = rcquery['It_ref_Nos']       
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        query = "SELECT mts_modelresult.feature,mts_modelresult.profile_id,mts_modelresult.model_id,mts_modelresult.value,mts_modelresult.score,mts_modelresult.rank,mts_modelresult.year_d,mts_modelresult.it_ref_no,mts_modelresult.mean,mts_modelresult.median,mts_modelresult.standard_deviation,mts_modelresult.max_value,mts_modelresult.min_value,mts_modelresult.val_year_1,mts_modelresult.val_year_2 FROM mts_modelresult where profile_id = '"+str(profile_id)+"' and it_ref_no = '"+str(It_ref_no)+"'"
        
        #query = "SELECT * FROM mts_modelresult where profile_id = '"+str(profile_id)+"' and it_ref_no = '"+str(It_ref_no)+"'"
        df = spark.sql(query)
        return df
    
    # Getting result from Hive through pagination    
    def GetResultsFromHive(self,rcquery,spark):
        profile_id = rcquery['ProfileId']
        limit = rcquery['Limit']
        offset = rcquery['Offset']               
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        query = "select * from (select ROW_NUMBER() over() as rownumber,* from (select * from mts_modelresult where profile_id = '"+str(profile_id)+"' Order by cast(it_ref_no as int)) as t) as k where k.rownumber >= "+str(offset)+" and k.rownumber <= "+str(limit)+""
        #print(query)
        query1= "select * from (SELECT ROW_NUMBER() OVER (PARTITION BY profile_id ORDER BY cast(it_ref_no as int)) AS rn ,* FROM mts_modelresult where profile_id = '"+str(profile_id)+"') as k where k.rn >=  "+str(offset)+" and k.rn <= "+str(limit)+""
        #print("*******************************")
        #print(query1)
        query2= "select mts_modelresult.feature,mts_modelresult.profile_id,mts_modelresult.model_id,mts_modelresult.value,mts_modelresult.score,mts_modelresult.rank,mts_modelresult.year_d,mts_modelresult.it_ref_no,mts_modelresult.mean,mts_modelresult.median,mts_modelresult.standard_deviation,mts_modelresult.max_value,mts_modelresult.min_value,mts_modelresult.val_year_1,mts_modelresult.val_year_2 from (SELECT ROW_NUMBER() OVER (PARTITION BY profile_id ORDER BY cast(it_ref_no as int)) AS rn ,* FROM mts_modelresult where profile_id = '"+str(profile_id)+"') as mts_modelresult where mts_modelresult.rn >=  "+str(offset)+" and mts_modelresult.rn <= "+str(limit)+""
        df = spark.sql(query2)
        return df

    # Getting Feature count of the MTS models
    def GetFeatureCount(self,rcquery,spark):
        profile_id = rcquery['profile_id']
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])
        query ="select feature_count,count(*) as rec_count from (select it_ref_no, count(*) as feature_count from mts_modelresult where profile_id = '"+str(profile_id)+"' group by it_ref_no)as a group by a.feature_count"
        df = spark.sql(query)
        df = df.toPandas()
        return df
    
    # Gettig trends from hive for past 2 years
    def read_from_hive_trend_years(self,complete_query,spark):
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        df = spark.sql(complete_query)
        return df
    
    # Integrate TP results
    def tp_integration(self,TPJson,spark):
        import pandas as pd
        year = TPJson['AssesmentYear']
        # Combining all it_ref_nos in string comma seprated
        it_ref_no_str=""
        counter =0
        for it_ref_no in TPJson["It_Ref_nos"]:
            if(counter != len(TPJson["It_Ref_nos"])-1):
                it_ref_no_str += "'"+it_ref_no+"',"
            else:
                it_ref_no_str += "'"+it_ref_no+"'"
            counter=counter+1 
        # latesubm_score,nonsubm_score,repnc_score for SG , OG and all other filetypes
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["DatabaseTP"])## Database ?
        if(TPJson['file_type'] == 'SG'):
            print("sg")
            # query1 = "select score as latesubm_score ,year_asst,it_ref_no from scoring_latesubm_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query1 = "select * from scoring_latesubm_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df1 = spark.sql(query1)
            #query2 = "select score as nonsubm_score,year_asst,it_ref_no from scoring_nonsubm_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query2 = "select * from scoring_nonsubm_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df2 = spark.sql(query2)
            #query3 = "select score as repnc_score,year_asst,it_ref_no from scoring_repnc_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query3 = "select * from scoring_repnc_sg where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df3 = spark.sql(query3)
            
        elif(TPJson['file_type'] == 'OG'):
            print("og")
            #query1 = "select score as latesubm_score ,year_asst,it_ref_no from scoring_latesubm_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query1 = "select * from scoring_latesubm_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df1 = spark.sql(query1)
            #query2 = "select score as nonsubm_score ,year_asst,it_ref_no from scoring_nonsubm_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query2 = "select* from scoring_nonsubm_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df2 = spark.sql(query2)
            #query3 = "select score as repnc_score ,year_asst,it_ref_no from scoring_repnc_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            query3 = "select * from scoring_repnc_og where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+")"
            df3 = spark.sql(query3)
            
        else:
            print("other than og and sg")
            #query1 = "select score as latesubm_score ,year_asst,it_ref_no from scoring_latesubm_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            query1 = "select * from scoring_latesubm_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            df1 = spark.sql(query1)
            #query2 = "select score as nonsubm_score,year_asst,it_ref_no from scoring_nonsubm_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            query2 = "select * from scoring_nonsubm_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            df2 = spark.sql(query2)
            #query3 = "select score as repnc_score,year_asst,it_ref_no from scoring_repnc_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            query3 = "select * from scoring_repnc_c where year_asst ="+str(year)+"  and it_ref_no in ("+it_ref_no_str+") and file_type = '"+str(TPJson['file_type'])+"'"
            df3 = spark.sql(query3)
        #query = "select wht_final_scoringrisk_factor,wht_final_scoringassessment_year,wht_final_scoringit_ref_no from wht_final_scoring where wht_final_scoringassessment_year ="+str(year)+"  and wht_final_scoringit_ref_no in ("+it_ref_no_str+")"
        #df = spark.sql(query)
        df1= df1.toPandas()
        df2= df2.toPandas()
        df3= df3.toPandas()
        
        # Removing duplicates
        df1.drop_duplicates(keep='first', inplace=True)
        df1=df1.groupby('it_ref_no').filter(lambda x: len(x) == 1)
        df2.drop_duplicates(keep='first', inplace=True)
        df2=df2.groupby('it_ref_no').filter(lambda x: len(x) == 1)
        df3.drop_duplicates(keep='first', inplace=True)
        df3=df3.groupby('it_ref_no').filter(lambda x: len(x) == 1)
        #df_inner = pd.merge(df1, df2, on='id', how='inner')
        #pd.merge(pd.merge(df1,df2,on='name'),df3,on='name')
        
        #renaming score columns
        df1.rename(columns={'score':'latesubm_score'}, inplace=True)
        df2.rename(columns={'score':'nonsubm_score'}, inplace=True)
        df3.rename(columns={'score':'repnc_score'}, inplace=True)
        
        # Merging all 3 dataframes 
        df=df1.merge(df2,on=['it_ref_no','year_asst'], how='outer').merge(df3,on=['it_ref_no','year_asst'], how='outer')
        df.drop_duplicates(keep='first', inplace=True)
        for col in ['latesubm_score', 'nonsubm_score', 'repnc_score']:
            df[col] = df[col].astype('str')
        return df
    # Columns to add for CMS functionality. Name and main_business_code cols are added
    def ColsToAdd_CMS(self,ColsToAddJson,spark):
        import pandas as pd
        year = ColsToAddJson['assesmentYear']
        query = ColsToAddJson['query']
        tablename = ColsToAddJson['tableName']
        # Combining all it_ref_nos in string comma seprated
        it_ref_no_str=""
        counter =0
        for it_ref_no in ColsToAddJson["itRefNo"]:
            if(counter != len(ColsToAddJson["itRefNo"])-1):
                it_ref_no_str += "'"+it_ref_no+"',"
            else:
                it_ref_no_str += "'"+it_ref_no+"'"
            counter=counter+1 
        
        # preparing query to fetch name and main_business_code
        colsToAdd = ['name','main_business_code']
        for i in colsToAdd:
            if (i not in query):     
                k,g=query.split('FROM')
                query= k +","+ tablename + "."+ i + " FROM " + g + " AND "+tablename + "." + "it_ref_no_taxassm_dim in ("+it_ref_no_str+")"
            
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])## Database ?
        df = spark.sql(query)
        df = df.filter(df['assessment_year'].isin([int(year)]))
        
        # remove duplicate function
        def removed_duplicates(df_data):
            originalDfPandas = df_data.toPandas()
            originalDfPandas = originalDfPandas.loc[:,~originalDfPandas.columns.duplicated()]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                print("if groupby")
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['seq_no'].transform(max) == originalDfPandas['seq_no']
                originalDfPandas = originalDfPandas[idx]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['assessment_key'].transform(max) == originalDfPandas['assessment_key']
                originalDfPandas = originalDfPandas[idx]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['snapshot_date'].transform(max) == originalDfPandas['snapshot_date']
                originalDfPandas = originalDfPandas[idx]
            originalDfPandas.reset_index(inplace=True,drop=True)
            print(len(originalDfPandas))
            return originalDfPandas
        
        #removing duplicates
        df_pandas = removed_duplicates(df)
        colsToSelect =['it_ref_no_taxassm_dim','name','main_business_code','assessment_year']
        df_pandas=df_pandas[colsToSelect]
        return df_pandas


        
        
        