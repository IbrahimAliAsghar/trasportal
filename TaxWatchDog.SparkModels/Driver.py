# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 12:14:59 2019


"""
#Packages Imported
import os
os.getcwd()
import logging
import Config
from JobManager import JobManager as JobManager
#from SparkModels.JobManager import JobManager as JobManager
#import SparkModels.Config as Config
import pandas as pd
import numpy as np
#from Init_Spark_Connection import Init_Spark_Connection
#from Spark_Connection import Spark_Connection
#from Read_write_hive import Read_write_hive
#from Calculated_columns import Calculated_columns
#from Ratios import Ratios
#from computed_features import Computed_features
#from scorer import Scorer
import math

def set_logging(    
    log_format = '%(asctime)s:%(name)s:%(levelname)s = %(message)s',
    log_name = '',
    log_file_name  = 'output.log'):
    
    log = logging.getLogger(log_name)
    log_formatter = logging.Formatter(log_format)

# =============================================================================
#     stream_handler = logging.StreamHandler()
#     stream_handler.setFormatter(log_formatter)
#     log.addHandler(stream_handler)
# =============================================================================

    file_handler_info = logging.FileHandler(log_file_name, mode='a')
    file_handler_info.setFormatter(log_formatter)
    file_handler_info.setLevel(logging.ERROR)
    log.addHandler(file_handler_info)
    
    log.setLevel(logging.ERROR)

    return log

def main(ModelParam):
    logger = set_logging()

    try:
        # updating status
        print("updating status")
        JobManager.UpdateStatus(JobManager, ModelParam["JobID"], Config.JobStatus['Running'])
        modelParams = ModelParam
        from Spark_Connection import Spark_Connection
        #creating object of class
        con_spark = Spark_Connection()
        #using the same class object to call the method from class
        sc ,sql_sc,spark = con_spark.Make_Spark_Connection()
        
        from Read_write_hive import Read_write_hive
        from Calculated_columns import Calculated_columns
        from Ratios import Ratios
        from computed_features import Computed_features
        from scorer import Scorer
        #con_spark = Spark_Connection()
        #spark_session = con_spark.Make_Spark_Connection()
        print("updating status")
        JobManager.UpdateStatus(JobManager, ModelParam["JobID"], Config.JobStatus['GettingData'])
        # read data from hive
        read_from_hive = Read_write_hive()
        df_data = read_from_hive.read_from_hive(modelParams,spark)
        #df_data = read_from_hive.read_csv(sql_sc)
        # Filtering assessment type
        df_data=df_data.filter(df_data['assessment_type']< 900)        
        
        print("updating status")
        JobManager.UpdateStatus(JobManager, ModelParam["JobID"], Config.JobStatus['ProcessingData'])
        # remove duplicate functions
        def removed_duplicates(df_data):
            print("enter dup func")
            originalDfPandas = df_data.toPandas()
            print("done to pandas")
            originalDfPandas = originalDfPandas.loc[:,~originalDfPandas.columns.duplicated()]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                print("if groupby")
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['seq_no'].transform(max) == originalDfPandas['seq_no']
                originalDfPandas = originalDfPandas[idx]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['assessment_key'].transform(max) == originalDfPandas['assessment_key']
                originalDfPandas = originalDfPandas[idx]
            if (len(originalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
                idx = originalDfPandas.groupby(['it_ref_no_taxassm_dim'])['snapshot_date'].transform(max) == originalDfPandas['snapshot_date']
                originalDfPandas = originalDfPandas[idx]
            originalDfPandas.reset_index(inplace=True,drop=True)
            print(len(originalDfPandas))
            return originalDfPandas
        # Filtering selected assessment year data
        df_cy = df_data.filter(df_data['assessment_year'].isin([int(ModelParam["AssesmentYear"])])) #int(ModelParam["AssesmentYear"])        
        #df_cy_1 = df_data.filter(df_data['assessment_year'].isin([int(ModelParam["AssesmentYear"])-1]))
        #df_cy_2 = df_data.filter(df_data['assessment_year'].isin([int(ModelParam["AssesmentYear"])-2]))
        
        # removing duplicates 
        originalDfPandas = removed_duplicates(df_cy)
        # identifying itrefnos present in selected year data
        it_ref_no_list = originalDfPandas['it_ref_no_taxassm_dim'].tolist()
        print(it_ref_no_list)
        it_ref_no_str_list = "','".join(it_ref_no_list)
        print("join done")
        #it_ref_no_list = "','".join(map(str, originalDfPandas['it_ref_no_taxassm_dim'].tolist()))
        it_ref_no_str_list= "'"+ it_ref_no_str_list + "'"
        
        # Created query for past two years data
        query_without_where_clause,where_clause = modelParams["ODSQuery"].split('WHERE', 1)
        print("split done")
        complete_query_trend = query_without_where_clause + "WHERE (financialfact_ods.it_ref_no_taxassm_dim IN ("+ it_ref_no_str_list +") AND financialfact_ods.assessment_year IN ("+str(int(ModelParam["AssesmentYear"])-1)+","+str(int(ModelParam["AssesmentYear"])-2)+"))"
        #complete_query_trend = query_without_where_clause + "WHERE (financialfact_ods.it_ref_no_taxassm_dim IN ('585394608') AND financialfact_ods.assessment_year IN ("+str(int(ModelParam["AssesmentYear"])-1)+","+str(int(ModelParam["AssesmentYear"])-2)+") AND financialfact_ods.file_type = 'C')"
        print(complete_query_trend)
        print("comp query created and fetching trend data")
        df_data_trends = read_from_hive.read_from_hive_trend_years(complete_query_trend,spark)
        print(df_data_trends.show())
        print("done fetch")
        
        # filtering data for -1 year
        df_cy_1 = df_data_trends.filter(df_data_trends['assessment_year'].isin([int(ModelParam["AssesmentYear"])-1]))
        # filtering data for -2 year
        df_cy_2 = df_data_trends.filter(df_data_trends['assessment_year'].isin([int(ModelParam["AssesmentYear"])-2]))
        print("done filter")
        print(df_cy_1.show())
        
        # removing duplicate
        originalDfPandas_1 = removed_duplicates(df_cy_1)
        originalDfPandas_2 = removed_duplicates(df_cy_2)
        #print(len(originalDfPandas))
        it_ref_df = originalDfPandas.copy()
        originalDfPandas = originalDfPandas.apply(pd.to_numeric, errors='ignore')
        originalDfPandas_1 = originalDfPandas_1.apply(pd.to_numeric, errors='ignore')
        originalDfPandas_2 = originalDfPandas_2.apply(pd.to_numeric, errors='ignore')
        dateList=[]
        for item in modelParams["Features"]:
            if item["FeatureDataType"] == "date":
                dateList.append(item["FeatureName"])
        
        featurelist = originalDfPandas.columns
        # exclude dimension feature from scoring	
        feature_list_withoutKeys = [] 
        exludeColList = ["business_key", "business_code", "branch_key", "assessment_key", "taxpayer_key","state","process_date","snapshot_date","seq_no","state_cd","main_business_code","assessment_type","postcode"]
        df = spark.createDataFrame(originalDfPandas)
    
        df_features= df
        df_bin = df_features.toPandas()
        
        for col in df_bin.columns:
            if col not in exludeColList:
                feature_list_withoutKeys.append(col)    
        
        computed = Computed_features()
        # apply binning
        binned_df = computed.Binning(df_bin[df_bin.select_dtypes(include=[np.number]).columns],exludeColList)
        df = spark.createDataFrame(binned_df)
        
        # Calculate ratios 
        ratios = Ratios()
        fields = []
        for rationame in modelParams["Ratios"]:
            fields.append(rationame["RatioName"])
        #fields=['RT_GP_GS','RT_EXP_GS','RT_NP_GS','RT_NP_TOTAST','RT_PRD_GS','RT_TRCRT_PUR','RT_TRDR_GS','RT_PUR_GS','RT_GS_TOTAST','RT_GP_EXP','INC_OPPT_B1','RT_STKTURN','RT_TRCRT_COS']
        
        for f in fields:
            df=getattr(ratios, 'Add_%s' % f)(df)
       
        #cal_col = Calculated_columns() 
        df1=df
        df_pandas=df1.toPandas()
        
        # Apply binning on ratios
        binned_ratios = computed.Normalize_And_Bin_Ratios(df_pandas,fields)
        NullLogicDict ={'RT_ADVRT_GS':'promo_advert_expense','RT_COM_GS':'commission','RT_EXP_GS':'total_expenses','RT_NALW_GS':'non_allow_expense','RT_PRD_GS':'production_cost','RT_PUR_GS':'purchases','RT_PURPRD_GS':['purchases','production_cost'],'RT_RND_GS':'r_d_expense','RT_ROYLTY_GS':'royalty_expense','RT_CSUBC_GS':'contract_subcon_expense','RT_TOTEX_OCR':'total_expenses','RT_TOTEXP_GS':'total_expenses','RT_TRAVEL_GS':'travelling_expense','RT_WAGES_GS':'salary_expense','RT_FA_TOTINC':'total_fixed_assets','RT_INT_GS':'loan_interest_expense','RT_TOTCL_EQY':'total_current_liability'}
        for i in fields:
            if i in NullLogicDict:
                if i != 'RT_PURPRD_GS':
                    binned_ratios.loc[(binned_ratios[i].isnull()) & (binned_ratios[NullLogicDict[i]] > 0),i+'_bin']=5
                else:
                    binned_ratios.loc[(binned_ratios[i].isnull()) & (binned_ratios[NullLogicDict[i][0]] +binned_ratios[NullLogicDict[i][1]] > 0),i+'_bin']=5
        			
        #binned_ratios=binned_ratios.astype(np.float64)
        
        # Conversion to spark dataframe
        df1= spark.createDataFrame(binned_ratios)
        feature_list = df_features[feature_list_withoutKeys].columns
        from pyspark.sql.types import IntegerType,FloatType
        from pyspark.sql.functions import udf ,lit
        transform_df_to_write_list=[]
        Group_dict = {"ASSETS":"1","EXPENSE":"1","INCOME":"0","ASSET_RT":"1","TAX":"0","LIABILITY":"1","EXT_INCOME":"1","STOCK":"1","INTERCOMPANY":"1","PNL":"1","INFORMATIONAL":"1","KEY_DERIVATV":"0","DEFAULT":"1"}
        
        # Assign Weightage wrt to the propotionality for each feature and ratio
        def AssignWeigtagesFeature(x,y,z):
            if x== np.nan:
                k =None
                return k
            else:  
                if z==1.0:
                    if x ==1.0:
                        k= y* 10.0
                        return k
                    elif x==2.0:
                        k= y* 25.0
                        return k
                    elif x==3:
                        k= y* 50.0
                        return k
                    elif x==4:
                        k= y* 100.0
                        return k
                    elif x ==5.0:
                        k= y* 150.0
                        return k
                else:
                    if x ==1.0:
                        k= y* 100.0
                        return k
                    elif x==2:
                        k= y* 50.0
                        return k
                    elif x==3:
                        k= y* 25.0
                        return k
                    elif x==4:
                        k= y* 10.0
                        return k
                    elif x ==5.0:
                        k= y* 150.0
                        return k
                
        func_udf2 = udf(AssignWeigtagesFeature, FloatType())
        
        # creating Scoring object
        scoring = Scorer()
        
        # Applying fuctions for scoring 
        ratio_feature_dict = scoring.ratios_proportionality(modelParams)
        direct_features,inverse_features,mergelist,all_features,direct_features_list,inverse_features_list = scoring.direct_and_inverse_features(ratio_feature_dict)
        same_list = scoring.get_explicit_features(feature_list,mergelist)
        DirectFeatureFrequency=scoring.get_direct_feature_frequency(direct_features,direct_features_list)
        InverseFeatureFrequency=scoring.get_inverse_feature_frequency(inverse_features,inverse_features_list)
        AllFeaturesFrequency= scoring.get_all_feature_frequency(all_features)
        print("*************** Done get_all_feature_frequency ****************")
        #df1=scoring.direct_feature_scoring(direct_features, fields,ratio_feature_dict, DirectFeatureFrequency,df1)
        #df1=scoring.inverse_feature_Scoring(inverse_features,direct_features,DirectFeatureFrequency,InverseFeatureFrequency,fields,ratio_feature_dict,df1)
        
        # direct features scoring
        for feature in direct_features:
            count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "0" in ratio_feature_dict[ratio]:
                    if DirectFeatureFrequency[feature]==1:
                        df1=df1.withColumn(feature + "_score"+str(count),func_udf2(df1[ratio+ "_Bin"],df1[feature],lit(0)))
                    else:
                        df1=df1.withColumn(feature + "_score"+str(count),func_udf2(df1[ratio+ "_Bin"],df1[feature],lit(0)))
                        count=count+1
        print("*************** Done direct features scoring ****************")
        
        # Indirect features scoring
        for feature in inverse_features:
            if feature in direct_features:
                count = DirectFeatureFrequency[feature]
            else:
                count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "1" in ratio_feature_dict[ratio]:
                        if InverseFeatureFrequency[feature]==1:
                            df1=df1.withColumn(feature + "_score"+str(count),func_udf2(df1[ratio+ "_Bin"],df1[feature],lit(1)))
                        else:
                            df1=df1.withColumn(feature + "_score"+str(count),func_udf2(df1[ratio+ "_Bin"],df1[feature],lit(1)))
                            count=count+1
        print("*************** Done Indirect features scoring ****************")

#        for i in mergelist:
#            df1 = df1.withColumn("sum_"+i,sum(df1[i+"_score"+str(j)].cast("float") for j in range(AllFeaturesFrequency[i]))) 
#        for i in mergelist:
#            df1 = df1.withColumn("Final"+i+"_score",df1["sum_"+i]/AllFeaturesFrequency[i])
        df1=scoring.get_final_sum_scored_features(mergelist,AllFeaturesFrequency,df1)
        print("*************** Done get_final_sum_scored_features ****************")

        #func_udf2= scoring.__init__()
        #df1=scoring.get_final_score_features(Group_dict,same_list,modelParams,df1)
        
        for feature in same_list:
            a=0
            for item in modelParams["Features"]:
                if item["FeatureName"] == feature.upper():
                    feature_group=item["FeatureGroup"]
                    a=int(Group_dict[feature_group])
                    break
            df1=df1.withColumn("Final"+feature + "_score",func_udf2(df1[feature+ "_Bin"],df1[feature],lit(a)))
        print("*************** Done same list features scoring ****************")

        df1_pandas= scoring.manipulate_scores(df1,fields,featurelist)
        print("*************** Done manipulate_scores ****************")
        df1_pandas=scoring.normalize_scores_rank(df1_pandas)
        print("*************** Done normalize_scores_rank ****************")
        print("*************** enter transform_df_to_write ****************")
        mean_dict,median_dict,std_dict,mode_dict,max_dict,min_dict = scoring.calculate_summary_statistics(df1_pandas,feature_list,exludeColList,fields)
        print("*************** Done calculate_summary_statistics ****************")
        #originalDfPandas_1["it_ref_no_taxassm_dim"] = pd.to_numeric(originalDfPandas_1["it_ref_no_taxassm_dim"])
        #originalDfPandas_2["it_ref_no_taxassm_dim"] = pd.to_numeric(originalDfPandas_2["it_ref_no_taxassm_dim"])
        loop_var= math.ceil(float(len(df1_pandas))/2000)
        for i in range(1,loop_var+1):
           print("***** iteration start :", i)
           #print(df1_pandas[(i-1)*100:(i*100)])
           df_transform = df1_pandas[(i-1)*2000:(i*2000)]
           it_ref_df_transform = it_ref_df[(i-1)*2000:(i*2000)]
           df_val_1 = pd.merge(df_transform, originalDfPandas_1, how='inner', on=['it_ref_no_taxassm_dim'],suffixes=('_left', '_right'))
           df_val_2 = pd.merge(df_transform, originalDfPandas_2, how='inner', on=['it_ref_no_taxassm_dim'],suffixes=('_left', '_right'))
           print(len(df_val_1))
           print(len(df_val_2))
           print(len(df_transform))
           #df_transform2 = originalDfPandas_1[(i-1)*2000:(i*2000)]
           #df_transform3 = originalDfPandas_2[(i-1)*2000:(i*2000)]
           print("Merge Done")
           transform_df_to_write_list = scoring.transform_df_to_write(df_transform,feature_list,exludeColList,fields,modelParams,it_ref_df_transform,df_val_1,df_val_2,mean_dict,median_dict,std_dict,mode_dict,max_dict,min_dict,(i-1)*2000)
           #transform_df_to_write_list = scoring.transform_df_to_write(df_transform,feature_list,exludeColList,fields,modelParams,it_ref_df,originalDfPandas_1,originalDfPandas_2)
           print("*************** Done transform_df_to_write ****************")
           print("*************** enter df conversion ****************")
           sparkResult = scoring.df_conversion(transform_df_to_write_list,spark)
           print("*************** Done df conversion ****************")
           print("*************** enter hive insert ****************")
           sparkResult= read_from_hive.write_to_hive(spark,sparkResult)
           print("*************** done hive insert ****************")
           print("***** iteration done :", i)

        #sparkResult = read_from_hive.write_csv(sparkResult)
        print("updating status")
        JobManager.UpdateStatus(JobManager, ModelParam["JobID"], Config.JobStatus['Done'])
    except Exception as Exp:
       logger.error(str(Exp), exc_info=True)
       print("updating status")
       print(str(Exp))
       
       JobManager.UpdateStatus(JobManager, ModelParam["JobID"], Config.JobStatus['Failed'])#with python trace backing
    return "successful"


    #main(Config.ModelParam)
    
    
#    for i in fields:
#        if i in dict1:
#            if i != 'RT_PURPRD_GS':
#                binned_ratios.loc[(binned_ratios[i].isnull()) & (binned_ratios[dict1[i]] > 0),i+'_bin']=5
#            else:
#                binned_ratios.loc[(binned_ratios[i].isnull()) & (binned_ratios[dict1[i][0]] +binned_ratios[dict1[i][1]] > 0),i+'_bin']=5
    			
#df_cy = df_data.filter(df_data['assessment_year'].isin([2017]))
