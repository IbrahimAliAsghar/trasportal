﻿#!/usr/bin/env python3
##TODO: make spark connection
def runModel(params):
    try:
        #from JobManager import JobManager as JobManager
        from SparkModels.JobManager import JobManager as JobManager   #import SparkModels.JobManager as JobManager
        import SparkModels.Config as Config
        import findspark
    
        #findspark.init('/var/opt/teradata/cloudera/parcels/SPARK2-2.2.0.cloudera4-1.cdh5.13.3.p0.603055/lib/spark2')
    
            #conf = SparkConf().setAppName('hello').setMaster('spark://172.20.244.166:7077').setSparkHome('/etc/spark2')
        #sc = SparkContext(conf=conf)
    
        findspark.init(Config.Spark['Location'])
        #findspark.init('/var/opt/teradata/cloudera/parcels/SPARK2-2.2.0.cloudera4-1.cdh5.13.3.p0.603055/lib/spark2')
        
        import pyspark
        from pyspark import SparkConf, SparkContext
        from pyspark.sql import SparkSession
        spark = SparkSession.builder.getOrCreate()
        import pandas as pd
        sc = spark.sparkContext
        sc
        
        
        # In[3]:
        
        
        #from pyspark import SparkContext
        from pyspark.sql import SQLContext
        from datetime import date
        sql_sc = SQLContext(sc)
        import numpy as np
        
        import json
        
        
        # In[50]:
        
        
        #with open('C:/Users/qasim.usmani/Desktop/LHDN FILES/pyspark/ModelParameters.json', encoding='utf-8') as data_file:
        #data = {"ODSQuery":"SELECT financialfact_ods.assessment_year,financialfact_ods.branch_key,financialfact_ods.taxpayer_key,financialfact_ods.business_key,financialfact_ods.assessment_key,financialfact_ods.plant_and_machinery,financialfact_ods.total_assets,financialfact_ods.trade_debtors,related_comp_fact_ods.loans_from_outside_malaysia,related_comp_fact_ods.total_purchase_outside_malaysi,related_comp_fact_ods.other_payment_outside_malaysia,related_comp_fact_ods.receipt_from_outside_malaysia,related_comp_fact_ods.total_sales_outside_malaysia,financialfact_ods.promo_advert_expense,financialfact_ods.cost_of_goods_sold,financialfact_ods.opening_stock,financialfact_ods.production_cost,financialfact_ods.gross_profit,financialfact_ods.gross_sales,financialfact_ods.net_profit_or_loss,financialfact_ods.contract_subcon_expense,financialfact_ods.loan_interest_expense,financialfact_ods.non_allow_expense,financialfact_ods.purchases,financialfact_ods.total_expenses,financialfact_ods.other_business_income,financialfact_ods.other_non_sales_income,financialfact_ods.closing_stock,financialfact_ods.loan_from_director,financialfact_ods.other_creditors,tax_assm_fact_ods.total_tax,tax_assm_fact_ods.total_tax_liability,tax_assm_fact_ods.tax_payable_repayable,financialfact_ods.state,financialfact_ods.tax_exempted_income,financialfact_ods.other_non_sales_income,financialfact_ods.trade_creditors,financialfact_ods.it_assm_branch,financialfact_ods.it_assm_branch FROM financialfact_ods JOIN related_comp_fact_ods ON financialfact_ods.branch_key = related_comp_fact_ods.branch_key AND financialfact_ods.taxpayer_key = related_comp_fact_ods.taxpayer_key AND financialfact_ods.business_key = related_comp_fact_ods.business_key AND financialfact_ods.assessment_key = related_comp_fact_ods.assessment_key JOIN tax_assm_fact_ods ON financialfact_ods.branch_key = tax_assm_fact_ods.branch_key AND financialfact_ods.taxpayer_key = tax_assm_fact_ods.taxpayer_key AND financialfact_ods.business_key = tax_assm_fact_ods.business_key AND financialfact_ods.assessment_key = tax_assm_fact_ods.assessment_key","ProfileID":1,"ModelID":1,"Ratios":[{"RatioID":1,"RatioName":"RT_GP_GS","Propotional":0,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":2,"RatioName":"RT_EXP_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":3,"RatioName":"RT_NP_GS","Propotional":0,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":4,"RatioName":"RT_NP_TOTAST","Propotional":0,"Features":[{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":5,"RatioName":"RT_PRD_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":27,"FeatureName":"production_cost","FeatureDescription":"Production Cost","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":6,"RatioName":"RT_TRCRT_PUR","Propotional":1,"Features":[{"FeatureID":34,"FeatureName":"purchases","FeatureDescription":"Purchases","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":47,"FeatureName":"trade_creditors","FeatureDescription":"Trade Creditors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":7,"RatioName":"RT_TRDR_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":18,"FeatureName":"trade_debtors","FeatureDescription":"Trade Debtors","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":8,"RatioName":"RT_PUR_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":34,"FeatureName":"purchases","FeatureDescription":"Purchases","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":9,"RatioName":"RT_GS_TOTAST","Propotional":0,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":10,"RatioName":"RT_GP_EXP","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":11,"RatioName":"INC_OPPT_B1","Propotional":1,"Features":[{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":37,"FeatureName":"other_non_sales_income","FeatureDescription":"Other Income","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":12,"RatioName":"RT_STKTURN","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":13,"RatioName":"RT_TRCRT_COS","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":47,"FeatureName":"trade_creditors","FeatureDescription":"Trade Creditors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":14,"RatioName":"RT_GP_GS","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":15,"RatioName":"RT_EXP_GS","Propotional":0,"Features":[{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":16,"RatioName":"RT_NP_GS","Propotional":1,"Features":[{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":17,"RatioName":"RT_NP_TOTAST","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":18,"RatioName":"RT_STKTURN","Propotional":0,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":19,"RatioName":"RT_TRDR_GS","Propotional":0,"Features":[{"FeatureID":18,"FeatureName":"trade_debtors","FeatureDescription":"Trade Debtors","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":20,"RatioName":"RT_PUR_GS","Propotional":0,"Features":[{"FeatureID":34,"FeatureName":"purchases","FeatureDescription":"Purchases","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":21,"RatioName":"RT_AV_STK","Propotional":0,"Features":[{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":22,"RatioName":"RT_COGS_GS","Propotional":0,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":23,"RatioName":"RT_GP_EXP","Propotional":1,"Features":[{"FeatureID":39,"FeatureName":"loan_from_director","FeatureDescription":"Loan from Directors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":24,"RatioName":"RT_EXP_GS","Propotional":0,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":25,"RatioName":"RT_GP_GS","Propotional":1,"Features":[{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":26,"RatioName":"RT_GS_TOTAST","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":27,"RatioName":"RT_STKTURN","Propotional":0,"Features":[{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":28,"RatioName":"RT_TRDR_GS","Propotional":0,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":29,"RatioName":"RT_NP_TOTAST","Propotional":1,"Features":[{"FeatureID":18,"FeatureName":"trade_debtors","FeatureDescription":"Trade Debtors","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":30,"RatioName":"RT_TRCRT_PUR","Propotional":0,"Features":[{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":31,"RatioName":"RT_GP_TOTAST","Propotional":0,"Features":[{"FeatureID":47,"FeatureName":"trade_creditors","FeatureDescription":"Trade Creditors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":34,"FeatureName":"purchases","FeatureDescription":"Purchases","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":32,"RatioName":"RT_NP_GS","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":27,"FeatureName":"production_cost","FeatureDescription":"Production Cost","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":33,"RatioName":"RT_AV_STK","Propotional":0,"Features":[{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":34,"RatioName":"RT_COGS_GS","Propotional":0,"Features":[{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":35,"RatioName":"RT_GP_GS","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":36,"RatioName":"RT_NP_GS","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":37,"RatioName":"RT_PRD_GS","Propotional":0,"Features":[{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":38,"RatioName":"RT_STKTURN","Propotional":0,"Features":[{"FeatureID":27,"FeatureName":"production_cost","FeatureDescription":"Production Cost","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]},{"RatioID":39,"RatioName":"RT_NP_TOTAST","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"}]}],"Features":[{"FeatureID":16,"FeatureName":"plant_and_machinery","FeatureDescription":"Plant and Machinery","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":17,"FeatureName":"total_assets","FeatureDescription":"Total Assets","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":18,"FeatureName":"trade_debtors","FeatureDescription":"Trade Debtors","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":19,"FeatureName":"loans_from_outside_malaysia","FeatureDescription":"Loans from related companies outside Mal","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"related_comp_fact_ods"},{"FeatureID":20,"FeatureName":"total_purchase_outside_malaysi","FeatureDescription":"","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"related_comp_fact_ods"},{"FeatureID":21,"FeatureName":"other_payment_outside_malaysia","FeatureDescription":"","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"related_comp_fact_ods"},{"FeatureID":22,"FeatureName":"receipt_from_outside_malaysia","FeatureDescription":"Receipt from related companies outside Msia","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"related_comp_fact_ods"},{"FeatureID":23,"FeatureName":"total_sales_outside_malaysia","FeatureDescription":"Total sales from related companies outside Malaysia","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"related_comp_fact_ods"},{"FeatureID":24,"FeatureName":"promo_advert_expense","FeatureDescription":"Advertising Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":25,"FeatureName":"cost_of_goods_sold","FeatureDescription":"Cost of Sales or Cost of Goods Sold","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":26,"FeatureName":"opening_stock","FeatureDescription":"Opening Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":27,"FeatureName":"production_cost","FeatureDescription":"Production Cost","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":28,"FeatureName":"gross_profit","FeatureDescription":"Gross Profit or Loss","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":29,"FeatureName":"gross_sales","FeatureDescription":"Gross Sales","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":30,"FeatureName":"net_profit_or_loss","FeatureDescription":"Net Profit or Loss","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":31,"FeatureName":"contract_subcon_expense","FeatureDescription":"Contractor and Subcontractor Expense","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":32,"FeatureName":"loan_interest_expense","FeatureDescription":"Loan Interest Expense","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":33,"FeatureName":"non_allow_expense","FeatureDescription":"Non-allowable expenses","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":34,"FeatureName":"purchases","FeatureDescription":"Purchases","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":35,"FeatureName":"total_expenses","FeatureDescription":"Total Business Expense","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":36,"FeatureName":"other_business_income","FeatureDescription":"Other Business Income","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":37,"FeatureName":"other_non_sales_income","FeatureDescription":"Other Income","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":38,"FeatureName":"closing_stock","FeatureDescription":"Closing Stock","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":39,"FeatureName":"loan_from_director","FeatureDescription":"Loan from Directors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":40,"FeatureName":"other_creditors","FeatureDescription":"Other Creditors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":124,"FeatureName":"total_tax","FeatureDescription":"Tax","FeatureGroup":"LIABILITY","FeatureDataType":"numeric","TableName":"tax_assm_fact_ods"},{"FeatureID":42,"FeatureName":"total_tax_liability","FeatureDescription":"Tax Liability Balance (Owe / Refund)","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"tax_assm_fact_ods"},{"FeatureID":125,"FeatureName":"tax_payable_repayable","FeatureDescription":"Tax Payable","FeatureGroup":"LIABILITY","FeatureDataType":"numeric","TableName":"tax_assm_fact_ods"},{"FeatureID":44,"FeatureName":"state","FeatureDescription":"State Code","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":45,"FeatureName":"tax_exempted_income","FeatureDescription":"Non-taxable profits","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":46,"FeatureName":"other_non_sales_income","FeatureDescription":"Other Non-Business Income","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":47,"FeatureName":"trade_creditors","FeatureDescription":"Trade Creditors","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"},{"FeatureID":48,"FeatureName":"it_assm_branch","FeatureDescription":"Branch Code","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"financialfact_ods"}]}
        data = params
        print("updating status")
        JobManager.UpdateStatus(JobManager, data["JobID"], Config.JobStatus['Running'])
        
        
        # In[6]:
        
        
        class Ratios(object):
            #df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load('E:\\LHDN Docs\\Data\\model1.csv')
            def Add_RT_ADVRT_GS(self):
                df1=df.withColumn('RT_ADVRT_GS', (df['promo_advert_expense'] / df['gross_sales']))
                return df1
            def Add_RT_BADDBT_GS(self):
                df1=df.withColumn('RT_BADDBT_GS', (df['bad_debt'] / df['gross_sales']))
                return df1
            def Add_RT_CAST_TA(self):
                df1=df.withColumn('RT_CAST_TA', (df['total_current_assets'] / df['total_assets']))
                return df1
            def Add_RT_CLST_CAST(self):
                df1=df.withColumn('RT_CLST_CAST', (df['closing_stock'] / df['total_current_assets']))
                return df1
            def Add_RT_COM_GS(self):
                df1=df.withColumn('RT_COM_GS', (df['commission'] / df['gross_sales']))
                return df1
            def Add_RT_EXP_GS(self):
                df1=df.withColumn('RT_EXP_GS', (df['total_expenses'] / df['gross_sales']))
                return df1
            def Add_RT_FA_TOTAST(self):
                df1=df.withColumn('RT_FA_TOTAST', (df['total_fixed_assets'] / df['total_assets']))
                return df1
            def Add_RT_GP_GS(self):
                df1=df.withColumn('RT_GP_GS', (df['gross_profit'] / df['gross_sales']))
                return df1
            def Add_RT_GP_TOTAST(self):
                df1=df.withColumn('RT_GP_TOTAST', (df['gross_profit'] / df['total_assets']))
                return df1
            def Add_RT_GP_EXP(self):
                df1=df.withColumn('RT_GP_EXP',(df['gross_profit'] / df['total_expenses']))
                return df1
            def Add_RT_GS_TOTAST(self):
                df1=df.withColumn('RT_GS_TOTAST', (df['gross_sales'] / df['total_assets']))
                return df1
            def Add_RT_LIQUIDITY(self):
                df1=df.withColumn('RT_LIQUIDITY', (df['total_current_assets'] / df['total_current_liability']))
                return df1
            def Add_RT_LOAN_EQY(self):
                df1=df.withColumn('RT_LOAN_EQY', (df['loans'] / df['total_equity']))
                return df1
            def Add_RT_NAEXP_TEX(self):
                df1=df.withColumn('RT_NAEXP_TEX', (df['non_allow_expense'] / df['total_expenses']))
                return df1
            def Add_RT_NALW_GS(self):
                df1=df.withColumn('RT_NALW_GS', (df['non_allow_expense'] / df['gross_sales']))
                return df1
            def Add_RT_NP_FA(self):
                df1=df.withColumn('RT_NP_FA', (df['net_profit_or_loss'] / df['total_fixed_asset']))
                return df1
            def Add_RT_NP_GS(self):
                df1=df.withColumn('RT_NP_GS', (df['net_profit_or_loss'] / df['gross_sales']))
                return df1
            def Add_RT_NP_INT(self):
                df1=df.withColumn('RT_NP_INT', (df['net_profit_or_loss'] / df['loan_interest_expense']))
                return df1
            def Add_RT_NP_TOTAST(self):
                df1=df.withColumn('RT_NP_TOTAST', (df['net_profit_or_loss'] / df['total_assets']))
                return df1
            def Add_RT_OTHCR_GS(self):
                df1=df.withColumn('RT_OTHCR_GS', (df['other_creditors'] / df['gross_sales']))
                return df1
            def Add_RT_OCR_TOTEX(self):
                df1=df.withColumn('RT_OTHCR_GS', (df['other_creditors'] / df['total_expenses']))
                return df1
            def Add_RT_OTHEXP_GS(self):
                df1=df.withColumn('RT_OTHEXP_GS', (df['other_expenses'] / df['gross_sales']))
                return df1
            def Add_RT_OTHINC_TI(self):
                df1=df.withColumn('RT_OTHINC_TI', (df['other_non_sales_income'] / df['total_income']))
                return df1
            def Add_RT_PRD_GS(self):
                df1=df.withColumn('RT_PRD_GS', (df['production_cost'] / df['gross_sales']))
                return df1
            def Add_RT_PUR_GS(self):
                df1=df.withColumn('RT_PUR_GS', (df['purchases'] / df['gross_sales']))
                return df1
            def Add_RT_RND_GS(self):
                df1=df.withColumn('RT_RND_GS', (df['r_d_expense'] / df['gross_sales']))
                return df1
            def Add_RT_RND_TEX(self):
                df1=df.withColumn('RT_RND_TEX', (df['r_d_expense'] / df['total_expenses']))
                return df1
            def Add_RT_ROYLTY_GS(self):
                df1=df.withColumn('RT_ROYLTY_GS', (df['royalty_expense'] / df['gross_sales']))
                return df1
            def Add_RT_CSUBC_GS(self):
                df1=df.withColumn('RT_CSUBC_GS', (df['contract_subcon_expense'] / df['gross_sales']))
                return df1
            def Add_RT_TOTEX_OCR(self):
                df1=df.withColumn('RT_TOTEX_OCR', (df['total_expenses'] / df['other_creditors']))
                return df1
            def Add_RT_TOTEXP_GS(self):
                df1=df.withColumn('RT_TOTEXP_GS', (df['total_expenses'] / df['gross_sales']))
                return df1
            def Add_RT_TRAVEL_GS(self):
                df1=df.withColumn('RT_TRAVEL_GS', (df['travelling_expense'] / df['gross_sales']))
                return df1
            def Add_RT_AV_STK(self):
                df1=df.withColumn('RT_AV_STK', (df['opening_stock'] + df['closing_stock'])/2)
                return df1
            def Add_RT_CAP(self):
                df1=df.withColumn('RT_CAP', (df['total_current_assets'] - df['total_current_liability']))
                return df1
            def Add_RT_DEBT_EQY(self):
                df1=df.withColumn('RT_DEBT_EQY', ((df['other_creditors'] + df['loan_from_director'] + df['other_current_liability'])/df['total_equity_liability']))
                return df1
            def Add_RT_FA_TOTPRD(self):
                df1=df.withColumn('RT_FA_TOTPRD', (df['total_fixed_assets'] / (df['purchases'] + df['production_cost'])))
                return df1
            def Add_RT_PURPRD_GS(self):
                df1=df.withColumn('RT_PURPRD_GS', ((df['purchases'] + df['production_cost']) / df['gross_sales']))
                return df1
            def Add_RT_TRDR_GS(self):
                df1=df.withColumn('RT_TRDR_GS', (df['trade_debtors'] / (df['gross_sales'] * 365)))
                return df1
            def Add_RT_WAGES_GS(self):
                df1 = df.withColumn('RT_WAGES_GS', df["salary_wages"] / df["gross_sales"])
                return df1
            def Add_RT_CA_GS(self):
                df1 = df.withColumn('RT_CA_GS', df["BUSINESS_INCOME_FACT.cap_allowance"] / df["gross_sales"])
                return df1
            def Add_RT_CA_TOTFA(self):
                df1 = df.withColumn('RT_CA_TOTFA', df["BUSINESS_INCOME_FACT.cap_allowance"] / df["total_fixed_assets"])
                return df1
            def Add_RT_CASH_TCL(self):
                df1 = df.withColumn('RT_CASH_TCL', df["cash_in_hand"] / df["total_current_liability"])
                return df1
            def Add_RT_COGS_GS(self):
                df1 = df.withColumn('RT_COGS_GS', df["cost_of_goods_sold"] / df["gross_sales"])
                return df1
            def Add_RT_COM_SINC(self):
                df1 = df.withColumn('RT_COM_SINC', df["commission"] / df["INCOME_FACT.aggr_stat_bus_inc"])
                return df1
            def Add_RT_FA_TOTINC(self):
                df1 = df.withColumn('RT_FA_TOTINC', df["total_fixed_assets"] / (df["gross_sales"]+df["other_business_income"]+df["other_non_sales_income"]))
                return df1
            def Add_RT_GS_TOTFA(self):
                df1 = df.withColumn('RT_GS_TOTFA', df["gross_sales"] / df["total_fixed_assets"])
                return df1
            def Add_RT_INT_GS(self):
                df1 = df.withColumn('RT_INT_GS', df["loan_interest_expense"] / df["gross_sales"])
                return df1
            def Add_RT_LTLIB_EQY(self):
                df1 = df.withColumn('RT_LTLIB_EQY', df["long_term_liability"] / df["total_equity_liability"])
                return df1
            def Add_RT_NAINT_TEX(self):
                df1 = df.withColumn('RT_NAINT_TEX', (df["total_investment"] + df["loan_to_director"]) / (df["total_liabilities"] - df["trade_creditors"]) * df["loan_interest_expense"])
                return df1
            def Add_RT_STKTURN(self):
                df1 = df.withColumn('RT_STKTURN', df["cost_of_goods_sold"] / ((df["opening_stock"] + df["closing_stock"]/2)))
                #due to mismatch formulas in Financial_Ratios and TACS_Features_Mapping_Document_v3.0 [excel sheet]
                #df1 = df.withColumn('RT_STKTURN', 365/(df["cost_of_goods_sold"] / ((df["opening_stock"] + df["closing_stock"])/2)))
                return df1
            def Add_RT_TINE_TINC(self):
                df1 = df.withColumn('RT_TINE_TINC', df["TAX_ASSM_FACT.total_claim"] / df["total_income"])
                return df1
            def Add_RT_TINE_NPNL(self):
                df1 = df.withColumn('RT_TINE_NPNL', df["TAX_ASSM_FACT.total_claim"] / df["net_profit_or_loss"])
                return df1
            def Add_RT_TOTCL_EQY(self):
                df1 = df.withColumn('RT_TOTCL_EQY', df["total_current_liability"] / df["total_equity_liability"])
                return df1
            def Add_RT_TRCRT_PUR(self):
                df1 = df.withColumn('RT_TRCRT_PUR', df["trade_creditors"] / df["purchases"])
                return df1
            def Add_RT_GS_CAP(self):
                df1 = df.withColumn('RT_GS_CAP', df["gross_sales"] / df(["total_current_assets"] - df["total_current_liability"]))
                return df1
            def Add_INC_OPPT_B1(self):
                df1 = df.withColumn('INC_OPPT_B1', df["net_profit_or_loss"] - df["other_non_sales_income"])
                return df1
            def Add_RT_TRCRT_COS(self):
                df1 = df.withColumn('RT_TRCRT_COS', df["trade_creditors"] - df["cost_of_goods_sold"])
                return df1
        
        
        # In[7]:
        
        
        import math
        def binning(df):
            for feature in df.columns:
                if feature not in exludeColList :
                    df[str(feature)+ "_Bin"]=pd.qcut(df[feature].rank(method='first'), 4, labels=False,duplicates='drop')+1
    #        features2 =df
    #        for i in df.columns:
    #            features2 = features2.sort_values(by=[i])
    #            features2 =features2.reset_index(drop=True)
    #            bins = [features2[i][0]-1,features2[i][math.ceil(len(features2)/4)],features2[i][math.ceil(len(features2)/2)],features2[i][math.ceil((len(features2)/4 *3))],features2[i][len(features2)-1]+1]
    #            #bin_set = set(bins)
    #            unique_bin = len(set(bins))
    #            
    #            labels =[]
    #            if unique_bin > 1:
    #                for j in range(len(set(bins))-1):
    #                    labels.append(j+1)
    #                a = str(i)
    #                features2[a +"_Bin"]= pd.cut(features2[i], bins=bins, labels=labels,duplicates ='drop')
    #            else:
    #                features2[a +"_Bin"]= 1
    #        features2 = features2.apply(pd.to_numeric)
            features2=df.apply(pd.to_numeric)
            return features2
        
        # In[8]:
        
        
        #df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load(Config.SampleDataFile['Location'])
        #df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load('C:/Users/hafsa.lutfi/Desktop/LHDN/datasetcsv/model1_test.csv')
        #df = df.filter(df['assessment_year'] <= date.today().year)
        
        #from pyspark import SparkContext,SparkConf
        from pyspark.sql import HiveContext
        #SparkContext.setSystemProperty("hive.metastore.uris", "thrift://172.20.244.166:9083")
        #spark.sql('use mtsods')
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark.sql('use ' + Config.Hive["Database"])
        df = spark.sql(data["ODSQuery"])
        print(df.count())
        
    # =============================================================================
    #     import mysql.connector
    #     mydb = mysql.connector.connect(
    #       host=Config.Mysql["Host"],
    #       user=Config.Mysql["UserId"],
    #       passwd=Config.Mysql["Password"]
    #     )
    #     mycursor = mydb.cursor()
    #     mycursor.execute("Use modelsource")
    #     mycursor.execute("Select * from model1source limit 100")
    #     df = mycursor.fetchall()
    #     df = spark.createDataFrame(df)
    #     colNames =  [i[0] for i in mycursor.description]
    #     
    #     loopCounter = 0
    #     for name in df.columns:
    #         df = df.withColumnRenamed(name, colNames[loopCounter])
    #         loopCounter = loopCounter + 1
    # =============================================================================
        
        
        orginalDfPandas = df.toPandas()
        orginalDfPandas = orginalDfPandas.loc[:,~orginalDfPandas.columns.duplicated()]
        idx = orginalDfPandas.groupby(['it_ref_no_taxassm_dim'])['seq_no'].transform(max) == orginalDfPandas['seq_no']
        orginalDfPandas = orginalDfPandas[idx]
        if (len(orginalDfPandas.groupby('it_ref_no_taxassm_dim').filter(lambda x: len(x) > 1))>0):
            idx = orginalDfPandas.groupby(['it_ref_no_taxassm_dim'])['assessment_key'].transform(max) == orginalDfPandas['assessment_key']
            orginalDfPandas = orginalDfPandas[idx]
        orginalDfPandas.reset_index(inplace=True,drop=True)
        print(len(orginalDfPandas))
        
        orginalDfPandas = orginalDfPandas.apply(pd.to_numeric, errors='ignore')
        print(orginalDfPandas.dtypes)
        featurelist = orginalDfPandas.columns
        	
        feature_list_withoutKeys = [] 
        exludeColList = ["business_key", "business_code", "branch_key", "assessment_key", "taxpayer_key","state","process_date","snapshot_date","seq_no"]
        
        
        df = spark.createDataFrame(orginalDfPandas)
    
        df_features= df
        df_bin = df_features.toPandas()
        
        for col in df_bin.columns:
            if col not in exludeColList:
                feature_list_withoutKeys.append(col)
        
        binned_df = binning(df_bin[df_bin.select_dtypes(include=[np.number]).columns])
        df = spark.createDataFrame(binned_df)
        ratios = Ratios()
        #fields =[]
        #for r in data["Ratios"]:
            #fields.append(r["RatioName"])
        fields=['RT_AV_STK','RT_COGS_GS','RT_GP_GS','RT_NP_GS','RT_PRD_GS','RT_STKTURN','RT_NP_TOTAST']
        #fields=['RT_AV_STK','RT_COGS_GS','RT_GP_GS','RT_NP_GS','RT_PRD_GS','RT_STKTURN','RT_NP_TOTAST']
        for f in fields:
            df=getattr(ratios, 'Add_%s' % f)()
        #df.show(1)
        
        
        ##TODO: calculate year function
        
        from pyspark.sql.types import IntegerType,FloatType
        from pyspark.sql.functions import udf ,lit
        def Create_TR_NBIN_YRM(x,y,i):
            if (y == date.today().year -i ):
                x=x
            else:
                x=None
            return x
        func_udf = udf(Create_TR_NBIN_YRM, IntegerType())
        
        
        ##TODO: calling year function of 7 different features [cal_cols based on year]
        Cal_col_list ={"TR_PNM_YM":"plant_and_machinery","TR_OPSTK_YM":"opening_stock","TR_NPNL_YM":"net_profit_or_loss","TR_GP_YM":"gross_profit","TR_GS_YM":"gross_sales","TR_COGS_YM":"cost_of_goods_sold","TR_CLSTK_YM":"closing_stock"}
        year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
        for key in Cal_col_list:
            for i in range(len(year_list)):
                df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
                
        print(len(df.columns))
        #127
        
        ##TODO: calculating columns [based on features only] 'TACS Feature Group Name'->[ASSET_RT , NPNL - ,EXPENSE_RT –]
        df=df.withColumn('AST_CASH_B1', df["cash_in_hand"] + df["cash_in_bank"])
        df=df.withColumn('RT_TOTEXP_GS', df["total_expenses"] / df["gross_sales"])
        df=df.withColumn('RT_FA_TOTAST', df["total_fixed_assets"] / df["total_assets"])
        df=df.withColumn('RT_GS_TOTFA', df["gross_sales"] / df["total_fixed_assets"])
        df=df.withColumn('RT_ADVRT_GS', df["promo_advert_expense"] / df["gross_sales"])
        df=df.withColumn('RT_CSUBC_GS', df["contract_subcon_expense"] / df["gross_sales"])
        df=df.withColumn('RT_NAEXP_TEX', df["non_allow_expense"] / df["total_expenses"])
        df=df.withColumn('RT_NP_INT', df["net_profit_or_loss"] / df["loan_interest_expense"])
        df=df.withColumn('RT_OEX_TOTEX', df["other_expenses"] / df["total_expenses"])
        df=df.withColumn('RT_OTHEXP_GS', df["other_expenses"] / df["gross_sales"])
        df=df.withColumn('RT_WAGES_GS', df["salary_expense"] / df["gross_sales"])
        
        print(len(df.columns))
        #138
        
        ##TODO: ratios binning 
        #    def Binnig_ratios(x):    
    #        if x is None:
    #            return 0
    #        elif x <= float(diff)/4:
    #            return 1
    #        elif (x > float(diff)/4 and x <= float(diff)/2):
    #            return 2
    #        elif (x > float(diff)/2 and x <= float(diff)* 3/4) :
    #            return 3
    #        elif (x > float(diff)* 3/4):
    #            return 4
    #    func_udf1 = udf(Binnig_ratios, IntegerType())
        
        
        # In[19]:
        
        
        df1=df
        df_pandas=df1.toPandas()
        #breakCounter = 0
        for ratio in fields:
            df_pandas[str(ratio)+ "_bin"]=pd.qcut(df_pandas[ratio].rank(method='first'), 4, labels=False)+1
    #          while True:
    #            try:
    #                #print(ratio)
    #                #df1 = df1.withColumn(str(ratio)+ '_norm', (df1[ratio]-df1.agg({ratio: "min"}).collect()[0][0])/(df1.agg({ratio: "max"}).collect()[0][0]-df1.agg({ratio: "min"}).collect()[0][0]))
    #                #print("finished norm")
    #                #diff= df1.agg({str(ratio)+ '_norm': "max"}).collect()[0][0] - df1.agg({str(ratio)+ '_norm': "min"}).collect()[0][0]
    #                #print("finished norm 2")
    #                df1=df1.withColumn(str(ratio)+ '_Bin',func_udf1(df1[ratio]))
    #                print("finished bin")
    #            except:
    #                if breakCounter > 5:
    #                    break
    #                breakCounter = breakCounter + 1
    #                continue
    #            break
        df_pandas=df_pandas.astype(np.float64)
        df1= spark.createDataFrame(df_pandas)
        feature_list = df_features[feature_list_withoutKeys].columns
        def AssignWeigtagesFeature(x,y,z):
            if x== np.nan:
                k =None
                return k
            else:  
                if z==1.0:
                    if x ==1.0:
                        k= y* 10.0
                        return k
                    elif x==2.0:
                        k= y* 25.0
                        return k
                    elif x==3:
                        k= y* 50.0
                        return k
                    elif x==4:
                        k= y* 100.0
                        return k
                else:
                    if x ==1.0:
                        k= y* 100.0
                        return k
                    elif x==2:
                        k= y* 50.0
                        return k
                    elif x==3:
                        k= y* 25.0
                        return k
                    elif x==4:
                        k= y* 10.0
                        return k
                
        func_udf2 = udf(AssignWeigtagesFeature, FloatType())
        
       
        ##TODO:creating ratio feature dictionary with proportionality
        ratio_feature_dict ={}
        
        temp = []
        #for ratios in data["Ratios"]:
            #ratio_feature_dict[ratios["RatioName"]] = [str(ratios["Propotional"])]
            #for features in ratios["Features"]:
                #ratio_feature_dict[ratios["RatioName"]].append(features["FeatureName"])
        ratio_feature_dict ={'RT_AV_STK':['1','opening_stock','closing_stock'],'RT_COGS_GS':['1','cost_of_goods_sold','gross_sales'] ,'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_PRD_GS':['1','production_cost','gross_sales'] ,'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets']}
        
        ##TODO:direct and indirect feature
        
        all_features=[]
        direct_features=[]
        inverse_features=[]
        for key in ratio_feature_dict:
            for i in range(1,len(ratio_feature_dict[key])):
                if len(ratio_feature_dict[key]) >0:
                    all_features.append(ratio_feature_dict[key][i])
                    if ratio_feature_dict[key][0] == '0':
                        direct_features.append(ratio_feature_dict[key][i])
                    else:
                        inverse_features.append(ratio_feature_dict[key][i])
                        
        direct_features_copy = direct_features.copy()
        inverse_features_copy = inverse_features.copy()
        
        direct_features =list(set(direct_features))  
        inverse_features =list(set(inverse_features)) 
        print(direct_features,inverse_features)
        
        
        # In[23]:
        
        
        same_list =[]
        direct_features1 = direct_features.copy()
        direct_features1.extend(inverse_features)
        mergelist = list(set(direct_features1))
        
        for f in feature_list:
            if f not in mergelist:
                same_list.append(f)
        
        
        # In[24]:
        
        print("same list : ")
        print(same_list)
        DirectFeatureFrequency = {}
        for feature in direct_features:
            DirectFeatureFrequency[feature] = direct_features_copy.count(feature)
            
        InverseFeatureFrequency = {}
        for feature in inverse_features:
            InverseFeatureFrequency[feature] = inverse_features_copy.count(feature)
        
        print(DirectFeatureFrequency)
        print(InverseFeatureFrequency)
        
        
        # In[25]:
        
        
        temp = df1
        temp.columns
        
        
        # In[26]:
        
        
        AllFeaturesFrequency = {}
        for feature in all_features:
            AllFeaturesFrequency[feature] = all_features.count(feature)
        print(AllFeaturesFrequency)
        
        
        # In[27]:
        
        
        temp = df1
        for feature in direct_features:
            count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "0" in ratio_feature_dict[ratio]:
                    if DirectFeatureFrequency[feature]==1:
                        temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_bin"],temp[feature],lit(0)))
                    else:
                        temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_bin"],temp[feature],lit(0)))
                        count=count+1
        
        
        # # for inverse
        
        # In[28]:
        
        
        for feature in inverse_features:
            if feature in direct_features:
                count = DirectFeatureFrequency[feature]
            else:
                count = 0
            for ratio in fields:
                if feature in ratio_feature_dict[ratio] and "1" in ratio_feature_dict[ratio]:
                        if InverseFeatureFrequency[feature]==1:
                            #print(feature + "_score"+str(count))
                            temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_bin"],temp[feature],lit(1)))
                        else:
                            #print(feature + "_score"+str(count))
                            temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_bin"],temp[feature],lit(1)))
                            count=count+1
        
        
        # In[29]:
        
        
        for i in mergelist:
            temp = temp.withColumn("sum_"+i,sum(temp[i+"_score"+str(j)].cast("float") for j in range(AllFeaturesFrequency[i]))) 
        
        
        # In[30]:
        
        
        for i in mergelist:
            temp = temp.withColumn("Final"+i+"_score",temp["sum_"+i]/AllFeaturesFrequency[i])
        
        
        # In[31]:
        
        Group_dict = {"ASSETS":"1","EXPENSE":"1","INCOME":"0","ASSET_RT":"1","TAX":"0","LIABILITY":"1","EXT_INCOME":"1","STOCK":"1","INTERCOMPANY":"1","PNL":"1","INFORMATIONAL":"1","KEY_DERIVATV":"0","DEFAULT":"1"}
        for feature in same_list:
            a=0
            for item in data["Features"]:
                if item["FeatureName"] == feature.upper():
                    #print("feature: "+feature.upper())
                    #ss=item["FeatureGroup"]
                    feature_group=item["FeatureGroup"]
                    #print("ss: "+ss)
                    a=int(Group_dict[feature_group])
                    #a=int(Group_dict[ss])
                    #print("A: "+str(a))
                    break
            #print(a)
            temp=temp.withColumn("Final"+feature + "_score",func_udf2(temp[feature+ "_Bin"],temp[feature],lit(a)))
            #print(int(Group_dict[ss]))
        #final_score columns present in temp df
        Final_score_columns=[]
        for names in temp.columns:
            if "Final" in names or names in fields or names in featurelist or "TR" in names:    #" or assessment_year" in names :
                Final_score_columns.append(names) 
        
        temp_score = temp.select(Final_score_columns)
        print(len(temp_score.columns))
        final = temp_score.toPandas()
        finalcolnames = []
        for name in final.columns:
            if "Final" in name:
                finalcolnames.append(name)
        for col in finalcolnames:
            #print(col)
            final[str(col) + '_Ranked'] = final[col].rank(ascending= False ,method = 'dense')
        
        for col in finalcolnames:
            final[col+"norm_score"]=((final[col]-final[col].min())/(final[col].max()-final[col].min()))*100
            
        #casting final score columns with float
    #    for i in Final_score_columns:
    #        if "Final" in i:
    #            temp_score = temp_score.withColumn("float_"+i, temp_score[i].cast("float"))
    #    temp_score
    #    
    #    #selecting columns that have float datatype and starts with float
    #    Float_Final_score_columns = []
    #    for names in temp_score.columns:
    #        if "float" in names or "assessment_year" in names :
    #            Float_Final_score_columns.append(names) 
    #    Float_Final_score_columns
    #    #saving final_selected float columns in final_score and replacing null values with 0
    #    final_score = temp_score.select(Float_Final_score_columns) #33 cols
    #    final_score = final_score.na.fill(0)
    #    print(len(final_score.columns))
    #    
    #    
    #    # In[16]:
    #    
    #    final = final_score.toPandas()
    #    
    #    # In[17]:
    #    
    #    from sklearn import preprocessing
    #    floatcolnames = []
    #    for name in final.columns:
    #        if "float" in name:
    #            floatcolnames.append(name)
    #    
    #    scored= final[floatcolnames].values #returns a numpy array
    #    min_max_scaler = preprocessing.MinMaxScaler()
    #    scored_scaled = min_max_scaler.fit_transform(scored)
    #    final1 = pd.DataFrame(scored_scaled)
    #    features = floatcolnames
    #    string = '_norm'
    #    new_feature_list = [x + string for x in features]
    #    print(new_feature_list)
    #    
    #    final1.columns = new_feature_list
    #    #df.dtypes
    #    
    #    
    #    # In[18]:
    #    
    #    
    #    df1 = final1
    #    for col in df1.columns:
    #        df1[str(col)+'_Range'] = (df1[col] *100)
    #    #df1.dtypes
    #    
    #    
    #    # In[19]:
    #    norm_range_columns = []
    #    for names in df1.columns:
    #        if "_norm_Range" in names:
    #            norm_range_columns.append(names) 
    #    norm_range_columns
    #    
    #    for col in norm_range_columns:
    #        #print(col)
    #        df1[str(col) + '_Ranked'] = df1[col].rank(ascending= False ,method = 'dense')
    #    
    #    # In[20]:
    #    
    #    
    #    data1 = [final,final1]
    #    data1 = pd.concat(data1,axis=1)
    #    data1 = data1.astype(float)
    #    mainDfPandas = df.toPandas()
    #    data2 = [mainDfPandas, data1]
    #    data2 = pd.concat(data2,axis=1)
    #    ranks_score_cols_names=[]
    #    for names in data2.columns:
    #        if "_norm_Range_Ranked" in names:
    #            ranks_score_cols_names.append(names)
    #        if  "_score" in names: 
    #            if "_norm" not in names:
    #                ranks_score_cols_names.append(names)
    #    cols_to_select= feature_list + fields + ["assessment_year"] + ranks_score_cols_names
    #    
    #    final_df_to_write = data2[cols_to_select]
       ##TODO: Change pandas df into spark df
       
        #data3 = data2.astype(float)
        #dataframe = spark.createDataFrame(data3)
        #val = spark_df.withColumn("assessment_year", df.assessment_year.cast("Int"))
        final_df_to_write = final
        mean_dict ={}
        median_dict = {}
        std_dict = {}
        mode_dict = {}
        max_dict ={}
        min_dict={}
        for i in feature_list:
            if i not in exludeColList and "assessment_year" not in i and "branch_code" not in i :
                 mean_dict[i]=final_df_to_write[i].mean()
                 median_dict[i]=final_df_to_write[i].median()
                 std_dict[i]=final_df_to_write[i].std()
                 mode_dict[i]=final_df_to_write[i].mode()
                 max_dict[i]=final_df_to_write[i].max()
                 min_dict[i]=final_df_to_write[i].min()
        for i in fields:
            if i not in exludeColList and "assessment_year" not in i and "branch_code" not in i :
                 mean_dict[i]=final_df_to_write[i].mean()
                 median_dict[i]=final_df_to_write[i].median()
                 std_dict[i]=final_df_to_write[i].std()
                 mode_dict[i]=final_df_to_write[i].mode()
                 max_dict[i]=final_df_to_write[i].max()
                 min_dict[i]=final_df_to_write[i].min()
        #final_df_to_write1 = spark.createDataFrame(final)
        counter = 0
        #tt = pd.DataFrame(columns=['feature','profile_id','model_id','value','score','rank','year_d','taxpayer'])
        #sqlInsertQuery = "INSERT INTO model_result_abt VALUES"
        final_df_to_write = final_df_to_write.astype(object).replace(np.nan, 'Null')
        dictList=[]
        for finalRow in final_df_to_write.iterrows():
            if counter < len(final_df_to_write):
                finalRow = finalRow[1]
                for feature in feature_list:
                     if feature not in exludeColList and "assessment_year" not in feature and "branch_code" not in feature :
                         dict1={}
                         dict1 ={'feature':feature,'profile_id':str(data['ProfileID']),'model_id':str(data['ModelID']),'value':str(finalRow[feature]),'score':str(finalRow['Final' + feature + '_scorenorm_score']),'rank':str(finalRow['Final' +feature + '_score_Ranked']),'year_d':str(finalRow['assessment_year']),'taxpayer':str(finalRow['taxpayer_key']),'it_ref_no':str(finalRow['it_ref_no_taxassm_dim']),'mean':str(mean_dict[feature]),'median':str(median_dict[feature]),'mode':str(mode_dict[feature].tolist()),'standard_deviation':str(std_dict[feature]),'max_value':str(max_dict[feature]),'min_value':str(min_dict[feature])}
                         dictList.append(dict1)
                         #sqlInsertQuery += "('" + feature + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[feature]) + "," + str(finalRow['Final' + feature + '_scorenorm_score']) + ", " +  str(finalRow['Final' +feature + '_score_Ranked']) + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) +"," + str(finalRow['it_ref_no_taxpay_dim']) + "),"
                for ratio in fields:
                     if ratio not in exludeColList and "assessment_year" not in ratio and "branch_code" not in ratio :
                         dict2={}
                         dict2={'feature':ratio,'profile_id':str(data['ProfileID']),'model_id':str(data['ModelID']),'value':str(finalRow[ratio]),'score':"Null",'rank':"Null",'year_d':str(finalRow['assessment_year']),'taxpayer':str(finalRow['taxpayer_key']),'it_ref_no':str(finalRow['it_ref_no_taxassm_dim']),'mean':str(mean_dict[ratio]),'median':str(median_dict[ratio]),'mode':str(mode_dict[ratio].tolist()),'standard_deviation':str(std_dict[ratio]),'max_value':str(max_dict[ratio]),'min_value':str(min_dict[ratio])}
                         dictList.append(dict2)
                         #sqlInsertQuery += "('" + ratio + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[ratio]) + "," + "Null" + ", " +  "Null" + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) + "," + str(finalRow['it_ref_no_taxpay_dim']) +"),"
            counter = counter + 1
    #    counter = 0
    #    tt = pd.DataFrame(columns=['feature','profile_id','model_id','value','score','rank','year_d','taxpayer','it_ref_no'])
    #    #sqlInsertQuery = "INSERT INTO model_result_abt VALUES"
    #    final_df_to_write = final_df_to_write.astype(object).replace(np.nan, 'Null')
    #    for finalRow in final_df_to_write.iterrows():
    #        if counter < len(final_df_to_write):
    #            finalRow = finalRow[1]
    #            for feature in feature_list:
    #                 if feature not in exludeColList and "assessment_year" not in feature and "branch_code" not in feature and "it_ref_no_taxpay_dim" not in feature:
    #                     tt.add(feature,str(data['ProfileID']),str(data['ModelID']),str(finalRow[feature]),str(finalRow['Final' + feature + '_scorenorm_score']),str(finalRow['Final' +feature + '_score_Ranked']),str(finalRow['assessment_year']),str(finalRow['taxpayer_key']),str(finalRow['it_ref_no_taxpay_dim']))
    #                     #sqlInsertQuery += "('" + feature + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[feature]) + "," + str(finalRow['Final' + feature + '_scorenorm_score']) + ", " +  str(finalRow['Final' +feature + '_score_Ranked']) + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) +"," + str(finalRow['it_ref_no_taxpay_dim']) + "),"
    #            for ratio in fields:
    #                 if ratio not in exludeColList and "assessment_year" not in ratio and "branch_code" not in ratio and "it_ref_no_taxpay_dim" not in ratio:
    #                     tt.add(ratio,str(data['ProfileID']),str(data['ModelID']),str(finalRow[ratio]),"Null","Null",str(finalRow['assessment_year']),str(finalRow['taxpayer_key']),str(finalRow['it_ref_no_taxpay_dim']))
    #                     #sqlInsertQuery += "('" + ratio + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[ratio]) + "," + "Null" + ", " +  "Null" + ", " +  str(finalRow['assessment_year']) + "," + str(finalRow['taxpayer_key']) + "," + str(finalRow['it_ref_no_taxpay_dim']) +"),"
    #                    
    #        counter = counter + 1
            print(counter)
        #sqlInsertQuery = sqlInsertQuery[:-1]
        
        #print(sqlInsertQuery)
        #mycursor = mydb.cursor()
        #mycursor.execute(sqlInsertQuery)
        #mydb.commit()
        finalResult = pd.DataFrame(dictList)
        sparkResult = spark.createDataFrame(finalResult)
    
        print("\n******* Start Insert ********")
        spark1 = SparkSession.builder.getOrCreate()
        #SparkContext.setSystemProperty("hive.metastore.uris", "thrift://172.20.244.166:9083")
        #spark1.sql('use mtsods')
        SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
        spark1.sql('use ' + Config.Hive["Database"])
        #writeResult = spark1.sql(sqlInsertQuery)
        sparkResult.write.mode("append").saveAsTable(Config.ConfigurationDb["ResultTableName"],format="hive")
        print("\n******** Done *************")
       
        JobManager.UpdateStatus(JobManager, data["JobID"], Config.JobStatus['Done'])
        #return writeResult
        return "successful"
    
    except Exception as ex:
        print("updating status")
        JobManager.UpdateStatus(JobManager, data["JobID"], Config.JobStatus['Failed'])
#f = open('fileprev.txt','w')
#f.write('bkbjkbljb')
#f.close()
#
#params = {"ODSQuery":"SELECT * FROM model1source WHERE TOTAL_ASSETS \u003e 1 AND TRADE_DEBTORS BETWEEN 1 AND 100000","ProfileID":7,"ModelID":1,"Ratios":[{"RatioID":1,"RatioName":"RT_GP_GS","Propotional":0,"Features":[{"FeatureID":28,"FeatureName":"GROSS_PROFIT","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":2,"RatioName":"RT_EXP_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":35,"FeatureName":"TOTAL_EXPENSES","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":3,"RatioName":"RT_NP_GS","Propotional":0,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":30,"FeatureName":"NET_PROFIT_OR_LOSS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":4,"RatioName":"RT_NP_TOTAST","Propotional":0,"Features":[{"FeatureID":30,"FeatureName":"NET_PROFIT_OR_LOSS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":17,"FeatureName":"TOTAL_ASSETS","FeatureDescription":"Feature 2 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":5,"RatioName":"RT_PRD_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":27,"FeatureName":"PRODUCTION_COST","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":6,"RatioName":"RT_TRCRT_PUR","Propotional":1,"Features":[{"FeatureID":34,"FeatureName":"PURCHASES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":47,"FeatureName":"TRADE_CREDITORS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":7,"RatioName":"RT_TRDR_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":18,"FeatureName":"TRADE_DEBTORS","FeatureDescription":"Feature 3 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":8,"RatioName":"RT_PUR_GS","Propotional":1,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":34,"FeatureName":"PURCHASES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":9,"RatioName":"RT_GS_TOTAST","Propotional":0,"Features":[{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":17,"FeatureName":"TOTAL_ASSETS","FeatureDescription":"Feature 2 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":10,"RatioName":"RT_GP_EXP","Propotional":1,"Features":[{"FeatureID":28,"FeatureName":"GROSS_PROFIT","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":35,"FeatureName":"TOTAL_EXPENSES","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":11,"RatioName":"INC_OPPT_B1","Propotional":1,"Features":[{"FeatureID":30,"FeatureName":"NET_PROFIT_OR_LOSS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":37,"FeatureName":"OTHER_NON_SALES_INCOME","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":12,"RatioName":"RT_STKTURN","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"COST_OF_GOODS_SOLD","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":26,"FeatureName":"OPENING_STOCK","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":38,"FeatureName":"CLOSING_STOCK","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"}]},{"RatioID":13,"RatioName":"RT_TRCRT_COS","Propotional":1,"Features":[{"FeatureID":25,"FeatureName":"COST_OF_GOODS_SOLD","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":47,"FeatureName":"TRADE_CREDITORS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]}],"Features":[{"FeatureID":16,"FeatureName":"PLANT_AND_MACHINERY","FeatureDescription":"Feature 1 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":17,"FeatureName":"TOTAL_ASSETS","FeatureDescription":"Feature 2 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":18,"FeatureName":"TRADE_DEBTORS","FeatureDescription":"Feature 3 Des","FeatureGroup":"ASSETS","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":19,"FeatureName":"LOANS_FROM_OUTSIDE_MALAYSIA","FeatureDescription":"Feature 4 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":20,"FeatureName":"TOTAL_PURCHASE_OUTSIDE_MALAYSI","FeatureDescription":"Feature 5 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":21,"FeatureName":"OTHER_PAYMENT_OUTSIDE_MALAYSIA","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":22,"FeatureName":"RECEIPT_FROM_OUTSIDE_MALAYSIA","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":23,"FeatureName":"TOTAL_SALES_OUTSIDE_MALAYSIA","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":24,"FeatureName":"PROMO_ADVERT_EXPENSE","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":25,"FeatureName":"COST_OF_GOODS_SOLD","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":26,"FeatureName":"OPENING_STOCK","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":27,"FeatureName":"PRODUCTION_COST","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":28,"FeatureName":"GROSS_PROFIT","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":29,"FeatureName":"GROSS_SALES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":30,"FeatureName":"NET_PROFIT_OR_LOSS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":31,"FeatureName":"CONTRACT_SUBCON_EXPENSE","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":32,"FeatureName":"LOAN_INTEREST_EXPENSE","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":33,"FeatureName":"NON_ALLOW_EXPENSE","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":34,"FeatureName":"PURCHASES","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":35,"FeatureName":"TOTAL_EXPENSES","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":36,"FeatureName":"OTHER_BUSINESS_INCOME","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":37,"FeatureName":"OTHER_NON_SALES_INCOME","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":38,"FeatureName":"CLOSING_STOCK","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":39,"FeatureName":"LOAN_FROM_DIRECTOR","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":40,"FeatureName":"OTHER_CREDITORS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":41,"FeatureName":"TOTAL_TAX","FeatureDescription":"Feature 1 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":42,"FeatureName":"TOTAL_TAX_LIABILITY","FeatureDescription":"Feature 2 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":43,"FeatureName":"TAX_PAYABLE_REPAYABLE","FeatureDescription":"Feature 3 Des","FeatureGroup":"INTERCOMPANY","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":44,"FeatureName":"STATE","FeatureDescription":"Feature 4 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":45,"FeatureName":"TAX_EXEMPTED_INCOME","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":46,"FeatureName":"OTHER_NON_SALES_INCOME","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":47,"FeatureName":"TRADE_CREDITORS","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"},{"FeatureID":48,"FeatureName":"IT_ASSM_BRANCH","FeatureDescription":"Feature 5 Des","FeatureGroup":"EXPENSE","FeatureDataType":"numeric","TableName":"model1source"}]}
#runModel(params)
#f = open('file.txt','w')
#f.write('bkbjaasdasdasdkbljb')
#f.close()
##TODO: NUll Check Logic if numerator is greater than zero
#df_pandas.loc[(df_pandas['RT_TRCRT_PUR'].isnull()) & (df_pandas['trade_creditors'] > 0),'RT_TRCRT_PUR_bin']=5
