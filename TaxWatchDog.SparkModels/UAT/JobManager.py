# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 13:00:08 2019

@author: abdulraheem.abid
"""
import MySQLdb as mysql
#import mysql.connector
import SparkModels.Config as Config

class JobManager():
    
    def GetConfigurationDb():
        mydb = mysql.connect(
                    host=Config.ConfigurationDb["Host"],
                    port=3306,
                    user=Config.ConfigurationDb["UserId"],
                    passwd=Config.ConfigurationDb["Password"]
                    )
        return mydb
    
    def UpdateStatus(self, jobId, status):
        try:
            db = self.GetConfigurationDb()
            cursor = db.cursor()
            cursor.execute("Use " + Config.ConfigurationDb["Database"])
            sqlQuery = "Update job set JobStatus = '" + str(status) + "' where JobId = " + str(jobId)
            cursor.execute(sqlQuery)
            db.commit()
            print("updated status to " + str(status))
            return True
        except Exception as ex:
            print(ex)
            return False
        
    def GetJobStatus(self, jobId):
        try:
            db = self.GetConfigurationDb()
            cursor = db.cursor()
            cursor.execute("Use " + Config.ConfigurationDb["Database"])
            sqlQuery = "Select JobStatus from job where JobId = " + jobId
            cursor.execute(sqlQuery)
            return cursor
        except:
            return False

