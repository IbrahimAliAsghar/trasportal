from flask import Flask, jsonify
from flask import request
#from flask import json
#import SparkModels.Model_1_demo_1_itref as model1
#import SparkModels.Model_1_uat as model1
import SparkModels.Model_1_uat_test as model1
#import SparkModels.Model_2_uat as model2
import SparkModels.Model_2_uat_test as model2
#import SparkModels.Model_3_uat as model3
import SparkModels.Model_3_uat_test as model3
#import SparkModels.Model_4_uat as model4
import SparkModels.Model_4_uat_test as model4
#from SparkConnection import SparkConnection
import SparkModels.Config

app = Flask(__name__)

@app.route('/RunModel', methods=['POST'])

def RunModel():
    #print("hello World")
    #params ="abs"
    try:
    	params = request.get_json()
    except Exception as e:
        print("in exception")
        print(str(e))

 
    print(params)
    if(params["ModelID"] == 1 or params["ModelID"] == '1'):
        result = model1.runModel(params)
    elif (params["ModelID"] == 2 or params["ModelID"] == '2'):
        result = model2.runModel(params)
    elif (params["ModelID"] == 3 or params["ModelID"] == '3'):
        result = model3.runModel(params)
    elif (params["ModelID"] == 4 or params["ModelID"] == '4'):
        result = model4.runModel(params)
    else:
        result = "model " + params["ModelID"] + " not available"
    print(result)
    return jsonify(result)

    #if __name__ == '__main__':
    #app.run(debug=Config.API['DebugMode'])
    #app.run()
