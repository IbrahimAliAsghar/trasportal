##TODO: make spark connection
def runModel(params):
    import Config
    import findspark
    findspark.init(Config.Spark['Location'])
    import pyspark
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.getOrCreate()
    import pandas as pd
    sc = spark.sparkContext
    sc
    
    
    from pyspark import SparkContext
    from pyspark.sql import SQLContext
    from datetime import date
    sql_sc = SQLContext(sc)
    
    ##TODO: Ratios Class
    data = params
    
    
    class Ratios(object):
        #df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load('E:\\LHDN Docs\\Data\\model1.csv')
        def Add_RT_ADVRT_GS(self):
            df1=df.withColumn('RT_ADVRT_GS', (df['promo_advert_expense'] / df['gross_sales']))
            return df1
        def Add_RT_BADDBT_GS(self):
            df1=df.withColumn('RT_BADDBT_GS', (df['bad_debt'] / df['gross_sales']))
            return df1
        def Add_RT_CAST_TA(self):
            df1=df.withColumn('RT_CAST_TA', (df['total_current_assets'] / df['total_assets']))
            return df1
        def Add_RT_CLST_CAST(self):
            df1=df.withColumn('RT_CLST_CAST', (df['closing_stock'] / df['total_current_assets']))
            return df1
        def Add_RT_COM_GS(self):
            df1=df.withColumn('RT_COM_GS', (df['commission'] / df['gross_sales']))
            return df1
        def Add_RT_EXP_GS(self):
            df1=df.withColumn('RT_EXP_GS', (df['total_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_FA_TOTAST(self):
            df1=df.withColumn('RT_FA_TOTAST', (df['total_fixed_assets'] / df['total_assets']))
            return df1
        def Add_RT_GP_GS(self):
            df1=df.withColumn('RT_GP_GS', (df['gross_profit'] / df['gross_sales']))
            return df1
        def Add_RT_GP_TOTAST(self):
            df1=df.withColumn('RT_GP_TOTAST', (df['gross_profit'] / df['total_assets']))
            return df1
        def Add_RT_GP_EXP(self):
            df1=df.withColumn('RT_GP_EXP',(df['gross_profit'] / df['total_expenses']))
            return df1
        def Add_RT_GS_TOTAST(self):
            df1=df.withColumn('RT_GS_TOTAST', (df['gross_sales'] / df['total_assets']))
            return df1
        def Add_RT_LIQUIDITY(self):
            df1=df.withColumn('RT_LIQUIDITY', (df['total_current_assets'] / df['total_current_liability']))
            return df1
        def Add_RT_LOAN_EQY(self):
            df1=df.withColumn('RT_LOAN_EQY', (df['loans'] / df['total_equity']))
            return df1
        def Add_RT_NAEXP_TEX(self):
            df1=df.withColumn('RT_NAEXP_TEX', (df['non_allow_expense'] / df['total_expenses']))
            return df1
        def Add_RT_NALW_GS(self):
            df1=df.withColumn('RT_NALW_GS', (df['non_allow_expense'] / df['gross_sales']))
            return df1
        def Add_RT_NP_FA(self):
            df1=df.withColumn('RT_NP_FA', (df['net_profit_or_loss'] / df['total_fixed_asset']))
            return df1
        def Add_RT_NP_GS(self):
            df1=df.withColumn('RT_NP_GS', (df['net_profit_or_loss'] / df['gross_sales']))
            return df1
        def Add_RT_NP_INT(self):
            df1=df.withColumn('RT_NP_INT', (df['net_profit_or_loss'] / df['loan_interest_expense']))
            return df1
        def Add_RT_NP_TOTAST(self):
            df1=df.withColumn('RT_NP_TOTAST', (df['net_profit_or_loss'] / df['total_assets']))
            return df1
        def Add_RT_OTHCR_GS(self):
            df1=df.withColumn('RT_OTHCR_GS', (df['other_creditors'] / df['gross_sales']))
            return df1
        def Add_RT_OCR_TOTEX(self):
            df1=df.withColumn('RT_OTHCR_GS', (df['other_creditors'] / df['total_expenses']))
            return df1
        def Add_RT_OTHEXP_GS(self):
            df1=df.withColumn('RT_OTHEXP_GS', (df['other_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_OTHINC_TI(self):
            df1=df.withColumn('RT_OTHINC_TI', (df['other_non_sales_income'] / df['total_income']))
            return df1
        def Add_RT_PRD_GS(self):
            df1=df.withColumn('RT_PRD_GS', (df['production_cost'] / df['gross_sales']))
            return df1
        def Add_RT_PUR_GS(self):
            df1=df.withColumn('RT_PUR_GS', (df['purchases'] / df['gross_sales']))
            return df1
        def Add_RT_RND_GS(self):
            df1=df.withColumn('RT_RND_GS', (df['r_d_expense'] / df['gross_sales']))
            return df1
        def Add_RT_RND_TEX(self):
            df1=df.withColumn('RT_RND_TEX', (df['r_d_expense'] / df['total_expenses']))
            return df1
        def Add_RT_ROYLTY_GS(self):
            df1=df.withColumn('RT_ROYLTY_GS', (df['royalty_expense'] / df['gross_sales']))
            return df1
        def Add_RT_CSUBC_GS(self):
            df1=df.withColumn('RT_CSUBC_GS', (df['contract_subcon_expense'] / df['gross_sales']))
            return df1
        def Add_RT_TOTEX_OCR(self):
            df1=df.withColumn('RT_TOTEX_OCR', (df['total_expenses'] / df['other_creditors']))
            return df1
        def Add_RT_TOTEXP_GS(self):
            df1=df.withColumn('RT_TOTEXP_GS', (df['total_expenses'] / df['gross_sales']))
            return df1
        def Add_RT_TRAVEL_GS(self):
            df1=df.withColumn('RT_TRAVEL_GS', (df['travelling_expense'] / df['gross_sales']))
            return df1
        def Add_RT_AV_STK(self):
            df1=df.withColumn('RT_AV_STK', (df['opening_stock'] + df['closing_stock'])/2)
            return df1
        def Add_RT_CAP(self):
            df1=df.withColumn('RT_CAP', (df['total_current_assets'] - df['total_current_liability']))
            return df1
        def Add_RT_DEBT_EQY(self):
            df1=df.withColumn('RT_DEBT_EQY', ((df['other_creditors'] + df['loan_from_director'] + df['other_current_liability'])/df['total_equity_liability']))
            return df1
        def Add_RT_FA_TOTPRD(self):
            df1=df.withColumn('RT_FA_TOTPRD', (df['total_fixed_assets'] / (df['purchases'] + df['production_cost'])))
            return df1
        def Add_RT_PURPRD_GS(self):
            df1=df.withColumn('RT_PURPRD_GS', ((df['purchases'] + df['production_cost']) / df['gross_sales']))
            return df1
        def Add_RT_TRDR_GS(self):
            df1=df.withColumn('RT_TRDR_GS', (df['trade_debtors'] / (df['gross_sales'] * 365)))
            return df1
        def Add_RT_WAGES_GS(self):
            df1 = df.withColumn('RT_WAGES_GS', df["salary_wages"] / df["gross_sales"])
            return df1
        def Add_RT_CA_GS(self):
            df1 = df.withColumn('RT_CA_GS', df["BUSINESS_INCOME_FACT.cap_allowance"] / df["gross_sales"])
            return df1
        def Add_RT_CA_TOTFA(self):
            df1 = df.withColumn('RT_CA_TOTFA', df["BUSINESS_INCOME_FACT.cap_allowance"] / df["total_fixed_assets"])
            return df1
        def Add_RT_CASH_TCL(self):
            df1 = df.withColumn('RT_CASH_TCL', df["cash_in_hand"] / df["total_current_liability"])
            return df1
        def Add_RT_COGS_GS(self):
            df1 = df.withColumn('RT_COGS_GS', df["cost_of_goods_sold"] / df["gross_sales"])
            return df1
        def Add_RT_COM_SINC(self):
            df1 = df.withColumn('RT_COM_SINC', df["commission"] / df["INCOME_FACT.aggr_stat_bus_inc"])
            return df1
        def Add_RT_FA_TOTINC(self):
            df1 = df.withColumn('RT_FA_TOTINC', df["total_fixed_assets"] / (df["gross_sales"]+df["other_business_income"]+df["other_non_sales_income"]))
            return df1
        def Add_RT_GS_TOTFA(self):
            df1 = df.withColumn('RT_GS_TOTFA', df["gross_sales"] / df["total_fixed_assets"])
            return df1
        def Add_RT_INT_GS(self):
            df1 = df.withColumn('RT_INT_GS', df["loan_interest_expense"] / df["gross_sales"])
            return df1
        def Add_RT_LTLIB_EQY(self):
            df1 = df.withColumn('RT_LTLIB_EQY', df["long_term_liability"] / df["total_equity_liability"])
            return df1
        def Add_RT_NAINT_TEX(self):
            df1 = df.withColumn('RT_NAINT_TEX', (df["total_investment"] + df["loan_to_director"]) / (df["total_liabilities"] - df["trade_creditors"]) * df["loan_interest_expense"])
            return df1
        def Add_RT_STKTURN(self):
            df1 = df.withColumn('RT_STKTURN', df["cost_of_goods_sold"] / ((df["opening_stock"] + df["closing_stock"]/2)))
            #due to mismatch formulas in Financial_Ratios and TACS_Features_Mapping_Document_v3.0 [excel sheet]
            #df1 = df.withColumn('RT_STKTURN', 365/(df["cost_of_goods_sold"] / ((df["opening_stock"] + df["closing_stock"])/2)))
            return df1
        def Add_RT_TINE_TINC(self):
            df1 = df.withColumn('RT_TINE_TINC', df["TAX_ASSM_FACT.total_claim"] / df["total_income"])
            return df1
        def Add_RT_TINE_NPNL(self):
            df1 = df.withColumn('RT_TINE_NPNL', df["TAX_ASSM_FACT.total_claim"] / df["net_profit_or_loss"])
            return df1
        def Add_RT_TOTCL_EQY(self):
            df1 = df.withColumn('RT_TOTCL_EQY', df["total_current_liability"] / df["total_equity_liability"])
            return df1
        def Add_RT_TRCRT_PUR(self):
            df1 = df.withColumn('RT_TRCRT_PUR', df["trade_creditors"] / df["purchases"])
            return df1
        def Add_RT_GS_CAP(self):
            df1 = df.withColumn('RT_GS_CAP', df["gross_sales"] / df(["total_current_assets"] - df["total_current_liability"]))
            return df1
        def Add_INC_OPPT_B1(self):
            df1 = df.withColumn('INC_OPPT_B1', df["net_profit_or_loss"] - df["other_non_sales_income"])
            return df1
        def Add_RT_TRCRT_COS(self):
            df1 = df.withColumn('RT_TRCRT_COS', df["trade_creditors"] - df["cost_of_goods_sold"])
            return df1
    
    ##TODO: Binning of features
    
    import math
    def binning(df):
        features2 =df
        for i in df.columns:
            features2 = features2.sort_values(by=[i])
            features2 =features2.reset_index(drop=True)
            bins = [features2[i][0]-1,features2[i][math.ceil(len(features2)/4)],features2[i][math.ceil(len(features2)/2)],features2[i][math.ceil((len(features2)/4 *3))],features2[i][len(features2)-1]+1]
            #bin_set = set(bins)
            unique_bin = len(set(bins))
            
            labels =[]
            if unique_bin > 1:
                for j in range(len(set(bins))-1):
                    labels.append(j+1)
                a = str(i)
                features2[a +"_Bin"]= pd.cut(features2[i], bins=bins, labels=labels,duplicates ='drop')
            else:
                features2[a +"_Bin"]= 1
        features2 = features2.apply(pd.to_numeric)
        return features2
    
    
    ##TODO: reading csv ,conversion of df ,adding ratios to df
    
    #df = sql_sc.read.format(source="com.databricks.spark.csv").options(header = True,inferSchema = True).load('C:/Users/hafsa.lutfi/Desktop/LHDN/datasetcsv/model4_test.csv')
    #df = df.filter(df['assessment_year'] <= date.today().year)
    
    from pyspark import SparkConf
    from pyspark.sql import SparkSession, HiveContext
    sparkSession = (SparkSession
                .builder
                .appName('example-pyspark-read-and-write-from-hive')
                .enableHiveSupport()
                .getOrCreate())
    SparkContext.setSystemProperty("hive.metastore.uris", "thrift://" + Config.Hive["Server"])
    sparkSession.sql('use ' + Config.Hive["Database"])
    df = sparkSession.sql(data["ODSQuery"])
    
    
# =============================================================================
#     import mysql.connector
#     mydb = mysql.connector.connect(
#       host=Config.Mysql["Host"],
#       user=Config.Mysql["UserId"],
#       passwd=Config.Mysql["Password"]
#     )
#     mycursor = mydb.cursor()
#     mycursor.execute("Use modelsource")
#     mycursor.execute("Select * from model4source limit 100")
#     df = mycursor.fetchall()
#     df = spark.createDataFrame(df)
#     colNames =  [i[0] for i in mycursor.description]
#     
#     loopCounter = 0
#     for name in df.columns:
#         df = df.withColumnRenamed(name, colNames[loopCounter])
#         loopCounter = loopCounter + 1
# =============================================================================
    
    orginalDfPandas = df.toPandas()
        
    feature_list_withoutKeys = [] 
    exludeColList = ["business_key", "business_code", "branch_key", "assessment_key", "tax_payer_key"]
    df_features= df
    df_bin = df_features.toPandas()
    
    for col in df_bin.columns:
        if col not in exludeColList:
            feature_list_withoutKeys.append(col)
    
    binned_df = binning(df_bin[feature_list_withoutKeys])
    df = spark.createDataFrame(binned_df)
    ratios = Ratios()
    fields =[]
    #for r in data["Ratios"]:
        #fields.append(r["RatioName"])
    fields=['RT_AV_STK','RT_COGS_GS','RT_GP_GS','RT_NP_GS','RT_PRD_GS','RT_STKTURN','RT_NP_TOTAST']
    #fields=['RT_AV_STK','RT_COGS_GS','RT_GP_GS','RT_NP_GS','RT_PRD_GS','RT_STKTURN','RT_NP_TOTAST']
    for f in fields:
        df=getattr(ratios, 'Add_%s' % f)()
    #df.show(1)
    
    
    ##TODO: calculate year function
    
    from pyspark.sql.types import IntegerType
    from pyspark.sql.functions import udf ,lit
    def Create_TR_NBIN_YRM(x,y,i):
        if (y == date.today().year -i ):
            x=x
        else:
            x=None
        return x
    func_udf = udf(Create_TR_NBIN_YRM, IntegerType())
    
    
    ##TODO: calling year function of 7 different features [cal_cols based on year]
    Cal_col_list ={"TR_PNM_YM":"plant_and_machinery","TR_OPSTK_YM":"opening_stock","TR_NPNL_YM":"net_profit_or_loss","TR_GP_YM":"gross_profit","TR_GS_YM":"gross_sales","TR_COGS_YM":"cost_of_goods_sold","TR_CLSTK_YM":"closing_stock"}
    year_list = df.select(df['assessment_year']).distinct().orderBy(df['assessment_year'].desc()).limit(6).collect()
    for key in Cal_col_list:
        for i in range(len(year_list)):
            df=df.withColumn(key+str(i+1),func_udf(df[Cal_col_list[key]],df.assessment_year,lit(i+1)))
            
    print(len(df.columns))
    #127
    
    ##TODO: calculating columns [based on features only] 'TACS Feature Group Name'->[ASSET_RT , NPNL - ,EXPENSE_RT –]
    df=df.withColumn('AST_CASH_B1', df["cash_in_hand"] + df["cash_in_bank"])
    df=df.withColumn('RT_TOTEXP_GS', df["total_expenses"] / df["gross_sales"])
    df=df.withColumn('RT_FA_TOTAST', df["total_fixed_assets"] / df["total_assets"])
    df=df.withColumn('RT_GS_TOTFA', df["gross_sales"] / df["total_fixed_assets"])
    df=df.withColumn('RT_ADVRT_GS', df["promo_advert_expense"] / df["gross_sales"])
    df=df.withColumn('RT_CSUBC_GS', df["contract_subcon_expense"] / df["gross_sales"])
    df=df.withColumn('RT_NAEXP_TEX', df["non_allow_expense"] / df["total_expenses"])
    df=df.withColumn('RT_NP_INT', df["net_profit_or_loss"] / df["loan_interest_expense"])
    df=df.withColumn('RT_OEX_TOTEX', df["other_expenses"] / df["total_expenses"])
    df=df.withColumn('RT_OTHEXP_GS', df["other_expenses"] / df["gross_sales"])
    df=df.withColumn('RT_WAGES_GS', df["salary_expense"] / df["gross_sales"])
    
    print(len(df.columns))
    #138
    
    ##TODO: ratios binning 
    def Binnig_ratios(x):    
        if x is None:
            return 0
        elif x <= float(diff)/4:
            return 1
        elif (x > float(diff)/4 and x <= float(diff)/2):
            return 2
        elif (x > float(diff)/2 and x <= float(diff)* 3/4) :
            return 3
        elif (x > float(diff)* 3/4):
            return 4
    func_udf1 = udf(Binnig_ratios, IntegerType())
    
    ##TODO: calling function of ratios binning 
    
    df1=df
    breakCounter = 0
    for ratio in fields:
          while True:
            try:
                print(ratio)
                df1 = df1.withColumn(str(ratio)+ '_norm', (df1[ratio]-df1.agg({ratio: "min"}).collect()[0][0])/(df1.agg({ratio: "max"}).collect()[0][0]-df1.agg({ratio: "min"}).collect()[0][0]))
                print("finished norm")
                diff= df1.agg({str(ratio)+ '_norm': "max"}).collect()[0][0] - df1.agg({str(ratio)+ '_norm': "min"}).collect()[0][0]
                print("finished norm 2")
                df1=df1.withColumn(str(ratio)+ '_Bin',func_udf1(df1[ratio]))
                print("finished bin")
            except:
                if breakCounter > 5:
                    break
                breakCounter = breakCounter + 1
                continue
            break
    
    print(len(df1.columns))
    #152 norm and bin cols of ratios
    
    ##TODO: function AssignWeigtagesFeature
    feature_list = df_features[feature_list_withoutKeys].columns
    def AssignWeigtagesFeature(x,y,z):
        if x== 0:
            k =None
            return k
        else:  
            if z==1:
                if x ==1:
                    k= y* 10
                    return k
                elif x==2:
                    k= y* 25
                    return k
                elif x==3:
                    k= y* 50
                    return k
                elif x==4:
                    k= y* 100
                    return k
            else:
                if x ==1:
                    k= y* 100
                    return k
                elif x==2:
                    k= y* 50
                    return k
                elif x==3:
                    k= y* 25
                    return k
                elif x==4:
                    k= y* 10
                    return k
            
    func_udf2 = udf(AssignWeigtagesFeature, IntegerType())
    
   
    ##TODO:creating ratio feature dictionary with proportionality
    ratio_feature_dict ={}
    
    temp = []
    #for ratios in data["Ratios"]:
        #ratio_feature_dict[ratios["RatioName"]] = [str(ratios["Propotional"])]
        #for features in ratios["Features"]:
            #ratio_feature_dict[ratios["RatioName"]].append(features["FeatureName"])
    """
    for ratio_feature_dict inverse is considered 1 and direct is considered 0
    """        
    ratio_feature_dict ={'RT_AV_STK':['1','opening_stock','closing_stock'],'RT_COGS_GS':['1','cost_of_goods_sold','gross_sales'] ,'RT_GP_GS':['0','gross_profit','gross_sales'],'RT_NP_GS':['0','net_profit_or_loss','gross_sales'],'RT_PRD_GS':['1','production_cost','gross_sales'] ,'RT_STKTURN':['1','cost_of_goods_sold','opening_stock','closing_stock'],'RT_NP_TOTAST':['0','net_profit_or_loss','total_assets']}
    
    ##TODO:direct and indirect feature
    
    all_features=[]
    direct_features=[]
    inverse_features=[]
    for key in ratio_feature_dict:
        for i in range(1,len(ratio_feature_dict[key])):
            if len(ratio_feature_dict[key]) >0:
                all_features.append(ratio_feature_dict[key][i])
                if ratio_feature_dict[key][0] == '0':
                    direct_features.append(ratio_feature_dict[key][i])
                else:
                    inverse_features.append(ratio_feature_dict[key][i])
                    
    direct_features_copy = direct_features.copy()
    inverse_features_copy = inverse_features.copy()
    
    direct_features =list(set(direct_features))  
    inverse_features =list(set(inverse_features)) 
    print(direct_features,inverse_features)
    
    ##TODO: features that aren't present in direct and indirect feature
    
    same_list =[]
    direct_features1 = direct_features.copy()
    direct_features1.extend(inverse_features)
    mergelist = list(set(direct_features1))
    
    for f in feature_list:
        if f not in mergelist:
            same_list.append(f)
    same_list
    
    ##TODO: direct and indirect feature frequency
    DirectFeatureFrequency = {}
    for feature in direct_features:
        DirectFeatureFrequency[feature] = direct_features_copy.count(feature)
        
    InverseFeatureFrequency = {}
    for feature in inverse_features:
        InverseFeatureFrequency[feature] = inverse_features_copy.count(feature)
    
    print(DirectFeatureFrequency)
    print(InverseFeatureFrequency)
    
    #df1 all except scored features (_score 39 features)
    temp = df1
    temp.columns
    
    AllFeaturesFrequency = {}
    for feature in all_features:
        AllFeaturesFrequency[feature] = all_features.count(feature)
    print(AllFeaturesFrequency)
    
    ##TODO:score of direct and indirect feature 
    temp = df1
    for feature in direct_features:
        count = 0
        for ratio in fields:
            if feature in ratio_feature_dict[ratio] and "0" in ratio_feature_dict[ratio]:
                if DirectFeatureFrequency[feature]==1:
                    temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_Bin"],temp[feature],lit(0)))
                else:
                    temp = temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_Bin"],temp[feature],lit(0)))
                    count=count+1
    
    
    for feature in inverse_features:
        if feature in direct_features:
            count = DirectFeatureFrequency[feature]
        else:
            count = 0
        for ratio in fields:
            if feature in ratio_feature_dict[ratio] and "1" in ratio_feature_dict[ratio]:
                    if InverseFeatureFrequency[feature]==1:
                        #print(feature + "_score"+str(count))
                        temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_Bin"],temp[feature],lit(1)))
                    else:
                        #print(feature + "_score"+str(count))
                        temp=temp.withColumn(feature + "_score"+str(count),func_udf2(temp[ratio+ "_Bin"],temp[feature],lit(1)))
                        count=count+1
    
    for i in mergelist:
        temp = temp.withColumn("sum_"+i,sum(temp[i+"_score"+str(j)].cast("float") for j in range(AllFeaturesFrequency[i]))) 
    
    
    for i in mergelist:
        temp = temp.withColumn("Final"+i+"_score",temp["sum_"+i]/AllFeaturesFrequency[i])
    
    Group_dict = {"ASSETS":"1","EXPENSE":"1","INCOME":"0","ASSET_RT":"1","TAX":"0","LIABILITY":"1","EXT_INCOME":"1","STOCK":"1","INTERCOMPANY":"1","PNL":"1","INFORMATIONAL":"1","KEY_DERIVATV":"0","DEFAULT":"1"}
    for feature in same_list:
        a=0
        for item in data["Features"]:
            if item["FeatureName"] == feature.upper():
                #print("feature: "+feature.upper())
                #ss=item["FeatureGroup"]
                feature_group=item["FeatureGroup"]
                #print("ss: "+ss)
                a=int(Group_dict[feature_group])
                #a=int(Group_dict[ss])
                #print("A: "+str(a))
                break
        #print(a)
        temp=temp.withColumn("Final"+feature + "_score",func_udf2(temp[feature+ "_Bin"],temp[feature],lit(a)))
   
    #final_score columns present in temp df
    Final_score_columns=[]
    for names in temp.columns:
        if "Final" in names:
            Final_score_columns.append(names) 
    
    temp_score = temp.select(Final_score_columns)
    print(len(temp_score.columns))
    #33
    #casting final score columns with float
    for i in Final_score_columns:
        temp_score = temp_score.withColumn("float_"+i, temp_score[i].cast("float"))
    temp_score
    
    #selecting columns that have float datatype and starts with float
    Float_Final_score_columns = []
    for names in temp_score.columns:
        if "float" in names:
            Float_Final_score_columns.append(names) 
    Float_Final_score_columns
    #saving final_selected float columns in final_score and replacing null values with 0
    final_score = temp_score.select(Float_Final_score_columns) #33 cols
    final_score = final_score.na.fill(0)
    print(len(final_score.columns))
    
    final = final_score.toPandas()
    
    ##TODO:normalize
   
    from sklearn import preprocessing
    floatcolnames = []
    for name in final.columns:
        if "float" in name:
            floatcolnames.append(name)
    
    scored= final[floatcolnames].values #returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    scored_scaled = min_max_scaler.fit_transform(scored)
    final1 = pd.DataFrame(scored_scaled)
    features = floatcolnames
    string = '_norm'
    new_feature_list = [x + string for x in features]
    print(new_feature_list)
    
    final1.columns = new_feature_list
    ##TODO:range
    df1 = final1
    for col in df1.columns:
        df1[str(col)+'_Range'] = (df1[col] *100)
    
    ##TODO:ranking    
    norm_range_columns = []
    for names in df1.columns:
        if "_norm_Range" in names:
            norm_range_columns.append(names) 
    norm_range_columns
    
    for col in norm_range_columns:
        #print(col)
        df1[str(col) + '_Ranked'] = df1[col].rank(ascending= False,method = 'dense')
    
    data1 = [final,final1]
    data1 = pd.concat(data1,axis=1)
    data1 = data1.astype(float)
    mainDfPandas = df.toPandas()
    data2 = [mainDfPandas, data1]
    data2 = pd.concat(data2,axis=1)
    ranks_score_cols_names=[]
    for names in data2.columns:
        if "_norm_Range_Ranked" in names:
            ranks_score_cols_names.append(names)
        if  "_score" in names: 
            if "_norm" not in names:
                ranks_score_cols_names.append(names)
    cols_to_select= feature_list + fields + ["assessment_year"] + ranks_score_cols_names
    
    final_df_to_write = data2[cols_to_select]
    #data2 = data2.astype(float)
    ##TODO: Change pandas df into spark df
    #sparkdf = spark.createDataFrame(data2)
    #val = spark_df.withColumn("assessment_year", df.assessment_year.cast("Int"))
    import numpy as np
    counter = 0
    sqlInsertQuery = "INSERT INTO modelresult_abt(feature, profile_id, model_id, value,score,ranks,year_d, taxpayer) VALUES"
    final_df_to_write = final_df_to_write.astype(object).replace(np.nan, 'Null')
    for finalRow in final_df_to_write.iterrows():
        if counter < len(final_df_to_write):
            finalRow = finalRow[1]
            for feature in feature_list:
                 if "assessment_year" not in feature:
                     sqlInsertQuery += "('" + feature + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[feature]) + "," + str(finalRow['float_Final' + feature + '_score']) + ", " +  str(finalRow['float_Final' +feature + '_score_norm_Range_Ranked']) + ", " +  str(finalRow['assessment_year'].iloc[0]) + "," + str(orginalDfPandas.loc[counter, 'tax_payer_key']) + "),"
            for ratio in fields:
                 if "assessment_year" not in ratio:
                     sqlInsertQuery += "('" + ratio + "'," + str(data['ProfileID'])  + "," + str(data['ModelID']) + ", " + str(finalRow[ratio]) + "," + "Null" + ", " +  "Null" + ", " +  str(finalRow['assessment_year'].iloc[0]) + "," + str(orginalDfPandas.loc[counter, 'tax_payer_key']) + "),"
                    
        counter = counter + 1
        print(counter)
    sqlInsertQuery = sqlInsertQuery[:-1]
    
# =============================================================================
#     print(sqlInsertQuery)
#     mycursor = mydb.cursor()
#     mycursor.execute(sqlInsertQuery)
#     mydb.commit()
# =============================================================================
    writeResult = sparkSession.sql(sqlInsertQuery)
    
    return writeResult