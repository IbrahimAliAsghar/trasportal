CREATE DATABASE  IF NOT EXISTS `twdwebdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `twdwebdb`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: twdwebdb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__migrationhistory`
--

DROP TABLE IF EXISTS `__migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__migrationhistory` (
  `MigrationId` varchar(150) NOT NULL,
  `ContextKey` varchar(300) NOT NULL,
  `Model` longblob NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`,`ContextKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__migrationhistory`
--

LOCK TABLES `__migrationhistory` WRITE;
/*!40000 ALTER TABLE `__migrationhistory` DISABLE KEYS */;
INSERT INTO `__migrationhistory` VALUES ('201902062001207_AutomaticMigration','TaxWatchDog.Models.DBContext.Migrations.Configuration','�\0\0\0\0\0\0\�]oܸ�@���\�\"\�ur(\��or�5�}Yߵ}2\������6�Q����?���DI�J��v]��(��E\�h\��\��s��\�6\r���L�\�<|}r\Z([\�q�m\�\�}��\�\�\��\�o\�>\�ۧ\����o\�{�gV���U�{�X�\�G��ʓm�.�2�N\��v\��\�\�\�\��_/bXAp�e�U\�\�?�ϋ<[�]��\�\�y�Ғ>\�-�\ZjpmQ��\�\�<����U\�\�e�9i\�>Y�\�\0*�T���4�0N+�>�A�eyU\�?�hUy�Y\���(�}\�!�\�C���\�\�m��\�NߐI-��-�����!�\�\�R*-\�\�V�;*b:~����ɬkZ��\�\�U�?��o\�@\�\�EZ�wAj3=_r��NJ�0����}Z\�t��}UD\�\�f�&\�?�\�\��\�(;\��i\�b��\�m\��\�\�w�����y�\�0X� \"�ܽ��eV}�&�0J\�}�:a������2TD�o��B\��e�j\"K�\�bQ,\�\���\�#\�bj���\r�4 �Z`XQ����\�\�\�\�6\�\�y����\��\�\'�\�Y��\�T{\����\\ɮ�l\�c^E_�M\�(�\�a��uc��\�\Z;�\n�]�\�\�\"\�~\�S^F궻U�/քT�\�ۨؠ�G\�l\�\�ߠV6\��#\�r`=l%\�T�4��摱&t2P6�F-]-ɇm���\�I\�\�h]]\���D�g\�y��%����\�\�^\�����3MN*N@�8\�r`\'(بx\�o.\'\�M��is�#��\rL{�\�63]]9}�g�\�7˦��5\��\"\�\�f�_TE����D��?\���\�{X516jI�\�e|�p��$g�1ޗs�4�R\�qn�\"\�N0#\�\��|H\�^\�a|\��~{\�<��Gl�\��\�HGi�\�)\�\�FW��s\�+r��Y\�\�\�@fA�z��i��\�\�_̀\�|RW��+KT�[,]CQ\���M���E��/0	&�\�*��:l\�\�\�\�E�1�T\�jK����?��F�j�\�}k�\��\�\�\Z$�ϷBV\�* ׂ�q\�s%.\�A8J�\��\���OgbM�M\�\�o\�Ў+���9p4�\���^\��I\�fw�� �],�68�\�i�*�\�g�C�d�v��v�U�C���\�J���b�Z+�\�h�o�.\�5\r<\��X6\�\�8\�w\�\�\���6:�ފ�q\�]O�=+\�,\�����tL��\�i\�n�\Z\�\�=Q�\�J~>�V�Sa\�~.d�m|��\�+\0Fu�\Z��\�Ęb�?_\'5&~�&��>dq���ژ�v\Z\�2`HvX\��\�\�\�\�\�k�D �y`��<�߉\�c&2<?)\'D��:A�G�˵\Z\�p40k���C\�F\\gK��\n$\�F��\�u\�\Z���O�YA\�\�(��r�\rU�U�\rJ�u��\�̅~\�$�n�e�v(#\�g�\'n\�w�$���\�	n�J0T>i/4�a�^\nW��ڹ$���;�SĠz��/��G�\�(xx\��֑kEp\�H�@F8>�Vq\�\�1�P\�p{Y�\�z&�\r�\�\�m6��T%	\�w\�^\�}�����K�l�\r&	T�D!�=\�2Z��cS.ZPpCK1\�\�B�D�\r�hh\��H�\r�v\�\Z�\0\�]M\�N\�(Z�\�o��\�6��\�ηq�\�\�;�\�����\�\��;Z#�B�4\ZRҰ��>�B�\�S����	�I-x`d�0��#\�	1\�\�\rwF�S��!t�<��1�\0\��`�3��0�n	R\�\�`t������tI\�Z3<\�V\����%f��Q\�\Z(0����\�@󂜑.\�c�*ʌRH;��(\n�\�qs\�OHc��\Zy\�\�Q�8��h`�����Sӂ\�\�e�C���\�Ϡ\�+���\�>�5q\��K\��D4bڬ]�?\��kP\�a�*-P��\���̧�z.�T��\�q\Zu�l\�5t`��E6�)+>\�ʓ\�p�\\*րK+\�\0i������ux�D���\�iy!�\�[�0��F�\�\�T���\�%hz��\�2\0\�\�$\���\�\0\0_L\�s�\0\�9�\��c[\�wumg�\�\��\�l��\�\�s�\�%ن�/�>	V\�e	߬\�\�\�60k����؍T\�E�AB+9���IQV\�d\�}D>\�^\�[\�5�\�T\�\�\��@GRf[�Ko���\�\�G\�\�#8�S�;R`�lIt=q\�>\�\��\�\"J�b���<\�o3�I�N5\�6\�S�H\�P�+X8�S}H�\��s�ܡ<0���l!0U\n K�$\�\�y	Ւ\�\�Vx\\h\�!�p�iŪ?�/\�i��A\�$�\�\Z�\�Rҽ�٬\�)z}�C8#\�\���F�\�eߛ ױsA���\�\���Ϸ�A�-Z��h�\�\�ݽ񩍿��J\�SEg&Ö%��,�m\\�1<\�2�C�\����\�\�\�7�lר�9\�\�Bd�6Ѐ�7M �zs-\0{��KOq��σ����\�=4�\�� ���`\�R\�=4�\�� ���F��`�7	T\�\�4�P\�SEi\�2Ki\�g\rX�Tr\r��#�2�h���XV��\�?5�\�\�1a\�dwO\��œ�\�\�Yh3\�dr&X�t�	�P0�}����e\�Gƒ.�d^|\�h\�V�5ě\�?�h\�,E�c�\n�\�\��\�3���pw\�E\�Y\���x��t(�\\�\�\�\�;gS/,���\�\��;\�9\��:\�rtH��\�-\�\r\�2�xŜC� �\�ณ�\�7\r\�Z^\�˖�9u\�i�\�\�\�\�,B85��/�2\�ۏ\�\�l�\�R\�gP�\��we�(F\�S��3�!�<�\�\��A�0�\�\��,\�y\�\�,�\�\�\�\�!暙\�\�n�q[�p��&\�{���CDF2��i\�g�p���\�;\�����\��h\�6.\0YY�,B\�ݨ�Q����i���u��\�y;K�\"�\�\�i�b_S\�4\�2[�Dg0�\�M\���R��g#Hm_-�c�A|�\�1��|���1�_x\�\�HC\�\�ÞWΧ����=���y��^Ŵ�IM5�\�M\'Gj�	\\�\�1I��\�\��tHٴ\�+��N�t��lZ�\�:^�LJmm^	<��IL\�Z�������ϋ4AYտ�9ʒTV\�MO\�\�O� \�.;�:b����/.\�\�\�x���WB�=zQ�\�S�\�U�ҩ\�\�,FO\�\�?\�.o�˿RG�Up]`ƿ\rN�9\����\��}S�u\�\�;�R�+ͳMs\�\�X\�K�Y�,1S\\\�\�TM&X\�-\�L\�_��&\�RU\�	�h�e�\�\�\\iW�;H�����\"\�$\�\�\'���\�\�\�\�\�-5`�d�2O6�(\�d+ \�\�L��\�K�\�	��l�I��l	�d3�\�:���-0\�C\�>��Iq\��J5Mb��\�L�\�#_r\0\�C-�\�I���@��HJ\0&J0ew�\�D�(��\��\�Y\Z7GWM�ma;���2$Trٍ\����\�l=��C6\0\�C�\�\�\n\n\�\�y귚�g���u֚4�KU�+\�bA<��\�\'!�\0\�\��S�?(.�qp𸦣�!\�ZLGt��M!U��\�\�\�;Kp�\�U:`�\�T�\�^\�K/�V[h�5d`hܨ\ZX\�9���B7��\\2�Hd�\�\Z\�K�\�ji\�\"Z��z�8x��}ы���᫁L�<�2\�e#��X8zwn\��]utP\��0\����b��D\��\�\�\�\��D6�\�76-��d�ҙ�w,�1��\�/*s.+&�r\�%E�ʅ�F֔�t�V��j\'ZPЍX_�m�<\���/�\�\�\��;���\�:.\�d{�<� �ײ\'\�N^�<�\\�4]Jܢ\�\"\�\�\�	\����ቁ$�9�\\\�<׵v�/�\�F{\�9�.$\�w��\�\�\�\"\��**\\,��sB��&��<�\�yo¥�u��E� \�M\�\�FT%�\�cj\�2�[�$�]۠����֠\�\�P\�!t\�8�\� $\��9ڼN\�h�\"\�06U�F+�\�ý�3������*)�oR��\0SVE�n\�죸}O��:Ҙ�\�G\�L�t;��4}4�K\�NR\�-\�\0��\���C�\"��	���\�\�\�T��:�\�\��Ri\�\0E9X%\�\�>�Y*	)K\�P�&�d7in�e�\Zp��x>\�C\��\�\�A\�C�x��r>\�\�2W~*�\�g���\�\�Y\�\�\��ɦANFgh͹]\�;�\�C޺�F\�+bf���d\�*y�\�n^��L2�\���{D�\�ݣ�2�\�W�}ERk��)w1�\"�Ư\��8�]ׇoJS�h&\�x\�u�~��q��G gF���4��\"IA�\�\�U�i�\�\�\�[�ݥXy���]ܰ\�}B�h�|CO����3�\'�\�2�6\�$�\��\�?�\�ۧ\���\�n\0�\0\0','6.2.0-61023');
/*!40000 ALTER TABLE `__migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitylog`
--

DROP TABLE IF EXISTS `activitylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activitylog` (
  `ActivityLogID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `DateTime` datetime NOT NULL,
  `ActivityName` longtext,
  `ActivityDescription` longtext,
  PRIMARY KEY (`ActivityLogID`),
  KEY `IX_UserID` (`UserID`) USING HASH,
  CONSTRAINT `FK_ActivityLog_User_UserID` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitylog`
--

LOCK TABLES `activitylog` WRITE;
/*!40000 ALTER TABLE `activitylog` DISABLE KEYS */;
/*!40000 ALTER TABLE `activitylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature` (
  `FeatureID` int(11) NOT NULL,
  `FeatureName` longtext,
  `FeatureDescription` longtext,
  `FeatureGroup` longtext,
  `FeatureDataType` longtext,
  `TableName` longtext,
  `Model_ModelID` int(11) DEFAULT NULL,
  `RatioFeature_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FeatureID`),
  KEY `IX_Model_ModelID` (`Model_ModelID`) USING HASH,
  KEY `IX_RatioFeature_ID` (`RatioFeature_ID`) USING HASH,
  CONSTRAINT `FK_Feature_Model_Model_ModelID` FOREIGN KEY (`Model_ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `FK_Feature_RatioFeature_RatioFeature_ID` FOREIGN KEY (`RatioFeature_ID`) REFERENCES `ratiofeature` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature`
--

LOCK TABLES `feature` WRITE;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
INSERT INTO `feature` VALUES (1,'plant_and_machinery','Plant and Machinery','ASSETS','numeric','financialfact_ods',NULL,NULL),(2,'total_assets','Total Assets','ASSETS','numeric','financialfact_ods',NULL,NULL),(3,'trade_debtors','Trade Debtors','ASSETS','numeric','financialfact_ods',NULL,NULL),(4,'loans_from_outside_malaysia','Loans from related companies outside Mal','INTERCOMPANY','numeric','related_comp_fact_ods',NULL,NULL),(5,'total_purchase_outside_malaysi','','INTERCOMPANY','numeric','related_comp_fact_ods',NULL,NULL),(6,'other_payment_outside_malaysia','Other payments to related companies out Malaysia','INTERCOMPANY','numeric','related_comp_fact_ods',NULL,NULL),(7,'receipt_from_outside_malaysia','Receipt from related companies outside Msia','INTERCOMPANY','numeric','related_comp_fact_ods',NULL,NULL),(8,'total_sales_outside_malaysia','Total sales from related companies outside Malaysia','INTERCOMPANY','numeric','related_comp_fact_ods',NULL,NULL),(9,'promo_advert_expense','Advertising Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(10,'cost_of_goods_sold','Cost of Sales or Cost of Goods Sold','EXPENSE','numeric','financialfact_ods',NULL,NULL),(11,'contarct_subcon_expense','Contractor and Subcontractor Expense','INTERCOMPANY','numeric','financialfact_ods',NULL,NULL),(12,'loan_interest_expense','Loan Interest Expense','INTERCOMPANY','numeric','financialfact_ods',NULL,NULL),(13,'non_allow_expense','Non-allowable expenses','INTERCOMPANY','numeric','financialfact_ods',NULL,NULL),(14,'purchases','Purchases','EXPENSE','numeric','financialfact_ods',NULL,NULL),(15,'total_expenses','Total Business Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(16,'opening_stock','Opening Stock','EXPENSE','numeric','financialfact_ods',NULL,NULL),(17,'production_cost','Production Cost','EXPENSE','numeric','financialfact_ods',NULL,NULL),(18,'gross_profit','Gross Profit or Loss','INCOME','numeric','financialfact_ods',NULL,NULL),(19,'gross_sales','Gross Sales','INCOME','numeric','financialfact_ods',NULL,NULL),(20,'net_profit_or_loss','Net Profit or Loss','INCOME','numeric','financialfact_ods',NULL,NULL),(21,'other_business_income','Other Business Income','INCOME','numeric','financialfact_ods',NULL,NULL),(22,'closing_stock','Closing Stock','INCOME','numeric','financialfact_ods',NULL,NULL),(23,'trade_creditors','Trade Creditors','LIABILITY','numeric','financialfact_ods',NULL,NULL),(24,'loan_from_director','Loan from Directors','LIABILITY','numeric','financialfact_ods',NULL,NULL),(25,'other_creditors','Other Creditors','LIABILITY','numeric','financialfact_ods',NULL,NULL),(26,'total_tax','Tax','LIABILITY','numeric','tax_assm_fact_ods',NULL,NULL),(27,'total_tax_liability','Tax Liability Balance (Owe / Refund)','LIABILITY','numeric','tax_assm_fact_ods',NULL,NULL),(28,'tax_payable_repayable','Total Tax Liability','LIABILITY','numeric','tax_assm_fact_ods',NULL,NULL),(29,'other_non_sales_income','Other non Business Income','INCOME','numeric','financialfact_ods',NULL,NULL),(30,'total_equity','Total Equity','INFORMATIONAL','numeric','financialfact_ods',NULL,NULL),(31,'total_income','Total Income','INFORMATIONAL','numeric','tax_assm_fact_ods',NULL,NULL),(32,'tax_exempted_income','Non-taxable profits','INCOME','numeric','financialfact_ods',NULL,NULL),(33,'interest_income','Interest Income','INCOME','numeric','income_fact_ods',NULL,NULL),(34,'dividend_income ','Dividends','INCOME','numeric','income_fact_ods',NULL,NULL),(35,'loans','','LIABILITY','numeric','financialfact_ods',NULL,NULL),(36,'total_current_liability','Total Current Liabilities','LIABILITY','numeric','financialfact_ods',NULL,NULL),(37,'stocks','Stocks','ASSETS','numeric','financialfact_ods',NULL,NULL),(38,'cap_fund_adv','Advances','LIABILITY','numeric','financialfact_ods',NULL,NULL),(39,'cap_fund_withdrawal','Drawings','LIABILITY','numeric','financialfact_ods',NULL,NULL),(40,'bad_debt','Bad Debt','EXPENSE','numeric','financialfact_ods',NULL,NULL),(41,'salary_and_wages','Employment Income (salary and wages)','INFORMATIONAL','numeric','income_fact_ods',NULL,NULL),(42,'cash_in_hand','Cash balance in hand and in bank','ASSET_RT','numeric','financialfact_ods',NULL,NULL),(43,'cash_in_bank','Cash balance in hand and in bank','ASSET_RT','numeric','financialfact_ods',NULL,NULL),(44,'total_investment','Investments','ASSET_RT','numeric','financialfact_ods',NULL,NULL),(45,'other_debtors','Other Debtors','ASSET_RT','numeric','financialfact_ods',NULL,NULL),(46,'total_fixed_assets','Total Fixed Assets','ASSET_RT','numeric','financialfact_ods',NULL,NULL),(47,'commission_expense','Commissions Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(48,'other_expenses','Other Business Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(49,'interest_business_income','Interest Income','EXT_INCOME','numeric','financialfact_ods',NULL,NULL),(50,'std_paid','Tax paid','TAX','numeric','tax_assm_fact_ods',NULL,NULL),(51,'std_from_spouse','Tax paid','TAX','numeric','tax_assm_fact_ods',NULL,NULL),(52,'chargeable_income','Chargeable Income','TAX','numeric','tax_assm_fact_ods',NULL,NULL),(53,'total_current_assets','Total Current Assets','ASSETS','numeric','financialfact_ods',NULL,NULL),(54,'rent_or_lease_expense','Lease Rental Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(55,'royalty_expense','Royalties','EXPENSE','numeric','financialfact_ods',NULL,NULL),(56,'maint_expense','Repairs and Maintenance Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(57,'travelling_expense','Travel Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(58,'salary_expense','Wage Expense','EXPENSE','numeric','financialfact_ods',NULL,NULL),(59,'total_liabilities','Total Liabilities','LIABILITY','numeric','financialfact_ods',NULL,NULL),(60,'loan_to_director','Loan to Directors','INFORMATIONAL','numeric','financialfact_ods',NULL,NULL),(61,'total_purchase_in_malaysia','Total purchases fr rel companies in Msia','INFORMATIONAL','numeric','related_comp_fact_ods',NULL,NULL),(62,'state','State Code','CODE','string','financialfact_ods',NULL,NULL),(63,'assessment_type','Assessment type','DIMENSION','numeric','financialfact_ods',NULL,NULL),(64,'assessment_year','Assessment year','DIMENSION','numeric','financialfact_ods',NULL,NULL),(65,'assessment_key','Assessment key','DIMENSION','numeric','financialfact_ods',NULL,NULL),(66,'branch_code','Branch code','DIMENSION','numeric','financialfact_ods',NULL,NULL),(67,'branch_key','Branch key','DIMENSION','numeric','financialfact_ods',NULL,NULL),(68,'taxpayer_key','Tax payer key','DIMENSION','numeric','financialfact_ods',NULL,NULL),(69,'business_key','Business key','DIMENSION','numeric','financialfact_ods',NULL,NULL),(70,'business_code','Business code','DIMENSION','numeric','financialfact_ods',NULL,NULL),(71,'it_ref_no_taxassm_dim','It ref no','DIMENSION','numeric','financialfact_ods',NULL,NULL),(72,'seq_no','Sequence no','DIMENSION','numeric','financialfact_ods',NULL,NULL),(73,'process_date','Process date','DIMENSION','numeric','financialfact_ods',NULL,NULL),(74,'snapshot_date','Snap shot date','DIMENSION','numeric','financialfact_ods',NULL,NULL);
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `JobID` int(11) NOT NULL AUTO_INCREMENT,
  `JobName` longtext,
  `JobStatus` longtext,
  `JobType` longtext,
  `JobParams` longtext,
  `Profile_ProfileID` int(11) DEFAULT NULL,
  PRIMARY KEY (`JobID`),
  KEY `IX_Profile_ProfileID` (`Profile_ProfileID`) USING HASH,
  CONSTRAINT `FK_Job_Profile_Profile_ProfileID` FOREIGN KEY (`Profile_ProfileID`) REFERENCES `profile` (`ProfileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `ModelID` int(11) NOT NULL AUTO_INCREMENT,
  `ModelName` longtext,
  PRIMARY KEY (`ModelID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (1,'Model (GPNP3 & GPRATIO)'),(2,'Model (OV_DIR)'),(3,'Model (GPNP & EXPTAX)'),(4,'Model (INDNORM)');
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelfeature`
--

DROP TABLE IF EXISTS `modelfeature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelfeature` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FeatureID` int(11) NOT NULL,
  `ModelID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelfeature`
--

LOCK TABLES `modelfeature` WRITE;
/*!40000 ALTER TABLE `modelfeature` DISABLE KEYS */;
INSERT INTO `modelfeature` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,1),(9,9,1),(10,10,1),(11,11,1),(12,12,1),(13,13,1),(14,14,1),(15,15,1),(16,16,1),(17,17,1),(18,18,1),(19,19,1),(20,20,1),(21,21,1),(22,29,1),(23,22,1),(24,23,1),(25,24,1),(26,25,1),(27,26,1),(28,27,1),(29,28,1),(30,32,1),(31,2,2),(32,3,2),(33,10,2),(34,14,2),(35,16,2),(36,18,2),(37,19,2),(38,20,2),(39,21,2),(40,22,2),(41,15,2),(42,24,2),(43,25,2),(44,23,2),(45,26,2),(46,29,2),(47,30,2),(48,31,2),(49,32,2),(50,33,2),(51,34,2),(52,35,2),(53,36,2),(54,2,3),(55,3,3),(56,10,3),(57,11,3),(58,12,3),(59,13,3),(60,14,3),(61,15,3),(62,16,3),(63,18,3),(64,20,3),(65,21,3),(66,29,3),(67,22,3),(68,19,3),(69,25,3),(70,23,3),(71,32,3),(72,37,3),(73,38,3),(74,39,3),(75,26,3),(76,28,3),(77,40,3),(78,41,3),(79,42,3),(80,43,3),(81,44,3),(82,45,3),(83,46,3),(84,47,3),(85,48,3),(86,49,3),(87,50,3),(88,51,3),(89,52,3),(90,1,4),(91,2,4),(92,3,4),(93,9,4),(94,10,4),(95,11,4),(96,12,4),(97,13,4),(98,14,4),(99,15,4),(100,16,4),(101,18,4),(102,19,4),(103,20,4),(104,21,4),(105,22,4),(106,17,4),(107,25,4),(108,23,4),(109,29,4),(110,35,4),(111,37,4),(112,40,4),(113,42,4),(114,43,4),(115,44,4),(116,46,4),(117,48,4),(118,53,4),(119,54,4),(120,55,4),(121,56,4),(122,57,4),(123,58,4),(124,59,4),(125,62,1),(126,62,2),(127,62,3),(128,62,4),(129,63,1),(130,63,2),(131,63,3),(132,63,4),(135,64,1),(136,64,2),(137,64,3),(138,64,4),(139,65,1),(140,65,2),(141,65,3),(142,65,4),(143,66,1),(144,66,2),(145,66,3),(146,66,4),(147,67,1),(148,67,2),(149,67,3),(150,67,4),(151,68,1),(152,68,2),(153,68,3),(154,68,4),(155,69,1),(156,69,2),(157,69,3),(158,69,4),(159,70,1),(160,70,2),(161,70,3),(162,70,4),(163,71,1),(164,71,2),(165,71,3),(166,71,4),(167,72,1),(168,72,2),(169,72,3),(170,72,4),(171,73,1),(172,73,2),(173,73,3),(174,73,4),(175,74,1),(176,74,2),(177,74,3),(178,74,4);
/*!40000 ALTER TABLE `modelfeature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mts_modelresult`
--

DROP TABLE IF EXISTS `mts_modelresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mts_modelresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature` text,
  `profile_id` text,
  `model_id` text,
  `value` text,
  `score` text,
  `rank` text,
  `year_d` text,
  `taxpayer` text,
  `it_ref_no` text,
  `mean` text,
  `median` text,
  `mode` text,
  `standard_deviation` text,
  `max_value` text,
  `min_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mts_modelresult`
--

LOCK TABLES `mts_modelresult` WRITE;
/*!40000 ALTER TABLE `mts_modelresult` DISABLE KEYS */;
/*!40000 ALTER TABLE `mts_modelresult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `ProfileID` int(11) NOT NULL AUTO_INCREMENT,
  `ProfileName` longtext,
  `ModelID` int(11) NOT NULL,
  `ModelName` longtext,
  `ODSQuery` longtext,
  `FileType` longtext,
  `UserID` int(11) NOT NULL,
  `AssessmentYear` int(11) NOT NULL,
  `JobId` int(11) NOT NULL,
  `BranchCode` longtext,
  `Saved` tinyint(1) NOT NULL,
  `ProfileParametersJson` longtext,
  PRIMARY KEY (`ProfileID`),
  KEY `IX_ModelID` (`ModelID`) USING HASH,
  KEY `IX_UserID` (`UserID`) USING HASH,
  CONSTRAINT `FK_Profile_Model_ModelID` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Profile_User_UserID` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratio`
--

DROP TABLE IF EXISTS `ratio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratio` (
  `RatioID` int(11) NOT NULL AUTO_INCREMENT,
  `RatioName` longtext,
  `Propotional` int(11) NOT NULL,
  `RatioFeature_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RatioID`),
  KEY `IX_RatioFeature_ID` (`RatioFeature_ID`) USING HASH,
  CONSTRAINT `FK_Ratio_RatioFeature_RatioFeature_ID` FOREIGN KEY (`RatioFeature_ID`) REFERENCES `ratiofeature` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratio`
--

LOCK TABLES `ratio` WRITE;
/*!40000 ALTER TABLE `ratio` DISABLE KEYS */;
INSERT INTO `ratio` VALUES (1,'RT_EXP_GS',1,NULL),(2,'RT_GP_EXP',0,NULL),(3,'RT_GP_GS',0,NULL),(4,'RT_GS_TOTAST',0,NULL),(5,'RT_NP_GS',0,NULL),(6,'RT_NP_TOTAST',0,NULL),(7,'RT_PRD_GS',1,NULL),(8,'RT_STKTURN',1,NULL),(9,'RT_TRCRT_COS',1,NULL),(10,'RT_TRCRT_PUR',1,NULL),(11,'RT_TRDR_GS',1,NULL),(12,'RT_PUR_GS',1,NULL),(13,'INC_OPPT_B1',1,NULL),(14,'RT_AV_STK',1,NULL),(15,'RT_COGS_GS',1,NULL),(16,'RT_LNFRDR_GS',0,NULL),(17,'RT_GP_TOTAST',1,NULL),(18,'RT_NP_TOTAST',0,NULL);
/*!40000 ALTER TABLE `ratio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratiofeature`
--

DROP TABLE IF EXISTS `ratiofeature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratiofeature` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RatioID` int(11) NOT NULL,
  `FeatureID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratiofeature`
--

LOCK TABLES `ratiofeature` WRITE;
/*!40000 ALTER TABLE `ratiofeature` DISABLE KEYS */;
INSERT INTO `ratiofeature` VALUES (1,1,15),(2,1,19),(3,2,18),(4,2,15),(5,3,18),(6,3,19),(7,4,19),(8,4,2),(9,5,20),(10,5,19),(11,6,20),(12,6,2),(13,7,17),(14,7,19),(15,8,10),(16,8,16),(17,8,22),(18,9,23),(19,9,10),(20,10,23),(21,10,14),(22,11,3),(23,11,19),(24,12,14),(25,12,19),(26,13,20),(27,13,29),(28,14,16),(29,14,22),(30,15,10),(31,15,19),(32,16,24),(33,16,19),(34,17,18),(35,17,2),(36,18,20),(37,18,2);
/*!40000 ALTER TABLE `ratiofeature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` longtext,
  `User_UserID` int(11) DEFAULT NULL,
  `UserRole_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RoleID`),
  KEY `IX_User_UserID` (`User_UserID`) USING HASH,
  KEY `IX_UserRole_ID` (`UserRole_ID`) USING HASH,
  CONSTRAINT `FK_Role_UserRole_UserRole_ID` FOREIGN KEY (`UserRole_ID`) REFERENCES `userrole` (`ID`),
  CONSTRAINT `FK_Role_User_User_UserID` FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin',1,NULL),(2,'BusinessUser',2,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserType` longtext,
  `Username` longtext,
  `ADID` int(11) NOT NULL,
  `Email` longtext,
  `ContactNumber` longtext,
  `UserRole_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `IX_UserRole_ID` (`UserRole_ID`) USING HASH,
  CONSTRAINT `FK_User_UserRole_UserRole_ID` FOREIGN KEY (`UserRole_ID`) REFERENCES `userrole` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,'admin',0,NULL,NULL,NULL),(2,NULL,'buser',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrole`
--

LOCK TABLES `userrole` WRITE;
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'twdwebdb'
--

--
-- Dumping routines for database 'twdwebdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07  1:08:36
