﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Data.Entity;
using System.Linq;
using TaxWatchDog.Models;
using TaxWatchDog.Models.DBContext;
using TaxWatchDog.ServiceContracts;
using System.DirectoryServices.AccountManagement;
using System.Collections.Generic;
using JWT.Algorithms;
using JWT;
using JWT.Serializers;
using TaxWatchDog.Models.HelpingModels;
using System.Web;
using System.Web.Hosting;

namespace TaxWatchDog.ServiceImplementations
{
    public class LoginService : ILoginService
    {

        public bool GetUserRole()
        {
            throw new NotImplementedException();
        }
        public bool IsLoggedIn()
        {
            throw new NotImplementedException();
        }
        public User Login(string username, string password)
        {

            try
            {
                AuthenticationHelper authenticated = AuthenticateUserFromAD(username, password);
                if (authenticated.inGroup == false && authenticated.isAuthentic == true )
                {
                    User UnAuthorized = new User();
                    UnAuthorized.UserType = "unauthorized";
                    return UnAuthorized;
                }
                if (authenticated.isAuthentic == true && authenticated.inGroup == true)
                {

                    using (MySQLDbContext db = new MySQLDbContext())
                    {
                        var user = db.UserSet.Where(a => a.Username == username).FirstOrDefault();
                        if (user != null)
                        {

                            var payload = new Dictionary<string, string>();
                            payload.Add("username", user.Username.ToString());
                            payload.Add("role", user.Role.ToString());
                            // payload.Add("userid", user.UserID.ToString());

                            string secret = System.Configuration.ConfigurationManager.AppSettings["JWTKeySecret"];

                            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                            IJsonSerializer serializer = new JsonNetSerializer();
                            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                            var token = encoder.Encode(payload, secret);
                            user.Status = 1;
                            user.SecurityToken = token;
                            db.SaveChanges();
                            return user;

                        }
                        else
                        {
                            return AddNewUser(username, password);
                        }

                    }
                }
                else
                {

                    User UnAuthenticatedUser = new User();
                    UnAuthenticatedUser.UserType = "unauthenticated";
                    return UnAuthenticatedUser;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public User GetUserByUsername(string name)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {

                    //throw new NotImplementedException();
                    var obj = db.UserSet.Where(a => a.Username == name).FirstOrDefault();
                    //var user = (from m in db.ModelSet
                    //            join mfb in db.ModelFeatureBridgeTableSet
                    //            on m.ID equals mfb.ModelID
                    //            join f in db.FeatureSet
                    //            on mfb.FeatureID equals f.ID
                    //            select m).Include(a => a.Features).ToList()
                    return obj;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }

        }
        public AuthenticationHelper AuthenticateUserFromAD(string username, string password)
        {

            try
            {
                List<GroupPrincipal> result = new List<GroupPrincipal>();
                string domainName = System.Configuration.ConfigurationManager.AppSettings["DomainNameForAuthentication"];


                string domainNameforAD = System.Configuration.ConfigurationManager.AppSettings["domainnameAD"];
                string containerNameforAD = System.Configuration.ConfigurationManager.AppSettings["containernameAD"];


                ContextType authenticationType = ContextType.Domain;
                PrincipalContext principalContext = new PrincipalContext(authenticationType, domainName);
                bool isAuthenticated = false;
                isAuthenticated = principalContext.ValidateCredentials(username, password);
                string AdminGroup = System.Configuration.ConfigurationManager.AppSettings["AD_ADMIN"];
                string BUGroup = System.Configuration.ConfigurationManager.AppSettings["AD_BU"];


                if (isAuthenticated == true)
                {

                    try
                    {
                        PrincipalContext ctx = new PrincipalContext(ContextType.Domain, domainNameforAD, containerNameforAD);
                        UserPrincipal userfromAD = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);
                        GroupPrincipal group2 = GroupPrincipal.FindByIdentity(ctx, BUGroup);
                        GroupPrincipal group1 = GroupPrincipal.FindByIdentity(ctx, AdminGroup);

                        if (userfromAD != null)
                        {
                            // check if user is member of group1
                            if (userfromAD.IsMemberOf(group1))
                            {
                                AuthenticationHelper reply = new AuthenticationHelper();
                                reply.isAuthentic = true;
                                reply.inGroup = true;
                                reply.GroupRole = "admin";
                                reply.MessageNo = 1;
                                return reply;
                            }

                            // check if user is member of group2
                            if (userfromAD.IsMemberOf(group2))
                            {
                                AuthenticationHelper reply = new AuthenticationHelper();
                                reply.isAuthentic = true;
                                reply.inGroup = true;
                                reply.GroupRole = "businessuser";
                                reply.MessageNo = 1;
                                return reply;

                            }
                            else
                            {
                                AuthenticationHelper reply = new AuthenticationHelper();
                                reply.inGroup = false;
                                reply.isAuthentic = true;
                                reply.GroupRole = "CANNOT FIND USER IN GROUPS PLEASE CONTACT ADMINISTRATION";
                                reply.MessageNo = 2;
                                return reply;
                            }

                        }
                        else
                        {
                            AuthenticationHelper reply = new AuthenticationHelper();
                            reply.isAuthentic = false;
                            reply.GroupRole = "Could not find user in the two Groups";
                            reply.MessageNo = 3;
                            return reply;
                        }


                    }
                    catch (Exception ex)
                    {
                        this._logService.Error(ex);
                        throw;

                    }
                }

                else
                {
                    AuthenticationHelper reply = new AuthenticationHelper();
                    reply.isAuthentic = false;
                    reply.inGroup = false;
                    reply.GroupRole = "UN  AUTHENTICATED USER";
                    reply.MessageNo = 3;
                    return reply;
                }

            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }


        }

        private User AddNewUser(string username, string password)
        {
            try
            {
                AuthenticationHelper authenticated = AuthenticateUserFromAD(username, password);
                if (authenticated.isAuthentic == true)
                {
                    using (MySQLDbContext db = new MySQLDbContext())
                    {
                        var user = db.UserSet.Where(a => a.Username == username).FirstOrDefault();

                        if (user == null)
                        {
                            var payload = new Dictionary<string, object>
                            {
                               { "username", username },
                               { "role", authenticated.GroupRole }
                            };
                            string secret = System.Configuration.ConfigurationManager.AppSettings["JWTKeySecret"];

                            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                            IJsonSerializer serializer = new JsonNetSerializer();
                            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
                            var token = encoder.Encode(payload, secret);

                            User newUser = new User();
                            newUser.Username = username;
                            newUser.UserType = authenticated.GroupRole;
                            newUser.Role = authenticated.GroupRole;
                            newUser.SecurityToken = token;
                            newUser.Status = 1;
                            var addedUser = db.UserSet.Add(newUser);
                            db.SaveChanges();
                            return addedUser;
                        }
                        else
                        {
                            return user;

                        }

                    }
                }
                else
                {
                    User emptyUser = new User();
                    emptyUser.Username = "Not Authenticated";
                    return emptyUser;
                }
            }

            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public User Logout(string username)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {

                    var user = db.UserSet.Where(a => a.Username == username && a.Status.Equals(1)).FirstOrDefault();
                    user.SecurityToken = null;
                    user.Status = 0;
                    db.SaveChanges();
                    return user;
                }
            }

            catch (Exception ex)
            {
                this._logService.Error(ex);
                return null;


            }


        }



        #region injected properties
        [Dependency]
        public ILog _logService { get; set; }

        #endregion

    }
}
