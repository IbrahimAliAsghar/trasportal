﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TaxWatchDog.Models;
using TaxWatchDog.Models.DBContext;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using System.Data.Odbc;
using System.Configuration;


namespace TaxWatchDog.ServiceImplementations
{
    public class JobService : IJobService
    {
        public int CreateJob(int profileId)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    Job job = new Job();
                    job.JobStatus = HelperStrings.JobStatus_InQueue;
                    job.JobType = HelperStrings.JobSType_Spark;
                    job.Profile = db.ProfileSet.Where(a => a.ProfileID == profileId).FirstOrDefault();
                    db.JobSet.Add(job);
                    db.SaveChanges();
                    job.JobParams = CreateJobParams(profileId, job.JobID);
                    db.SaveChanges();
                    return job.JobID;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public string GetJobStatusByJobId(int id)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    return db.JobSet.Where(a => a.JobID == id).FirstOrDefault().JobStatus;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public string GetJobStatusByProfileId(int id)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    return (from j in db.JobSet
                            join p in db.ProfileSet on j.Profile.ProfileID equals p.ProfileID
                            where p.ProfileID == id
                            select j.JobStatus).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public string ExecuteJob(Job job)
        {
            try
            {
                //TODO: manage this URL
                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string RunModelFunctionName = System.Configuration.ConfigurationManager.AppSettings["FlaskRunModelFuntionName"];
                string fullPath = FlaskApiURL + RunModelFunctionName;

                using (CustomWebClient wc = new CustomWebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    string HtmlResult = wc.UploadString(fullPath, job.JobParams);
                    return HtmlResult;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public int UpdateLastRunDate(int jobId)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    var job = db.JobSet.Where(j => j.JobID == jobId).FirstOrDefault();
                    job.LastRunDate = DateTime.Now;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }

        public string ExecuteCMSJob(Job job)
        {
            try
            {
                var a = System.Configuration.ConfigurationManager.AppSettings.AllKeys.ToList();
                string rejected_list_table = ConfigurationManager.AppSettings["REJECTED_LIST"];
                string register_list_table = ConfigurationManager.AppSettings["REGISTER_LIST"];
                string tax_payer_table = ConfigurationManager.AppSettings["TAX_PAYER"];
                string schema_name = ConfigurationManager.AppSettings["SchemaName"];

                string commandText = "select IT_REF_NO from " + schema_name + "." + tax_payer_table + " where STATUS = 'null'";
                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DB2ConnectionString"].ToString();
                List<string> insertedItRefNos = new List<string>();
                using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                {
                    odbcConnection.Open();
                    using (OdbcCommand command = new OdbcCommand(commandText, odbcConnection))
                    {
                        command.CommandType = System.Data.CommandType.Text;
                        OdbcDataReader dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            insertedItRefNos.Add(dr.GetString(0));
                        }
                        dr.Close();
                        //command.ExecuteNonQuery();                      
                    }
                }
                if (insertedItRefNos.Count > 0)
                {
                    int count = 0;
                    string insertedItrenos = "";
                    string successCommandText = "select IT_REF_NO from " + schema_name + "." + register_list_table + " where IT_REF_NO in (";
                    foreach (string itRefNo in insertedItRefNos)
                    {
                        if (count < insertedItRefNos.Count - 1)
                        {
                            insertedItrenos += "'" + itRefNo + "',";
                        }
                        else
                        {
                            insertedItrenos += "'" + itRefNo + "')";
                        }
                        count++;
                    }
                    successCommandText += insertedItrenos;
                    List<string> succesfulItRefNos = new List<string>();
                    string SuccesfulItrefnos = "";
                    using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                    {
                        odbcConnection.Open();
                        using (OdbcCommand command = new OdbcCommand(successCommandText, odbcConnection))
                        {
                            command.CommandType = System.Data.CommandType.Text;
                            OdbcDataReader dr1 = command.ExecuteReader();
                            while (dr1.Read())
                            {
                                succesfulItRefNos.Add(dr1.GetString(0));
                                //Console.WriteLine(dr.GetInt32(0) + ", " + dr.GetString(1));
                            }
                            //command.ExecuteNonQuery();                      
                        }
                    }
                    if (succesfulItRefNos.Count > 0)
                    {
                        count = 0;
                        string updateCommandText = "update " + schema_name + "." + register_list_table + " SET READ_CHECK=1 where IT_REF_NO in (";

                        foreach (string itRefNo in succesfulItRefNos)
                        {
                            if (count < succesfulItRefNos.Count - 1)
                            {
                                SuccesfulItrefnos += "'" + itRefNo + "',";
                            }
                            else
                            {
                                SuccesfulItrefnos += "'" + itRefNo + "')";
                            }
                            count++;
                        }
                        updateCommandText += SuccesfulItrefnos;
                        using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                        {
                            odbcConnection.Open();
                            using (OdbcCommand command = new OdbcCommand(updateCommandText, odbcConnection))
                            {
                                command.CommandType = System.Data.CommandType.Text;
                                //OdbcDataReader dr = command.ExecuteReader();

                                command.ExecuteNonQuery();
                            }
                        }
                    }
                    count = 0;
                    string RemainingItrefnos = "";
                    List<string> rejectedItRefNos = new List<string>();
                    var remainingItrefnos = insertedItRefNos.Select(i => i.ToString()).Except(succesfulItRefNos).ToList<string>();
                    if (remainingItrefnos.Count > 0)
                    {
                        foreach (string itRefNo in remainingItrefnos)
                        {
                            if (count < remainingItrefnos.Count - 1)
                            {
                                RemainingItrefnos += "'" + itRefNo + "',";
                            }
                            else
                            {
                                RemainingItrefnos += "'" + itRefNo + "')";
                            }
                            count++;
                        }

                        count = 0;

                        string rejectedCommandText = "select IT_REF_NO from " + schema_name + "." + rejected_list_table + " where IT_REF_NO in (";

                        rejectedCommandText += RemainingItrefnos;

                        using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                        {
                            odbcConnection.Open();
                            using (OdbcCommand command = new OdbcCommand(rejectedCommandText, odbcConnection))
                            {
                                command.CommandType = System.Data.CommandType.Text;
                                OdbcDataReader dr1 = command.ExecuteReader();
                                while (dr1.Read())
                                {
                                    rejectedItRefNos.Add(dr1.GetString(0));
                                    //Console.WriteLine(dr.GetInt32(0) + ", " + dr.GetString(1));
                                }
                                //command.ExecuteNonQuery();                      
                            }
                        }
                    }
                    string rejectedItRefnos = "";
                    if (rejectedItRefNos.Count > 0)
                    {
                        count = 0;
                        foreach (string itRefNo in rejectedItRefNos)
                        {
                            if (count < rejectedItRefNos.Count - 1)
                            {
                                rejectedItRefnos += "'" + itRefNo + "',";
                            }
                            else
                            {
                                rejectedItRefnos += "'" + itRefNo + "')";
                            }
                            count++;
                        }
                    }
                    List<string> processedItRefNos = succesfulItRefNos.Concat(rejectedItRefNos).ToList();
                    string processedItRefnos = "";
                    if (processedItRefNos.Count > 0)
                    {
                        count = 0;
                        foreach (string itRefNo in processedItRefNos)
                        {
                            if (count < processedItRefNos.Count - 1)
                            {
                                processedItRefnos += "'" + itRefNo + "',";
                            }
                            else
                            {
                                processedItRefnos += "'" + itRefNo + "')";
                            }
                            count++;
                        }
                        if (succesfulItRefNos.Count > 0 && rejectedItRefNos.Count > 0)
                        {
                            string updateTaxPayerCommandText = "update " + schema_name + "." + tax_payer_table + " SET STATUS = CASE WHEN IT_REF_NO in (" + SuccesfulItrefnos + " THEN 'Successful' WHEN IT_REF_NO in (" + rejectedItRefnos + "  THEN 'REJECTED' END,READ_CHECK =1 where IT_REF_NO in (" + processedItRefnos;
                            using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                            {
                                odbcConnection.Open();
                                using (OdbcCommand command = new OdbcCommand(updateTaxPayerCommandText, odbcConnection))
                                {
                                    command.CommandType = System.Data.CommandType.Text;
                                    //OdbcDataReader dr = command.ExecuteReader();

                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        if (succesfulItRefNos.Count > 0 && rejectedItRefNos.Count == 0)
                        {
                            string updateTaxPayerCommandText = "update " + schema_name + "." + tax_payer_table + " SET STATUS = CASE WHEN IT_REF_NO in (" + SuccesfulItrefnos + " THEN 'Successful' END,READ_CHECK =1 where IT_REF_NO in (" + processedItRefnos;
                            using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                            {
                                odbcConnection.Open();
                                using (OdbcCommand command = new OdbcCommand(updateTaxPayerCommandText, odbcConnection))
                                {
                                    command.CommandType = System.Data.CommandType.Text;
                                    //OdbcDataReader dr = command.ExecuteReader();

                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        if (succesfulItRefNos.Count == 0 && rejectedItRefNos.Count > 0)
                        {
                            string updateTaxPayerCommandText = "update " + schema_name + "." + tax_payer_table + " SET STATUS = CASE WHEN IT_REF_NO in (" + rejectedItRefnos + "  THEN 'REJECTED' END,READ_CHECK =1 where IT_REF_NO in (" + processedItRefnos;
                            using (OdbcConnection odbcConnection = new OdbcConnection(conn))
                            {
                                odbcConnection.Open();
                                using (OdbcCommand command = new OdbcCommand(updateTaxPayerCommandText, odbcConnection))
                                {
                                    command.CommandType = System.Data.CommandType.Text;
                                    //OdbcDataReader dr = command.ExecuteReader();

                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    return "success";
                }
                else
                {
                    return "success";
                }
            }

            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }

        }
        public int UpdateJobStatus(int jobId, string status)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    var job = db.JobSet.Where(j => j.JobID == jobId).FirstOrDefault();
                    job.JobStatus = status;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }
        public Job GetNewJob()
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    //var test = db.JobSet.ToList();
                    var time = db.JobSet.Where(j => j.JobType == HelperStrings.JobSType_CMS).Select(k => k.ExecutionTime).FirstOrDefault();
                    if (DateTime.Now.TimeOfDay > time)
                    {
                        if (db.JobSet.Where(j => j.JobType == HelperStrings.JobSType_CMS).Select(k => k.LastRunDate).FirstOrDefault().Date != DateTime.Now.Date)
                        {
                            return db.JobSet.Where(j => j.JobType == HelperStrings.JobSType_CMS && j.JobStatus == HelperStrings.JobStatus_InQueue).FirstOrDefault();
                        }
                    }

                    return db.JobSet.Where(j => j.JobStatus == HelperStrings.JobStatus_InQueue && j.JobType == HelperStrings.JobSType_Spark).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }
        private string CreateJobParams(int profileId, int jobId)
        {
            try
            {
                ProfileOutput profileObjToSend = new ProfileOutput();

                using (MySQLDbContext db = new MySQLDbContext())
                {

                    var profileObFromDB = db.ProfileSet.Where(a => a.ProfileID == profileId).FirstOrDefault();
                    profileObjToSend.ODSQuery = profileObFromDB.ODSQuery;
                    profileObjToSend.ProfileID = profileId;
                    profileObjToSend.ModelID = profileObFromDB.ModelID;
                    profileObjToSend.JobID = jobId;
                    profileObjToSend.AssesmentYear = profileObFromDB.AssessmentYear;
                    List<Feature> allFeaturesUsedInModel = (from p in db.ProfileSet
                                                            join m in db.ModelSet on p.ModelID equals m.ModelID
                                                            join mf in db.ModelFeaturesSet on m.ModelID equals mf.ModelID
                                                            join f in db.FeatureSet on mf.FeatureID equals f.FeatureID
                                                            where p.ProfileID == profileId
                                                            select f).ToList();
                    profileObjToSend.Features = allFeaturesUsedInModel;
                    List<Ratio> allRatiosUsedInModel = (from p in db.ProfileSet
                                                        join m in db.ModelSet on p.ModelID equals m.ModelID
                                                        join mf in db.RatioModelSet on m.ModelID equals mf.ModelID
                                                        join f in db.RatioSet on mf.RatioID equals f.RatioID
                                                        where p.ProfileID == profileId
                                                        select f).ToList();
                    //profileObjToSend.Ratios = GetPossibleRatios(allFeaturesUsedInModel);
                    foreach (var ratio in allRatiosUsedInModel)
                    {
                        ratio.Features = new List<Feature>();
                        ratio.Features = (from rf in db.RatioFeaturesSet
                                          join f in db.FeatureSet on rf.FeatureID equals f.FeatureID
                                          where rf.RatioID == ratio.RatioID
                                          select f).ToList();
                    }
                    profileObjToSend.Ratios = allRatiosUsedInModel;
                    return new JavaScriptSerializer().Serialize(profileObjToSend);
                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }
        private List<Ratio> GetPossibleRatios(List<Feature> features)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    List<RatioFeature> allRatioFeatures = new List<RatioFeature>();
                    List<int> allRatios = new List<int>();
                    List<int> possibleRatiosIds = new List<int>();
                    List<Ratio> possibleRatios = new List<Ratio>();

                    allRatioFeatures = db.RatioFeaturesSet.ToList();
                    allRatios = allRatioFeatures.Select(a => a.RatioID).Distinct().ToList();

                    foreach (int ratio in allRatios)
                    {
                        List<int> featuresUsedInRatio = allRatioFeatures.Where(r => r.RatioID == ratio).Select(a => a.FeatureID).ToList();
                        if (features.Select(a => a.FeatureID).Intersect(featuresUsedInRatio).ToList().Count == featuresUsedInRatio.Count)
                            possibleRatiosIds.Add(ratio);
                    }

                    possibleRatios = db.RatioSet.Where(a => possibleRatiosIds.Contains(a.RatioID)).ToList();

                    foreach (var ratio in possibleRatios)
                    {
                        ratio.Features = new List<Feature>();
                        ratio.Features = (from rf in allRatioFeatures
                                          join f in features on rf.FeatureID equals f.FeatureID
                                          where rf.RatioID == ratio.RatioID
                                          select f).ToList();
                    }

                    return possibleRatios;
                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }

        #region injected properties
        [Dependency]
        public ILog _logService { get; set; }
        #endregion
    }

    public class CustomWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 20 * 60 * 1000;
            return w;
        }
    }
}


