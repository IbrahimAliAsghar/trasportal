﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.Practices.Unity;
using System.Data.Entity;
using TaxWatchDog.Models;
using TaxWatchDog.Models.DBContext;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using System.Configuration;
using System.Data.Odbc;
using System.Data;
using TaxWatchDog.CrossCutting;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Collections;
using TaxWatchDog.ServiceImplementations;
using System.Globalization;
namespace TaxWatchDog.ServiceImplementations
{
    public class PushtoCMSService : IPushtoCMSService
    {

        #region Public functions
        public DB2Reply PushToCMS(Profile_Input Profile_Input, string UdpUser, string AuditType, string itrefno, List<FeatureStatistics> statistics)

        {

            try
            {

                Profile_Input.Year = Profile_Input.AssessmentYear;
                string CommandText = "";
                List<string> itrefnos = new List<string>();
                var obj = JArray.Parse(itrefno);
                foreach (var element in obj)
                {

                    itrefnos.Add(element["it_ref_no"].ToString());
                }

                bool InsertItrefs = ItrefInsert(Profile_Input.Year, itrefnos, Profile_Input.ProfileID);



                if (InsertItrefs == true)
                {
                    Profile selectedProfile = _profile.GetProfileById(Profile_Input.ProfileID);
                    string Query = selectedProfile.ODSQuery;
                    List<string> Tablenames = new List<string>()
             {"financialfact_ods","related_comp_fact_ods","tax_assm_fact_ods","income_fact_ods", "bus_inc_fact_ods"};
                    string tablename = "";
                    foreach (string item in Tablenames)
                    {
                        if (Query.Contains(item))
                        {
                            tablename = item;
                            break;
                        }
                    }
                    List<AddColumnsHelper> AddedColValues = new List<AddColumnsHelper>();
                    string jsonResult;
                    string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                    string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetColsToAddResultsFuntionName"];
                    string fullPath = FlaskApiURL + GetDataFunctionName;
                    ColstoAddHelper paramhelper = new ColstoAddHelper();
                    paramhelper.assesmentYear = Profile_Input.Year;
                    paramhelper.itRefNo = itrefnos;
                    paramhelper.query = selectedProfile.ODSQuery;
                    paramhelper.tableName = tablename;
                    var paramJson = new JavaScriptSerializer().Serialize(paramhelper);


                    try

                    {
                        using (CustomWebClient wc = new CustomWebClient())
                        {
                            this._logService.Info("Calling data api: " + fullPath);
                            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                            jsonResult = wc.UploadString(fullPath, paramJson);
                        }
                    }

                    catch (Exception ex)
                    {
                        _logService.Error(ex);
                        throw;
                    }


                    AddedColValues = JsonConvert.DeserializeObject<List<AddColumnsHelper>>(jsonResult);


                    string tax_payer_table = System.Configuration.ConfigurationManager.AppSettings["TAX_PAYER"].ToString();
                    string schema_name = System.Configuration.ConfigurationManager.AppSettings["SchemaName"].ToString();


                    CommandText = "INSERT INTO " + schema_name + "." + tax_payer_table + " (FILE_TYPE,IT_REF_NO,AUDIT_LIST_DATE,ID_MODL," +
                            "NAME,NOTE_DETL,ID_PEER,ID_VALUE,DESC_VALUE,NOTE_VALUE,NOTE_MODL,DESC_MODL ,ID_PRFL,DESC_PRFL," +
                            "NOTE_PRFL,ID_RSTR,INDUSTRY_CODE,BRANCH_CODE,SIZE,ASSM_YEAR,AUDIT_TYPE,SOURCE,UPDUSER,STATUS,READ_CHECK) VALUES ";

                    foreach (string itferfno in itrefnos)
                    {

                        var iter = (from av in AddedColValues
                                    where av.it_ref_no_taxassm_dim == itferfno
                                    select av).FirstOrDefault();


                        string ModelDesc = Profile_Input.ModelDesc ?? "NA";
                        string NoteProfile = Profile_Input.NoteProfile ?? "NA";
                        string BusinessCode = iter.main_business_code.ToString() ?? "NA";
                        string Name = iter.name.ToString() ?? "NA";


                        if (itferfno.Equals(itrefnos.Last()))
                        {
                            CommandText += " ('" + Profile_Input.FileType + "','" + itferfno + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + Profile_Input.ModelName + "','" + Name + "','" + Profile_Input.NoteDetail + "','" + Profile_Input.BranchCode + "_" + Profile_Input.BranchName + "','" + "null" + "','" +
                                            "null" + "','" + "null" + "','" + "null" + "','" + ModelDesc + "','" + Profile_Input.ProfileID + "','" + Profile_Input.ProfileName + "','" + NoteProfile + "','" + "null" + "','" + BusinessCode + "','" + Profile_Input.BranchCode + "','" +
                                            0.0 + "','" + Profile_Input.Year + "','" + AuditType + "','" + "null" + "','" + UdpUser + "','" + "null" + "','" + 0 + "')";
                        }
                        else
                        {
                            CommandText += " ('" + Profile_Input.FileType + "','" + itferfno + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + Profile_Input.ModelName + "','" + Name + "','" + Profile_Input.NoteDetail + "','" + Profile_Input.BranchCode + "_" + Profile_Input.BranchName + "','" + "null" + "','" +
                                           "null" + "','" + "null" + "','" + "null" + "','" + ModelDesc + "','" + Profile_Input.ProfileID + "','" + Profile_Input.ProfileName + "','" + NoteProfile + "','" + "null" + "','" + BusinessCode + "','" + Profile_Input.BranchCode + "','" +
                                           0.0 + "','" + Profile_Input.Year + "','" + AuditType + "','" + "null" + "','" + UdpUser + "','" + "null" + "','" + 0 + "'),";


                        }


                    }
                    DB2Reply response = new DB2Reply();

                    bool taxpayer, scorecard;
                    taxpayer = InsertToDB2(CommandText);
                    scorecard = PushToScoreCard(Profile_Input, obj, AuditType, Profile_Input.ProfileID, UdpUser, statistics);
                    if (taxpayer == true && scorecard == true)
                    {
                        response.flag = true;
                        response.message = "INSERTED IN BOTH TABLES";


                    }
                    if (taxpayer == false && scorecard == true)
                    {
                        response.message = "Inserted in table scorecard Only";
                        response.flag = false;

                    }
                    if (taxpayer == true && scorecard == false)
                    {
                        response.message = " Inserted in table taxpayer";
                        response.flag = false;
                    }
                    if (taxpayer == false && scorecard == false)
                    {
                        response.message = "Failed for both tables";
                        response.flag = false;
                    }
                    return response;
                }


                DB2Reply response2 = new DB2Reply();
                response2.message = "UNABLE TO ENTER ITREFS INTO TABLE";
                response2.flag = false;
                return response2;
            }

            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;

            }
        }
        public List<LoopkupResponse> CMSLookUp(int year, List<string> ItRefno, int ProfileID)
        {

            try
            {
                List<LoopkupResponse> response = new List<LoopkupResponse>();
                foreach (var iter in ItRefno)
                {

                    LoopkupResponse reply = new LoopkupResponse();

                    using (MySQLDbContext db = new MySQLDbContext())
                    {
                        var fromDB = (from cms in db.CMS_LookupSet
                                      where (cms.AssessmentYear == year && cms.it_ref_no == iter && cms.ProfileID == ProfileID)
                                      select cms.it_ref_no).FirstOrDefault();

                        if (fromDB == null)
                        {
                            reply.ItrefNo = iter;
                            reply.flag = false;
                            response.Add(reply);
                        }
                        else
                        {

                            reply.ItrefNo = iter;
                            reply.flag = true;
                            response.Add(reply);
                        }
                    }


                }

                return response;
            }
            catch (Exception ex)
            {
                //Lookup.Add(iter, "false");
                _logService.Error(ex);
                throw;
            }
        }
        public List<CaseStatusReportReply> CaseStatusReport(string StartDate, string EndDate, string flag)
        {

            string tax_payer_table = System.Configuration.ConfigurationManager.AppSettings["TAX_PAYER"].ToString();
            string schema_name = System.Configuration.ConfigurationManager.AppSettings["SchemaName"].ToString();

            try
            {
                string Query = "";

                if (flag == "Successful")
                {
                    Query = "select count(IT_REF_NO) AS COUNT ,ID_PEER,STATUS ,audit_type AS RESULT FROM " + schema_name + "." + tax_payer_table +
                            " WHERE AUDIT_LIST_DATE >= DATE('" + StartDate + "') AND AUDIT_LIST_DATE <= DATE('" + EndDate + "') and Status = ('Successful')" +
                            " GROUP BY ID_PEER, audit_type, STATUS";
                }

                else if (flag == "Rejected")
                {
                    Query = "select count(IT_REF_NO) AS COUNT ,ID_PEER,STATUS ,audit_type AS RESULT FROM " + schema_name + "." + tax_payer_table +
                            " WHERE AUDIT_LIST_DATE >=  DATE('" + StartDate + "') AND AUDIT_LIST_DATE <= DATE('" + EndDate + "') and Status = ('REJECTED')" +
                            " GROUP BY ID_PEER, audit_type, STATUS";
                }
                else if (flag == "All")
                {
                    Query = "select count(IT_REF_NO) AS COUNT ,ID_PEER,STATUS ,audit_type AS RESULT FROM " + schema_name + "." + tax_payer_table +
                            " WHERE AUDIT_LIST_DATE >= DATE('" + StartDate + "') AND AUDIT_LIST_DATE <= DATE('" + EndDate + "') and Status in('Successful', 'REJECTED')" +
                           "  GROUP BY ID_PEER,audit_type,STATUS";

                }
                else// (flag!= "All" || flag != "Rejected" || flag != "Successful")
                {
                    List<CaseStatusReportReply> response2 = new List<CaseStatusReportReply>();
                    CaseStatusReportReply ERROR_RESPONSE = new CaseStatusReportReply();
                    ERROR_RESPONSE.branchname = "ERROR IN FLAG";
                    ERROR_RESPONSE.branchcode = "ERROR IN FLAG";
                    ERROR_RESPONSE.audit_type = "ERROR IN FLAG";
                    ERROR_RESPONSE.result = "ERROR IN FLAG";
                    response2.Add(ERROR_RESPONSE);
                    return response2;


                }

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DB2ConnectionString"].ToString();
                List<CaseStatusReportReply> response = new List<CaseStatusReportReply>();
                //string conn = "Driver={IBM DB2 ODBC DRIVER};Database=sample;Hostname=localhost;Protocol=TCPIP;Port=50000;Uid=qasim.usmani;Pwd=Qasim@1234;";
                using (OdbcConnection odbcConnection = new OdbcConnection(connectionString))
                {

                    odbcConnection.Open();
                    using (OdbcCommand command = new OdbcCommand(Query, odbcConnection))
                    {
                        command.CommandType = System.Data.CommandType.Text;
                        OdbcDataReader dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            CaseStatusReportReply replyfromDB = new CaseStatusReportReply();
                            replyfromDB.Count = dr.GetString(0);

                            string ans = dr.GetString(1);
                            var items = ans.Split('_');
                            replyfromDB.branchname = items[0];
                            replyfromDB.branchcode = items[1];

                            replyfromDB.result = dr.GetString(2);
                            replyfromDB.audit_type = dr.GetString(3);
                            response.Add(replyfromDB);

                        }
                    }

                }
                if (response.Count == 0)
                {
                    List<CaseStatusReportReply> response1 = new List<CaseStatusReportReply>();
                    CaseStatusReportReply ERROR_RESPONSE = new CaseStatusReportReply();
                    ERROR_RESPONSE.branchname = "NO RECORDS FOUND";
                    ERROR_RESPONSE.branchcode = "NO RECORDS FOUND";
                    ERROR_RESPONSE.audit_type = "NO RECORDS FOUND";
                    ERROR_RESPONSE.result = "NO RECORDS FOUND";
                    ERROR_RESPONSE.Count = "0";
                    response1.Add(ERROR_RESPONSE);
                    return response1;


                }
                else
                {
                    return response;
                }

            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }




        }
        #endregion
        #region Private functions
        private bool PushToScoreCard(Profile_Input Profile_Input, JArray Featureobj, string AuditType, int ProfileID, string UdpUser, List<FeatureStatistics> Statistics)
        {
            string commandText = "";
            string ProfileParametersJson;


            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    ProfileParametersJson = (from p in db.ProfileSet
                                             where p.ProfileID == ProfileID
                                             select p.ProfileParametersJson).FirstOrDefault();
                }


                var ProfileParameters = JsonConvert.DeserializeObject<Profile_Input>(ProfileParametersJson);
                var featurelist = ProfileParameters.Features.ToList();

                featurelist.RemoveAll(x => x.FeatureGroup == "CODE" || x.FeatureGroup == "DIMENSION");


                Dictionary<string, List<string>> Featuremapping = new Dictionary<string, List<string>>();
                int ItrefCount = Featureobj.Count;


                string scorecard_table = System.Configuration.ConfigurationManager.AppSettings["SCORECARD"].ToString();
                string schema_name = System.Configuration.ConfigurationManager.AppSettings["SchemaName"].ToString();


                commandText += "INSERT INTO " + schema_name + "." + scorecard_table + " (IT_REF_NO ,ID_PEER ,ID_VALU,ID_PRFL ,ID_GRUP,ID_FEET,ELEMENT_LEVEL,ELEMENT_VALUE,ELEMENT_RANK," +
                              "ELEMENT_SCORE,ELEMENT_CATEGORY,ELEMENT_DESCRIPTION ,ELEMENT_TYPE ,DISTRIBUTION_COUNT ,MIN_DIST_VALUE,MAX_DIST_VALUE," +
                              "MEDIAN_DIST_VALUE,ELEMENT_WEIGHT,UPDUSER,UPDTSTMP) " +
                              "VALUES ";


                foreach (var itref in Featureobj)
                {
                    foreach (var feature in featurelist)
                    {

                        if (feature.FeatureGroup != "CODE" && feature.FeatureGroup != "DIMENSION")
                        {
                            string fname = feature.FeatureName.ToString();
                            Feature featureDetails = new Feature();
                            using (MySQLDbContext db = new MySQLDbContext())
                            {
                                featureDetails = (from fd in db.FeatureSet
                                                  where fd.FeatureName == fname
                                                  select fd).FirstOrDefault();
                            }

                            var Stats = (from sts in Statistics
                                         where sts.feature == fname
                                         select sts).FirstOrDefault();

                            string bname = Profile_Input.BranchName ?? "NA";
                            string rank = itref[fname + "_rank"].ToString() ?? "NA";
                            string Feature_value = itref[fname + "_val"].ToString().Replace(",", "");
                            string feature_desc = featureDetails.FeatureDescription.ToString() ?? "THIS IS FEATURE DESCRIPTION";
                            string last = featurelist.LastOrDefault().FeatureName.ToString();

                            if (fname == last)
                            {
                                string c = itref[fname + "_score"].ToString();

                                commandText += "('" + itref["it_ref_no"].ToString() + "','" + Profile_Input.BranchCode + "_" + bname + "','" +
                                                AuditType + "','" + Profile_Input.ProfileID + "','" + featureDetails.FeatureGroup + "','" + fname + "','" + "null" + "'," + Feature_value + ","
                                                 + itref[fname + "_rank"].ToString() + "," + itref[fname + "_score"].ToString() + ",'" + "null" + "','" + feature_desc +
                                                "','" + "null" + "','" + 0 + "'," + Stats.min_value + "," + Stats.max_value + "," + Stats.median + "," + Stats.mean + ",'" + UdpUser + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                                 "')";
                            }
                            else
                            {
                                commandText += "('" + itref["it_ref_no"].ToString() + "','" + Profile_Input.BranchCode + "_" + bname + "','" +
                                                AuditType + "','" + Profile_Input.ProfileID + "','" + featureDetails.FeatureGroup + "','" + fname + "','" + "null" + "'," + Feature_value + ","
                                                 + itref[fname + "_rank"].ToString() + "," + itref[fname + "_score"].ToString() + ",'" + "null" + "','" + feature_desc +
                                                "','" + "null" + "','" + 0 + "'," + Stats.min_value + "," + Stats.max_value + "," + Stats.median + "," + Stats.mean + ",'" + UdpUser + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                                 "'),";
                            }

                        }
                    }

                    if (itref.Next == null)
                    {
                    }
                    else
                    {
                        commandText += ",";

                    }


                }

                bool flag = InsertToDB2(commandText);
                return flag;
            }

            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }

        }
        
    private bool InsertToDB2(string Query)
    {
        try
        {

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DB2ConnectionString"].ToString();
            // string conn = "Driver={IBM DB2 ODBC DRIVER};Database=sample;Hostname=localhost;Protocol=TCPIP;Port=50000;Uid=qasim.usmani;Pwd=Qasim@1234;";
            using (OdbcConnection odbcConnection = new OdbcConnection(connectionString))
            {
                odbcConnection.Open();
                using (OdbcCommand command = new OdbcCommand(Query, odbcConnection))
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
            return true;
        }
        catch (Exception ex)
        {

            _logService.Error(ex);
            throw;
        }
    }
    private bool ItrefInsert(int year, List<string> ItRefno, int ProfileID)
    {

        CMS_Lookup newRecords = new CMS_Lookup();
        try
        {
            foreach (var it in ItRefno)
            {
                newRecords.AssessmentYear = year;
                newRecords.ProfileID = ProfileID;
                newRecords.it_ref_no = it;

                using (MySQLDbContext db = new MySQLDbContext())
                {
                    db.CMS_LookupSet.Add(newRecords);
                    db.SaveChanges();
                }
            }


            return true;
        }
        catch (Exception ex)
        {
            _logService.Error(ex);
            throw;

        }

    }

    #endregion


    #region injected properties
    [Dependency]
    public ILog _logService { get; set; }
    [Dependency]
    public IProfileService _profile { get; set; }



    #endregion
}
}

