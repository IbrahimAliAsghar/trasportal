﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TaxWatchDog.Models;
using TaxWatchDog.Models.DBContext;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using System.Text;
using System.Configuration;
using System.Data.Odbc;
using System.Data;
using TaxWatchDog.CrossCutting;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Collections;


namespace TaxWatchDog.ServiceImplementations
{
    public class ProfileService : IProfileService
    {
        #region public functions
        public List<Model> GetAllModels(bool includeFeatures = true)
        {
            try
            {
                List<Model> allModels = new List<Model>();

                using (MySQLDbContext db = new MySQLDbContext())
                {
                    allModels = (from a in db.ModelSet select a).ToList();
                    if (includeFeatures)
                    {
                        foreach (var model in allModels)
                        {
                            List<Feature> featureList = new List<Feature>();
                            featureList = (from f in db.FeatureSet
                                           join mf in db.ModelFeaturesSet on f.FeatureID equals mf.FeatureID
                                           where mf.ModelID == model.ModelID
                                           select f).ToList();
                            model.Features = featureList;
                        }
                    }
                }

                return allModels;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public Model GetModelById(int id, bool includeFeatures)
        {
            try
            {
                Model selectedModel = new Model();

                using (MySQLDbContext db = new MySQLDbContext())
                {
                    selectedModel = db.ModelSet.Where(s => s.ModelID == id).FirstOrDefault();
                    if (includeFeatures)
                    {
                        selectedModel = db.ModelSet.Where(s => s.ModelID == id).FirstOrDefault();
                        List<Feature> featureList = new List<Feature>();
                        featureList = (from f in db.FeatureSet
                                       join mf in db.ModelFeaturesSet on f.FeatureID equals mf.FeatureID
                                       where mf.ModelID == selectedModel.ModelID
                                       select f).ToList();
                        selectedModel.Features = featureList;
                    }
                }

                return selectedModel;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public List<Profile> GetAllProfiles(User userobj)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    var result = (from U in db.UserSet
                                  where U.UserID == userobj.UserID
                                  select U.Role).FirstOrDefault();


                    if (result == "admin")
                    {
                        List<Profile> allProfiles = new List<Profile>();
                        var adminresult = (from p in db.ProfileSet
                                           join j in db.JobSet on p.ProfileID equals j.Profile.ProfileID
                                           where p.IsActive == true
                                           select new { p, j.JobStatus }).ToList();

                        if (adminresult.Count == 0)
                        {
                            return null;
                            
                        }


                        foreach (var item in adminresult)
                        {
                            Profile p = new Profile();
                            p = item.p;
                            p.ODSQuery = "";
                            p.ProfileParametersJson = "";
                            p.JobStatus = item.JobStatus;
                            allProfiles.Add(p);
                        }

                        return allProfiles;
                    }

                    if (result == "businessuser")
                    {
                        List<Profile> allProfiles = new List<Profile>();



                        var businessresult = (from p in db.ProfileSet
                                              join j in db.JobSet on p.ProfileID equals j.Profile.ProfileID
                                              where p.IsActive == true && p.UserID == userobj.UserID
                                              select new { p, j.JobStatus }).ToList();

                        if (businessresult.Count == 0)
                        {
                            return null;

                        }


                        foreach (var item in businessresult)
                        {
                            Profile p = new Profile();
                            p = item.p;
                            p.ODSQuery = "";
                            p.ProfileParametersJson = "";
                            p.JobStatus = item.JobStatus;
                            allProfiles.Add(p);
                        }
                        return allProfiles;
                    }
                    else
                    {

                        return null;
                        
                        //List<Profile> allProfiles = new List<Profile>();
                        //Profile No_match = new Profile();
                        //No_match.BranchName = "USER ID WAS NOT AVAILABLE";
                        //No_match.ODSQuery = "USER ID WAS NOT AVAILABLE";
                        //No_match.ProfileParametersJson = "USER ID WAS NOT AVAILABLE";
                        //No_match.JobStatus = "USER ID WAS NOT AVAILABLE";
                        //allProfiles.Add(No_match);
                        //return allProfiles;


                    }
                }


            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public Profile GetProfileById(int id)
        {
            try
            {
                Profile selectedProfile = new Profile();
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    selectedProfile = db.ProfileSet.Where(s => s.ProfileID == id).FirstOrDefault();

                }
                return selectedProfile;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public Profile CreateProfile(Profile_Input inputProfile)
        {
            try
            {
                Profile profileToSave = new Profile();
                profileToSave.ModelID = inputProfile.ModelID;
                profileToSave.UserID = inputProfile.UserID;
                profileToSave.ModelName = inputProfile.ModelName;
                profileToSave.AssessmentYear = inputProfile.Year;
                profileToSave.FileType = inputProfile.FileType;
                profileToSave.ODSQuery = this.GenerateOdsQuery(inputProfile);
                profileToSave.ProfileName = inputProfile.ProfileName;
                profileToSave.BranchCode = inputProfile.BranchCode;
                profileToSave.BusinessCode = inputProfile.BusinessCode; //added
                profileToSave.StateCode = inputProfile.StateCode; //added
                profileToSave.NoteDetail = inputProfile.NoteDetail; //newly added
                profileToSave.BranchName = inputProfile.BranchName; //newly added
                profileToSave.ModelDesc = inputProfile.ModelDesc; //newly added
                profileToSave.NoteProfile = inputProfile.NoteProfile; //newly added

                profileToSave.IsActive = true; //added
                                               // var a = inputProfile.Features;
                                               // add statecode etc
                profileToSave.Saved = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SaveProfileDirectly"]);
                profileToSave.ProfileParametersJson = Newtonsoft.Json.JsonConvert.SerializeObject(inputProfile, Formatting.Indented);
                this._logService.Info("Features" + profileToSave.ProfileParametersJson);
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    db.ProfileSet.Add(profileToSave);
                    db.SaveChanges();
                    profileToSave.JobId = _jobService.CreateJob(profileToSave.ProfileID);
                    db.SaveChanges();
                }
                return profileToSave;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public List<Feature> GetFeaturesByModelId(int Id)
        {
            string BranchCode = System.Configuration.ConfigurationManager.AppSettings["BranchCode"];
            string StateCode = System.Configuration.ConfigurationManager.AppSettings["StateCode"];
            // string temp = "plant_and_machinery";

            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    List<Feature> features = (from mf in db.ModelFeaturesSet
                                              join f in db.FeatureSet on mf.FeatureID equals f.FeatureID
                                              where mf.ModelID == Id && f.FeatureGroup != "DIMENSION"
                                              select f).ToList();


                    // List<Feature> toRemove = features.ConvertAll(x => x.Clone());

                    //toRemove = features.clone();
                    //  features.RemoveAll(item => item.FeatureName == BranchCode || item.FeatureName == StateCode );

                    return features;

                }
            }
            catch (Exception ex)
            {
                _logService.Error(ex);
                throw;
            }
        }
        public List<FileTypeHelper> GetAllFileTypesFromHive()
        {
            List<FileTypeHelper> fhlist = new List<FileTypeHelper>();
            FileTypeHelper fh1 = new FileTypeHelper();
            FileTypeHelper fh2 = new FileTypeHelper();
            FileTypeHelper fh3 = new FileTypeHelper();
            List<string> yearList = new List<string> { "2012", "2013", "2014", "2015", "2016", "2017", "2018" };
            fh1.FileTypeName = "C";
            fh2.FileTypeName = "OG";
            fh3.FileTypeName = "D";
            fh1.Years = yearList;
            fh2.Years = yearList;
            fh3.Years = yearList;
            fhlist.Add(fh1);
            fhlist.Add(fh2);
            fhlist.Add(fh3);
            return fhlist;//"[{'FileTypeName': 'C','Years': ['2012', '2013', '2014', '2015', '2016', '2017', '2018']},{'FileTypeName': OG','Years': ['2012', '2013', '2014', '2016', '2018']},{'FileTypeName': 'File Type 3', 'Years': ['2016', '2017', '2018']},{'FileTypeName': 'D','Years': ['2012', '2013', '2014', '2015']}]";
                          ////var conn = new OdbcConnection { ConnectionString = @"DSN=hive" };

            ////conn.Open();


            //string query = "SELECT DISTINCT file_type from financialfact_ods;";

            ////var adp = new OdbcDataAdapter(query, conn);
            ////var ds = new DataSet();
            ////adp.Fill(ds);

            //DataSet fileTypes = HiveOdbcConnector.GetData(query);
            //return fileTypes;
        }
        public DataSet GetProfileResultsById(int profileID, int limit, int offset)
        {

            try
            {
                Profile selectedProfile = GetProfileById(profileID);


                this._logService.Info("Inside GetProfileResultsById service");
                //Get result datatable from table. Depending on if the profile is saved or not, use the relavant source.
                // string sourceToUse = "";
                bool includeTableNameInColumnNames = false;

                //if (CheckIfProfileIsSaved(profileID))
                //{
                //    sourceToUse = HelperStrings.DataSource_Hive;
                //    includeTableNameInColumnNames = true;
                //}
                //else {
                //    //change line below if want to get results from MySQL
                //    //sourceToUse = HelperStrings.DataSource_MySql;
                //    sourceToUse = HelperStrings.DataSource_Hive;
                //    includeTableNameInColumnNames = false;
                //}

                this._logService.Info("Now calling GetResultTableMetadataFromConfig");
                //Get metadata of result table. Either in mysql or hive, both should have exact same schema and their column names should be updated in web config
                var resultTableMd = GetResultTableMetadataFromConfig(includeTableNameInColumnNames);

                this._logService.Info("Now calling GetModelResultsFromAbt");
                //uncomment if want to use mysql or hive directly to get data.
                //DataTable modelResults = GetModelResultsFromAbt(profileID, sourceToUse, resultTableMd.TableName);
                DataTable modelResults = GetModelResultsFromAbt(profileID, limit, offset);

                if (modelResults.Rows.Count == 0)
                {
                    DataSet Termination = new DataSet();
                    return Termination;
                }

                //DataSet Termination = new DataSet();
                // FinalSet.Tables.Add(transposedDt);
                // Termination.Tables.Add();

                //Transpose the result for display
                DataTable transposedDt = TransposeData(modelResults, resultTableMd);


                //Add summary table to return set then return.
                this._logService.Info("modelResults count is: " + modelResults.Rows.Count);
                this._logService.Info("Now calling GetFeatureStatistics");
                DataTable statistics = GetFeatureStatistics(modelResults, resultTableMd);

                DataTable transposedDT_wht = GetWHTDetails(transposedDt);
                DataTable transposedDT_ExtModels = GetTaxProfilingDetails(transposedDT_wht, selectedProfile);
                //Add both tables to data set
                DataSet FinalSet = new DataSet();
                FinalSet.Tables.Add(transposedDT_ExtModels);
                FinalSet.Tables.Add(statistics);

                //transposedDT_wht.Columns.Add("Selected_Check",typeof(bool));      //column added  for Push to CMS task
                //transposedDT_wht.Columns["selected_check"].DefaultValue = false;

                //// FinalSet.Tables.Add(transposedDt);
                //FinalSet.Tables.Add(transposedDT_wht);
                //FinalSet.Tables.Add(statistics);

                ////comapre it_ref_no and year and mark true false.
                //// transposedDT_wht and lookup_Cms lookup 

                //foreach (DataRow row in transposedDT_wht.Rows)
                //{
                //    int assesment_year = row.Field<int>("assesment_year");
                //    string it_refno = row.Field<string>("it_refno");

                //    using (MySQLDbContext db = new MySQLDbContext())
                //    {
                //        var result  = db.CMS_LookupSet.Where(a => a.AssessmentYear == assesment_year & a.it_ref_no == it_refno).FirstOrDefault();

                //        if (result != null)
                //        {
                //            row["Read_Check"] = true;
                //        }
                //        else
                //        {
                //            row["Read_Check"] = false;
                //        }

                //    }

                //}








                return FinalSet;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public int SaveProfile(int profileId)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    var profile = db.ProfileSet.Where(a => a.ProfileID == profileId).FirstOrDefault();
                    profile.Saved = true;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public string GetDropDownData()
        {
            try
            {
                string jsonResult;
                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetDropDownDataFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;
                // List<string> filetypes = new List<string>();
                //  filetypes.Add("C");
                //   filetypes.Add("OG");
                //   filetypes.Add("SG");
                //    filetypes.Add("D");

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult = wc.UploadString(fullPath, "");
                    //jsonResult = jsonResult.ToString();
                    // var a = JsonConvert.DeserializeObject<Dictionary<string,Object>>(jsonResult);
                    // a.Add("File_Type", filetypes);
                    //  jsonResult = JsonConvert.SerializeObject(a);
                }
                return jsonResult;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }

        }
        public ODSCount GetODSQueryCount(Profile_Input profile)
        {
            try
            {
                string jsonResult = "";
                ODS_Helper input = new ODS_Helper();
                input.ODSQuery = GenerateOdsQuery(profile);
                input.AssesmentYear = profile.Year;
                string paramJson = new JavaScriptSerializer().Serialize(input);
                int threshold = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Threshold"].ToString());


                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetPeerGroupCountFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;
                ODSCount response = new ODSCount();




                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult = wc.UploadString(fullPath, paramJson);

                }
                try
                {
                    var details = JObject.Parse(jsonResult);
                    int count = Convert.ToInt32(details["recordscount"]);

                    if (count >= threshold)
                    {

                        response.flag = true;
                        response.message = "result count is greater than or equal to threshold";
                    }
                    else

                    {

                        response.flag = false;
                        response.message = "Record Count is insufficeint. Please change applied filter conditions";

                    }


                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }

                return response;


            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }


        }
        public Profile EditProfile(Profile_Input inputProfile)
        {
            try
            {
                inputProfile.IsActive = false;

                using (MySQLDbContext db = new MySQLDbContext())
                {
                    var query =
                    (from profile in db.ProfileSet
                     where profile.ProfileID == inputProfile.ProfileID
                     select profile);
                    foreach (Profile iter in query)
                    {
                        iter.IsActive = false;
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        this._logService.Error(ex);
                        throw;
                    }
                    Profile editProfile = new Profile();
                    editProfile = CreateProfile(inputProfile);
                    return editProfile;
                }

            }


            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public string GetReportCardData(string ItRefNo, int ProfileId)
        {
            try
            {
                string jsonResult;
                Profile selectedProfile = GetProfileById(ProfileId);
                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetReportCardDataFuntionName"];
                ReportInput parameters = new ReportInput();
                parameters.It_ref_Nos = ItRefNo;
                parameters.Profile_Id = ProfileId;

                string fullPath = FlaskApiURL + GetDataFunctionName;

                this._logService.Info("Serializing Report Param");
                string paramJson = new JavaScriptSerializer().Serialize(parameters);

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult = wc.UploadString(fullPath, paramJson);
                }
                List<string> itref_list = new List<string>();
                itref_list.Add(ItRefNo);
                WHT_Helper wht_helper = new WHT_Helper();
                wht_helper.AssesmentYear = selectedProfile.AssessmentYear;
                wht_helper.It_Ref_nos = itref_list;
                DataTable tablefromAPI = new DataTable();


                string GetDataFunctionName_wht = System.Configuration.ConfigurationManager.AppSettings["GetWHTResultsFuntionName"];
                string fullPath_wht = FlaskApiURL + GetDataFunctionName_wht;

                this._logService.Info("Serializing ModelResultsParam");
                var paramJson_wht = new JavaScriptSerializer().Serialize(wht_helper);
                string jsonResult_wht = "";
                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath_wht);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult_wht = wc.UploadString(fullPath_wht, paramJson_wht);
                    tablefromAPI = JsonConvert.DeserializeObject<DataTable>(jsonResult_wht);
                }



                TaxProfilingHelper tp_helper = new TaxProfilingHelper();
                tp_helper.AssesmentYear = selectedProfile.AssessmentYear;
                tp_helper.file_type = selectedProfile.FileType;
                tp_helper.It_Ref_nos = itref_list;

                DataTable tablefromAPI_taxProfiling = new DataTable();


                string GetDataFunctionName_taxProfiling = System.Configuration.ConfigurationManager.AppSettings["GetTaxProfilingResultsFuntionName"];
                string fullPath_taxProfiling = FlaskApiURL + GetDataFunctionName_taxProfiling;

                this._logService.Info("Serializing ModelResultsParam");
                var paramJson_taxProfiling = new JavaScriptSerializer().Serialize(tp_helper);
                string jsonResult_taxProfiling = "";
                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath_taxProfiling);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult_taxProfiling = wc.UploadString(fullPath_taxProfiling, paramJson_taxProfiling);
                    tablefromAPI_taxProfiling = JsonConvert.DeserializeObject<DataTable>(jsonResult_taxProfiling);
                }

                string meansTestFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetMeansTestResultsFunctionName"];
                string fullPathMeansTest = FlaskApiURL + meansTestFunctionName;
                string jsonResult_meansTest = "";
                var itref_param = new JavaScriptSerializer().Serialize("{\"It_RefNo\": "+ ItRefNo + "}");

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPathMeansTest);
                    try
                    {
                        wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                        jsonResult_meansTest = wc.UploadString(fullPathMeansTest, itref_param);
                    }
                    catch (Exception ex)
                    {
                        this._logService.Error(ex);
                    }
                }

                string digitalEconomyFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetDigitalEconomyResultsFunctionName"];
                string digitalEconomyFullPath = FlaskApiURL + digitalEconomyFunctionName;
                string jsonResult_digitalEconomy = "";

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + digitalEconomyFullPath);
                    try
                    {
                        wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                        jsonResult_digitalEconomy = wc.UploadString(digitalEconomyFullPath, itref_param);
                        this._logService.Info("jsonResult_digitalEconomy: " + jsonResult_digitalEconomy);
                    }
                    catch (Exception ex)
                    {
                        this._logService.Error(ex);
                    }
                }


                string finalJsonResult = "{\"report_card_res\":" + jsonResult + ",\"wht_res\":" + jsonResult_wht + ",\"tax_profiling_res\":" + jsonResult_taxProfiling + ",\"means_test_res\":" + jsonResult_meansTest + ",\"digital_economy_res\":" + jsonResult_digitalEconomy + "}";

                this._logService.Info("final result: " + finalJsonResult);
                return finalJsonResult;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        public Status GetJobStatusByProfileID(int ProfileID)
        {
            Status response = new Status();
            using (MySQLDbContext db = new MySQLDbContext())
            {

                var query = (from profile in db.ProfileSet
                             join job in db.JobSet
                             on profile.JobId equals job.JobID
                             where profile.ProfileID == ProfileID
                             select job.JobStatus).FirstOrDefault();

                response.status = query.ToString();
                return response;
            }





        }
        public string GetFeatureCount(int profile_id)
        {
            try
            {

                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetFeatureCountFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;
                string jsonResult = "";


                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    jsonResult = wc.UploadString(fullPath, "{\"profile_id\" :" + profile_id.ToString() + "}");
                }
                //Feature_Count response = new Feature_Count();

                //var details = JArray.Parse(jsonResult);
                //int count = Convert.ToInt32(details[0]["feature_count"]);
                //response.feature_count = count;
                //return response;
                return jsonResult;



            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }




        }
        #endregion

        #region Private functions
        private string GenerateOdsQuery(Profile_Input input)
        {
            Model modelFeatures = new Model();
            modelFeatures = GetModelById(input.ModelID, true);
            StringBuilder Query = new StringBuilder();

            string MappingKey = System.Configuration.ConfigurationManager.AppSettings["ODSMappingKey"];
            string AssessmentYearColumnName = System.Configuration.ConfigurationManager.AppSettings["AssessmentYearColumnName"];
            string BusinessCodeColumnName = System.Configuration.ConfigurationManager.AppSettings["BusinessCodeColumnName"];
            string BranchCodeColumnName = System.Configuration.ConfigurationManager.AppSettings["BranchCodeColumnName"];
            string StateCodeColumnName = System.Configuration.ConfigurationManager.AppSettings["StateCodeColumnName"];

            string[] KeyColumns = MappingKey.Split(',');
            List<string> Keys = new List<string>(KeyColumns);
            Query.Append("SELECT ");

            //Query.Append(modelFeatures.Features[0].TableName + "." + AssessmentYearColumnName + ",");

            //adding key columns in select list
            //foreach (var key in KeyColumns)
            //{
            //    Query.Append(modelFeatures.Features[0].TableName + "." + key + ",");
            //}

            foreach (var feature in modelFeatures.Features)
            {

                if (feature.Equals(modelFeatures.Features.Last()))
                {
                    Query.Append(feature.TableName + "." + feature.FeatureName + " FROM ");
                }
                else
                {
                    Query.Append(feature.TableName + "." + feature.FeatureName + ",");
                }
            }

            //bus_inc_fact_ods ==> branch_key

            var DistinctTableNames = modelFeatures.Features.Select(a => a.TableName).ToList().Distinct();



            Query.Append(DistinctTableNames.ElementAt(0));

            for (int j = 1; j <= DistinctTableNames.Count() - 1; j++)
            {
                Query.Append(" JOIN " + DistinctTableNames.ElementAt(j) + " ON ");

                if (DistinctTableNames.ElementAt(j) == "bus_inc_fact_ods")
                {
                    Keys.Remove("branch_key");
                }

                for (int k = 0; k <= Keys.Count() - 1; k++)
                {

                    if (k == Keys.Count() - 1)
                    {
                        Query.Append(DistinctTableNames.ElementAt(0) + "." + Keys.ElementAt(k) + " = " + DistinctTableNames.ElementAt(j) + "." + Keys.ElementAt(k));
                    }
                    else
                    {
                        Query.Append(DistinctTableNames.ElementAt(0) + "." + Keys.ElementAt(k) + " = " + DistinctTableNames.ElementAt(j) + "." + Keys.ElementAt(k) + " AND ");
                    }

                }
            }



            Query.Append(" WHERE ");

            Query.Append(DistinctTableNames.ElementAt(0) + "." + AssessmentYearColumnName + " IN (" + input.Year + " , " + Convert.ToString(input.Year - 1) + " , " + Convert.ToString(input.Year - 2) + ")");
            Query.Append(" AND " + DistinctTableNames.ElementAt(0) + "." + "file_type" + " = '" + input.FileType + "'");

            if (input.BusinessCode != "all")
            {
                Query.Append(" AND " + DistinctTableNames.ElementAt(0) + "." + BusinessCodeColumnName + "  = '" + Convert.ToString(input.BusinessCode) + "'");

            }

            if (input.BranchCode != "all")
            {
                Query.Append(" AND " + DistinctTableNames.ElementAt(0) + "." + BranchCodeColumnName + "  = '" + Convert.ToString(input.BranchCode) + "'");

            }

            if (input.StateCode != "all")
            {
                Query.Append(" AND " + DistinctTableNames.ElementAt(0) + "." + StateCodeColumnName + "  =  '" + Convert.ToString(input.StateCode) + "'");

            }



            var filteredFeatures = input.Features.Where(a => a.Filters[0].Type != null).ToList();
            foreach (var feature in filteredFeatures)
            {
                string filterType = feature.Filters[0].Type;
                string tablename = string.Empty;
                Query.Append(" AND ");
                string featureName = feature.FeatureName;
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    tablename = db.FeatureSet.Where(a => a.FeatureName == featureName).FirstOrDefault().TableName;
                }
                featureName = tablename + "." + featureName;

                if (filterType == HelperStrings.FilterType_GreaterThan)
                {
                    Query.Append(featureName + " > " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_LessThan)
                {
                    Query.Append(featureName + " < " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_EqualTo)
                {
                    Query.Append(featureName + " = " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_Between)
                {
                    Query.Append(featureName + " BETWEEN " + feature.Filters[0].Values[0] + " AND " + feature.Filters[0].Values[1]);
                }

                if (filterType == HelperStrings.FilterType_NotEqualTo)
                {
                    Query.Append(featureName + " <> " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_GreaterThanOrEqualTo)
                {
                    Query.Append(featureName + " >= " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_LessThanOrEqualTo)
                {
                    Query.Append(featureName + " <= " + feature.Filters[0].Values[0]);
                }

                if (filterType == HelperStrings.FilterType_IsNull)
                {
                    Query.Append(featureName + " IS NULL ");
                }

                if (filterType == HelperStrings.FilterType_IsNotNull)
                {
                    Query.Append(featureName + " IS NOT NULL ");
                }



            }
            return Query.ToString();
        }
        private DataTable TransposeData(DataTable inputDt, ModelResultsMetadata resultTableMd)
        {
            try
            {
                DataTable resultDt = new DataTable();

                DataView inputView = new DataView(inputDt);
                DataTable distinctFeatures = inputView.ToTable(true, resultTableMd.FeatureColumnName);
                DataTable distinct_it_ref_no = inputView.ToTable(true, resultTableMd.ItRefColumnName);

                resultDt.Columns.Add(resultTableMd.ItRefColumnName);
                resultDt.Columns.Add("assessment_year");

                foreach (DataRow feature in distinctFeatures.Rows)
                {
                    if (feature[0].ToString().Contains("RT"))
                    {
                        resultDt.Columns.Add(feature[0] + "_val");
                    }
                    else
                    {
                        resultDt.Columns.Add(feature[0] + "_val");
                        resultDt.Columns.Add(feature[0] + "_score");
                        resultDt.Columns.Add(feature[0] + "_rank");
                        //resultDt.Columns.Add(feature[0] + "_year");
                        resultDt.Columns.Add(feature[0] + "_year_1_val");
                        resultDt.Columns.Add(feature[0] + "_year_2_val");

                    }
                }

                foreach (DataRow it_ref_no in distinct_it_ref_no.Rows)
                {
                    DataRow row = resultDt.NewRow();
                    row["it_ref_no"] = it_ref_no[0];
                    row["assessment_year"] = inputDt.Rows[0][resultTableMd.YearColumnName];
                    var taxPayerInputDt = inputDt.AsEnumerable().Where(a => a.Field<string>(resultTableMd.ItRefColumnName) == it_ref_no[0].ToString()).CopyToDataTable();
                    foreach (DataRow inputRow in taxPayerInputDt.Rows)
                    {
                        if (inputRow[resultTableMd.FeatureColumnName].ToString().Contains("RT"))
                        {
                            row[inputRow[resultTableMd.FeatureColumnName] + "_val"] = inputRow[resultTableMd.ValueColumnName];
                        }
                        else
                        {
                            row[inputRow[resultTableMd.FeatureColumnName] + "_val"] = inputRow[resultTableMd.ValueColumnName];
                            row[inputRow[resultTableMd.FeatureColumnName] + "_score"] = inputRow[resultTableMd.ScoreColumnName];
                            row[inputRow[resultTableMd.FeatureColumnName] + "_rank"] = inputRow[resultTableMd.RankColumnName];
                            row[inputRow[resultTableMd.FeatureColumnName] + "_year_1_val"] = inputRow[resultTableMd.Year1ColumnName];
                            row[inputRow[resultTableMd.FeatureColumnName] + "_year_2_val"] = inputRow[resultTableMd.Year2ColumnName];

                        }
                    }
                    resultDt.Rows.Add(row);
                }

                resultDt.TableName = "Transposed";


                //List<string> filtered = new List<string>();
                //foreach (DataColumn columnname in resultDt.Columns)
                //{
                //    if (
                //        (columnname.ColumnName.Contains("RT") && columnname.ColumnName.Contains("score")) ||
                //        (columnname.ColumnName.Contains("RT") && columnname.ColumnName.Contains("rank")) ||
                //        (columnname.ColumnName.Contains("RT") && columnname.ColumnName.Contains("_year_1_val")) ||
                //        (columnname.ColumnName.Contains("RT") && columnname.ColumnName.Contains("_year_2_val"))
                //        )

                //    {
                //    }
                //    else
                //    {
                //        filtered.Add(columnname.ColumnName);
                //    }
                //}

                //DataView dtView = new DataView(resultDt);
                //DataTable result = new DataTable();
                //foreach (var iter in filtered)
                //{
                //    result =  dtView.ToTable(true,iter);
                //}


                return resultDt;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        //private DataTable GetModelResultsFromAbt(int profileID, string dataSourceToUse, string tableName)
        private DataTable GetModelResultsFromAbt(int profileID, int Limit, int Offset)
        {
            try
            {
                this._logService.Info("Inside GetModelResultsFromAbt");

                ModelResultsParam param = new ModelResultsParam();
                param.ProfileId = profileID;
                param.Limit = Limit;
                param.Offset = Offset;

                DataTable resultTable = new DataTable();
                resultTable = GetModelResultsFromHiveByApiByPage(param);

                return resultTable;

                //uncomment if want to use mysql or hive directly for getting results
                //string query = "Select * from " + tableName + " where profile_id = " + profileID + " and isActive = 1";
                //this._logService.Info("query is    " + query);
                //if (dataSourceToUse == HelperStrings.DataSource_MySql)
                //{
                //    return OdbcConnector.GetDataFromMySql(query).Tables[0];
                //}
                //else
                //{
                //    this._logService.Info("Calling GetDataFromHive");
                //    return OdbcConnector.GetDataFromHive(query).Tables[0];
                //}
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private DataTable GetModelResultsFromHiveByApiByPage(ModelResultsParam parameters)
        {
            try
            {
                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetDataFromHiveFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;

                this._logService.Info("Serializing ModelResultsParam");
                string paramJson = new JavaScriptSerializer().Serialize(parameters);

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    string jsonResult = wc.UploadString(fullPath, paramJson);
                    //if (jsonResult.ToString().Length == null)
                    //{
                    //    return 
                    //}
                    DataTable dataTable = JsonConvert.DeserializeObject<DataTable>(jsonResult);
                    return dataTable;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private ModelResultsMetadata GetResultTableMetadataFromConfig(bool appendTableName)
        {
            try
            {
                ModelResultsMetadata metadataObj = new ModelResultsMetadata();
                string tableNameToAppend = "";
                metadataObj.TableName = System.Configuration.ConfigurationManager.AppSettings["ModelResultsTableName"];
                if (appendTableName)
                    tableNameToAppend = metadataObj.TableName + ".";
                metadataObj.ValueColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsValueColumnName"];
                metadataObj.ScoreColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsScoreColumnName"];
                metadataObj.RankColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsRankColumnName"];
                //metadataObj.TaxpyerColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsTaxpayerColumnName"];
                metadataObj.YearColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsYearColumnName"];
                metadataObj.FeatureColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsFeatureColumnName"];
                metadataObj.ItRefColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsItRefColumnName"];
                metadataObj.MeanColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsMeanColumnName"];
                metadataObj.MedianColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsMedianColumnName"];
                metadataObj.ModeColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsModeColumnName"];
                metadataObj.SDColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsSDColumnName"];
                metadataObj.MinColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsMinColumnName"];
                metadataObj.MaxColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsMaxColumnName"];
                metadataObj.Year1ColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsValueYear1"];
                metadataObj.Year2ColumnName = tableNameToAppend + System.Configuration.ConfigurationManager.AppSettings["ModelResultsValueYear2"];
                return metadataObj;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private bool CheckIfProfileIsSaved(int profileId)
        {
            try
            {
                using (MySQLDbContext db = new MySQLDbContext())
                {
                    return db.ProfileSet.Where(a => a.ProfileID == profileId).FirstOrDefault().Saved;
                }
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private DataTable GetFeatureStatistics(DataTable modelResults, ModelResultsMetadata resultTableMd)
        {
            try
            {
                var colsToRemove = new List<string>();
                //filter datatable and get data for only one taxpayer. Picked first taxpayer
                DataView inputView = new DataView(modelResults);
                DataTable distinctTaxPayers = inputView.ToTable(true, resultTableMd.ItRefColumnName);
                var taxPayerInputDt = modelResults.AsEnumerable().Where(a => a.Field<string>(resultTableMd.ItRefColumnName) == distinctTaxPayers.Rows[0][0].ToString()).CopyToDataTable();

                this._logService.Info("taxPayerInputDt created. count is :" + taxPayerInputDt.Rows.Count);

                //remove columns except stats cols
                List<string> StatsCols = GetFeatureStatisticsColumnNamesInModelResult();
                StatsCols.Add(resultTableMd.FeatureColumnName);
                for (int i = 0; i < taxPayerInputDt.Columns.Count; i++)
                {
                    string currentColName = taxPayerInputDt.Columns[i].ColumnName;
                    if (!StatsCols.Contains(currentColName))
                    {
                        this._logService.Info("removing column: " + currentColName);
                        colsToRemove.Add(currentColName);
                    }
                }
                foreach (var col in colsToRemove)
                {
                    taxPayerInputDt.Columns.Remove(col);
                }

                taxPayerInputDt.TableName = "Statistics";

                this._logService.Info("returning taxPayerInputDt: " + taxPayerInputDt.Rows[0][0].ToString() + "  " + taxPayerInputDt.Rows[0][1].ToString());

                return taxPayerInputDt;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private List<string> GetFeatureStatisticsColumnNamesInModelResult()
        {
            try
            {
                string CommaSeperatedKeyNames = System.Configuration.ConfigurationManager.AppSettings["FeatureStatisticsColumnNameKeys"];
                string[] KeyNamesList = CommaSeperatedKeyNames.Split(',');
                List<string> StatsColumNames = new List<string>();
                foreach (var key in KeyNamesList)
                {
                    this._logService.Info("stat col name: " + "mts_modelresult." + System.Configuration.ConfigurationManager.AppSettings[key]);
                    // StatsColumNames.Add(System.Configuration.ConfigurationManager.AppSettings["ModelResultsTableName"] + "." + System.Configuration.ConfigurationManager.AppSettings[key]);
                    StatsColumNames.Add(System.Configuration.ConfigurationManager.AppSettings[key]);
                }
                return StatsColumNames;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private DataTable GetWHTDetails(DataTable modelResult) //transposed datatable
        {

            try
            {
                var resultTableMd = GetResultTableMetadataFromConfig(false);
                List<string> refIDList = new List<string>();
                DataView dv = new DataView(modelResult);
                DataTable resultant = dv.ToTable(true, resultTableMd.ItRefColumnName, "assessment_year");
                List<string> list = modelResult.AsEnumerable()
                           .Select(r => r.Field<string>(resultTableMd.ItRefColumnName))
                           .ToList();
                foreach (DataRow item in modelResult.Rows)
                {

                    refIDList.Add(item[resultTableMd.ItRefColumnName].ToString());

                    //if (resultant.Rows.IndexOf(item) < resultant.Rows.Count - 1)
                    //{
                    //    refIDList += "'" + item[resultTableMd.ItRefColumnName].ToString() + "'" + ",";
                    //}

                    //else
                    //// if (resultant.Rows.IndexOf(item) == resultant.Rows.Count - 1)
                    //{
                    //    refIDList += "'" + item[resultTableMd.ItRefColumnName].ToString() + "'";
                    //}

                }

                var distinctYear = (from row in modelResult.AsEnumerable()
                                    select row.Field<string>("assessment_year")).Distinct();

                WHT_Helper dt_helper = new WHT_Helper();
                dt_helper.It_Ref_nos = refIDList;
                dt_helper.AssesmentYear = Int32.Parse(distinctYear.FirstOrDefault().ToString());
                DataTable tablefromAPI = new DataTable();


                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetWHTResultsFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;

                this._logService.Info("Serializing ModelResultsParam");
                var paramJson = new JavaScriptSerializer().Serialize(dt_helper);

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    string jsonResult = wc.UploadString(fullPath, paramJson);
                    tablefromAPI = JsonConvert.DeserializeObject<DataTable>(jsonResult);
                }

                //  DataTable resultTable = modelResult.Clone();
                // Object resultzz = new Object();
                //var rowDataLeftOuter = from rowLeft in modelResult.AsEnumerable()
                //                       join rowRight in tablefromAPI.AsEnumerable() on rowLeft["it_ref_no"] equals rowRight["it_ref_no"] into gj
                //                       from subRight in gj.DefaultIfEmpty()
                //                       select rowLeft.ItemArray.Concat((subRight == null) ? (subRight.ItemArray) : modelResult.NewRow().ItemArray).ToArray();

                //  var combined = rowDataLeftOuter.Zip(resultTable.Columns.ToString(), (n, w) => new { Number = n, Word = w });
                //Add row data to dtblResult
                //foreach (var values in resultzz)

                //{

                //    //row["it_ref_no"] = values[0];
                //    //row["assessment_year"] = values[1];
                //    //resultTable.Rows.Add(row);
                //}

                var it_ref_nolist = (from row in tablefromAPI.AsEnumerable()
                                     select row.Field<string>("wht_final_scoringit_ref_no"));

                modelResult.Columns.Add("risk_factor", typeof(String));
                // resultTable.GetChanges();

                foreach (DataRow row in modelResult.Rows)
                {

                    var query = from A in tablefromAPI.AsEnumerable()
                                where A.Field<string>("wht_final_scoringit_ref_no") == row["it_ref_no"].ToString()
                                select (A.Field<string>("wht_final_scoringrisk_factor")).ToString();

                    if (query.Count() > 0)
                    {
                        row["risk_factor"] = query.FirstOrDefault().ToString();
                    }
                    else
                    {
                        row["risk_factor"] = "Null";
                    }

                }
                return modelResult;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }
        private DataTable GetTaxProfilingDetails(DataTable modelResult, Profile profile) //transposed datatable
        {

            try
            {
                var resultTableMd = GetResultTableMetadataFromConfig(false);
                //List<string> refIDList = new List<string>();
                //DataView dv = new DataView(modelResult);
                //DataTable resultant = dv.ToTable(true, resultTableMd.ItRefColumnName, "assessment_year");
                List<string> refIDList = modelResult.AsEnumerable()
                           .Select(r => r.Field<string>(resultTableMd.ItRefColumnName))
                           .ToList();

                //foreach (DataRow item in modelResult.Rows)
                //{

                //    refIDList.Add(item[resultTableMd.ItRefColumnName].ToString());

                //    //if (resultant.Rows.IndexOf(item) < resultant.Rows.Count - 1)
                //    //{
                //    //    refIDList += "'" + item[resultTableMd.ItRefColumnName].ToString() + "'" + ",";
                //    //}

                //    //else
                //    //// if (resultant.Rows.IndexOf(item) == resultant.Rows.Count - 1)
                //    //{
                //    //    refIDList += "'" + item[resultTableMd.ItRefColumnName].ToString() + "'";
                //    //}

                //}

                //var distinctYear = (from row in modelResult.AsEnumerable()
                //                    select row.Field<string>("assessment_year")).Distinct();

                TaxProfilingHelper tp_helper = new TaxProfilingHelper();
                tp_helper.It_Ref_nos = refIDList;
                tp_helper.AssesmentYear = profile.AssessmentYear;
                tp_helper.file_type = profile.FileType;
                DataTable tablefromAPI = new DataTable();


                string FlaskApiURL = System.Configuration.ConfigurationManager.AppSettings["FlaskApiURL"];
                string GetDataFunctionName = System.Configuration.ConfigurationManager.AppSettings["GetTaxProfilingResultsFuntionName"];
                string fullPath = FlaskApiURL + GetDataFunctionName;

                this._logService.Info("Serializing ModelResultsParam");
                var paramJson = new JavaScriptSerializer().Serialize(tp_helper);

                using (CustomWebClient wc = new CustomWebClient())
                {
                    this._logService.Info("Calling data api: " + fullPath);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    string jsonResult = wc.UploadString(fullPath, paramJson);
                    tablefromAPI = JsonConvert.DeserializeObject<DataTable>(jsonResult);
                }

                //  DataTable resultTable = modelResult.Clone();
                // Object resultzz = new Object();
                //var rowDataLeftOuter = from rowLeft in modelResult.AsEnumerable()
                //                       join rowRight in tablefromAPI.AsEnumerable() on rowLeft["it_ref_no"] equals rowRight["it_ref_no"] into gj
                //                       from subRight in gj.DefaultIfEmpty()
                //                       select rowLeft.ItemArray.Concat((subRight == null) ? (subRight.ItemArray) : modelResult.NewRow().ItemArray).ToArray();

                //  var combined = rowDataLeftOuter.Zip(resultTable.Columns.ToString(), (n, w) => new { Number = n, Word = w });
                //Add row data to dtblResult
                //foreach (var values in resultzz)

                //{

                //    //row["it_ref_no"] = values[0];
                //    //row["assessment_year"] = values[1];
                //    //resultTable.Rows.Add(row);
                //}

                var it_ref_nolist = (from row in tablefromAPI.AsEnumerable()
                                     select row.Field<string>("it_ref_no"));

                modelResult.Columns.Add("latesubm_score", typeof(String));
                modelResult.Columns.Add("nonsubm_score", typeof(String));
                modelResult.Columns.Add("repnc_score", typeof(String));
                // resultTable.GetChanges();

                foreach (DataRow row in modelResult.Rows)
                {

                    var query = from A in tablefromAPI.AsEnumerable()
                                where A.Field<string>("it_ref_no") == row["it_ref_no"].ToString()
                                select new
                                {
                                    latesubm_score = A.Field<string>("latesubm_score"),
                                    nonsubm_score = A.Field<string>("nonsubm_score"),
                                    repnc_score = A.Field<string>("repnc_score")
                                };

                    if (query.Count() > 0)
                    {
                        if (query.First().latesubm_score.ToString() != "None")
                        {
                            row["latesubm_score"] = query.First().latesubm_score.ToString();
                        }
                        else
                        {
                            row["latesubm_score"] = "Null";
                        }
                        if (query.First().nonsubm_score.ToString() != "None")
                        {
                            row["nonsubm_score"] = query.First().nonsubm_score.ToString();
                        }
                        else
                        {
                            row["nonsubm_score"] = "Null";
                        }
                        if (query.First().repnc_score.ToString() != "None")
                        {
                            row["repnc_score"] = query.First().repnc_score.ToString();
                        }
                        else
                        {
                            row["repnc_score"] = "Null";
                        }

                    }
                    else
                    {
                        row["latesubm_score"] = "Null";
                        row["nonsubm_score"] = "Null";
                        row["repnc_score"] = "Null";
                    }

                }
                return modelResult;
            }
            catch (Exception ex)
            {
                this._logService.Error(ex);
                throw;
            }
        }

        #endregion

        #region injected properties
        [Dependency]
        public ILog _logService { get; set; }
        [Dependency]
        public IJobService _jobService { get; set; }
        #endregion
    }
}


