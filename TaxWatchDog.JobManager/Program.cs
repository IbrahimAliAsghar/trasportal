using log4net;
using SedaAtlas.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.ServiceContracts;
using Microsoft.Practices.Unity;
using TaxWatchDog.Models;
using TaxWatchDog.Models.DBContext;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.JobExecutioners;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace TaxWatchDog.JobManager
{
    class Program
    {
        public static IJobService _jobService { get; set; }
        public static IJobExecutioner _jobExecutioner { get; set; }
        public static ILog _logService = UnityConfig.GetConfiguredContainer().Resolve<ILog>();
        static void Main(string[] args)
        {
            _jobService = UnityConfig.GetConfiguredContainer().Resolve<IJobService>();

            using (MySQLDbContext db = new MySQLDbContext())
            {
                _logService.Info("Job manager started.");
                _logService.Info("Looking for new jobs to assign in the queue");
                while (true)
                {
                    try
                    {
                        

                        Job jobToRun = _jobService.GetNewJob();
                        if (jobToRun != null && jobToRun.JobType == HelperStrings.JobSType_CMS)
                        {
                            _jobExecutioner = UnityConfig.GetConfiguredContainer().Resolve<IJobExecutioner>(jobToRun.JobType);
                            _logService.Info(_jobExecutioner.Name + " assigned for job " + jobToRun.JobID + " of type " + jobToRun.JobType);
                            _jobService.UpdateJobStatus(jobToRun.JobID, HelperStrings.JobStatus_Assigned);
                            Task.Run(() => {
                                _jobExecutioner.Execute(jobToRun);
                                _logService.Info("Job " + jobToRun.JobID + " finished");
                            });

                            _logService.Info("Looking for new jobs to assign in the queue");
                        }
                        if (jobToRun != null && jobToRun.JobParams != null)
                        {
                            _jobExecutioner = UnityConfig.GetConfiguredContainer().Resolve<IJobExecutioner>(jobToRun.JobType);
                            _logService.Info(_jobExecutioner.Name + " assigned for job " + jobToRun.JobID + " of type " + jobToRun.JobType);
                            _jobService.UpdateJobStatus(jobToRun.JobID, HelperStrings.JobStatus_Assigned);
                            Task.Run(() => { _jobExecutioner.Execute(jobToRun);
                                _logService.Info("Job " + jobToRun.JobID + " finished");
                            }); 
                            
                            _logService.Info("Looking for new jobs to assign in the queue");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logService.Error(ex);
                    }
                }
            }
        }
    }
}
