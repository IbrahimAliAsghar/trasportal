﻿using System.Collections.Generic;
using log4net;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using TaxWatchDog.WebApp.ActionFilters;

namespace TaxWatchDog.WebApp.Controllers
{
    [TokenValidator]
    public class PushtoCMSController : Controller
    {

        public JsonResult PushToCMS(Profile_Input Profile_Input, string UdpUser, string AuditType, string itrefno, List<FeatureStatistics> statistics)
        {
            this._logService.Info("Inside Push_to_CMS_TaxPayer action");
            var response = this._pushtoCMS.PushToCMS(Profile_Input, UdpUser , AuditType, itrefno, statistics);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CaseStatusReport(string StartDate, string EndDate, string flag)
        {
            this._logService.Info("Inside CaseStatus action");
            var response = this._pushtoCMS.CaseStatusReport(StartDate, EndDate, flag);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CMSLookUp(int year, List<string> itrefno, int ProfileID)
        {
            this._logService.Info("Inside CMSLookUp action");
            var response = this._pushtoCMS.CMSLookUp(year, itrefno, ProfileID);
            return Json(response, JsonRequestBehavior.AllowGet);

        }


        #region injected properties
        [Dependency]
        public IPushtoCMSService _pushtoCMS { get; set; }
        [Dependency]
        public ILog _logService { get; set; }
        #endregion
    }
}