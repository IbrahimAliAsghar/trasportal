﻿using log4net;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaxWatchDog.Models;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using TaxWatchDog.ServiceImplementations;

namespace TaxWatchDog.WebApp.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //public JsonResult xxxxLoginxxxx(UserModel user)
        //{
        //    this._logService.Info("In login action");
        //    UserModel returnedUser = new UserModel();
        //    if (user.username == "admin" && user.password == "admin")
        //    {
        //        returnedUser.username = "Administrator";
        //    }
        //    //var b = Newtonsoft.Json.JsonConvert.SerializeObject(a, Formatting.Indented);
        //    return Json(returnedUser, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetUserByUsername(string name)
        {
            //this._logService.Info("Getting user");
            User user = this._loginService.GetUserByUsername(name);

            return Json(user, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AuthenticateUserFromAD(string username, string password)
        {
            var a = _loginService.AuthenticateUserFromAD(username, password);
            return Json(a, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Login(UserModel user)
        {
            string username = user.username.ToString();
            string password = user.password.ToString();

            var loginresponse = _loginService.Login(username, password);

            if (loginresponse.UserType == "unauthorized")
            {
                var unauthorizedresposnse = new Dictionary<string, string>()
                 {
                    { "message", "USER IS NOT AUTHORIZED" }

                 };

                return Json(unauthorizedresposnse, JsonRequestBehavior.AllowGet);
            }

            if (loginresponse.UserType == "unauthenticated")
            {
                var unauthorizedresposnse = new Dictionary<string, string>()
                 {
                    { "message", "CAN NOT MATCH USER OR PASSWORD FROM ACTIVE DIRECTORY" }

                 };

                return Json(unauthorizedresposnse, JsonRequestBehavior.AllowGet);
            }

            else

            {
                return Json(loginresponse, JsonRequestBehavior.AllowGet);
            }

          
        }
        public JsonResult Logout(UserModel user)
        {
            string username = user.username.ToString();
            var logoutresponse = _loginService.Logout(username);
            if (logoutresponse != null)
            {
                return Json(logoutresponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var invalidaction= new Dictionary<string, string>()
                 {
                    { "message", "The user is not logged in and trying to logout" }

                 };

                return Json(invalidaction, JsonRequestBehavior.AllowGet);

            }
                
            
        }


        #region injected properties
        [Dependency]
        public ILoginService _loginService { get; set; }
        [Dependency]
        public ILog _logService { get; set; }

        #endregion
    }
}