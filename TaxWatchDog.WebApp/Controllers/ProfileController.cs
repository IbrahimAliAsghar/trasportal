﻿using log4net;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaxWatchDog.Models;
using TaxWatchDog.Models.HelpingModels;
using TaxWatchDog.ServiceContracts;
using TaxWatchDog.ServiceImplementations;
using TaxWatchDog.WebApp.ActionFilters;

namespace TaxWatchDog.WebApp.Controllers
{

    [TokenValidator]

    public class ProfileController : Controller
    {
        public JsonResult GetAllModels(bool includeFeatures)
        {
            List<Model> models = this._profileService.GetAllModels(includeFeatures);
            return Json(models, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelByID(int Id, bool includeFeatures)
        {
            Model model = this._profileService.GetModelById(Id, includeFeatures);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFeaturesByModelId(int Id)
        {
            List<Feature> features = this._profileService.GetFeaturesByModelId(Id);
            return Json(features, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 2419200, VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.Server)]
        public JsonResult GetAllFileTypesFromHive()
        {
            List<FileTypeHelper> filetypes = this._profileService.GetAllFileTypesFromHive();
            //var a = Newtonsoft.Json.JsonConvert.SerializeObject(filetypes, Formatting.Indented);
            return Json(filetypes, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProfileById(int id)
        {
            Profile profile = this._profileService.GetProfileById(id);
            return Json(profile, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllProfiles(User userobj)
        {
            List<Profile> profiles = this._profileService.GetAllProfiles(userobj);
            if (profiles != null)
            {
                return Json(profiles, JsonRequestBehavior.AllowGet);
            }

            else
            {
                var NoProfilesresposnse = new Dictionary<string, string>()
                 {
                    { "message", "NO PROFILE CREATED YET" }

                 };

                return Json(NoProfilesresposnse, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CreateProfile(Profile_Input Profile)
        {
            Profile createdProfile = this._profileService.CreateProfile(Profile);
            return Json(createdProfile, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveProfile(int profileId)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            int profileSaved = this._profileService.SaveProfile(profileId);
            if (profileSaved == 1)
            {
                response.Add("status", "True");
            }
            else response.Add("status", "False");
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 2419200, VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.Server)]
        public JsonResult GetProfileResultsById(int profileId, int limit, int offset)
        {
            this._logService.Info("Inside GetProfileResultsById action");
            DataSet ModelResults = this._profileService.GetProfileResultsById(profileId, limit, offset);
            var serializedResult = Newtonsoft.Json.JsonConvert.SerializeObject(ModelResults, Formatting.Indented);
            JsonResult jsonresult = Json(serializedResult, JsonRequestBehavior.AllowGet);
            jsonresult.MaxJsonLength = Int32.MaxValue;
            return jsonresult;
            //return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 2419200, VaryByParam = "none", VaryByHeader = "none", VaryByContentEncoding = "none", Location = System.Web.UI.OutputCacheLocation.ServerAndClient)]
        public ActionResult GetDropDownData()
        {
            this._logService.Info("Inside GetDropDownData action");
            var dropdown = this._profileService.GetDropDownData();
            return Content(dropdown);
            //var a = Content(dropdown);
            // var a =  Json(dropdown, JsonRequestBehavior.AllowGet);

            // return dropdown;
            //return Content(" test");
        }
        public JsonResult GetODSQueryCount(Profile_Input Profile)
        {
            this._logService.Info("Inside GetODSQueryCount action");
            var response = this._profileService.GetODSQueryCount(Profile);
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult EditProfile(Profile_Input Profile)
        {
            Profile EditedProfile = this._profileService.EditProfile(Profile);
            return Json(EditedProfile, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetReportCardData(string It_RefNo, int ProfileId)
        {
            this._logService.Info("Inside GetODSQueryCount action");
            var response = this._profileService.GetReportCardData(It_RefNo, ProfileId);
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetJobStatusByProfileID(int profileId)
        {
            this._logService.Info("Inside GetJobStatusByProfileID action");
            var response = this._profileService.GetJobStatusByProfileID(profileId);
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetFeatureCount(int profile_id)
        {

            this._logService.Info("Inside GetFeatureCount action");
            var response = this._profileService.GetFeatureCount(profile_id);
            return Json(response, JsonRequestBehavior.AllowGet); ;

        }
        //public JsonResult GenerateOdsQuery(Profile_Input input)
        //{
        //    this._logService.Info("Inside GetFeatureCount action");
        //    var response = this._profileService.GenerateOdsQuery(input);
        //    return Json(response, JsonRequestBehavior.AllowGet); ;

        //}

        #region injected properties
        [Dependency]
        public IProfileService _profileService { get; set; }
        [Dependency]
        public ILog _logService { get; set; }
        #endregion
    }
}