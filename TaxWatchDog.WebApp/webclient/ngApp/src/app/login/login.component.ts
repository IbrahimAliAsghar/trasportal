import { Component, OnInit } from '@angular/core';
import { ServiceInvokerComponent } from '../service-invoker/service-invoker.component';
import { ApiUrls } from '../api-urls';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  PAGE = "LOGIN";
  loginUserData = {
    username: '',
    password: ''
  };
  errorMessage: string;

  constructor(private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls, private _router: Router) { }

  ngOnInit() {
  }

  // loginUser() {
  //   this._serviceInvoker.getData(this._apiUrls.loginDemo, { "user": this.loginUserData })
  //     .subscribe(
  //       res => {
  //         if (res.username != '') {
  //           console.log(res.username);
  //           localStorage.setItem('username', res.username);
  //           this._router.navigate(['/profiles']);
  //         } else {
  //           // this.showErrorAlert = true;
  //           console.log('login error !');
  //           this.errorMessage = "there was an error with your login!!"
  //         }
  //       },
  //       error => {
  //         console.log("ERROR: " + error)
  //         this.errorMessage = error;
  //       }
  //     );
  // }

  loginUser() {
    this._serviceInvoker.getData(this._apiUrls.loginDemo, { "user": this.loginUserData })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, response: res });
          if (res.Status == 1) {
            console.log(res.Username);
            console.log(res.SecurityToken);
            localStorage.setItem('token', res.SecurityToken);
            localStorage.setItem('username', res.Username);
            localStorage.setItem('userobject', JSON.stringify(res));
            this._router.navigate(['/profiles']);
          } else if (res.message) {
            this.errorMessage = res.message;
          } else {
            console.log({ page: this.PAGE, errorReponse: res });
            this.errorMessage = "Username/Password is invalid!";
          }
        },
        err => {
          console.log({ page: this.PAGE, Error: err.error });
          this.errorMessage = "Server Error!";
        }
      );
  }

}
