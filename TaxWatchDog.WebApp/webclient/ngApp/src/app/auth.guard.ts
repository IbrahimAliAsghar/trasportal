import { Injectable } from '@angular/core';
import { CanActivate, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataSharingService } from './data-sharing.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  PAGE = "AUTHGUARD";

  constructor(private _router: Router) {

  }

  canActivate(): boolean {
    if(localStorage.getItem('username') && localStorage.getItem('userobject') && localStorage.getItem('token')){
      console.log({ page: this.PAGE, logged_in: true });
      return true;
    } else {
      console.log({ page: this.PAGE, logged_in: false });
      this._router.navigate(['/login']);
      return false;
    }
  }
}
