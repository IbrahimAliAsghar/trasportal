import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceInvokerComponent } from '../service-invoker/service-invoker.component';
import { ApiUrls } from '../api-urls';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-cms-report',
  templateUrl: './cms-report.component.html',
  styleUrls: ['./cms-report.component.css']
})
export class CmsReportComponent implements OnInit {

  PAGE = "CMS-REPORT";
  fromDate: Date;
  toDate: Date;
  selectedCase: string;
  reportHeaders = [];
  reportData = [];

  constructor(private _router: Router, private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls, private _excelService: ExcelService) { }

  ngOnInit() {
  }

  navToProfiles() {
    this._router.navigate(['/profiles']);
  }

  generateCmsReport() {
    // console.log("CMS Report Generated!!");
    console.log({ page: this.PAGE, StartDate: this.fromDate.toLocaleDateString(), EndDate: this.toDate.toLocaleDateString(), flag: this.selectedCase });
    this._serviceInvoker.getData(this._apiUrls.caseStatusReport, { StartDate: this.fromDate.toLocaleDateString(), EndDate: this.toDate.toLocaleDateString(), flag: this.selectedCase })
      .subscribe(
        res => {
          this.reportData = res;
          console.log({ page: this.PAGE, cms_report_data: this.reportData });
          this.reportHeaders = Object.keys(this.reportData[0]);
          console.log({ page: this.PAGE, reportHeaders: this.reportHeaders });
        },
        err => console.log({ page: this.PAGE, cms_report_error: err })
      );
  }

  exportReportAsExcel(): void {
    let fileName = `${this.fromDate.toLocaleDateString()}_${this.toDate.toLocaleDateString()}_${this.selectedCase}_data`;
    this._excelService.exportAsExcelFileTwo(this.reportData, fileName);
  }

}
