import { Component, OnInit } from '@angular/core';
import { ServiceInvokerComponent } from '../service-invoker/service-invoker.component';
import { ApiUrls } from '../api-urls';
import { DataSharingService } from '../data-sharing.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.css']
})
export class JobStatusComponent implements OnInit {

  // removed for time being "sending to spark" from statuses
  statuses = ["in queue", "assigned", "running", "getting data", "data processing"];
  finalStatuses = ["done", "failed"];
  showStatusList = [];
  profileId = 0;
  profileObj: any = {};
  totalStatuses = 6;
  navProfiles = true;
  navResults = true;

  progressColor;
  progressWidth;

  statusCheck;

  constructor(private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls, private _dataService: DataSharingService, private _router: Router) { }

  ngOnInit() {
    this._dataService.currentProfileId
      .subscribe(id => {
        // if (id != 0) {
        this.profileId = id;
        this.getStatus();
        console.log(this.profileId);
      }
      );
    this.statusCheck = setInterval(() => {
      this.getStatus();
    }, 10000);
    // }
    this._dataService.currentProfileObj
      .subscribe(
        obj => this.profileObj = obj
      );
  }

  getStatus() {
    this._serviceInvoker.getData(this._apiUrls.getJobStatus, { 'profileId': this.profileId })
      .subscribe(
        res => {
          console.log({ status_from_service: res.status.toLowerCase() });
          let statusLower = res.status.toLowerCase();
          if (statusLower === this.finalStatuses[0]) {
            this.showStatusList = [...this.statuses, this.finalStatuses[0]];
            clearInterval(this.statusCheck);
            this.setProgressClasses('bg-success', ((this.showStatusList.length / this.totalStatuses) * 100), false, false);
          }
          else if (statusLower === this.finalStatuses[1]) {
            this.showStatusList = [...this.statuses, this.finalStatuses[1]];
            clearInterval(this.statusCheck);
            this.setProgressClasses('bg-danger progress-bar-animated', ((this.showStatusList.length / this.totalStatuses) * 100), false, true);
          }
          else {
            let index = this.statuses.indexOf(statusLower);
            console.log({ indexInArray: index }, { array: this.statuses });
            this.showStatusList = this.statuses.slice(0, index + 1);
            this.setProgressClasses('bg-primary progress-bar-animated progress-bar-striped', ((this.showStatusList.length / this.totalStatuses) * 100).toPrecision(2), true, true);
          }
          console.log({ showStatusList: this.showStatusList });
        },
        err => {
          console.log(err);
        }
      );
  }

  setProgressClasses(color, widthVal, profilesCheck, resultsCheck) {
    this.progressColor = color;
    this.progressWidth = `${widthVal}%`;
    this.navProfiles = profilesCheck;
    this.navResults = resultsCheck;
  }

  navToProfiles() {
    this._dataService.changeProfileId(0);
    this._dataService.changeProfileObj({});
    this._router.navigate(['/profiles']);
  }

  navToResults() {
    this._dataService.changeProfileId(this.profileId);
    this._dataService.changeProfileObj(this.profileObj);
    this._router.navigate(['/results']);
  }

  ngOnDestroy() {
    clearInterval(this.statusCheck);
  }

}
