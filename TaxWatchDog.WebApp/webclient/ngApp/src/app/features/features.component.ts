import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceInvokerComponent } from '../service-invoker/service-invoker.component';
import { ApiUrls } from '../api-urls';
import { Router } from '@angular/router';
import { DataSharingService } from '../data-sharing.service';
import { parse } from 'querystring';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {

  PAGE = "CREATE PROFILE/FEATURES"

  showSelectModel = false;
  showFeatureDetail = false;

  models = [];
  fileTypes = [];
  branchCodes = [];
  businessCodes = [];
  stateCodes = [];
  assessmentYears = [];
  featuresList = [];
  groupedFeaturesList = [];
  featureForDetail: any = {};

  profileName = "";
  selectedFileType = "";
  selectedBranchCode = "";
  selectedBusinessCode = "";
  selectedStateCode = "";
  selectedAsstYear = "";
  selectedModelName: string = "";
  selectedModelId = "";
  noteProfile: string = "";
  modelDesc: string = "";

  selectedFilter = "";
  valueOne = "";
  valueTwo = "";
  disableSaveFilter = true;

  countMessage = "";
  countFlag: boolean;
  display = 'none';

  profileIdForEdit = 0;
  profileObjForEdit: any = {};

  profileIdForStatus = 0;
  profileObjForStatus: any = {};

  constructor(private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls, private _router: Router, private _dataService: DataSharingService) { }

  ngOnInit() {
    this.getDropDownData();
    this.getModelsData();
    this._dataService.currentEditCheck
      .subscribe(check => {
        console.log({ page: this.PAGE, editCheck: check });
        if (check) {
          this._dataService.currentProfileId
            .subscribe(id => {
              this.profileIdForEdit = id;
              console.log(this.profileIdForEdit);
              if (this.profileIdForEdit != 0) {
                this._dataService.currentProfileObj.subscribe(obj => {
                  // get profile object from data service
                  this.profileObjForEdit = obj;
                  console.log(this.profileObjForEdit);
                  this.selectedModelId = this.profileObjForEdit.ModelID;
                  this.selectedModelName = this.profileObjForEdit.ModelName;
                  this.displaySelectModel();
                  this.selectedFileType = this.profileObjForEdit.FileType;
                  this.selectedAsstYear = this.profileObjForEdit.AssessmentYear;
                  this.selectedBranchCode = this.profileObjForEdit.BranchCode == null ? '' : this.profileObjForEdit.BranchCode == 'all' ? '' : this.profileObjForEdit.BranchCode;
                  this.selectedBusinessCode = this.profileObjForEdit.BusinessCode == null ? '' : this.profileObjForEdit.BusinessCode == 'all' ? '' : this.profileObjForEdit.BusinessCode;
                  this.selectedStateCode = this.profileObjForEdit.StateCode == null ? '' : this.profileObjForEdit.StateCode == 'all' ? '' : this.profileObjForEdit.StateCode;
                  this.profileName = this.profileObjForEdit.ProfileName;
                  this.noteProfile = this.profileObjForEdit.NoteProfile;
                  this.modelDesc = this.profileObjForEdit.ModelDesc;
                  console.log((JSON.parse(this.profileObjForEdit.ProfileParametersJson)).Features);
                  this.featuresList = (JSON.parse(this.profileObjForEdit.ProfileParametersJson)).Features;
                  this.featuresByGroups();
                  console.log({ ungrouped: this.featuresList }, { grouped: this.groupedFeaturesList });
                });
              }
            });
        }
      });
  }

  getDropDownData() {
    this._serviceInvoker.getData(this._apiUrls.getDropdowns, {})
      .subscribe(
        res => {
          console.log({ page: this.PAGE, dropdown_data: res });
          // this.fileTypes = JSON.parse(res['File Type']);
          // this.fileTypes = res['File Type'];
          this.fileTypes = ["C", "D", "OG", "SG"];
          console.log({ page: this.PAGE, fileTypes: this.fileTypes });
          this.branchCodes = JSON.parse(res.branchCode);
          // this.branchCodes = res['branchCode'];
          console.log({ page: this.PAGE, branchCodes: this.branchCodes });
          this.businessCodes = JSON.parse(res.businessCode);
          // this.businessCodes = res['businessCode'];
          console.log({ page: this.PAGE, businessCodes: this.businessCodes });
          this.stateCodes = JSON.parse(res.stateCode);
          // this.stateCodes = res['stateCode'];
          console.log({ page: this.PAGE, stateCodes: this.stateCodes });
          this.assessmentYears = JSON.parse(res.assessmentYear);
          // this.assessmentYears = res['assessmentYear'];
          console.log({ page: this.PAGE, assessmentYears: this.assessmentYears });

          this.sortDropDowns();
        },
        err => {
          console.log(err);
        }
      );
  }

  sortDropDowns() {
    this.fileTypes.sort((a, b) => {
      if (a > b) {
        return 1;
      } else if (a < b) {
        return -1;
      }
      return 0;
    });
    this.assessmentYears.sort((a, b) => {
      if (a.assessment_year > b.assessment_year) {
        return 1;
      } else if (a.assessment_year < b.assessment_year) {
        return -1;
      }
      return 0;
    });
    this.branchCodes.sort((a, b) => {
      if (a.branch_code > b.branch_code) {
        return 1;
      } else if (a.branch_code < b.branch_code) {
        return -1;
      }
      return 0;
    });
    this.businessCodes.sort((a, b) => {
      if (a.business_code > b.business_code) {
        return 1;
      } else if (a.business_code < b.business_code) {
        return -1;
      }
      return 0;
    });
    this.stateCodes.sort((a, b) => {
      if (a.business_code > b.business_code) {
        return 1;
      } else if (a.state_cd < b.state_cd) {
        return -1;
      }
      return 0;
    });
    console.log({ page: this.PAGE, sortedFileTypes: this.fileTypes });
    console.log({ page: this.PAGE, sortedAssessmentYears: this.assessmentYears });
    console.log({ page: this.PAGE, sortedBranchCodes: this.branchCodes });
    console.log({ page: this.PAGE, sortedBusinessCodes: this.businessCodes });
    console.log({ page: this.PAGE, sortedStateCodes: this.stateCodes });
  }

  getModelsData() {
    this._serviceInvoker.getData(this._apiUrls.getAllModels, { "includeFeatures": "false" })
      .subscribe(
        res => {
          this.models = res;
          console.log({ page: this.PAGE, models: this.models });
        },
        err => {
          console.log(err);
        }
      );
  }

  getFeaturesForModel() {
    console.log(this.selectedModelId);
    this.setSelectedModelName(this.selectedModelId)
    this.showFeatureDetail = false;

    this._serviceInvoker.getData(this._apiUrls.getFeaturesByModel, { "Id": this.selectedModelId })
      .subscribe(
        res => {
          console.log(res);
          this.featuresList = res;
          this.featuresList.map(feature => feature.Filters = [{ 'Type': '', 'Values': [''] }]);
          this.featuresByGroups();
          console.log({ featureList: this.featuresList }, { grouped: this.groupedFeaturesList });
        },
        err => {
          console.log(err);
        }
      );
  }

  setSelectedModelName(modelId) {
    let model = this.models.filter(item => item.ModelID == modelId);
    console.log(model);
    this.selectedModelName = model[0].ModelName;
  }

  featuresByGroups() {
    var groups = new Set(this.featuresList.map(item => item.FeatureGroup));
    this.groupedFeaturesList = [];
    groups.forEach(g =>
      this.groupedFeaturesList.push({
        name: g,
        values: this.featuresList.filter(i => i.FeatureGroup === g)
      }
      ));
  }

  displaySelectModel() {
    // this.getModelsData();
    if (this.showSelectModel != true) {
      this.showSelectModel = !this.showSelectModel;
    }
  }

  showFeatureFilters(selectedFeature) {
    console.log(selectedFeature);
    this.featureForDetail = selectedFeature;

    if (this.featureForDetail != null && this.showFeatureDetail == false) {
      this.showFeatureDetail = !this.showFeatureDetail;
    }
    this.setFilterValues(selectedFeature);
  }

  setFilterValues(selectedFeature) {
    this.selectedFilter = selectedFeature.Filters[0].Type == null ? '' : selectedFeature.Filters[0].Type;
    this.valueOne = selectedFeature.Filters[0].Values[0];
    this.valueTwo = selectedFeature.Filters[0].Values[1];
    console.log(this.featuresList);
  }

  checkDisable(): boolean {
    if (this.selectedFilter != '') {
      if (this.selectedFilter != 'IS NULL' && this.selectedFilter != 'IS NOT NULL') {
        if (this.selectedFilter != 'Between') {
          if (this.valueOne != null && this.valueOne.length != 0 && this.valueOne != '') {
            this.disableSaveFilter = false;
          } else {
            this.disableSaveFilter = true;
          }
        } else if (this.selectedFilter == 'Between') {
          if ((this.valueOne != null && this.valueOne.length != 0 && this.valueOne != '') && (this.valueTwo != null && this.valueTwo.length != 0 && this.valueTwo != '')) {
            this.disableSaveFilter = false;
          } else {
            this.disableSaveFilter = true;
          }
        }
      } else if (this.selectedFilter == 'IS NULL' || this.selectedFilter == 'IS NOT NULL') {
        this.disableSaveFilter = false;
      }
    } else {
      this.disableSaveFilter = true;
    }
    return this.disableSaveFilter;
  }

  saveFilter(featureId) {
    const objIndex = this.featuresList.findIndex((obj => obj.FeatureID == featureId));

    if (this.selectedFilter != 'Between') {
      this.featuresList[objIndex].Filters[0].Type = this.selectedFilter;
      this.featuresList[objIndex].Filters[0].Values[0] = this.valueOne;
    } else if (this.selectedFilter == 'Between') {
      this.featuresList[objIndex].Filters[0].Type = this.selectedFilter;
      this.featuresList[objIndex].Filters[0].Values[0] = this.valueOne.toString();
      this.featuresList[objIndex].Filters[0].Values[1] = this.valueTwo.toString();
    }
    console.log(this.featuresList);
  }

  removeFilter(featureId) {
    const objIndex = this.featuresList.findIndex((obj => obj.FeatureID == featureId));

    this.featuresList[objIndex].Filters[0].Type = '';
    this.featuresList[objIndex].Filters[0].Values = [''];

    this.selectedFilter = '';
    this.valueOne = '';
    this.valueTwo = '';

    console.log(this.featuresList);
  }

  createProfile() {
    if (this.profileIdForEdit != 0) {
      const obj = {
        ProfileID: this.profileIdForEdit,
        ProfileName: this.profileName,
        ModelID: this.selectedModelId,
        ModelName: this.selectedModelName,
        UserID: JSON.parse(localStorage.getItem('userobject')).UserID,
        Year: this.selectedAsstYear,
        // User: "Administrator", //TODO: get from local storage
        User: localStorage.getItem('username'),
        FileType: this.selectedFileType,
        BranchCode: this.selectedBranchCode == '' ? 'all' : this.selectedBranchCode,
        Saved: false,
        StateCode: this.selectedStateCode == '' ? 'all' : this.selectedStateCode,
        BusinessCode: this.selectedBusinessCode == '' ? 'all' : this.selectedBusinessCode,
        Features: this.featuresList,
        NoteDetail: null,
        BranchName: this.selectedBranchCode == '' ? 'all' :
          (this.branchCodes.filter(item => { return this.selectedBranchCode == item.branch_code }))[0].branch_name,
        ModelDesc: this.modelDesc == undefined ? null : this.modelDesc,
        NoteProfile: this.noteProfile == undefined ? null : this.noteProfile
      };

      console.log({ EditProfileObject: obj });
      this.getQueryCount(obj, true);

    } else {
      const obj = {
        ProfileName: this.profileName,
        ModelID: this.selectedModelId,
        ModelName: this.selectedModelName,
        UserID: JSON.parse(localStorage.getItem('userobject')).UserID,
        Year: this.selectedAsstYear,
        // User: "Administrator", //TODO: get from local storage
        User: localStorage.getItem('username'),
        FileType: this.selectedFileType,
        BranchCode: this.selectedBranchCode == '' ? 'all' : this.selectedBranchCode,
        Saved: false,
        StateCode: this.selectedStateCode == '' ? 'all' : this.selectedStateCode,
        BusinessCode: this.selectedBusinessCode == '' ? 'all' : this.selectedBusinessCode,
        Features: this.featuresList,
        NoteDetail: null,
        BranchName: this.selectedBranchCode == '' ? 'all' :
          (this.branchCodes.filter(item => { return this.selectedBranchCode == item.branch_code }))[0].branch_name,
        ModelDesc: this.modelDesc == undefined ? null : this.modelDesc,
        NoteProfile: this.noteProfile == undefined ? null : this.noteProfile
      };

      console.log({ CreateProfileObject: obj });
      this.getQueryCount(obj, false);
    }
  }

  getQueryCount(obj, isEdit: boolean) {
    this._serviceInvoker.getData(this._apiUrls.getQueryCount, { "Profile": obj })
      .subscribe(
        res => {
          console.log(res);
          this.countFlag = res.flag;
          if (this.countFlag == false) {
            this.openModal();
            this.countMessage = res.message;
          } else {
            if (isEdit) {
              this.closeModal();
              console.log("edit profile");
              this.postEditProfileData(obj)
            } else {
              console.log("create profile");
              this.postProfileData(obj);
            }
          }
        },
        err => console.log(err)
      );
  }

  postProfileData(obj) {
    this._serviceInvoker.getData(this._apiUrls.createProfile, { "Profile": obj })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, postprofiledata_data: res });
          this.profileIdForStatus = res.ProfileID;
          this.profileObjForStatus = res;
          console.log({ page: this.PAGE, profileIdForStatus: this.profileIdForStatus, profileObjForStatus: this.profileObjForStatus });
          this.navToJobStatus();
        },
        err => console.log({ page: this.PAGE, postprofiledata_error: err })
      );
  }

  postEditProfileData(obj) {
    this._serviceInvoker.getData(this._apiUrls.editProfile, { "Profile": obj })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, posteditprofiledata_data: res });
          this.profileIdForStatus = res.ProfileID;
          this.profileObjForStatus = res;
          this._dataService.changeEditCheck(false);
          console.log({ page: this.PAGE, profileIdForStatus: this.profileIdForStatus, profileObjForStatus: this.profileObjForStatus });
          this.navToJobStatus();
        },
        err => console.log({ page: this.PAGE, posteditprofiledata_error: err })
      );
  }

  navToJobStatus() {
    this.closeModal();
    this._dataService.changeProfileId(this.profileIdForStatus);
    this._dataService.changeProfileObj(this.profileObjForStatus);
    this._router.navigate(['/status']);
  }

  navToProfiles() {
    this._dataService.changeEditCheck(false);
    this._dataService.changeProfileId(0);
    this._dataService.changeProfileObj({});
    this._router.navigate(['/profiles']);
  }

  openModal() {
    this.display = 'block';
  }

  closeModal() {
    this.display = 'none';
  }
}
