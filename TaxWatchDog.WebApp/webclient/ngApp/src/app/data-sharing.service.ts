import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  private profileId = new BehaviorSubject(0);
  private profileObj = new BehaviorSubject({});
  private editCheck = new BehaviorSubject(false);

  currentProfileId = this.profileId.asObservable();
  currentProfileObj = this.profileObj.asObservable();
  currentEditCheck = this.editCheck.asObservable();

  constructor() { }

  changeProfileObj(object){
    this.profileObj.next(object);
  }

  changeProfileId(id){
    this.profileId.next(id);
  }

  changeEditCheck(check){
    this.editCheck.next(check);
  }
}
