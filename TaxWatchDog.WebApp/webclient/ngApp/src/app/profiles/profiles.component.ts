import { Component, OnInit } from '@angular/core';
import { ServiceInvokerComponent } from '../service-invoker/service-invoker.component';
import { ApiUrls } from '../api-urls';
import { interval } from "rxjs/internal/observable/interval";
import { startWith, switchMap, isEmpty } from "rxjs/operators";
import { Router } from '@angular/router';
import { DataSharingService } from '../data-sharing.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  PAGE = "PROFILES";
  showDetail = false; //for profile details pane

  fileTypes = []; // dropdown
  assessmentYears = []; // dropdown
  branchCodes = []; // dropdown
  businessCodes = []; // dropdown
  stateCodes = []; // dropdown

  errorMessage: string;
  profiles = []; // all profile from service
  profilesViewList = [] // profiles to display on screen (created to assist in filtering profiles)
  detailProfile: any = {}; // selected profile object for showing details
  profilesInQueue = []; // filtered profiles having status other than 'Done'

  selectedFileType = "";
  selectedAsstYear = "";
  selectedBranchCode = "";

  profileStatusCheck; // interval variable to check profile status by service polling
  profileSaveStatus;

  constructor(private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls, private _router: Router, private _dataService: DataSharingService) { }

  ngOnInit() {
    // dropdowns are now populated from profile data
    // this.getDropDownData();
    this.getAllProfiles();

    //profile service polling
    this.profileStatusCheck = setInterval(() => {
      this._serviceInvoker.getData(this._apiUrls.getAllProfiles, { userobj: JSON.parse(localStorage.getItem('userobject')) })
        .subscribe(
          res => {
            console.log({ page: this.PAGE, profiles_data: res });
            if (res.message) {
              this.errorMessage = res.message;
              clearInterval(this.profileStatusCheck);
            } else {
              this.profiles = res;
              this.profilesViewList = res;
              this.filterProfiles();
              console.log({ page: this.PAGE, profiles: this.profiles });
              this.populateDropDowns();
              //filter profiles that have job status other than 'Done'
              this.profilesInQueue = this.profiles.filter(profile => profile.JobStatus != 'Done' && profile.JobStatus != 'Failed');

              //to update profile details pane when profile is selected and it's status changes
              if (Object.keys(this.detailProfile).length != 0) {
                //selecting first element of the filtered array and assign it to the detailProfile object
                this.detailProfile = this.profiles.filter(profile => this.detailProfile.ProfileID == profile.ProfileID)[0];
                console.log({ page: this.PAGE, detailProfile: this.detailProfile });
              }

              console.log({ page: this.PAGE, profiles_in_queue_length: this.profilesInQueue.length });
              //stopping profile service polling if all profiles have status 'Done'
              if (this.profilesInQueue.length == 0) {
                clearInterval(this.profileStatusCheck);
              }
            }
          },
          err => {
            console.log({ page: this.PAGE, profiles_error: err });
          }
        );
    }, 30000);
  }

  showProfileDetail(profile) {
    console.log({ page: this.PAGE, method: "showProfileDetail", profile: profile });
    console.log(profile);
    this.detailProfile = profile;
    if (this.detailProfile != null && this.showDetail == false) {
      this.showDetail = !this.showDetail;
    }
  }

  getDropDownData() {
    this._serviceInvoker.getData(this._apiUrls.getDropdowns, {})
      .subscribe(
        res => {
          console.log({ page: this.PAGE, dropdown_data: res });
          // this.fileTypes = JSON.parse(res['File Type']);
          // this.fileTypes = res['File Type'];
          this.fileTypes = ["C", "D", "OG", "SG"];
          console.log({ page: this.PAGE, fileTypes: this.fileTypes });
          this.branchCodes = JSON.parse(res.branchCode);
          // this.branchCodes = res['branchCode'];
          console.log({ page: this.PAGE, branchCodes: this.branchCodes });
          this.businessCodes = JSON.parse(res.businessCode);
          // this.businessCodes = res['businessCode'];
          console.log({ page: this.PAGE, businessCodes: this.businessCodes });
          this.stateCodes = JSON.parse(res.stateCode);
          // this.stateCodes = res['stateCode'];
          console.log({ page: this.PAGE, stateCodes: this.stateCodes });
          this.assessmentYears = JSON.parse(res.assessmentYear);
          // this.assessmentYears = res['assessmentYear'];
          console.log({ page: this.PAGE, assessmentYears: this.assessmentYears });
        },
        err => {
          console.log({ page: this.PAGE, dropdown_error: err });
        }
      );
  }

  getAllProfiles() {
    this._serviceInvoker.getData(this._apiUrls.getAllProfiles, { userobj: JSON.parse(localStorage.getItem('userobject')) })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, profiles_data: res });
          if (res.message) {
            this.errorMessage = res.message;
          } else {
            this.profiles = res;
            this.profilesViewList = res;
            console.log({ page: this.PAGE, profiles: this.profiles });
            if (Object.keys(this.detailProfile).length != 0) {
              //selecting first element of the filtered array and assign it to the detailProfile object
              this.detailProfile = this.profiles.filter(profile => this.detailProfile.ProfileID == profile.ProfileID)[0];
              console.log({ page: this.PAGE, detailProfile: this.detailProfile });
            }
          }
          this.populateDropDowns();
        },
        err => {
          console.log({ page: this.PAGE, profiles_error: err });
        }
      );
  }

  populateDropDowns() {
    this.fileTypes = this.profiles.map(profile => profile.FileType).filter((value, index, self) => self.indexOf(value) === index);
    this.assessmentYears = this.profiles.map(profile => profile.AssessmentYear).filter((value, index, self) => self.indexOf(value) === index);
    this.branchCodes = this.profiles.map(profile => profile.BranchCode).filter((value, index, self) => self.indexOf(value) === index);
    console.log({ page: this.PAGE, fileTypesList: this.fileTypes, assessmentYearsList: this.assessmentYears, branchCodesList: this.branchCodes });
    this.sortDropDown(this.fileTypes);
    this.sortDropDown(this.assessmentYears);
    this.sortDropDown(this.branchCodes);
  }

  sortDropDown(dropdown) {
    dropdown.sort((a, b) => {
      if (a > b) {
        return 1;
      } else if (a < b) {
        return -1;
      }
      return 0;
    });
    console.log({ page: this.PAGE, sortedDropDown: dropdown });
  }

  filterProfiles() {
    if (this.selectedFileType == '') {
      if (this.selectedAsstYear == '') {
        if (this.selectedBranchCode == '') {
          this.profilesViewList = this.profiles;
        } else if (this.selectedBranchCode != '') {
          this.profilesViewList = this.profiles.filter(profile => profile.BranchCode == this.selectedBranchCode);
        }
      } else if (this.selectedAsstYear != '') {
        if (this.selectedBranchCode == '') {
          this.profilesViewList = this.profiles.filter(profile => profile.AssessmentYear == this.selectedAsstYear);
        } else if (this.selectedBranchCode != '') {
          this.profilesViewList = this.profiles.filter(profile => profile.AssessmentYear == this.selectedAsstYear && profile.BranchCode == this.selectedBranchCode);
        }
      }
    } else if (this.selectedFileType != '') {
      if (this.selectedAsstYear == '') {
        if (this.selectedBranchCode == '') {
          this.profilesViewList = this.profiles.filter(profile => profile.FileType == this.selectedFileType);
        } else if (this.selectedBranchCode != '') {
          this.profilesViewList = this.profiles.filter(profile => profile.FileType == this.selectedFileType && profile.BranchCode == this.selectedBranchCode);
        }
      } else if (this.selectedAsstYear != '') {
        if (this.selectedBranchCode == '') {
          this.profilesViewList = this.profiles.filter(profile => profile.FileType == this.selectedFileType && profile.AssessmentYear == this.selectedAsstYear);
        } else if (this.selectedBranchCode != '') {
          this.profilesViewList = this.profiles.filter(profile => profile.FileType == this.selectedFileType && profile.AssessmentYear == this.selectedAsstYear && profile.BranchCode == this.selectedBranchCode);
        }

      }
    }
  }

  navToCmsReport() {
    this._router.navigate(['/cmsreport']);
  }

  navToFeatures() {
    this._dataService.changeProfileId(0);
    this._dataService.changeProfileObj({});
    this._router.navigate(['/features']);
  }

  navToFeaturesWithEdit() {
    this._dataService.changeEditCheck(true);
    this._dataService.changeProfileId(this.detailProfile.ProfileID);
    this._serviceInvoker.getData(this._apiUrls.getProfileObject, { 'id': this.detailProfile.ProfileID })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, profile_object_data: res });
          this._dataService.changeProfileObj(res);
          this._router.navigate(['/features']);
        },
        err => console.log({ page: this.PAGE, profiles_object_error: err })
      );
  }

  navToResults() {
    this._dataService.changeProfileId(this.detailProfile.ProfileID);
    this._dataService.changeProfileObj(this.detailProfile);
    this._router.navigate(['/results']);
  }

  saveProfile() {
    console.log({ page: this.PAGE, method: "saveProfile", profileId: this.detailProfile.ProfileID });
    this._serviceInvoker.getData(this._apiUrls.saveProfile, { 'profileId': this.detailProfile.ProfileID })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, method: "saveProfile", saveprofile_data: res });
          this.profileSaveStatus = res.status;
          if (res.status == "True") {
            console.log({ page: this.PAGE, status: this.profileSaveStatus });
            this.getAllProfiles();
            console.log({ page: this.PAGE, saveDetail: this.detailProfile });
          } else {
            console.log({ page: this.PAGE, status: this.profileSaveStatus });
          }
        },
        err => console.log({ page: this.PAGE, saveprofile_error: err })
      );
  }

  ngOnDestroy() {
    clearInterval(this.profileStatusCheck);
  }
}
