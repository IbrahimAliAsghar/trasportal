import { TestBed, async, inject } from '@angular/core/testing';

import { StatusBackGuard } from './status-back.guard';

describe('StatusBackGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatusBackGuard]
    });
  });

  it('should ...', inject([StatusBackGuard], (guard: StatusBackGuard) => {
    expect(guard).toBeTruthy();
  }));
});
