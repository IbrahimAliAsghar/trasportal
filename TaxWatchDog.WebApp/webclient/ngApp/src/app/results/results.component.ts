import { Component, OnInit } from "@angular/core";
import { TreeNode, SortEvent } from "primeng/api";
import { ServiceInvokerComponent } from "../service-invoker/service-invoker.component";
import { ApiUrls } from "../api-urls";
import { DataSharingService } from "../data-sharing.service";
import { Router } from "@angular/router";
import { ExcelService } from "../excel.service";
import { ReportCardModels } from "./reportCardModel";
import { filter } from 'rxjs/operators';

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"]
})
export class ResultsComponent implements OnInit {
  PAGE = "RESULTS";
  nullStringValueForReport = "null";

  dtOptions: DataTables.Settings = {}; //settings for angular datatable
  selectedColumns: TreeNode[]; // selected columns from tree for displaying in table
  tableHeaders = []; // column headers extracted from results service
  treeData: TreeNode[]; // object for showing tree with nodes
  resultSet = []; // actual data from results service
  statistics = []; // ratio summary data from results service
  filterFields: any[]; // columns names extracted from selected columns to be used for table filtering
  statisticsHeaders = []; // headers extracted from statistics object for summary table
  profileId = "";
  profileObject: any = {};
  resultsAccumalator = [];
  resultsCheck: boolean = true;
  sendToCmsCheck: boolean = false;
  selectedRow: any = {};
  reportCardData = [];
  reportCardDataHeaders = [];
  whtRCData: any = [];
  whtRCDataHeaders: any = [];
  whtConfig: any = [];
  whtDetails: boolean = false;
  meansTestData: any = [];
  meansTestDataHeaders: any = [];
  meansTestDetails: boolean = false;
  digitalEconomyData: any = [];
  digitalEconomyDataHeaders: any = [];
  digitalEconomyDetails: boolean = false;
  taxProfilingRCData: any = [];
  taxProfilingRCDataHeaders: any = [];
  taxProfilingRCDetails: boolean = false;
  selectedRecords = [];
  auditType: string;
  noteDetail: string;
  cmsResponse: boolean;
  cmsLookupList: any = [];

  cars: any = [];
  cols: any = [];

  featureCount = 0;
  recordsCount = 0;
  count = 0;
  limit = 0;
  offset = 0;

  display = "none";

  constructor(
    private _serviceInvoker: ServiceInvokerComponent,
    private _apiUrls: ApiUrls,
    private _dataService: DataSharingService,
    private _router: Router,
    private _excelService: ExcelService,
    private _reportCardModels: ReportCardModels
  ) {}

  ngOnInit() {
    this._dataService.currentProfileId.subscribe(
      id => (this.profileId = id.toString())
    );
    console.log({ page: this.PAGE, profileId: this.profileId });
    this._dataService.currentProfileObj.subscribe(
      obj => (this.profileObject = obj)
    );
    console.log({ page: this.PAGE, profileObject: this.profileObject });

    this._serviceInvoker
      .getData(this._apiUrls.getFeatureCount, { profile_id: this.profileId })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, featurecount_data: JSON.parse(res)[0] }); // for deployment
          // console.log({ page: this.PAGE, featurecount_data: res[0] }); // for local dev
          this.featureCount = JSON.parse(res)[0].feature_count; // for deployment
          this.recordsCount = JSON.parse(res)[0].rec_count; // for deployment
          // this.featureCount = res[0].feature_count; // for local dev
          // this.recordsCount = res[0].rec_count; // for local dev
          this.limit = this.getRecordsLimit(this.featureCount);
          this.getAllResults();
        },
        err => console.log({ page: this.PAGE, featurecount_error: err })
      );
  }

  getAllResults() {
    console.log({
      page: this.PAGE,
      count_before: this.count,
      offset: this.offset,
      limit: this.limit,
      featureCount: this.featureCount,
      recordCount: this.recordsCount
    });
    if (this.recordsCount > this.resultsAccumalator.length) {
      this._serviceInvoker
        .getData(this._apiUrls.getProfileResults, {
          profileId: this.profileId,
          limit: this.limit,
          offset: this.offset
        })
        .subscribe(
          res => {
            console.log({ page: this.PAGE, response: res });
            let response = JSON.parse(res); // for deployment
            // let response = res; // for local dev
            console.log({ page: this.PAGE, response: response });
            let transposedData = JSON.parse(res)["Transposed"]; // for deployment
            // let transposedData = res["Transposed"]; // for local dev
            let summaryData = JSON.parse(res)["Statistics"]; // for deployment
            // let summaryData = res["Statistics"]; // for local dev
            console.log({ page: this.PAGE, summaryData: summaryData });
            // let transposedLength = transposedData.length;
            console.log({
              page: this.PAGE,
              accLength: this.resultsAccumalator.length,
              recordsCount: this.recordsCount
            });
            this.count += 1;
            this.resultsAccumalator.push(...transposedData);
            console.log({
              page: this.PAGE,
              accumulator: this.resultsAccumalator
            });
            this.offset = this.limit + 1;
            this.limit += this.getRecordsLimit(this.featureCount);
            this.getAllResults();
            if (this.count == 1) {
              // this.setStatistics(res);
              this.statistics = JSON.parse(res)["Statistics"]; // for deployment
              // this.statistics = res["Statistics"]; // for local dev
              console.log({ page: this.PAGE, statistics: this.statistics });
              this.statisticsHeaders = Object.keys(this.statistics[0]);
            }
          },
          err => console.log(err)
        );
    } else {
      let itRefNoList = this.resultsAccumalator.map(item => item.it_ref_no);
      console.log({ page: this.PAGE, results_itRefs: itRefNoList });
      this._serviceInvoker
        .getData(this._apiUrls.cmsLookup, {
          ProfileID: this.profileId,
          itrefno: itRefNoList,
          year: this.profileObject.AssessmentYear
        })
        .subscribe(
          res => {
            this.cmsLookupList = res;
            console.log({ page: this.PAGE, cmsLookup: this.cmsLookupList });

            console.log({
              page: this.PAGE,
              accumulator: this.resultsAccumalator
            });
            this.changeDecimalToNumber(this.resultsAccumalator);
            console.log({ page: this.PAGE, cars: this.cars });
            this.generateTree(this.resultSet);
            this.preselectColumns();
            this.setSelectedColumnsAndFilterFields();
            this.roundOffValues(this.resultSet, "result");
            this.applyThousandSeparator(this.resultSet, "results");
            this.applyThousandSeparator(this.statistics, "stats");
            this.roundOffValues(this.statistics, "stats");
            this.insertStringNull(this.resultSet);
            this.floatScoreValues(this.resultSet);

            // inserting flag for disabling records
            this.resultSet.map(record => {
              let filteredItem = this.cmsLookupList.filter(item => {
                return item.ItrefNo == record.it_ref_no;
              });
              // console.log({ page: this.PAGE, filteredItem: filteredItem });
              record.flag = filteredItem[0].flag;
              console.log({
                page: this.PAGE,
                recordflag: record.flag,
                recorditref: record.it_ref_no
              });
            });
            this.cars = this.resultSet;
            console.log({ page: this.PAGE, cars: this.cars });

            // this.cars.forEach(element => {
            //   console.log({ page: this.PAGE, col_val: element['closing_stock_score'] });
            // });
          },
          err => {
            console.log({ page: this.PAGE, cmsLookup: err });
          }
        );
    }
  }

  getRecordsLimit(fcount) {
    const records = 5000;
    const recordCount = records / fcount; // 5000/24
    const finalLimit = fcount * Math.floor(recordCount); // 24 x 41
    return finalLimit;
  }

  nodeSelect(event) {
    console.log(this.selectedColumns);
    this.setSelectedColumnsAndFilterFields();
  }

  setStatistics(response) {
    this.statistics = JSON.parse(response)["Statistics"];
    this.statisticsHeaders = Object.keys(this.statistics[0]);
  }

  changeDecimalToNumber(resTransposed) {
    this.resultSet = resTransposed.map(item => {
      item.assessment_year = parseInt(item.assessment_year);
      return item;
    });
  }

  preselectColumns() {
    this.selectedColumns = [
      ...this.treeData[0].children,
      ...this.treeData[1].children,
      ...this.treeData[2].children
    ]; // creating array for pre-selection of columns
  }

  generateTree(res) {
    this.tableHeaders = Object.keys(res[0]);
    this.treeData = [
      {
        label: "Values",
        expandedIcon: "fa fa-folder-open",
        collapsedIcon: "fa fa-folder",
        selectable: false,
        children: []
      },
      {
        label: "Scores",
        expandedIcon: "fa fa-folder-open",
        collapsedIcon: "fa fa-folder",
        selectable: false,
        children: []
      },
      {
        label: "Ranks",
        expandedIcon: "fa fa-folder-open",
        collapsedIcon: "fa fa-folder",
        selectable: false,
        children: []
      }
    ];
    for (let i = 0; i < this.tableHeaders.length; i++) {
      if (this.tableHeaders[i].indexOf("rank") !== -1) {
        this.treeData[2]["children"].push({ label: this.tableHeaders[i] });
      } else if (this.tableHeaders[i].indexOf("score") !== -1) {
        this.treeData[1]["children"].push({ label: this.tableHeaders[i] });
      } else if (
        this.tableHeaders[i].indexOf("score") == -1 &&
        this.tableHeaders[i].indexOf("rank") == -1
      ) {
        this.treeData[0]["children"].push({ label: this.tableHeaders[i] });
      }
    }
    console.log({ page: this.PAGE, treeData: this.treeData });
  }

  setSelectedColumnsAndFilterFields() {
    this.cols = this.selectedColumns;
    this.cols = this.selectedColumns.filter(item => {
      if (
        item.label != "Values" &&
        item.label != "Ranks" &&
        item.label != "Scores"
      ) {
        return item;
      }
    });
    this.cols = this.cols.map(item => {
      item.field = item.label;
      return item;
    });
    this.filterFields = this.cols.map(item => item.label);

    console.log({ page: this.PAGE, filterFields: this.filterFields });
    console.log({ page: this.PAGE, cols: this.cols });
  }

  showReportCard(row) {
    console.log(row);
    this.selectedRow = row;
    this._serviceInvoker
      .getData(this._apiUrls.getReportCard, {
        ProfileId: this.profileId,
        It_RefNo: row.it_ref_no
      })
      .subscribe(
        res => {
          console.log({ page: this.PAGE, reportCardAndModels: res });

          const parsedReport = JSON.parse(res); // for deployment
          // const parsedReport = res; // for local dev
          this.reportCardData = parsedReport.report_card_res;
          this.whtRCData = parsedReport.wht_res;
          this.taxProfilingRCData = parsedReport.tax_profiling_res;
          this.meansTestData = parsedReport.means_test_res;
          this.digitalEconomyData = parsedReport.digital_economy_res;

          console.log({ page: this.PAGE, reportCardData: this.reportCardData });

          this.reportCardDataHeaders = this.reportCardData.length > 0 ? Object.keys(this.reportCardData[0]) : [];
          this.whtRCDataHeaders = this.whtRCData.length > 0 ? Object.keys(this.whtRCData[0]) :
          this._reportCardModels.wht_res.filter(object => {
            return object.isDetail == false;
          }).map( object => object.colName );
          // console.log({ page: this.PAGE, whtRCDataHeaders: this.whtRCDataHeaders });

          this.taxProfilingRCDataHeaders = this.taxProfilingRCData.length > 0 ? Object.keys(this.taxProfilingRCData[0]) :
          this._reportCardModels.tax_profiling_res.filter(object => {
            return object.isDetail == false;
          }).map( object => object.colName );

          this.meansTestDataHeaders = this.meansTestData.length > 0 ? Object.keys(this.meansTestData[0]) :
          this._reportCardModels.means_test_res.filter(object => {
            return object.isDetail == false;
          }).map( object => object.colName );

          this.digitalEconomyDataHeaders = this.digitalEconomyData.length > 0 ? Object.keys(this.digitalEconomyData[0]) :
          this._reportCardModels.digital_economy_res.filter(object => {
            return object.isDetail == false;
          }).map( object => object.colName );

          this.roundOffValues(this.reportCardData, "report");
          this.applyThousandSeparator(this.reportCardData, "report");
        },
        err => console.log(err)
      );
  }

  featureDetails(model, feature) {
    let featureDetails: any;
    if (model == "wht_res") {
      featureDetails = this._reportCardModels.wht_res.filter(object => {
        return object.colName == feature;
      });
    } else if (model == "tax_profiling_res") {
      featureDetails = this._reportCardModels.tax_profiling_res.filter(
        object => {
          return object.colName == feature;
        }
      );
    } else if (model == "digital_economy_res") {
      featureDetails = this._reportCardModels.digital_economy_res.filter(
        object => {
          return object.colName == feature;
        }
      );
    } else if (model == "means_test_res") {
      featureDetails = this._reportCardModels.means_test_res.filter(object => {
        return object.colName == feature;
      });
    }
    // console.log({ page: this.PAGE, model: model, feature: feature, featureDetails: featureDetails[0] });
    if(featureDetails[0] == null){
      return {colName: feature, isDetail: true, displayName: feature};
    }else{
      return featureDetails[0];
    }
  }

  exportReportAsExcel(): void {
    let fileName = `${this.selectedRow.it_ref_no}_data`;
    let reportDetails = [
      {
        Column: "Profile",
        Value: this.profileObject.ProfileName
      },
      {
        Column: "Model",
        Value: this.profileObject.ModelName
      },
      {
        Column: "IT Ref No.",
        Value: this.selectedRow.it_ref_no
      }
    ];
    let modelDetails = [
      {
        Column: "External Model",
        Value: "WHT"
      },
      {
        Column: "Feature",
        Value: "risk_factor"
      },
      {
        Column: "Value",
        Value:
          this.whtRCData.length > 0
            ? this.whtRCData[0].wht_final_scoringrisk_factor
            : null
      },
      {
        Column: "External Model",
        Value: "TAX PROFILING"
      },
      {
        Column: "Feature",
        Value: "latesubm_score"
      },
      {
        Column: "Value",
        Value:
          this.taxProfilingRCData.length > 0
            ? this.taxProfilingRCData[0].latesubm_score
            : null
      },
      {
        Column: "External Model",
        Value: "TAX PROFILING"
      },
      {
        Column: "Feature",
        Value: "nonsubm_score"
      },
      {
        Column: "Value",
        Value:
          this.taxProfilingRCData.length > 0
            ? this.taxProfilingRCData[0].nonsubm_score
            : null
      },
      {
        Column: "External Model",
        Value: "TAX PROFILING"
      },
      {
        Column: "Feature",
        Value: "repnc_score"
      },
      {
        Column: "Value",
        Value:
          this.taxProfilingRCData.length > 0
            ? this.taxProfilingRCData[0].repnc_score
            : null
      }
    ];

    let modelDetails_3 = [];

    let excelWhtHeaders = this._reportCardModels.wht_res.map( object => object.colName );
    for (let resKey of excelWhtHeaders ) {
      // if (!this.featureDetails('wht_res', resKey).isDetail) {
        if(this.whtRCData[0] == null){
          let excelModel = {
            "External Model" : "WHT",
            Feature : this.featureDetails('wht_res', resKey).displayName,
            Value : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        } else {
          let excelModel = {
            "External Model" : "WHT",
            Feature : this.featureDetails('wht_res', resKey).displayName,
            Value : this.whtRCData[0][resKey] != null ? this.whtRCData[0][resKey] : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        }
      // }
    }

    let excelTPHeaders = this._reportCardModels.tax_profiling_res.map( object => object.colName );
    for (let resKey of excelTPHeaders ) {
      // if (!this.featureDetails('tax_profiling_res', resKey).isDetail) {
        if(this.taxProfilingRCData[0] == null){
          let excelModel = {
            "External Model" : "TAX PROFILING",
            Feature : this.featureDetails('tax_profiling_res', resKey).displayName,
            Value : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        } else {
          let excelModel = {
            "External Model" : "TAX PROFILING",
            Feature : this.featureDetails('tax_profiling_res', resKey).displayName,
            Value : this.taxProfilingRCData[0][resKey] != null ? this.taxProfilingRCData[0][resKey] : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        }
      // }
    }

    let excelDEHeaders = this._reportCardModels.digital_economy_res.map( object => object.colName );
    for (let resKey of excelDEHeaders ) {
      // if (!this.featureDetails('digital_economy_res', resKey).isDetail) {
        if(this.digitalEconomyData[0] == null){
          let excelModel = {
            "External Model" : "DIGITAL ECONOMY",
            Feature : this.featureDetails('digital_economy_res', resKey).displayName,
            Value : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        } else {
          let excelModel = {
            "External Model" : "DIGITAL ECONOMY",
            Feature : this.featureDetails('digital_economy_res', resKey).displayName,
            Value : this.digitalEconomyData[0][resKey] != null ? this.digitalEconomyData[0][resKey] : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        }
      // }
    }

    let excelMTHeaders = this._reportCardModels.means_test_res.map( object => object.colName );
    for (let resKey of excelMTHeaders ) {
      // if (!this.featureDetails('means_test_res', resKey).isDetail) {
        if(this.meansTestData[0] == null){
          let excelModel = {
            "External Model" : "MEANS TEST",
            Feature : this.featureDetails('means_test_res', resKey).displayName,
            Value : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        } else {
          let excelModel = {
            "External Model" : "MEANS TEST",
            Feature : this.featureDetails('means_test_res', resKey).displayName,
            Value : this.meansTestData[0][resKey] != null ? this.meansTestData[0][resKey] : this.nullStringValueForReport
          };
          modelDetails_3.push(excelModel);
        }
      // }
    }

    console.log({ page: this.PAGE, modelDetails_3: modelDetails_3 });


    this._excelService.exportAsExcelFile(
      [reportDetails, this.reportCardData, modelDetails_3],
      fileName
    );
  }

  navToProfiles() {
    this._dataService.changeProfileId(0);
    this._dataService.changeProfileObj({});
    this._router.navigate(["/profiles"]);
  }

  isFloat(x) {
    // console.log({round: Math.round(x), val : x, type: isNaN(x)});
    return Math.round(x) == x || isNaN(x);
  }

  isInteger(x) {
    // console.log({ round: Math.round(x), val: x, type: !isNaN(x) });
    return Math.round(x) == x && !isNaN(x);
  }

  roundOffValues(arraylist, check) {
    arraylist.map(item => {
      for (let key in item) {
        if (key != "it_ref_no") {
          if (!isNaN(item[key])) {
            if (Math.round(item[key]) == item[key]) {
              item[key] = parseInt(item[key]);
            } else {
              item[key] = parseFloat(item[key]).toFixed(2);
            }
          } else {
            if (check == "result") {
              item[key] = null;
            }
          }
        }
      }
    });
  }

  insertStringNull(arraylist) {
    arraylist.map(item => {
      for (let key in item) {
        if (item[key] == null) {
          item[key] = "null";
          // console.log({ page: this.PAGE, key: key, value: item[key] });
        }
      }
    });
  }

  insertDataTypeNull(arraylist) {
    arraylist.map(item => {
      for (let key in item) {
        if (item[key] == "null") {
          item[key] = null;
          // console.log({ page: this.PAGE, key: key, value: item[key] });
        }
      }
    });
  }

  floatScoreValues(arraylist) {
    arraylist.map(item => {
      for (let key in item) {
        if (key.includes("_score")) {
          if (!isNaN(item[key])) {
            if (Math.round(item[key]) != item[key]) {
              item[key] = parseFloat(item[key]);
              // console.log({ page: this.PAGE, key: key, value: item[key], type: "Float" });
            } else {
              item[key] = parseInt(item[key]);
              // console.log({ page: this.PAGE, key: key, value: item[key], type: "Int" });
            }
          } else {
            item[key] = "null";
          }
        }
      }
    });
  }

  applyThousandSeparator(arraylist, check) {
    arraylist.map(item => {
      for (let key in item) {
        // if (!isNaN(item[key])) {
        if (check == "results") {
          if (key.includes("_val")) {
            if (item[key] != null) {
              item[key] = item[key].toLocaleString();
              // item[key] = item[key].toLocaleString(undefined, {maximumFractionDigits:2});
            }
          }
        } else if (check == "report") {
          if (key != "year_d" && key != "it_ref_no") {
            item[key] = item[key].toLocaleString();
          }
          // } else {
          //   item[key] = item[key].toLocaleString();
          // }
        }
      }
    });
  }

  removeThousandSeparator(arraylist) {
    arraylist.map(item => {
      for (let key in item) {
        if (item[key] != null) {
          if (key != "it_ref_no") {
            if (Math.round(item[key]) != item[key]) {
            } else {
              item[key] = parseInt(item[key].toString().replace("", ""));
            }
          }
        }
      }
    });
  }

  convertStringListToInt(arraylist, field) {
    arraylist.map(item => {
      if (item[field] != null) {
        if (field != "it_ref_no") {
          item[field] = item[field].toString().replace(/,/g, "");
          item[field] = parseFloat(item[field]);
        }
      }
    });
  }

  customSort(event: SortEvent) {
    this.insertDataTypeNull(this.cars);
    if (event.field.includes("_val")) {
      console.log({ page: this.PAGE, event: event, field: event.field });
      this.removeThousandSeparator(event.data);
    }
    this.convertStringListToInt(event.data, event.field);
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null) {
        result = -1;
      } else if (value1 != null && value2 == null) {
        result = 1;
      } else if (value1 == null && value2 == null) {
        result = 0;
      } else if (typeof value1 === "string" && typeof value2 === "string") {
        result = value1.localeCompare(value2);
      } else {
        result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
      }

      return event.order * result;
    });
    this.insertStringNull(this.cars);
    if (event.field.includes("_val")) {
      this.applyThousandSeparator(event.data, "results");
    }
  }

  onMouseEnter(rowData): void {
    rowData.hover = true;
  }

  onMouseLeave(rowData): void {
    rowData.hover = false;
  }

  expandAll() {
    this.treeData.forEach(node => {
      this.expandRecursive(node, true);
    });
  }

  collapseAll() {
    this.treeData.forEach(node => {
      this.expandRecursive(node, false);
    });
  }

  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  selectAll() {
    if (this.selectedColumns == null) {
      this.setSelectedColumnsAndFilterFields();
    } else {
      this.selectedColumns = null;
    }
  }

  sendToCmsSelection() {
    this.sendToCmsCheck = true;
  }

  sendToCmsCancel() {
    this.sendToCmsCheck = false;
  }

  sendToCms() {
    //sending data to cms service
    this.profileObject.NoteDetail = this.noteDetail;
    console.log({
      Profile_Input: this.profileObject,
      UdpUser: localStorage.getItem("username"),
      AuditType: this.auditType,
      itrefno: this.selectedRecords,
      statistics: this.statistics
    });
    this._serviceInvoker
      .getData(this._apiUrls.pushToCms, {
        Profile_Input: this.profileObject,
        UdpUser: "Administrator",
        AuditType: this.auditType,
        itrefno: JSON.stringify(this.selectedRecords),
        statistics: this.statistics
      })
      .subscribe(
        res => {
          console.log({ response: res });
          this.cmsResponse = res.flag;
          this.openModal();
        },
        err => {
          this.cmsResponse = err;
          console.log({ error: err });
          this.openModal();
        }
      );
  }

  sendToCmsForward() {
    if (this.cmsResponse == true) {
      this.closeModal();
      this.sendToCmsCancel();
      this.selectedRecords = [];
      this.auditType = null;
      this.noteDetail = null;

      let itRefNoList = this.resultsAccumalator.map(item => item.it_ref_no);
      this._serviceInvoker
        .getData(this._apiUrls.cmsLookup, {
          ProfileID: this.profileId,
          itrefno: itRefNoList,
          year: this.profileObject.AssessmentYear
        })
        .subscribe(
          res => {
            this.cmsLookupList = res;
            console.log({ page: this.PAGE, cmsLookup: this.cmsLookupList });

            // inserting flag for disabling records
            this.resultSet.map(record => {
              let filteredItem = this.cmsLookupList.filter(item => {
                return item.ItrefNo == record.it_ref_no;
              });
              // console.log({ page: this.PAGE, filteredItem: filteredItem });
              record.flag = filteredItem[0].flag;
              console.log({
                page: this.PAGE,
                recordflag: record.flag,
                recorditref: record.it_ref_no
              });
            });
            this.cars = this.resultSet;
            console.log({ page: this.PAGE, cars: this.cars });
          },
          err => {
            console.log({ page: this.PAGE, cmsLookup: err });
          }
        );
    } else {
      this.closeModal();
    }
  }

  cmsRowCheck(itref) {
    console.log({ itref: itref });
    console.log({ list: this.cmsLookupList });
    let record = this.cmsLookupList.filter(item => item.ItrefNo == itref);
    console.log({ record });
    return record.flag;
    // return true;
  }

  openModal() {
    this.display = "block";
  }

  closeModal() {
    this.display = "none";
  }
}
