export class ReportCardModels {
  public wht_res = [
    {
      colName: "wht_final_scoringrisk_factor",
      isDetail: false,
      displayName: "risk factor"
    },
    {
      colName: "wht_final_scoringassessment_year",
      isDetail: true,
      displayName: "assessment year"
    },
    {
      colName: "wht_final_scoringit_ref_no",
      isDetail: true,
      displayName: "it_ref_no"
    },
    {
      colName: "wht_final_scoringmain_business_code",
      isDetail: true,
      displayName: "main business code"
    },
    {
      colName: "wht_final_scoringit_assm_branch",
      isDetail: true,
      displayName: "it assm branch"
    },
    {
      colName: "wht_final_scoringscoring",
      isDetail: true,
      displayName: "scoring"
    },
    {
      colName: "wht_final_scoringl15",
      isDetail: true,
      displayName: "scoring l15"
    },
    {
      colName: "wht_final_scoringl16",
      isDetail: true,
      displayName: "scoring l16"
    },
    {
      colName: "wht_final_scoringl20",
      isDetail: true,
      displayName: "scoring l20"
    },
    {
      colName: "wht_final_scoringl21",
      isDetail: true,
      displayName: "scoring l21"
    },
    {
      colName: "wht_final_scoringm0",
      isDetail: true,
      displayName: "scoring m0"
    },
    {
      colName: "wht_final_scoringm1",
      isDetail: true,
      displayName: "scoring m1"
    },
    {
      colName: "wht_final_scoringm2",
      isDetail: true,
      displayName: "scoring m2"
    },
    {
      colName: "wht_final_scoringm3",
      isDetail: true,
      displayName: "scoring m3"
    },
    {
      colName: "wht_final_scoringm4",
      isDetail: true,
      displayName: "scoring m4"
    },
    {
      colName: "wht_final_scoringm5",
      isDetail: true,
      displayName: "scoring m5"
    },
    {
      colName: "wht_final_scoringm6",
      isDetail: true,
      displayName: "scoring m6"
    },
    {
      colName: "wht_final_scoringn4",
      isDetail: true,
      displayName: "scoring n4"
    },
    {
      colName: "wht_final_scoringn6",
      isDetail: true,
      displayName: "scoring n6"
    },
    {
      colName: "wht_final_scoringl13_n10",
      isDetail: true,
      displayName: "scoring l13_n10"
    },
    {
      colName: "wht_final_scoringr1",
      isDetail: true,
      displayName: "scoring r1"
    },
    {
      colName: "wht_final_scoringtaxhaven",
      isDetail: true,
      displayName: "scoring tax haven"
    },
    {
      colName: "wht_final_scoringflags",
      isDetail: true,
      displayName: "scoring flags"
    }
  ];

  public tax_profiling_res = [
    {
      colName: "latesubm_score",
      isDetail: false,
      displayName: "late submission score"
    },
    {
      colName: "nonsubm_score",
      isDetail: false,
      displayName: "non submission score"
    },
    {
      colName: "repnc_score",
      isDetail: false,
      displayName: "repnc score"
    },
    {
      colName: "year_asst",
      isDetail: true,
      displayName: "assessment year"
    },
    {
      colName: "it_ref_no",
      isDetail: true,
      displayName: "it ref no"
    },
    {
      colName: "rec_id",
      isDetail: true,
      displayName: "record id"
    },
    {
      colName: "file_type",
      isDetail: true,
      displayName: "file type"
    },
    {
      colName: "comb_id",
      isDetail: true,
      displayName: "comb id"
    },
    {
      colName: "comb_rule_id",
      isDetail: true,
      displayName: "comb rule id"
    },
    {
      colName: "rule_id",
      isDetail: true,
      displayName: "rule id"
    }
  ];

  public means_test_res = [
    {
      colName: "taxpayer_it_ref_no",
      isDetail: true,
      displayName: "taxpayer it ref no"
    },
    { colName: "year_7", isDetail: true, displayName: "year 7" },
    { colName: "year_6", isDetail: true, displayName: "year 6" },
    { colName: "year_5", isDetail: true, displayName: "year 5" },
    { colName: "year_4", isDetail: true, displayName: "year 4" },
    { colName: "year_3", isDetail: true, displayName: "year 3" },
    { colName: "year_2", isDetail: true, displayName: "year 2" },
    { colName: "year_1", isDetail: true, displayName: "year 1" },
    { colName: "year_0", isDetail: true, displayName: "year 0" },
    { colName: "s_year_7", isDetail: true, displayName: "s year 7" },
    { colName: "s_year_6", isDetail: true, displayName: "s year 6" },
    { colName: "s_year_5", isDetail: true, displayName: "s year 5" },
    { colName: "s_year_4", isDetail: true, displayName: "s year 4" },
    { colName: "s_year_3", isDetail: true, displayName: "s year 3" },
    { colName: "s_year_2", isDetail: true, displayName: "s year 2" },
    { colName: "s_year_1", isDetail: true, displayName: "s year 1" },
    { colName: "s_year_0", isDetail: true, displayName: "s year 0" },
    { colName: "inc_year_7", isDetail: true, displayName: "inc year 7" },
    { colName: "inc_year_6", isDetail: true, displayName: "inc year 6" },
    { colName: "inc_year_5", isDetail: true, displayName: "inc year 5" },
    { colName: "inc_year_4", isDetail: true, displayName: "inc year 4" },
    { colName: "inc_year_3", isDetail: true, displayName: "inc year 3" },
    { colName: "inc_year_2", isDetail: true, displayName: "inc year 2" },
    { colName: "inc_year_1", isDetail: true, displayName: "inc year 1" },
    { colName: "inc_year_0", isDetail: true, displayName: "inc year 0" },
    { colName: "sp_inc_year_7", isDetail: true, displayName: "sp inc year 7" },
    { colName: "sp_inc_year_6", isDetail: true, displayName: "sp inc year 6" },
    { colName: "sp_inc_year_5", isDetail: true, displayName: "sp inc year 5" },
    { colName: "sp_inc_year_4", isDetail: true, displayName: "sp inc year 4" },
    { colName: "sp_inc_year_3", isDetail: true, displayName: "sp inc year 3" },
    { colName: "sp_inc_year_2", isDetail: true, displayName: "sp inc year 2" },
    { colName: "sp_inc_year_1", isDetail: true, displayName: "sp inc year 1" },
    { colName: "sp_inc_year_0", isDetail: true, displayName: "sp inc year 0" },
    {
      colName: "total_unrep_income_y7",
      isDetail: true,
      displayName: "total unrep income y7"
    },
    {
      colName: "total_unrep_income_y6",
      isDetail: true,
      displayName: "total unrep income y6"
    },
    {
      colName: "total_unrep_income_y5",
      isDetail: true,
      displayName: "total unrep income y5"
    },
    {
      colName: "total_unrep_income_y4",
      isDetail: true,
      displayName: "total unrep income y4"
    },
    {
      colName: "total_unrep_income_y3",
      isDetail: true,
      displayName: "total unrep income y3"
    },
    {
      colName: "total_unrep_income_y2",
      isDetail: true,
      displayName: "total unrep income y2"
    },
    {
      colName: "total_unrep_income_y1",
      isDetail: true,
      displayName: "total unrep income y1"
    },
    {
      colName: "total_unrep_income_y0",
      isDetail: true,
      displayName: "total unrep income y0"
    },
    {
      colName: "tax_unrep_income_y7",
      isDetail: true,
      displayName: "tax unrep income y7"
    },
    {
      colName: "tax_unrep_income_y6",
      isDetail: true,
      displayName: "tax unrep income y6"
    },
    {
      colName: "tax_unrep_income_y5",
      isDetail: true,
      displayName: "tax unrep income y5"
    },
    {
      colName: "tax_unrep_income_y4",
      isDetail: true,
      displayName: "tax unrep income y4"
    },
    {
      colName: "tax_unrep_income_y3",
      isDetail: true,
      displayName: "tax unrep income y3"
    },
    {
      colName: "tax_unrep_income_y2",
      isDetail: true,
      displayName: "tax unrep income y2"
    },
    {
      colName: "tax_unrep_income_y1",
      isDetail: true,
      displayName: "tax unrep income y1"
    },
    {
      colName: "tax_unrep_income_y0",
      isDetail: false,
      displayName: "tax unrep income y0"
    },
    {
      colName: "sp_unrep_income_y7",
      isDetail: true,
      displayName: "sp unrep income y7"
    },
    {
      colName: "sp_unrep_income_y6",
      isDetail: true,
      displayName: "sp unrep income y6"
    },
    {
      colName: "sp_unrep_income_y5",
      isDetail: true,
      displayName: "sp unrep income y5"
    },
    {
      colName: "sp_unrep_income_y4",
      isDetail: true,
      displayName: "sp unrep income y4"
    },
    {
      colName: "sp_unrep_income_y3",
      isDetail: true,
      displayName: "sp unrep income y3"
    },
    {
      colName: "sp_unrep_income_y2",
      isDetail: true,
      displayName: "sp unrep income y2"
    },
    {
      colName: "sp_unrep_income_y1",
      isDetail: true,
      displayName: "sp unrep income y1"
    },
    {
      colName: "sp_unrep_income_y0",
      isDetail: true,
      displayName: "sp unrep income y0"
    },
    {
      colName: "total_assets_year_7",
      isDetail: true,
      displayName: "total assets year 7"
    },
    {
      colName: "total_assets_year_6",
      isDetail: true,
      displayName: "total assets year 6"
    },
    {
      colName: "total_assets_year_5",
      isDetail: true,
      displayName: "total assets year 5"
    },
    {
      colName: "total_assets_year_4",
      isDetail: true,
      displayName: "total assets year 4"
    },
    {
      colName: "total_assets_year_3",
      isDetail: true,
      displayName: "total assets year 3"
    },
    {
      colName: "total_assets_year_2",
      isDetail: true,
      displayName: "total assets year 2"
    },
    {
      colName: "total_assets_year_1",
      isDetail: true,
      displayName: "total assets year 1"
    },
    {
      colName: "total_assets_year_0",
      isDetail: true,
      displayName: "total assets year 0"
    },
    {
      colName: "total_income_year_7",
      isDetail: true,
      displayName: "total income year 7"
    },
    {
      colName: "total_income_year_6",
      isDetail: true,
      displayName: "total income year 6"
    },
    {
      colName: "total_income_year_5",
      isDetail: true,
      displayName: "total income year 5"
    },
    {
      colName: "total_income_year_4",
      isDetail: true,
      displayName: "total income year 4"
    },
    {
      colName: "total_income_year_3",
      isDetail: true,
      displayName: "total income year 3"
    },
    {
      colName: "total_income_year_2",
      isDetail: true,
      displayName: "total income year 2"
    },
    {
      colName: "total_income_year_1",
      isDetail: true,
      displayName: "total income year 1"
    },
    {
      colName: "total_income_year_0",
      isDetail: true,
      displayName: "total income year 0"
    },
    {
      colName: "taxpayer_per_year_7",
      isDetail: true,
      displayName: "taxpayer per year 7"
    },
    {
      colName: "taxpayer_per_year_6",
      isDetail: true,
      displayName: "taxpayer per year 6"
    },
    {
      colName: "taxpayer_per_year_5",
      isDetail: true,
      displayName: "taxpayer per year 5"
    },
    {
      colName: "taxpayer_per_year_4",
      isDetail: true,
      displayName: "taxpayer per year 4"
    },
    {
      colName: "taxpayer_per_year_3",
      isDetail: true,
      displayName: "taxpayer per year 3"
    },
    {
      colName: "taxpayer_per_year_2",
      isDetail: true,
      displayName: "taxpayer per year 2"
    },
    {
      colName: "taxpayer_per_year_1",
      isDetail: true,
      displayName: "taxpayer per year 1"
    },
    {
      colName: "taxpayer_per_year_0",
      isDetail: false,
      displayName: "taxpayer per year 0"
    },
    { colName: "sp_per_year_7", isDetail: true, displayName: "sp per year 7" },
    { colName: "sp_per_year_6", isDetail: true, displayName: "sp per year 6" },
    { colName: "sp_per_year_5", isDetail: true, displayName: "sp per year 5" },
    { colName: "sp_per_year_4", isDetail: true, displayName: "sp per year 4" },
    { colName: "sp_per_year_3", isDetail: true, displayName: "sp per year 3" },
    { colName: "sp_per_year_2", isDetail: true, displayName: "sp per year 2" },
    { colName: "sp_per_year_1", isDetail: true, displayName: "sp per year 1" },
    { colName: "sp_per_year_0", isDetail: true, displayName: "sp per year 0" },
    {
      colName: "percentage_year_7",
      isDetail: true,
      displayName: "percentage year 7"
    },
    {
      colName: "percentage_year_6",
      isDetail: true,
      displayName: "percentage year 6"
    },
    {
      colName: "percentage_year_5",
      isDetail: true,
      displayName: "percentage year 5"
    },
    {
      colName: "percentage_year_4",
      isDetail: true,
      displayName: "percentage year 4"
    },
    {
      colName: "percentage_year_3",
      isDetail: true,
      displayName: "percentage year 3"
    },
    {
      colName: "percentage_year_2",
      isDetail: true,
      displayName: "percentage year 2"
    },
    {
      colName: "percentage_year_1",
      isDetail: true,
      displayName: "percentage year 1"
    },
    {
      colName: "percentage_year_0",
      isDetail: true,
      displayName: "percentage year 0"
    },
    { colName: "taxpayer_key", isDetail: true, displayName: "taxpayer key" },
    { colName: "file_type", isDetail: true, displayName: "file type" },
    {
      colName: "it_ref_no__mt_assets_income_fact_calculation_",
      isDetail: true,
      displayName: "it ref no  mt assets income fact calculation"
    },
    { colName: "new_ic_no", isDetail: true, displayName: "new ic no" },
    { colName: "old_ic_no", isDetail: true, displayName: "old ic no" },
    { colName: "name", isDetail: true, displayName: "name" },
    { colName: "passport_no", isDetail: true, displayName: "passport no" },
    { colName: "police_no", isDetail: true, displayName: "police no" },
    { colName: "army_no", isDetail: true, displayName: "army no" },
    { colName: "rob_no", isDetail: true, displayName: "rob no" },
    { colName: "roc_no", isDetail: true, displayName: "roc no" },
    { colName: "phone_no", isDetail: true, displayName: "phone no" },
    { colName: "sex", isDetail: true, displayName: "sex" },
    {
      colName: "marital_status",
      isDetail: true,
      displayName: "marital status"
    },
    {
      colName: "malaysian_citizen",
      isDetail: true,
      displayName: "malaysian citizen"
    },
    {
      colName: "citizen_country",
      isDetail: true,
      displayName: "citizen country"
    },
    { colName: "epf_no", isDetail: true, displayName: "epf no" },
    { colName: "employer_no", isDetail: true, displayName: "employer no" },
    {
      colName: "registration_date",
      isDetail: true,
      displayName: "registration date"
    },
    { colName: "file_status", isDetail: true, displayName: "file status" },
    { colName: "vip_code", isDetail: true, displayName: "vip code" },
    {
      colName: "no_of_partnership",
      isDetail: true,
      displayName: "no of partnership"
    },
    {
      colName: "coorporation_type",
      isDetail: true,
      displayName: "coorporation type"
    },
    { colName: "trust_type", isDetail: true, displayName: "trust type" },
    {
      colName: "registration_cert_no",
      isDetail: true,
      displayName: "registration cert no"
    },
    {
      colName: "will_indicator",
      isDetail: true,
      displayName: "will indicator"
    },
    {
      colName: "authorisation_letter_indicator",
      isDetail: true,
      displayName: "authorisation letter indicator"
    },
    {
      colName: "authorisation_letter_date",
      isDetail: true,
      displayName: "authorisation letter date"
    },
    {
      colName: "local_deceased_indicator",
      isDetail: true,
      displayName: "local deceased indicator"
    },
    {
      colName: "decease_country_code",
      isDetail: true,
      displayName: "decease country code"
    },
    { colName: "race_code", isDetail: true, displayName: "race code" },
    { colName: "religion_code", isDetail: true, displayName: "religion code" },
    {
      colName: "profession_cord",
      isDetail: true,
      displayName: "profession cord"
    },
    {
      colName: "immigration_indicator",
      isDetail: true,
      displayName: "immigration indicator"
    },
    {
      colName: "total_children",
      isDetail: true,
      displayName: "total children"
    },
    { colName: "pcb_type", isDetail: true, displayName: "pcb type" },
    {
      colName: "it_assm_branch",
      isDetail: true,
      displayName: "it assm branch"
    },
    {
      colName: "it_collection_branch",
      isDetail: true,
      displayName: "it collection branch"
    },
    {
      colName: "ckht_assm_branch",
      isDetail: true,
      displayName: "ckht assm branch"
    },
    {
      colName: "ckht_collection_branch",
      isDetail: true,
      displayName: "ckht collection branch"
    },
    {
      colName: "start_of_account_period",
      isDetail: true,
      displayName: "start of account period"
    },
    {
      colName: "end_of_account_period",
      isDetail: true,
      displayName: "end of account period"
    },
    {
      colName: "company_status",
      isDetail: true,
      displayName: "company status"
    },
    { colName: "active_flag", isDetail: true, displayName: "active flag" },
    { colName: "est_ind", isDetail: true, displayName: "est ind" },
    { colName: "honorific_cd", isDetail: true, displayName: "honorific cd" },
    { colName: "title_name", isDetail: true, displayName: "title name" },
    { colName: "typ", isDetail: true, displayName: "typ" },
    { colName: "resident_ind", isDetail: true, displayName: "resident ind" },
    {
      colName: "resident_country_cd",
      isDetail: true,
      displayName: "resident country cd"
    },
    { colName: "class_cd", isDetail: true, displayName: "class cd" },
    { colName: "company_typ", isDetail: true, displayName: "company typ" },
    {
      colName: "creation_reason",
      isDetail: true,
      displayName: "creation reason"
    },
    { colName: "permanent_ind", isDetail: true, displayName: "permanent ind" },
    {
      colName: "net_it_lgr_dr_amt",
      isDetail: true,
      displayName: "net it lgr dr amt"
    },
    {
      colName: "net_it_refund_cr_amt",
      isDetail: true,
      displayName: "net it refund cr amt"
    },
    {
      colName: "net_ckht_lgrbl_amt",
      isDetail: true,
      displayName: "net ckht lgrbl amt"
    },
    {
      colName: "net_it_upos_cr_amt",
      isDetail: true,
      displayName: "net it upos cr amt"
    },
    {
      colName: "net_it_upos_dr_amt",
      isDetail: true,
      displayName: "net it upos dr amt"
    },
    {
      colName: "investigation_ind",
      isDetail: true,
      displayName: "investigation ind"
    },
    {
      colName: "invest_asmstart_yr",
      isDetail: true,
      displayName: "invest asmstart yr"
    },
    {
      colName: "invest_asm_end_yr",
      isDetail: true,
      displayName: "invest asm end yr"
    },
    {
      colName: "cmulatve_asset_amt",
      isDetail: true,
      displayName: "cmulatve asset amt"
    },
    {
      colName: "last_cp77_cp38_dt",
      isDetail: true,
      displayName: "last cp77 cp38 dt"
    },
    {
      colName: "sect_8_exempt_ind",
      isDetail: true,
      displayName: "sect 8 exempt ind"
    },
    {
      colName: "section_104_ind",
      isDetail: true,
      displayName: "section 104 ind"
    },
    {
      colName: "marked_as_dupl_ind",
      isDetail: true,
      displayName: "marked as dupl ind"
    },
    {
      colName: "write_off_stat",
      isDetail: true,
      displayName: "write off stat"
    },
    {
      colName: "write_off_reasoncd",
      isDetail: true,
      displayName: "write off reasoncd"
    },
    { colName: "remarks", isDetail: true, displayName: "remarks" },
    { colName: "inactive_dt", isDetail: true, displayName: "inactive dt" },
    { colName: "activation_dt", isDetail: true, displayName: "activation dt" },
    { colName: "bankrupt_dt", isDetail: true, displayName: "bankrupt dt" },
    { colName: "bankrupt_ind", isDetail: true, displayName: "bankrupt ind" },
    {
      colName: "e_malaysia_itrefno",
      isDetail: true,
      displayName: "e malaysia itrefno"
    },
    { colName: "bank_cd", isDetail: true, displayName: "bank cd" },
    { colName: "bank_acct_no", isDetail: true, displayName: "bank acct no" },
    {
      colName: "num_of_time_audit",
      isDetail: true,
      displayName: "num of time audit"
    },
    { colName: "audit_cycle", isDetail: true, displayName: "audit cycle" },
    {
      colName: "effective_date",
      isDetail: true,
      displayName: "effective date"
    },
    {
      colName: "source_system_id",
      isDetail: true,
      displayName: "source system id"
    },
    {
      colName: "loading_timestamp",
      isDetail: true,
      displayName: "loading timestamp"
    },
    { colName: "nr_start_year", isDetail: true, displayName: "nr start year" },
    { colName: "nr_end_year", isDetail: true, displayName: "nr end year" },
    { colName: "source_flag", isDetail: true, displayName: "source flag" },
    { colName: "birth_dt", isDetail: true, displayName: "birth dt" },
    { colName: "death_dt", isDetail: true, displayName: "death dt" },
    {
      colName: "bank_code_stsc",
      isDetail: true,
      displayName: "bank code stsc"
    },
    { colName: "bank_name", isDetail: true, displayName: "bank name" },
    {
      colName: "processing_dttm",
      isDetail: true,
      displayName: "processing dttm"
    },
    {
      colName: "loading_timestamp_month",
      isDetail: true,
      displayName: "loading timestamp month"
    },
    { colName: "rownum", isDetail: true, displayName: "rownum" },
    {
      colName: "taxpayer_it_ref_no_new",
      isDetail: true,
      displayName: "taxpayer it ref no new"
    },
    { colName: "it_ref_no", isDetail: true, displayName: "it ref no" },
    { colName: "model", isDetail: true, displayName: "model" },
    {
      colName: "vehicle_reg_no",
      isDetail: true,
      displayName: "vehicle reg no"
    },
    { colName: "asset_value", isDetail: true, displayName: "asset value" },
    { colName: "purchase_date", isDetail: true, displayName: "purchase date" },
    {
      colName: "monthly_installment",
      isDetail: true,
      displayName: "monthly installment"
    },
    {
      colName: "year_7__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 7  mt spouse vehicle information"
    },
    {
      colName: "year_6__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 6  mt spouse vehicle information"
    },
    {
      colName: "year_5__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 5  mt spouse vehicle information"
    },
    {
      colName: "year_4__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 4  mt spouse vehicle information"
    },
    {
      colName: "year_3__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 3  mt spouse vehicle information"
    },
    {
      colName: "year_2__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 2  mt spouse vehicle information"
    },
    {
      colName: "year_1__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 1  mt spouse vehicle information"
    },
    {
      colName: "year_0__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "year 0  mt spouse vehicle information"
    },
    {
      colName: "taxpayer_it_ref_no__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "taxpayer it ref no  mt spouse vehicle information"
    },
    {
      colName: "spouse_itref_no",
      isDetail: true,
      displayName: "spouse itref no"
    },
    { colName: "s_model", isDetail: true, displayName: "s model" },
    {
      colName: "s_vehicle_reg_no",
      isDetail: true,
      displayName: "s vehicle reg no"
    },
    { colName: "s_asset_value", isDetail: true, displayName: "s asset value" },
    {
      colName: "s_purchase_date",
      isDetail: true,
      displayName: "s purchase date"
    },
    {
      colName: "s_monthly_installment",
      isDetail: true,
      displayName: "s monthly installment"
    },
    {
      colName: "s_year_7__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 7  mt spouse vehicle information"
    },
    {
      colName: "s_year_6__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 6  mt spouse vehicle information"
    },
    {
      colName: "s_year_5__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 5  mt spouse vehicle information"
    },
    {
      colName: "s_year_4__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 4  mt spouse vehicle information"
    },
    {
      colName: "s_year_3__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 3  mt spouse vehicle information"
    },
    {
      colName: "s_year_2__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 2  mt spouse vehicle information"
    },
    {
      colName: "s_year_1__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 1  mt spouse vehicle information"
    },
    {
      colName: "s_year_0__mt_spouse_vehicle_information_",
      isDetail: true,
      displayName: "s year 0  mt spouse vehicle information"
    },
    {
      colName: "taxpayer_it_ref_no_new__mt_spouse_property_information_",
      isDetail: true,
      displayName: "taxpayer it ref no new  mt spouse property information"
    },
    {
      colName: "it_ref_no__mt_spouse_property_information_",
      isDetail: true,
      displayName: "it ref no  mt spouse property information"
    },
    { colName: "address", isDetail: true, displayName: "address" },
    { colName: "property_type", isDetail: true, displayName: "property type" },
    {
      colName: "asset_value__mt_spouse_property_information_",
      isDetail: true,
      displayName: "asset value  mt spouse property information"
    },
    {
      colName: "purchase_date__mt_spouse_property_information_",
      isDetail: true,
      displayName: "purchase date  mt spouse property information"
    },
    {
      colName: "monthly_installment__mt_spouse_property_information_",
      isDetail: true,
      displayName: "monthly installment  mt spouse property information"
    },
    {
      colName: "year_7__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 7  mt spouse property information"
    },
    {
      colName: "year_6__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 6  mt spouse property information"
    },
    {
      colName: "year_5__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 5  mt spouse property information"
    },
    {
      colName: "year_4__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 4  mt spouse property information"
    },
    {
      colName: "year_3__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 3  mt spouse property information"
    },
    {
      colName: "year_2__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 2  mt spouse property information"
    },
    {
      colName: "year_1__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 1  mt spouse property information"
    },
    {
      colName: "year_0__mt_spouse_property_information_",
      isDetail: true,
      displayName: "year 0  mt spouse property information"
    },
    {
      colName: "taxpayer_it_ref_no__mt_spouse_property_information_",
      isDetail: true,
      displayName: "taxpayer it ref no  mt spouse property information"
    },
    {
      colName: "spouse_it_ref_no",
      isDetail: true,
      displayName: "spouse it ref no"
    },
    { colName: "s_address", isDetail: true, displayName: "s address" },
    {
      colName: "s_property_type",
      isDetail: true,
      displayName: "s property type"
    },
    {
      colName: "s_asset_value__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s asset value  mt spouse property information"
    },
    {
      colName: "s_purchase_date__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s purchase date  mt spouse property information"
    },
    {
      colName: "s_monthly_installment__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s monthly installment  mt spouse property information"
    },
    {
      colName: "s_year_7__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 7  mt spouse property information"
    },
    {
      colName: "s_year_6__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 6  mt spouse property information"
    },
    {
      colName: "s_year_5__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 5  mt spouse property information"
    },
    {
      colName: "s_year_4__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 4  mt spouse property information"
    },
    {
      colName: "s_year_3__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 3  mt spouse property information"
    },
    {
      colName: "s_year_2__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 2  mt spouse property information"
    },
    {
      colName: "s_year_1__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 1  mt spouse property information"
    },
    {
      colName: "s_year_0__mt_spouse_property_information_",
      isDetail: true,
      displayName: "s year 0  mt spouse property information"
    },
    {
      colName: "taxpayer_it_ref_no_new__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "taxpayer it ref no new  mt spouse means share information"
    },
    {
      colName: "it_ref_no__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "it ref no  mt spouse means share information"
    },
    { colName: "company_name", isDetail: true, displayName: "company name" },
    { colName: "amount", isDetail: true, displayName: "amount" },
    { colName: "transfer_date", isDetail: true, displayName: "transfer date" },
    {
      colName: "year_7__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 7  mt spouse means share information"
    },
    {
      colName: "year_6__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 6  mt spouse means share information"
    },
    {
      colName: "year_5__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 5  mt spouse means share information"
    },
    {
      colName: "year_4__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 4  mt spouse means share information"
    },
    {
      colName: "year_3__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 3  mt spouse means share information"
    },
    {
      colName: "year_2__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 2  mt spouse means share information"
    },
    {
      colName: "year_1__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 1  mt spouse means share information"
    },
    {
      colName: "year_0__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "year 0  mt spouse means share information"
    },
    {
      colName: "taxpayer_it_ref_no__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "taxpayer it ref no  mt spouse means share information"
    },
    {
      colName: "spouse_it_ref_no__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "spouse it ref no  mt spouse means share information"
    },
    {
      colName: "s_company_name",
      isDetail: true,
      displayName: "s company name"
    },
    { colName: "s_amount", isDetail: true, displayName: "s amount" },
    {
      colName: "s_transfer_date",
      isDetail: true,
      displayName: "s transfer date"
    },
    {
      colName: "s_year_7__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 7  mt spouse means share information"
    },
    {
      colName: "s_year_6__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 6  mt spouse means share information"
    },
    {
      colName: "s_year_5__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 5  mt spouse means share information"
    },
    {
      colName: "s_year_4__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 4  mt spouse means share information"
    },
    {
      colName: "s_year_3__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 3  mt spouse means share information"
    },
    {
      colName: "s_year_2__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 2  mt spouse means share information"
    },
    {
      colName: "s_year_1__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 1  mt spouse means share information"
    },
    {
      colName: "s_year_0__mt_spouse_means_share_information_",
      isDetail: true,
      displayName: "s year 0  mt spouse means share information"
    },
    {
      colName: "taxpayer_it_ref_no_new__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "taxpayer it ref no new  mt spouse means rental information"
    },
    {
      colName: "it_ref_no__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "it ref no  mt spouse means rental information"
    },
    { colName: "building_type", isDetail: true, displayName: "building type" },
    {
      colName: "address__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "address  mt spouse means rental information"
    },
    { colName: "rent_amount", isDetail: true, displayName: "rent amount" },
    { colName: "start_date", isDetail: true, displayName: "start date" },
    { colName: "rent_period", isDetail: true, displayName: "rent period" },
    {
      colName: "con_rent_period",
      isDetail: true,
      displayName: "con rent period"
    },
    {
      colName: "year_7__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 7  mt spouse means rental information"
    },
    {
      colName: "year_6__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 6  mt spouse means rental information"
    },
    {
      colName: "year_5__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 5  mt spouse means rental information"
    },
    {
      colName: "year_4__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 4  mt spouse means rental information"
    },
    {
      colName: "year_3__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 3  mt spouse means rental information"
    },
    {
      colName: "year_2__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 2  mt spouse means rental information"
    },
    {
      colName: "year_1__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 1  mt spouse means rental information"
    },
    {
      colName: "year_0__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "year 0  mt spouse means rental information"
    },
    {
      colName: "taxpayer_it_ref_no__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "taxpayer it ref no  mt spouse means rental information"
    },
    {
      colName: "spouse_it_ref_no__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "spouse it ref no  mt spouse means rental information"
    },
    {
      colName: "s_building_type",
      isDetail: true,
      displayName: "s building type"
    },
    {
      colName: "s_address__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s address  mt spouse means rental information"
    },
    { colName: "s_rent_amount", isDetail: true, displayName: "s rent amount" },
    { colName: "s_start_date", isDetail: true, displayName: "s start date" },
    { colName: "s_rent_period", isDetail: true, displayName: "s rent period" },
    {
      colName: "s_con_rent_period",
      isDetail: true,
      displayName: "s con rent period"
    },
    {
      colName: "s_year_7__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 7  mt spouse means rental information"
    },
    {
      colName: "s_year_6__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 6  mt spouse means rental information"
    },
    {
      colName: "s_year_5__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 5  mt spouse means rental information"
    },
    {
      colName: "s_year_4__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 4  mt spouse means rental information"
    },
    {
      colName: "s_year_3__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 3  mt spouse means rental information"
    },
    {
      colName: "s_year_2__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 2  mt spouse means rental information"
    },
    {
      colName: "s_year_1__mt_spouse_means_rental_information_",
      isDetail: true,
      displayName: "s year 1  mt spouse means rental information"
    }
  ];

  public digital_economy_res = [
    {
      colName: "taxpayer_name",
      isDetail: true,
      displayName: "taxpayer name"
    },
    {
      colName: "it_ref_no",
      isDetail: true,
      displayName: "it ref no"
    },
    {
      colName: "similarity_score",
      isDetail: false,
      displayName: "similarity score"
    },
    {
      colName: "external_name",
      isDetail: true,
      displayName: "external name"
    },
    {
      colName: "phone",
      isDetail: true,
      displayName: "phone"
    },
    {
      colName: "category",
      isDetail: true,
      displayName: "category"
    },
    {
      colName: "source",
      isDetail: true,
      displayName: "source"
    },
    {
      colName: "url",
      isDetail: true,
      displayName: "url"
    },
    {
      colName: "address",
      isDetail: true,
      displayName: "address"
    },
    {
      colName: "price",
      isDetail: true,
      displayName: "price"
    }
  ];
}
