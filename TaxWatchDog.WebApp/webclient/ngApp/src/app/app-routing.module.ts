import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { FeaturesComponent } from './features/features.component';
import { ResultsComponent } from './results/results.component';
import { JobStatusComponent } from './job-status/job-status.component';
import { AuthGuard } from './auth.guard';
import { StatusBackGuard } from './status-back.guard';
import { CmsReportComponent } from './cms-report/cms-report.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'profiles',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profiles',
    component: ProfilesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'features',
    component: FeaturesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'results',
    component: ResultsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'status',
    component: JobStatusComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'cmsreport',
    component: CmsReportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: ProfilesComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
