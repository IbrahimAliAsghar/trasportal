export class ApiUrls {

  //EXPRESS SERVICES

  // public loginDemo = "http://localhost:3000/api/loginDemoExpress";
  // public logoutDemo = "http://localhost:3000/api/logoutDemo";

  // public getDropdowns = "http://localhost:3000/api/getdropdowndata";
  // public getAllProfiles = "http://localhost:3000/api/getallprofiles";
  // public saveProfile = "http://localhost:3000/api/saveprofile";
  // public getProfileObject = "http://localhost:3000/api/GetProfileById";

  // public getAllModels = "http://localhost:3000/api/getallmodels";
  // public getFeaturesByModel = "http://localhost:3000/api/getfeaturesbymodelid";
  // public getQueryCount = "http://localhost:3000/api/GetODSQueryCount";
  // public createProfile = "http://localhost:3000/api/createprofile";
  // public editProfile = "http://localhost:3000/api/EditProfile";

  // public getProfileResults = "http://localhost:3000/api/GetProfileResultsById";
  // public getReportCard = "http://localhost:3000/api/GetReportCardData";
  // public getFeatureCount = "http://localhost:3000/api/GetFeatureCount";
  // public pushToCms = "http://localhost:3000/api/PushToCMS";
  // public caseStatusReport = "http://localhost:3000/api/CaseStatusReport";
  // public cmsLookup = "http://localhost:3000/api/CMSLookUp";

  // public getJobStatus = "http://localhost:3000/api/GetJobStatusByProfileID";



  // .NET SERVICES

  public loginDemo = "http://172.20.250.88:80/login/login";
  public logoutDemo = "http://172.20.250.88:80/login/logout";

  public getDropdowns = "http://172.20.250.88:80/profile/getdropdowndata";
  public getAllProfiles = "http://172.20.250.88:80/profile/getallprofiles";
  public saveProfile = "http://172.20.250.88:80/profile/saveprofile";
  public getProfileObject = "http://172.20.250.88:80/profile/GetProfileById";

  public getAllModels = "http://172.20.250.88:80/profile/getallmodels";
  public getFeaturesByModel = "http://172.20.250.88:80/profile/getfeaturesbymodelid";
  public getQueryCount = "http://172.20.250.88:80/profile/GetODSQueryCount";
  public createProfile = "http://172.20.250.88:80/profile/createprofile";
  public editProfile = "http://172.20.250.88:80/profile/EditProfile";

  public getProfileResults = "http://172.20.250.88:80/profile/GetProfileResultsById";
  public getReportCard = "http://172.20.250.88:80/profile/GetReportCardData";
  public getFeatureCount = "http://172.20.250.88:80/profile/GetFeatureCount";
  public pushToCms = "http://172.20.250.88:80/pushtocms/PushToCMS";
  public caseStatusReport = "http://172.20.250.88:80/pushtocms/CaseStatusReport";
  public cmsLookup = "http://172.20.250.88:80/pushtocms/CMSLookUp";

  public getJobStatus = "http://172.20.250.88:80/profile/GetJobStatusByProfileID";


  // .NET Local

  // public loginDemo = "http://localhost:31603/login/login";
  // public logoutDemo = "http://localhost:31603/login/logout";

  // public getDropdowns = "http://localhost:31603/profile/getdropdowndata";
  // public getAllProfiles = "http://localhost:31603/profile/getallprofiles";
  // public saveProfile = "http://localhost:31603/profile/saveprofile";
  // public getProfileObject = "http://localhost:31603/profile/GetProfileById";

  // public getAllModels = "http://localhost:31603/profile/getallmodels";
  // public getFeaturesByModel = "http://localhost:31603/profile/getfeaturesbymodelid";
  // public getQueryCount = "http://localhost:31603/profile/GetODSQueryCount";
  // public createProfile = "http://localhost:31603/profile/createprofile";
  // public editProfile = "http://localhost:31603/profile/EditProfile";

  // public getProfileResults = "http://localhost:31603/profile/GetProfileResultsById";
  // public getReportCard = "http://localhost:31603/profile/GetReportCardData";
  // public getFeatureCount = "http://localhost:31603/profile/GetFeatureCount";
  // public pushToCms = "http://localhost:31603/pushtocms/PushToCMS";
  // public caseStatusReport = "http://localhost:31603/pushtocms/CaseStatusReport";
  // public cmsLookup = "http://localhost:31603/pushtocms/CMSLookUp";

  // public getJobStatus = "http://localhost:31603/profile/GetJobStatusByProfileID";


}
