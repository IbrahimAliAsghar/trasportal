import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Spinkit } from 'ng-http-loader';
import { ServiceInvokerComponent } from './service-invoker/service-invoker.component';
import { ApiUrls } from './api-urls';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  PAGE = "APP"
  title = 'ngApp';
  username: string;

  public spinkit = Spinkit;

  constructor(private _router: Router, private _serviceInvoker: ServiceInvokerComponent, private _apiUrls: ApiUrls) {
    if (this.userCheck()) {
      this._router.navigate[('profiles')];
    }
  }

  ngOnInit() {
    // console.log({page: this.PAGE, url : this._router.url});
    this.userCheck();
    if (this.userCheck()) {
      this._router.navigate[('profiles')];
    }
  }

  ngDoCheck() {
    // console.log({page: this.PAGE, url : this._router.url});
    this.userCheck();
    if (this.userCheck()) {
      this._router.navigate[('profiles')];
    }
  }

  // logout() {
  //   // this._authService.logoutUser();
  //   localStorage.removeItem('username');
  //   this._router.navigate(['login']);
  // }

  logout() {
    let userObject = JSON.parse(localStorage.getItem('userobject'))
    console.log({ page: this.PAGE, userobject: userObject });
    this._serviceInvoker.getData(this._apiUrls.logoutDemo, { "user": userObject })
      .subscribe(
        res => {
          if(res.Status == 0){
            localStorage.removeItem('username');
            localStorage.removeItem('token');
            localStorage.removeItem('userobject');
            this._router.navigate(['login']);
          }
        },
        err => {
          console.log({ page: this.PAGE, error: err });
        }
      );
  }

  userCheck(): boolean {
    let check = false;
    check = !!localStorage.getItem('username') && !!localStorage.getItem('userobject') && !!localStorage.getItem('token');
    if (check == true) {
      this.username = localStorage.getItem('username');
    }
    return check;
  }
}
