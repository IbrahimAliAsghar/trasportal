import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ListboxModule } from 'primeng/listbox';
import { TreeModule } from 'primeng/tree';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { FeaturesComponent } from './features/features.component';
import { ResultsComponent } from './results/results.component';
import { ServiceInvokerComponent } from './service-invoker/service-invoker.component';
import { ApiUrls } from './api-urls';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { DataSharingService } from './data-sharing.service';
import { JobStatusComponent } from './job-status/job-status.component';
import { AuthGuard } from './auth.guard';
import { StatusBackGuard } from './status-back.guard';
import { ExcelService } from './excel.service';
import { TokenInterceptorService } from './token-interceptor.service';
import { CmsReportComponent } from './cms-report/cms-report.component';
import { ReportCardModels } from './results/reportCardModel';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfilesComponent,
    FeaturesComponent,
    ResultsComponent,
    ServiceInvokerComponent,
    LoadingSpinnerComponent,
    JobStatusComponent,
    CmsReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ListboxModule,
    TreeModule,
    TableModule,
    RadioButtonModule,
    CalendarModule,
    DataTablesModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    ServiceInvokerComponent,
    ApiUrls,
    DataSharingService,
    AuthGuard,
    StatusBackGuard,
    ExcelService,
    ReportCardModels,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
