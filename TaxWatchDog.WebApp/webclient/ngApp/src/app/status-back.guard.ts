import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute, NavigationEnd, NavigationStart, Event } from '@angular/router';
import { Location } from "@angular/common";
import { Observable } from 'rxjs';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Injectable({
  providedIn: 'root'
})
export class StatusBackGuard implements CanActivate {

  PAGE = "STATUS-GUARD"

  constructor(private _router: Router, private _location: Location) { }

  canActivate(): boolean {
    // console.log({ page: this.PAGE, location_route: this._location.path() });
    // if(this._location.path() == '/features'){
    //   this._router.events
    //     .subscribe((event: Event) => {
    //       if (event instanceof NavigationStart) {
    //         // Navigation started.
    //         if(event.url.toString() == '/features'){
    //           alert("not allowed");
    //           return true;
    //         }
    //       }
    //     });

    // } else {
      return true;
    // }
  }
}
