﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaxWatchDog.Models.DBContext;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using JWT.Builder;
using Newtonsoft.Json.Linq;

namespace TaxWatchDog.WebApp.ActionFilters
{
    public class TokenValidator : ActionFilterAttribute
    {

        public override void OnActionExecuted(ActionExecutedContext filterContext) { }       
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            
            var token = filterContext.HttpContext.Request.Headers["Authorization"].Split(' ')[1] ;
            string secret = System.Configuration.ConfigurationManager.AppSettings["JWTKeySecret"];

            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
            var json = decoder.Decode(token, secret, verify: true);
            var data = JObject.Parse(json);
            string user = data["username"].ToString();

            using (MySQLDbContext db = new MySQLDbContext())
            {
                var verifyToken = db.UserSet.Where(a => a.Username == user).FirstOrDefault();
                if (verifyToken != null) { }

                else
                {
                    filterContext.Result = new HttpStatusCodeResult(401);
                }

            }

        }
        public override void OnResultExecuted(ResultExecutedContext filterContext) { }
        public override void OnResultExecuting(ResultExecutingContext filterContext) { }


    }
}