﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaxWatchDog.CrossCutting.Logging;

namespace TaxWatchDog.WebApp.App_Start
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            //TODO: change intialization logic of log4net obj
            Logger _logService = new Logger();

            string error_info = string.Format("StatusCode:{0},StatusDescription:{1},ExceptionMessage:{2}", filterContext.HttpContext.Response.StatusCode, filterContext.HttpContext.Response.StatusDescription, filterContext.Exception.Message);
            _logService.Error(error_info);
        }
        
    }
}