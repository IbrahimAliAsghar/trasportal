﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models;
using TaxWatchDog.Models.HelpingModels;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace TaxWatchDog.ServiceContracts
{
    public interface IProfileService
    {
        List<Model> GetAllModels(bool includeFeatures);
        Model GetModelById(int id, bool includeFeatures);
        List<Profile> GetAllProfiles(User userobj);
        List<FileTypeHelper> GetAllFileTypesFromHive();
        Profile GetProfileById(int id);
        Profile CreateProfile(Profile_Input helper);
        List<Feature> GetFeaturesByModelId(int Id);
        //DataTable TransposeData(DataTable inputDt, ModelResultsMetadata resultTableMd);
        DataSet GetProfileResultsById(int profileID,int limit,int offset);
        int SaveProfile(int profileId);
        string GetDropDownData();
        ODSCount GetODSQueryCount(Profile_Input profile);
        //StringBuilder GenerateQuery(Profile_Helper helper);
        Profile EditProfile(Profile_Input helper);
        string GetReportCardData(string It_ref_No,int ProfileId);
        Status GetJobStatusByProfileID(int ProfileID);
        string GetFeatureCount(int profile_id);
        //string GenerateOdsQuery(Profile_Input input);

    }
}
