﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models;
using TaxWatchDog.Models.HelpingModels;

namespace TaxWatchDog.ServiceContracts
{
    public interface ILoginService
    {
        bool IsLoggedIn();
        bool GetUserRole();
        User Login(string username, string password);
        User GetUserByUsername(string Name);
        AuthenticationHelper AuthenticateUserFromAD(string name, string password);
        User Logout(string username);

    }
}
