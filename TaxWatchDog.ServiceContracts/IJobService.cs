﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models;

namespace TaxWatchDog.ServiceContracts
{
    public interface IJobService
    {
        int CreateJob(int profileId);
        string GetJobStatusByJobId(int id);
        string GetJobStatusByProfileId(int id);
        string ExecuteJob(Job job);

        string ExecuteCMSJob(Job job);
        int UpdateLastRunDate(int jobId);//TODO Create UpdateLastRunDate Service


        int UpdateJobStatus(int jobId, string status);
        Job GetNewJob();
    }
}
