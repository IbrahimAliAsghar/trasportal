﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxWatchDog.Models.HelpingModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace TaxWatchDog.ServiceContracts
{
  public  interface IPushtoCMSService
    {

        DB2Reply PushToCMS(Profile_Input Profile_Input, string UdpUser, string AuditType, string itrefno,List< FeatureStatistics> statistics);
        List<LoopkupResponse> CMSLookUp(int year, List<string> ItRefno, int ProfileID);
        List<CaseStatusReportReply> CaseStatusReport(string StartDate, string EndDate, string flag);

    }
}
